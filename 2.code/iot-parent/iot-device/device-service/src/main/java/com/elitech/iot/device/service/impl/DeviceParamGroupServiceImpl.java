/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-02-13 16:24:55
 */

package com.elitech.iot.device.service.impl;

import com.elitech.iot.common.base.constant.DeviceConstant;
import com.elitech.iot.common.base.constant.SymbolConstant;
import com.elitech.iot.common.service.BaseService;
import com.elitech.iot.device.dao.DeviceParamDao;
import com.elitech.iot.device.dao.DeviceParamGroupDao;
import com.elitech.iot.device.domain.DeviceParam;
import com.elitech.iot.device.domain.DeviceParamGroup;
import com.elitech.iot.device.model.DeviceParamGroupVOModel;
import com.elitech.iot.device.model.DeviceParamModel;
import com.elitech.iot.device.service.DeviceParamGroupService;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 设备参数分组表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class DeviceParamGroupServiceImpl extends BaseService<DeviceParamGroup, java.math.BigInteger> implements DeviceParamGroupService {
    @Autowired
    private DeviceParamGroupDao dao;

    @Override
    protected DeviceParamGroupDao getDao() {
        return dao;
    }

    @Autowired
    private DeviceParamDao paramDao;

    @Override
    public List<DeviceParamGroupVOModel> queryParamGroupForDevice(BigInteger deviceId) {
        //定义返回对象
        List<DeviceParamGroupVOModel> list = new ArrayList<DeviceParamGroupVOModel>();
        //查询设备的所有分组
        List<DeviceParamGroup> deviceParamGroupVos = dao.selectParamGroupForDevice(deviceId);
        DeviceParam param = new DeviceParam();
        param.setDeviceId(deviceId);
        //查询设备的所有参数列表
        List<DeviceParam> deviceParams = paramDao.selectListByDomain(param);
        if (deviceParamGroupVos != null && deviceParamGroupVos.size() > 0) {
            //遍历分组列表
            for (DeviceParamGroup group : deviceParamGroupVos) {
                DeviceParamGroupVOModel groupVOModel = new DeviceParamGroupVOModel();
                groupVOModel.setGroupName(group.getGroupName());
                groupVOModel.setId(group.getId());
                groupVOModel.setTypeId(group.getTypeId());
                String paramIds = group.getParamIds();
                //将参数归入分组列表中
                if (StringUtils.isNoneBlank(paramIds)) {
                    List<DeviceParamModel> paramList = new ArrayList<DeviceParamModel>();
                    List<String> paramIdList = Arrays.asList(paramIds.split(SymbolConstant.COMMA_NAME));

                    Iterator iterator = deviceParams.iterator();
                    while (iterator.hasNext()) {
                        DeviceParam cur = (DeviceParam) iterator.next();
                        if (paramIdList.contains(cur.getId()+"")) {
                            paramList.add(BeanUtil.copy(cur, DeviceParamModel.class));
                            iterator.remove();
                        }
                        groupVOModel.setParamList(paramList);
                    }
                    list.add(groupVOModel);
                }
            }
            //没有分组的参数信息添加到默认分组中
            if (deviceParams.size() > 0) {
                DeviceParamGroupVOModel model = new DeviceParamGroupVOModel();
                model.setGroupName(DeviceConstant.NORMAL_GROUP_NAME);
                model.setParamList(BeanUtil.copy(deviceParams, DeviceParamModel.class));
                list.add(model);
            }
        } else {
            //没有分组信息直接返回所有参数
            DeviceParamGroupVOModel model = new DeviceParamGroupVOModel();
            model.setParamList(BeanUtil.copy(deviceParams, DeviceParamModel.class));
            list.add(model);
        }
        return list;
    }
}