/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-13 16:24:55
*/

package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.DeviceParamGroup;

import com.elitech.iot.common.service.IBaseService;
import com.elitech.iot.device.model.DeviceParamGroupVOModel;

import java.math.BigInteger;
import java.util.List;

/**
* 设备参数分组表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface DeviceParamGroupService extends IBaseService<DeviceParamGroup, java.math.BigInteger> {
    List<DeviceParamGroupVOModel> queryParamGroupForDevice(BigInteger deviceId);
}