
/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-02-13 16:24:55
 */
package com.elitech.iot.device.dao;
import com.elitech.iot.common.dao.IBaseDao;
import com.elitech.iot.device.domain.DeviceParamGroup;

import java.math.BigInteger;
import java.util.List;
/**
 * 设备参数分组表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
public interface DeviceParamGroupDao extends IBaseDao<DeviceParamGroup, java.math.BigInteger> {
    List<DeviceParamGroup> selectParamGroupForDevice(BigInteger deviceId);
}