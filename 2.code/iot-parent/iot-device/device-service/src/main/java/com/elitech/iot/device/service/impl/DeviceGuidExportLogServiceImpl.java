/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service.impl;

import com.elitech.iot.device.domain.DeviceGuidExportLog;
import com.elitech.iot.device.dao.DeviceGuidExportLogDao;
import com.elitech.iot.device.service.DeviceGuidExportLogService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 设备guid导出记录表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class DeviceGuidExportLogServiceImpl extends BaseService<DeviceGuidExportLog, java.math.BigInteger> implements DeviceGuidExportLogService {
    @Autowired
    private DeviceGuidExportLogDao dao;

    @Override
    protected DeviceGuidExportLogDao getDao() {
        return dao;
    }
}