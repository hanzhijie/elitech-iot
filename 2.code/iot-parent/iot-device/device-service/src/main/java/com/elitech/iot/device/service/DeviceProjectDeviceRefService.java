/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.DeviceProjectDeviceRef;

import com.elitech.iot.common.service.IBaseService;

/**
* 项目设备设备关联表: 行业应用如果没有项目的概念，就建一个默认的项目；去掉组织机构与设备的关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface DeviceProjectDeviceRefService extends IBaseService<DeviceProjectDeviceRef, java.math.BigInteger> {

}