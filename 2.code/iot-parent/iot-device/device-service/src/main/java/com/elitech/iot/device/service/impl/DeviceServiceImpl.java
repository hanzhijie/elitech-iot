/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service.impl;

import com.elitech.iot.device.domain.Device;
import com.elitech.iot.device.dao.DeviceDao;
import com.elitech.iot.device.service.DeviceService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 设备表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class DeviceServiceImpl extends BaseService<Device, java.math.BigInteger> implements DeviceService {
    @Autowired
    private DeviceDao dao;

    @Override
    protected DeviceDao getDao() {
        return dao;
    }
}