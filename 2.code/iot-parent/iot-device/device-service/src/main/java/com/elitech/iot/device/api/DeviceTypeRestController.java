/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/
package com.elitech.iot.device.api;

import com.alibaba.excel.EasyExcel;
import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.common.base.util.ExcelUtil;
import com.elitech.iot.device.domain.DeviceType;
import com.elitech.iot.device.excel.DeviceTypeExcel;
import com.elitech.iot.device.excel.DeviceTypeTemplate;
import com.elitech.iot.device.model.DeviceTypeModel;
import com.elitech.iot.device.service.DeviceTypeService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
* 设备类型表 API.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2019-12-27 09:48:30
* @since v1.0.0
*/
@RestController
@RequestMapping("DeviceType")
@RefreshScope
@Api(tags = {"DeviceType API"})
public class DeviceTypeRestController {
    @Autowired
    private DeviceTypeService deviceTypeService;

   /**
    * 新增数据.
    *
    * @param model 要新增数据
    * @return 返回插入行数
    */
    @ApiOperation(value = "新增数据", notes = "设备类型表", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insert(@RequestBody DeviceTypeModel model) {
        DeviceType domain = BeanUtil.copy(model, DeviceType.class);
        DeviceType deviceType =new DeviceType();
        deviceType.setTypeName(domain.getTypeName());
        List<DeviceType> deviceTypes = deviceTypeService.selectListByDomain(deviceType);
        if(deviceTypes!=null&&deviceTypes.size()>0){
            return ResultMessage.fail(ResultCode.DEVICE_TYPE_EXIST);
        }
        domain.setGmtCreate(DateTimeUtils.getNowZeroTime());
        BigInteger id=BigInteger.valueOf(1);
        domain.setTypeId(id);
        int res = deviceTypeService.insert(domain);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
    * 批量新增数据.
    *
    * @param list 数据列表
    * @return 返回插入行数
    */
    @ApiOperation(value = "批量新增", notes = "设备类型表", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<DeviceType> list) {
        int data = deviceTypeService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键删除实体对象.
    *
    * @param pk 主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "删除数据", notes = "设备类型表", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPk(@RequestParam BigInteger pk) {
        DeviceType data = deviceTypeService.selectByPk(pk);
        //验证当前用户是否为项目创建者,需用户认证完成
        //TODO
        if (data == null) {
            return ResultMessage.fail(ResultCode.PARAM_ERROR);
        }
        data.setDeletedFlag(true);
        data.setGmtModified(DateTimeUtils.getNowZeroTime());
        int res = deviceTypeService.updateByPk(data);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
    * 根据主键批量删除.
    *
    * @param pkList 要删除实体对象的主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "批量删除", notes = "设备类型表", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = deviceTypeService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 根据id更新实体对象.
    *
    * @param domain 要更新的实体对象
    * @return 返回更新的行数。
    */
    @ApiOperation(value = "更新数据", notes = "设备类型表", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody DeviceType domain) {
        DeviceType data = deviceTypeService.selectByPk(domain.getTypeId());
        //验证当前用户是否为项目创建者,需用户认证完成
        //TODO
        //根据项目名称查询项目是否存在
        DeviceType deviceType = new DeviceType();
        deviceType.setTypeName(domain.getTypeName());
        deviceType.setDeletedFlag(false);
        List<DeviceType> deviceTypes = deviceTypeService.selectListByDomain(deviceType);
        //如果项目存在并且与当前项目名称不一致
        if (deviceTypes != null && deviceTypes.size() > 0) {
            if (!deviceTypes.get(0).getTypeId().equals(domain.getTypeId())) {
                return ResultMessage.fail(ResultCode.DEVICE_TYPE_EXIST);
            }
        }
        //更新项目
        if (data != null) {
            domain.setGmtModified(DateTimeUtils.getNowZeroTime());
            int res = deviceTypeService.updateByPk(domain);
            if (res > 0) {
                return ResultMessage.success(res);
            }
            return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }
        return ResultMessage.fail(ResultCode.PARAM_ERROR);
    }


    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @ApiOperation(value = "根据主键查询", notes = "设备类型表", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResultMessage<DeviceType> selectByPk(@RequestParam BigInteger pk) {
        DeviceType data = deviceTypeService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据多个id查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @ApiOperation(value = "根据主键列表查询", notes = "设备类型表", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    public ResultMessage<List<DeviceType>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<DeviceType> data = deviceTypeService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param whereMap 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @ApiOperation(value = "分页查询", notes = "设备类型表", httpMethod = "POST")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
        @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
        @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/get/page", method = RequestMethod.POST)
    public ResultMessage<PageInfo<DeviceType>> pageListByWhere(@RequestParam int start, @RequestParam int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<DeviceType> data = deviceTypeService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
    * 根据实体类查询
    *
    * @param domain 实体类
    * @return List<Domain> 查询结果
    */
    @ApiOperation(value = "根据Model查询", notes = "设备类型表", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage<List<DeviceType>> selectListByDomain(@RequestBody DeviceType domain) {
        List<DeviceType> data = deviceTypeService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }
    /**
     * 导出设备类型数据
     *
     * @param typeName 设备类型名称
     *
     */
    @ApiOperation(value = "导出设备类型数据", notes = "导出设备类型数据", tags = "", httpMethod = "GET")
    @ApiImplicitParam(name = "typeName", required = false)
    @RequestMapping(value = "/exportDeviceType", method = RequestMethod.GET)
    public void exportDeviceType(HttpServletResponse response, @RequestParam(required = false,value = "typeName") String typeName) {
        DeviceType type = new DeviceType();
        if(StringUtils.isNotBlank(typeName)) {
            type.setTypeName(typeName);
        }
        List<DeviceType> data = deviceTypeService.selectListByDomain(type);
        List<DeviceTypeExcel> typeList = BeanUtil.copyWithConvert(data, DeviceTypeExcel.class);
        try {
            ExcelUtil.writeExcel(response, typeList, "设备类型", "设备类型", DeviceTypeExcel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 导出设备类型模板
     *
     */
    @ApiOperation(value = "导出设备类型模板", notes = "导出设备类型模板", tags = "", httpMethod = "GET")
    @RequestMapping(value = "/exportDeviceTypeTemplate", method = RequestMethod.GET)
    public void exportDeviceTypeTemplate(HttpServletResponse response) {
        List<DeviceTypeTemplate> typeList = new ArrayList<DeviceTypeTemplate>();
        DeviceTypeTemplate deviceTypeTemplate = new DeviceTypeTemplate();
        deviceTypeTemplate.setTypeName("test");
        typeList.add(deviceTypeTemplate);
        try {
            ExcelUtil.writeExcel(response, typeList, "设备类型模板", "设备类型模板", DeviceTypeTemplate.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 导入设备类型
     *
     * @param file 文件
     * @return 结果
     */
    @ApiOperation(value = "导入设备类型", notes = "导入设备类型", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "file",value = "文件",required = true)
    @RequestMapping(value = "/importDeviceType", method = RequestMethod.POST)
    public ResultMessage<Integer> importDeviceType(MultipartFile file) {
        List<DeviceTypeTemplate> typeList = null;
        try {
            typeList = EasyExcel.read(new BufferedInputStream(file.getInputStream())).head(DeviceTypeTemplate.class).sheet().doReadSync();
            ResultMessage resultMessage = deviceTypeService.importDeviceType(typeList);
            return resultMessage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultMessage.success("");
    }

}