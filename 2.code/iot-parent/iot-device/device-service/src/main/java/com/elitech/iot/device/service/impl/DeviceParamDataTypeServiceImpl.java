/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service.impl;

import com.elitech.iot.device.domain.DeviceParamDataType;
import com.elitech.iot.device.dao.DeviceParamDataTypeDao;
import com.elitech.iot.device.service.DeviceParamDataTypeService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 参数数据类型.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class DeviceParamDataTypeServiceImpl extends BaseService<DeviceParamDataType, java.math.BigInteger> implements DeviceParamDataTypeService {
    @Autowired
    private DeviceParamDataTypeDao dao;

    @Override
    protected DeviceParamDataTypeDao getDao() {
        return dao;
    }
}