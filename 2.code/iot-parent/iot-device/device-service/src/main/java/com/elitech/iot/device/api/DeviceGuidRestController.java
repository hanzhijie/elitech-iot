/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:48:30
 */
package com.elitech.iot.device.api;

import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.util.ExcelUtil;
import com.elitech.iot.device.domain.DeviceGuid;
import com.elitech.iot.device.excel.GUIDExportExcel;
import com.elitech.iot.device.model.DeviceGuidModel;
import com.elitech.iot.device.service.DeviceGuidService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 设备guid表 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-27 09:48:30
 * @since v1.0.0
 *
* 设备guid表 API.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2019-12-27 09:48:30
* @since v1.0.0
*/
@CrossOrigin
@RestController
@RequestMapping(path = "DeviceGuid")
@RefreshScope
@Api(tags = {"DeviceGuid API"})
public class DeviceGuidRestController {
    @Autowired
    private DeviceGuidService deviceGuidService;

    /**
     * 新增数据.
     *
     * @param model 要新增数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "设备guid表", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insert(@RequestBody DeviceGuidModel model) {
        DeviceGuid domain = BeanUtil.copy(model, DeviceGuid.class);
        int data = deviceGuidService.insert(domain);
        return ResultMessage.success(data);
    }

    /**
     * 批量新增数据.
     *
     * @param list 数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "设备guid表", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<DeviceGuid> list) {
        int data = deviceGuidService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "设备guid表", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete/{pk}", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPk(@PathVariable BigInteger pk) {
        int data = deviceGuidService.deleteByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "设备guid表", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = deviceGuidService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param domain 要更新的实体对象
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "设备guid表", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody DeviceGuid domain) {
        int data = deviceGuidService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "设备guid表", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResultMessage<DeviceGuid> selectByPk(@RequestParam BigInteger pk) {
        DeviceGuid data = deviceGuidService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "设备guid表", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    public ResultMessage<List<DeviceGuid>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<DeviceGuid> data = deviceGuidService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "设备guid表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/get/page/{start}/{pageSize}", method = RequestMethod.POST)
    public ResultMessage<PageInfo<DeviceGuid>> pageListByWhere(@PathVariable int start, @PathVariable int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<DeviceGuid> data = deviceGuidService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param domain 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "设备guid表", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage<List<DeviceGuid>> selectListByDomain(@RequestBody DeviceGuid domain) {
        List<DeviceGuid> data = deviceGuidService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }

    /****************************新增*****************************************/
    @ApiOperation(value = "导出设备guid", notes = "导出设备guid", tags = "", httpMethod = "GET")
    @RequestMapping(value = "/exportDeviceGuid", method = RequestMethod.GET)
    public void exportNewDeviceGuid(@RequestParam int num,
                                    @RequestParam String deviceType,
                                    @RequestParam BigInteger deviceTypeId,
                                    @RequestParam BigInteger accountId,
                                    @RequestParam String userName,
                                    @RequestParam String orderCode,
                                    HttpServletResponse response) {
        List<GUIDExportExcel> guidList = new ArrayList<>();
        guidList = deviceGuidService.exportNewDeviceGUID(num, deviceType, orderCode, accountId, deviceTypeId, userName);
        try {
            ExcelUtil.writeExcel(response, guidList, deviceType, "设备guid", GUIDExportExcel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}