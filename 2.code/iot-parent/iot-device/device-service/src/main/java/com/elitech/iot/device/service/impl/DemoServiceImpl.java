/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-19 18:16:48
 */

package com.elitech.iot.device.service.impl;

import com.elitech.iot.device.domain.Demo;
import com.elitech.iot.device.dao.DemoDao;
import com.elitech.iot.device.service.DemoService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 测试表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class DemoServiceImpl extends BaseService<Demo, java.math.BigInteger> implements DemoService {
    @Autowired
    private DemoDao dao;

    @Override
    protected DemoDao getDao() {
        return dao;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertTransaction(Demo demo) {
        dao.insert(demo);
        throw new RuntimeException("insert exception");
    }
}