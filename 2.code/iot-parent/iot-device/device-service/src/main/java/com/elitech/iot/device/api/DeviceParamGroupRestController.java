/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-02-13 16:24:55
 */
package com.elitech.iot.device.api;

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.device.domain.DeviceParamGroup;
import com.elitech.iot.device.model.DeviceParamGroupModel;
import com.elitech.iot.device.model.DeviceParamGroupVOModel;
import com.elitech.iot.device.service.DeviceParamGroupService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * 设备参数分组表 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020-02-13 16:24:55
 * @since v1.0.0
 */
@RestController
@RequestMapping("DeviceParamGroup")
@RefreshScope
@Api(tags = {"DeviceParamGroup API"})
public class DeviceParamGroupRestController {
    @Autowired
    private DeviceParamGroupService deviceParamGroupService;


    /**
     * 根据设备id查询参数分组信息
     *
     * @param deviceId
     * @return 返回设备的参数分组列表。
     */
    @ApiOperation(value = "根据设备id查询参数分组信息", notes = "根据设备id查询参数分组信息", httpMethod = "POST")
    @RequestMapping(value = "/queryProjectByAccount", method = RequestMethod.POST)
    public ResultMessage<List<DeviceParamGroupVOModel>> queryProjectByAccount(@ApiParam(value = "设备id", required = true) @RequestParam BigInteger deviceId) {
        List<DeviceParamGroupVOModel> data = deviceParamGroupService.queryParamGroupForDevice(deviceId);
        return ResultMessage.success(data);
    }

    /**
     * 新增数据.
     *
     * @param model 要新增的数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "设备参数分组表", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @PostMapping(path = "/insert")
    public ResultMessage<DeviceParamGroupModel> insert(@RequestBody DeviceParamGroupModel model) {
        DeviceParamGroup domain = BeanUtil.copy(model, DeviceParamGroup.class);
        //查询新增的数据是否存在相同的组名
        DeviceParamGroup paramGroup = new DeviceParamGroup();
        paramGroup.setGroupName(domain.getGroupName());
        paramGroup.setTypeId(domain.getTypeId());
        List<DeviceParamGroup> deviceParamGroups = deviceParamGroupService.selectListByDomain(paramGroup);
        if (deviceParamGroups != null && deviceParamGroups.size() > 0) {
            return ResultMessage.fail(ResultCode.PARAM_GROUP_EXIST);
        }
        //设置新增时间为零时区时间
        domain.setGmtCreate(DateTimeUtils.getNowZeroTime());
        BigInteger id=BigInteger.valueOf(1);
        domain.setId(id);
        int res = deviceParamGroupService.insert(domain);
        if (res > 0) {
            return ResultMessage.success(model);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 批量新增数据.
     *
     * @param list 要新增的数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "设备参数分组表", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @PostMapping(path = "/insert/list")
    public ResultMessage<List<DeviceParamGroupModel>> insertList(@RequestBody List<DeviceParamGroupModel> list) {
        List<DeviceParamGroup> domainList = BeanUtil.copy(list, DeviceParamGroup.class);
        int data = deviceParamGroupService.insertList(domainList);
        return ResultMessage.success(list);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "设备参数分组表", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @DeleteMapping(path = "/delete")
    public ResultMessage<Integer> deleteByPk(@RequestParam java.math.BigInteger pk) {
        //查询对象是否存在
        DeviceParamGroup deviceParamGroup = deviceParamGroupService.selectByPk(pk);
        if (deviceParamGroup != null) {
            //删除分组
            int res = deviceParamGroupService.deleteByPk(pk);
            if (res > 0) {
                return ResultMessage.success(res);
            }
            return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }
        return ResultMessage.fail(ResultCode.PARAM_ERROR);
    }

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "设备参数分组表", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @DeleteMapping(path = "/delete/list")
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = deviceParamGroupService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param model 要更新的数据
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "设备参数分组表", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @PutMapping(value = "/update")
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody DeviceParamGroupModel model) {
        DeviceParamGroup domain = BeanUtil.copy(model, DeviceParamGroup.class);
        //查询分组对象是否存在
        DeviceParamGroup data = deviceParamGroupService.selectByPk(domain.getId());
        if (data == null) {
            return ResultMessage.fail(ResultCode.PARAM_GROUP_NOT_EXIST);
        }
        //根据参数分组名称查询参数分组是否存在
        DeviceParamGroup paramGroup = new DeviceParamGroup();
        paramGroup.setGroupName(domain.getGroupName());
        paramGroup.setTypeId(domain.getTypeId());
        List<DeviceParamGroup> paramGroups = deviceParamGroupService.selectListByDomain(paramGroup);
        //如果分组名称存在并且不是当前当前分组
        if (paramGroups != null && paramGroups.size() > 0) {
            if (!paramGroups.get(0).getId().equals(domain.getId())) {
                return ResultMessage.fail(ResultCode.PARAM_GROUP_EXIST);
            }
        }
        int res = deviceParamGroupService.updateByPk(domain);
        if (res > 0) {
            return ResultMessage.success(data);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "设备参数分组表", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @GetMapping(value = "/get")
    public ResultMessage<DeviceParamGroup> selectByPk(@RequestParam BigInteger pk) {
        DeviceParamGroup data = deviceParamGroupService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "设备参数分组表", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @PostMapping(value = "/get/list")
    public ResultMessage<List<DeviceParamGroup>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<DeviceParamGroup> data = deviceParamGroupService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "设备参数分组表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @PostMapping(value = "/get/page")
    public ResultMessage<PageInfo<DeviceParamGroup>> pageListByWhere(@RequestParam int start, @RequestParam int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<DeviceParamGroup> data = deviceParamGroupService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param model 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "设备参数分组表", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "model", required = true)
    @PostMapping(value = "/get/model")
    public ResultMessage<List<DeviceParamGroupModel>> selectListByDomain(@RequestBody DeviceParamGroupModel model) {
        DeviceParamGroup domain = BeanUtil.copy(model, DeviceParamGroup.class);
        List<DeviceParamGroup> list = deviceParamGroupService.selectListByDomain(domain);
        List<DeviceParamGroupModel> modelList = BeanUtil.copy(list, DeviceParamGroupModel.class);
        return ResultMessage.success(modelList);
    }
}