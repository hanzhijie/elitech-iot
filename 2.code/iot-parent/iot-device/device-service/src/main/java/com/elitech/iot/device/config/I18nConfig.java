package com.elitech.iot.device.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/21
 */
@Configuration
@Slf4j
public class I18nConfig {

    @Autowired
    private MessageSource messageSource;

    /**
     * 获取国际化信息.
     *
     * @param key    国际化key
     * @param params String.format 填充参数
     * @return 返回key对应的国际化字符串
     */
    public String getMessage1(String key, Object... params) {
        try {
            // 获取语言，从header中的Accept-Language中获取
            Locale locale = LocaleContextHolder.getLocale();
            if (locale == null) {
                locale = Locale.SIMPLIFIED_CHINESE;
            }

            return messageSource.getMessage(key, params, locale);
        } catch (NoSuchMessageException ex) {
            log.error("国际化消息找不到", ex);
            return "";
        }
    }

}
