/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.DeviceRealTimeData;

import com.elitech.iot.common.service.IBaseService;

/**
* 设备实时数据表，存储每个设备最新的一条监测数据.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface DeviceRealTimeDataService extends IBaseService<DeviceRealTimeData, java.math.BigInteger> {

}