/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.dao;

import com.elitech.iot.device.domain.DeviceTypeParam;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface DeviceTypeParamDao extends IBaseDao<DeviceTypeParam, java.math.BigInteger> {

}