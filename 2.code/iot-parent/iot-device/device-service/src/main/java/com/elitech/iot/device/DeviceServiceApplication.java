package com.elitech.iot.device;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 设备服务器的程序.
 * 
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019年12月13日
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@Configuration
@MapperScan(basePackages = {"com.elitech.iot.device.dao"})
@EnableCaching
public class DeviceServiceApplication {
	/**
	 * 应用程序入口.
	 * 
	 * @param args 启动时传入命令行参数.
	 */
	public static void main(String[] args) {
		SpringApplication.run(DeviceServiceApplication.class, args);
	}



	/**
	 * 统一服务端时区为UTC 时间.
	 */
	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}


}
