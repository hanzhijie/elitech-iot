package com.elitech.iot.device.aop;

/**
 * 参数验证AOP.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 * @Ssince V1.0.0
 */

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.i18n.I18nUtil;
import com.elitech.iot.device.config.RuntimeProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.DefaultMessageCodesResolver;
import org.springframework.validation.Errors;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.Validation;
import javax.validation.metadata.ConstraintDescriptor;
import java.lang.reflect.Method;
import java.util.Set;

@Slf4j
@Aspect
@Component
@AllArgsConstructor
public class ParamValidateAop {
    @Autowired
    private RuntimeProperties runtimeProperties;


    private static javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Pointcut("execution(public * com.elitech.iot.device.api.*.*(..)) || execution(public * com.elitech.iot.device.api.*.*.*(..))")
    public void aspect() {
    }

    /**
     * 配置前置拦截器，进行方法参数验证.
     *
     * @param joinPoint
     */
    @Around("aspect()")
    public Object before(ProceedingJoinPoint joinPoint) {
        String[] paramNames = ((CodeSignature) joinPoint.getSignature()).getParameterNames();

        String signatureName = joinPoint.getSignature().getName();


        if (paramNames != null && paramNames.length > 0) {
            String methodName = "";
            try {
                Object target = joinPoint.getTarget();
                // 得到拦截的方法
                Signature signature = joinPoint.getSignature();
                MethodSignature methodSignature = (MethodSignature) signature;
                Method currentMethod = target.getClass().getMethod(methodSignature.getName(), methodSignature.getParameterTypes());
                methodName = currentMethod.getName();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            DefaultMessageCodesResolver r;
            Object[] paramValues = joinPoint.getArgs();
            if (paramValues != null) {
                for (int i = 0; i < paramNames.length; i++) {
                    String paramName = paramNames[i];
                    Object paramValue = paramValues[i];
                    Errors errorMap = new BeanPropertyBindingResult(paramValue, paramName);
                    Set<ConstraintViolation<Object>> set = validator.validate(paramValue);

                    if (set != null && set.size() > 0) {
                        for (ConstraintViolation cv : set) {
                            Object invalidValue = cv.getInvalidValue();
                            String message = cv.getMessage();
                            ConstraintDescriptor constraintDescriptor = cv.getConstraintDescriptor();
                            Path propertyPath = cv.getPropertyPath();
                            Object returnValue = cv.getExecutableReturnValue();
                            String messageTemplate = cv.getMessageTemplate();
                            Object[] parameters = cv.getExecutableParameters();

                            String key = message.replace("{", "").replace("}", "");
                            String errorMessage = I18nUtil.getMessage(key);
                            errorMessage = String.format("%s::%s", propertyPath, errorMessage);

                            // 发现一个错误就返回
                            return ResultMessage.fail(ResultCode.PARAM_ERROR).detailMessage(errorMessage);
                        }
                    }
                }
            }
        }

        try {
            Object result = joinPoint.proceed();
            return result;
        } catch (Throwable throwable) {
            log.error("服务调用失败", throwable);

            ResultMessage result = ResultMessage.fail(ResultCode.SERVER_INTERNAL_ERROR);
            if (runtimeProperties.isDebug()) {
                result.detailMessage(throwable.getMessage());
            }

            return result;
        }
    }

}
