/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-19 18:16:19
*/

package com.elitech.iot.device.dao;

import com.elitech.iot.device.domain.Demo;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 测试表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface DemoDao extends IBaseDao<Demo, java.math.BigInteger> {

}