/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service.impl;

import com.elitech.iot.device.domain.DeviceParam;
import com.elitech.iot.device.dao.DeviceParamDao;
import com.elitech.iot.device.service.DeviceParamService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 设备参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class DeviceParamServiceImpl extends BaseService<DeviceParam, java.math.BigInteger> implements DeviceParamService {
    @Autowired
    private DeviceParamDao dao;

    @Override
    protected DeviceParamDao getDao() {
        return dao;
    }
}