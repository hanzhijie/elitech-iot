package com.elitech.iot.device.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
@Component
@RefreshScope
@Setter
@Getter
public class RuntimeProperties {
    @Value("${service.runtime.metric}")
    private boolean metric;


    @Value("${service.runtime.debug}")
    private boolean debug;
}
