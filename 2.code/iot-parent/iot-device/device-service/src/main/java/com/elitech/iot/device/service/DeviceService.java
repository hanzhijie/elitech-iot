/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.Device;

import com.elitech.iot.common.service.IBaseService;

/**
* 设备表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface DeviceService extends IBaseService<Device, java.math.BigInteger> {

}