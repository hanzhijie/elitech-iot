/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service;

import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.device.domain.DeviceType;

import com.elitech.iot.common.service.IBaseService;
import com.elitech.iot.device.excel.DeviceTypeTemplate;

import java.util.List;

/**
* 设备类型表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface DeviceTypeService extends IBaseService<DeviceType, java.math.BigInteger> {
    /**
     * 导入设备类型
     * @param typeList
     * @return
     */
    ResultMessage importDeviceType(List<DeviceTypeTemplate> typeList);
}