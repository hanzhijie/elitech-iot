/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:48:30
 */

package com.elitech.iot.device.service.impl;

import com.elitech.iot.common.service.BaseService;
import com.elitech.iot.device.dao.DeviceDao;
import com.elitech.iot.device.dao.DeviceParamDao;
import com.elitech.iot.device.dao.DeviceTypeParamDao;
import com.elitech.iot.device.domain.Device;
import com.elitech.iot.device.domain.DeviceParam;
import com.elitech.iot.device.domain.DeviceTypeParam;
import com.elitech.iot.device.model.DeviceTypeParamModel;
import com.elitech.iot.device.service.DeviceTypeParamService;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

/**
 * 设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
@CacheConfig(cacheNames = "DeviceTypeParam")
public class DeviceTypeParamServiceImpl extends BaseService<DeviceTypeParam, java.math.BigInteger> implements DeviceTypeParamService {
    @Autowired
    private DeviceTypeParamDao dao;

    @Override
    protected DeviceTypeParamDao getDao() {
        return dao;
    }

    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private DeviceParamDao deviceParamDao;

    @Override
    public List<DeviceTypeParamModel> queryListByDevice(BigInteger deviceId) {
        //查询设备信息，获取设备类型
        Device device = deviceDao.selectByPk(deviceId);
        if (device != null && device.getTypeId() != null) {
            DeviceTypeParam domain = new DeviceTypeParam();
            domain.setDeviceTypeId(device.getTypeId());
            DeviceParam deviceParam = new DeviceParam();
            deviceParam.setDeviceId(deviceId);
            //查询设备的参数设置
            List<DeviceParam> deviceParams = deviceParamDao.selectListByDomain(deviceParam);
            //查询设备类型的参数设置
            List<DeviceTypeParam> deviceTypeParams =  ((DeviceTypeParamService) AopContext.currentProxy()).queryListByDeviceType(device.getTypeId());
//            List<DeviceTypeParam> deviceTypeParams = this.queryListByDeviceType(device.getTypeId());
            //如果设备参数设置为空直接返回设备类型的参数设置
            if (deviceParams == null) {
                return BeanUtil.copy(deviceTypeParams, DeviceTypeParamModel.class);
            }
            if (deviceTypeParams != null && deviceTypeParams.size() > 0) {
                //将设备的参数设置转移到设备类型的参数设置中
                for (DeviceTypeParam typeParam : deviceTypeParams) {
                    if (deviceParams != null && deviceParams.size() > 0) {
                        for (DeviceParam param : deviceParams) {
                            if (param.getParamCode().equals(typeParam.getParamCode())) {
                                typeParam.setParamKey(param.getParamKey());
                                typeParam.setParamName(param.getParamName());
                                typeParam.setAlarmUpper(param.getAlarmUpper());
                                typeParam.setAlarnLower(param.getAlarmLower());
                                typeParam.setParamLength(param.getParamLength());
                                typeParam.setParamUnit(param.getParamUnit());
                            }
                        }
                    }

                }
            }
            List<DeviceTypeParamModel> list = BeanUtil.copy(deviceTypeParams, DeviceTypeParamModel.class);
            return list;
        }
        return null;
    }

    @Override
    @Cacheable(value = "DeviceTypeParam",key = "#typeId")
    public List<DeviceTypeParam> queryListByDeviceType(BigInteger typeId) {
        DeviceTypeParam domain = new DeviceTypeParam();
        domain.setDeviceTypeId(typeId);
        List<DeviceTypeParam> deviceTypeParams = dao.selectListByDomain(domain);
        return deviceTypeParams;
    }
    @Override
    @CacheEvict(value="DeviceTypeParam",key = "#domain.deviceTypeId")
    public int insert(DeviceTypeParam domain) {
        return getDao().insert(domain);
    }
    @Override
    @CacheEvict(value="DeviceTypeParam",key = "#domain.deviceTypeId")
    public int updateByPk(DeviceTypeParam domain) {
        return getDao().updateByPk(domain);
    }

    @Override
    @CacheEvict(value="DeviceTypeParam",key = "#domain.deviceTypeId")
    public int deleteByPk(BigInteger pk) {
        return getDao().deleteByPk(pk);
    }

}