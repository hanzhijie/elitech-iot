/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.DeviceTypeParam;

import com.elitech.iot.common.service.IBaseService;
import com.elitech.iot.device.model.DeviceParamGroupVOModel;
import com.elitech.iot.device.model.DeviceTypeParamModel;

import java.math.BigInteger;
import java.util.List;

/**
* 设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface DeviceTypeParamService extends IBaseService<DeviceTypeParam, java.math.BigInteger> {
    /**
     * 根据设备id查询设备的实际参数列表
     * @param deviceId
     * @return
     */
    List<DeviceTypeParamModel> queryListByDevice(BigInteger deviceId);

    /**
     * 根据设备类型id查询设备参数（缓存）
     * @param typeId
     * @return
     */
    List<DeviceTypeParam> queryListByDeviceType(BigInteger typeId);
}