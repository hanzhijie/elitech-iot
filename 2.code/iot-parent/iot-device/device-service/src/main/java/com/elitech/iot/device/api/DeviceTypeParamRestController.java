/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:48:30
 */
package com.elitech.iot.device.api;

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.device.domain.DeviceTypeParam;
import com.elitech.iot.device.model.DeviceTypeParamModel;
import com.elitech.iot.device.service.DeviceTypeParamService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * 设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-27 09:48:30
 * @since v1.0.0
 */
@RestController
@RequestMapping("DeviceTypeParam")
@RefreshScope
@Api(tags = {"DeviceTypeParam API"})
public class DeviceTypeParamRestController {
    @Autowired
    private DeviceTypeParamService deviceTypeParamService;

    /**
     * 根据设备id
     *
     * @param deviceId 设备id
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据设备id查询设备参数列表", notes = "根据设备id查询设备参数列表", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/queryListByDevice", method = RequestMethod.POST)
    public ResultMessage<List<DeviceTypeParamModel>> queryListByDevice(@RequestParam BigInteger deviceId) {
        List<DeviceTypeParamModel> data = deviceTypeParamService.queryListByDevice(deviceId);
        return ResultMessage.success(data);
    }
    /**
     * 新增数据.
     *
     * @param model 要新增数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insert(@RequestBody DeviceTypeParamModel model) {
        DeviceTypeParam domain = BeanUtil.copy(model, DeviceTypeParam.class);
        DeviceTypeParam deviceTypeParam = new DeviceTypeParam();
        deviceTypeParam.setDeviceTypeId(domain.getDeviceTypeId());
        //验证参数key是否存在
        if (domain.getParamKey() != null) {
            deviceTypeParam.setParamKey(domain.getParamKey());
            List<DeviceTypeParam> deviceTypeParams = deviceTypeParamService.selectListByDomain(deviceTypeParam);
            if (deviceTypeParams != null && deviceTypeParams.size() > 0) {
                return ResultMessage.fail(ResultCode.PARAM_KEY_EXIST);
            }
        }
        //验证参数编码是否存在
        if (domain.getParamCode() != null) {
            deviceTypeParam.setParamCode(domain.getParamCode());
            List<DeviceTypeParam> deviceTypeParams = deviceTypeParamService.selectListByDomain(deviceTypeParam);
            if (deviceTypeParams != null && deviceTypeParams.size() > 0) {
                return ResultMessage.fail(ResultCode.PARAM_CODE_EXIST);
            }
        }
        //验证参数名称是否存在
        if (domain.getParamName() != null) {
            deviceTypeParam.setParamName(domain.getParamName());
            List<DeviceTypeParam> deviceTypeParams = deviceTypeParamService.selectListByDomain(deviceTypeParam);
            if (deviceTypeParams != null && deviceTypeParams.size() > 0) {
                return ResultMessage.fail(ResultCode.PARAM_NAME_EXIST);
            }
        }
        domain.setGmtCreate(DateTimeUtils.getNowZeroTime());
        BigInteger id=BigInteger.valueOf(1);
        domain.setId(id);
        int res = deviceTypeParamService.insert(domain);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 批量新增数据.
     *
     * @param list 数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<DeviceTypeParam> list) {
        int data = deviceTypeParamService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPk(@RequestParam BigInteger pk) {
        DeviceTypeParam deviceTypeParam = deviceTypeParamService.selectByPk(pk);
        if(deviceTypeParam==null){
            return ResultMessage.fail(ResultCode.PARAM_ERROR);
        }
        int res = deviceTypeParamService.deleteByPk(pk);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = deviceTypeParamService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param domain 要更新的实体对象
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody DeviceTypeParam domain) {
        DeviceTypeParam typeParam = deviceTypeParamService.selectByPk(domain.getId());
        if (typeParam == null) {
            return ResultMessage.fail(ResultCode.TYPE_PARAM_NOT_EXIST);
        }
        DeviceTypeParam deviceTypeParam = new DeviceTypeParam();
        deviceTypeParam.setDeviceTypeId(domain.getDeviceTypeId());
        //验证参数key是否存在
        if (domain.getParamKey() != null) {
            deviceTypeParam.setParamKey(domain.getParamKey());
            List<DeviceTypeParam> deviceTypeParams = deviceTypeParamService.selectListByDomain(deviceTypeParam);
            if (deviceTypeParams != null && deviceTypeParams.size() > 0) {
                if (!deviceTypeParams.get(0).getId().equals(domain.getId())) {
                    return ResultMessage.fail(ResultCode.PARAM_KEY_EXIST);
                }
            }
        }
        //验证参数编码是否存在
        if (domain.getParamCode() != null) {
            deviceTypeParam.setParamCode(domain.getParamCode());
            List<DeviceTypeParam> deviceTypeParams = deviceTypeParamService.selectListByDomain(deviceTypeParam);
            if (deviceTypeParams != null && deviceTypeParams.size() > 0) {
                if (!deviceTypeParams.get(0).getId().equals(domain.getId())) {
                    return ResultMessage.fail(ResultCode.PARAM_CODE_EXIST);
                }
            }
        }
        //验证参数名称是否存在
        if (domain.getParamName() != null) {
            deviceTypeParam.setParamName(domain.getParamName());
            List<DeviceTypeParam> deviceTypeParams = deviceTypeParamService.selectListByDomain(deviceTypeParam);
            if (deviceTypeParams != null && deviceTypeParams.size() > 0) {
                if (!deviceTypeParams.get(0).getId().equals(domain.getId())) {
                    return ResultMessage.fail(ResultCode.PARAM_NAME_EXIST);
                }
            }
        }
        domain.setGmtModified(DateTimeUtils.getNowZeroTime());
        int res = deviceTypeParamService.updateByPk(domain);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResultMessage<DeviceTypeParam> selectByPk(@RequestParam BigInteger pk) {
        DeviceTypeParam data = deviceTypeParamService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    public ResultMessage<List<DeviceTypeParam>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<DeviceTypeParam> data = deviceTypeParamService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/get/page", method = RequestMethod.POST)
    public ResultMessage<PageInfo<DeviceTypeParam>> pageListByWhere(@RequestParam int start, @RequestParam int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<DeviceTypeParam> data = deviceTypeParamService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param domain 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage<List<DeviceTypeParam>> selectListByDomain(@RequestBody DeviceTypeParam domain) {
        List<DeviceTypeParam> data = deviceTypeParamService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }
}