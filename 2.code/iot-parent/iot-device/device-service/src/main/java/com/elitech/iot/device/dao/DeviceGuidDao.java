/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.dao;

import com.elitech.iot.device.domain.DeviceGuid;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 设备guid表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface DeviceGuidDao extends IBaseDao<DeviceGuid, java.math.BigInteger> {

}