package com.baidu.fsg.uid.worker.dao;

import com.baidu.fsg.uid.worker.entity.WorkerNodeEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/14
 */
//@Mapper
//@Component
public interface WorkerNodeEntityDao {
    /**
     * Get {@link WorkerNodeEntity} by node host
     *
     * @param host
     * @param port
     * @return
     */
    //@Select("SELECT id, host_name, port, type, launch_date, gmt_modified, gmt_create FROM sys_worker_node WHERE host_name = #{host} AND port = #{port}")
    WorkerNodeEntity getWorkerNodeByHostPort(@Param("host") String host, @Param("port") String port);

    /**
     * Add {@link WorkerNodeEntity}
     *
     * @param workerNodeEntity
     */
    //@Insert("INSERT INTO sys_worker_node(host_name, port, type, launch_date, gmt_modified, gmt_create) VALUES (#{hostName}, #{port}, #{type}, #{launchDate}, NOW(), NOW())")
    void addWorkerNode(WorkerNodeEntity workerNodeEntity);
}
