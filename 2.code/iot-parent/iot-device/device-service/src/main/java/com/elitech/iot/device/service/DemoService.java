/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-19 18:16:40
 */

package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.Demo;

import com.elitech.iot.common.service.IBaseService;

/**
 * 测试表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
public interface DemoService extends IBaseService<Demo, java.math.BigInteger> {
    /**
     * 插入事务。
     *
     * @param demo
     */
    void insertTransaction(Demo demo);
}