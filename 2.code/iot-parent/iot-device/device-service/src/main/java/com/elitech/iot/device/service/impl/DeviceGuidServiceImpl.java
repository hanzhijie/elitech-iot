/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:48:30
 */

package com.elitech.iot.device.service.impl;

import com.elitech.iot.common.service.BaseService;
import com.elitech.iot.device.dao.DeviceGuidDao;
import com.elitech.iot.device.dao.DeviceGuidExportLogDao;
import com.elitech.iot.device.domain.DeviceGuid;
import com.elitech.iot.device.domain.DeviceGuidExportLog;
import com.elitech.iot.device.excel.GUIDExportExcel;
import com.elitech.iot.device.service.DeviceGuidService;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 设备guid表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class DeviceGuidServiceImpl extends BaseService<DeviceGuid, java.math.BigInteger> implements DeviceGuidService {
    @Autowired
    private DeviceGuidDao dao;

    @Override
    protected DeviceGuidDao getDao() {
        return dao;
    }

    @Autowired
    private DeviceGuidExportLogDao deviceGuidExportLogDao;

    /**
     * @MethodName: exportNewDeviceGUID
     * @Description: 生成新的guid
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/3/3
     **/
    @Override
    public List<GUIDExportExcel> exportNewDeviceGUID(int num,
                                                     String deviceType,
                                                     String orderCode,
                                                     BigInteger accountId,
                                                     BigInteger deviceTypeId,
                                                     String userName) {
        List<GUIDExportExcel> guidExportExcelList = new ArrayList<>();
        DeviceGuid deviceGuid = new DeviceGuid();
        for (int i = 0; i < num; i++) {
            GUIDExportExcel guidExportExcel = new GUIDExportExcel();
            deviceGuid.setId(createDeviceGUIDId());
            String guid = createDeviceGUID();
            deviceGuid.setDeiceGuid(guid);
            deviceGuid.setDeviceTypeName(deviceType);
            deviceGuid.setDeviceTypeId(deviceTypeId);
            deviceGuid.setOrderCode(orderCode);
            deviceGuid.setGmtCreate(LocalDateTime.now());
            deviceGuid.setGmtModified(LocalDateTime.now());
            dao.insert(deviceGuid);
            guidExportExcel.setDeviceType(deviceType);

            guidExportExcel.setOrderCode(orderCode);
            guidExportExcel.setIsUsed("no used");
            guidExportExcel.setId(i + 1);
            guidExportExcel.setDeviceGuid(guid);
            if (guid.length() == 20) {
                StringBuilder sb = new StringBuilder(guid);
                sb.insert(5, " ");
                sb.insert(11, " ");
                sb.insert(17, " ");
                guid = sb.toString();
            }
            guidExportExcel.setDeviceGuid1(guid);
            guidExportExcelList.add(guidExportExcel);
        }
        DeviceGuidExportLog deviceGuidExportLog = new DeviceGuidExportLog();
        deviceGuidExportLog.setAccountId(accountId);
        deviceGuidExportLog.setDeviceTypeId(deviceTypeId);
        deviceGuidExportLog.setGmtCreate(LocalDateTime.now());
        deviceGuidExportLog.setGmtModified(LocalDateTime.now());
        deviceGuidExportLog.setGuidCount(BigInteger.valueOf(num));
        deviceGuidExportLog.setId(createDeviceGUIDexportLogId());
        deviceGuidExportLog.setOrderCode(orderCode);
        deviceGuidExportLog.setUserName(userName);
        deviceGuidExportLogDao.insert(deviceGuidExportLog);

        return guidExportExcelList;
    }

    private BigInteger createDeviceGUIDId() {
        while (true) {
            String s = RandomStringUtils.randomNumeric(10);
            BigInteger id = BigInteger.valueOf(Long.parseLong(s));
            DeviceGuid deviceGuid = dao.selectByPk(id);
            if (Objects.isNull(deviceGuid)) {
                return id;
            }
        }
    }

    private BigInteger createDeviceGUIDexportLogId() {
        while (true) {
            String s = RandomStringUtils.randomNumeric(10);
            BigInteger id = BigInteger.valueOf(Long.parseLong(s));
            DeviceGuidExportLog deviceGuidExportLog = deviceGuidExportLogDao.selectByPk(id);
            if (Objects.isNull(deviceGuidExportLog)) {
                return id;
            }
        }
    }

    private String createDeviceGUID() {
        while (true) {
            String guid = RandomStringUtils.randomNumeric(20);
            if ("00000000000000000000".equals(guid)) {
                continue;
            }
            DeviceGuid deviceGuid = new DeviceGuid();
            deviceGuid.setDeiceGuid(guid);
            List<DeviceGuid> deviceGuids = dao.selectListByDomain(deviceGuid);
            if (deviceGuids.size() > 0) {
                continue;
            }
            return guid;
        }
    }
}