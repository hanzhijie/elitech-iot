/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:48:30
 */

package com.elitech.iot.device.service.impl;

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.DeviceConstant;
import com.elitech.iot.common.base.util.CheckCodeUtil;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.device.domain.DeviceType;
import com.elitech.iot.device.dao.DeviceTypeDao;
import com.elitech.iot.device.excel.DeviceTypeExcel;
import com.elitech.iot.device.excel.DeviceTypeTemplate;
import com.elitech.iot.device.service.DeviceTypeService;

import com.elitech.iot.common.service.BaseService;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

/**
 * 设备类型表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class DeviceTypeServiceImpl extends BaseService<DeviceType, java.math.BigInteger> implements DeviceTypeService {
    @Autowired
    private DeviceTypeDao dao;

    @Override
    protected DeviceTypeDao getDao() {
        return dao;
    }

    @Override
    public ResultMessage importDeviceType(List<DeviceTypeTemplate> typeList) {
        //设置验证的数据起始行号
        int rowNo = 2;
        // 3.导入校验所有行列格式
        ResultMessage resultMessage = checkImportData(typeList, rowNo);
        if(resultMessage.isFail()){
            return resultMessage;
        }
        // 4.将 DeviceTypeTemplate 转成 DeviceType
        List<DeviceType> deviceTypes = convertList(typeList);
        // 5.入库操作
        int res = dao.insertList(deviceTypes);
        return ResultMessage.success(res);
    }

    /**
     * 验证文件内容是否正确
     * @param typeList
     * @param rowNo
     * @return
     */
    private ResultMessage checkImportData(List<DeviceTypeTemplate> typeList, int rowNo) {
        for (DeviceTypeTemplate template : typeList) {
            String typeName = template.getTypeName();
            if (StringUtils.isNotBlank(typeName) && typeName.length() > 50) {
                return ResultMessage.fail(ResultCode.PARAM_ERROR.getCode(), "第" + rowNo + "行：设备名称长度不能大于50个字符");
            }
            Byte autoSms = template.getAutoSms();
            if (autoSms != null && DeviceConstant.SMS_AUTO_YES != autoSms.byteValue() && DeviceConstant.SMS_AUTO_NO != autoSms.byteValue()) {
                return ResultMessage.fail(ResultCode.PARAM_ERROR.getCode(), "第" + rowNo + "行：默认开启短信推送填写内容不正确");
            }
            Byte connectType = template.getConnectType();
            if (connectType != null && DeviceConstant.CONNECT_TYPE_SHORT != connectType.byteValue() && DeviceConstant.CONNECT_TYPE_LONG != connectType.byteValue()) {
                return ResultMessage.fail(ResultCode.PARAM_ERROR.getCode(), "第" + rowNo + "行：连接类型填写内容不正确");
            }
            Integer defaultSimTraffic = template.getDefaultSimTraffic();
            if (defaultSimTraffic != null && DeviceConstant.DEFAULT_SIM_TRAFFIC_30 != defaultSimTraffic.intValue() && DeviceConstant.DEFAULT_SIM_TRAFFIC_100 != defaultSimTraffic.intValue() && DeviceConstant.DEFAULT_SIM_TRAFFIC_300 != defaultSimTraffic.intValue()) {
                return ResultMessage.fail(ResultCode.PARAM_ERROR.getCode(), "第" + rowNo + "行：sim卡默认流量填写内容不正确");
            }
            Byte isWifi = template.getIsWifi();
            if (isWifi != null && DeviceConstant.IS_WIFI_YES != isWifi.byteValue() && DeviceConstant.IS_WIFI_NO != isWifi.byteValue()) {
                return ResultMessage.fail(ResultCode.PARAM_ERROR.getCode(), "第" + rowNo + "行：是否wifi设备填写内容不正确");
            }
            rowNo++;
        }
        return ResultMessage.success(true);
    }

    /**
     * 模板与实体类型转换
     * @param typeList
     * @return
     */
    private List<DeviceType> convertList(List<DeviceTypeTemplate> typeList) {
        List<DeviceType> deviceTypes = BeanUtil.copyWithConvert(typeList, DeviceType.class);
        for (DeviceType type:deviceTypes){
            BigInteger typeId=BigInteger.valueOf(Integer.valueOf(CheckCodeUtil.getCheckCodeNum(3)));
            type.setGmtCreate(DateTimeUtils.getNowZeroTime());
            type.setTypeId(typeId);
            //临时设置类型编码
            type.setTypeCode(createCode());
            typeId.add(BigInteger.valueOf(1));
        }
        return deviceTypes;
    }

    /**
     * 生成设备类型编码
     * @return
     */
    private int createCode(){
        Integer code = Integer.valueOf(CheckCodeUtil.getCheckCodeNum(6));
        DeviceType deviceType= new DeviceType();
        deviceType.setTypeCode(code);
        List<DeviceType> deviceTypes = dao.selectListByDomain(deviceType);
        if(deviceTypes!=null&&deviceTypes.size()>0){
            return createCode();
        }
        return code;
    }
}