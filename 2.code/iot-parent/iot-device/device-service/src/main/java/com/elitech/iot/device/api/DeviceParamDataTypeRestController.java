/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/
package com.elitech.iot.device.api;

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.device.domain.DeviceParamDataType;
import com.elitech.iot.device.model.DeviceParamDataTypeModel;
import com.elitech.iot.device.service.DeviceParamDataTypeService;

import com.elitech.iot.common.base.api.ResultMessage;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
* 参数数据类型 API.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2019-12-27 09:48:30
* @since v1.0.0
*/
@RestController
@RequestMapping("DeviceParamDataType")
@RefreshScope
@Api(tags = {"DeviceParamDataType API"})
public class DeviceParamDataTypeRestController {
    @Autowired
    private DeviceParamDataTypeService deviceParamDataTypeService;

   /**
    * 新增数据.
    *
    * @param model 要新增数据
    * @return 返回插入行数
    */
    @ApiOperation(value = "新增数据", notes = "参数数据类型", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insert(@RequestBody DeviceParamDataTypeModel model) {
        DeviceParamDataType domain = BeanUtil.copy(model, DeviceParamDataType.class);
        DeviceParamDataType paramDataType = new DeviceParamDataType();
        paramDataType.setDataTypeCode(model.getDataTypeCode());
        paramDataType.setOrganizationId(model.getOrganizationId());
        List<DeviceParamDataType> deviceParamDataTypes = deviceParamDataTypeService.selectListByDomain(paramDataType);
        if(deviceParamDataTypes!=null&&deviceParamDataTypes.size()>0){
            return ResultMessage.fail(ResultCode.PARAM_DATA_TYPE_CODE_EXIST);
        }
        domain.setGmtCreate(DateTimeUtils.getNowZeroTime());
        BigInteger id=BigInteger.valueOf(1);
        domain.setId(id);
        int res = deviceParamDataTypeService.insert(domain);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
    * 批量新增数据.
    *
    * @param list 数据列表
    * @return 返回插入行数
    */
    @ApiOperation(value = "批量新增", notes = "参数数据类型", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<DeviceParamDataType> list) {
        int data = deviceParamDataTypeService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键删除实体对象.
    *
    * @param pk 主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "删除数据", notes = "参数数据类型", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPk(@RequestParam BigInteger pk) {
        DeviceParamDataType paramDataType = deviceParamDataTypeService.selectByPk(pk);
        if(paramDataType==null){
            return ResultMessage.fail(ResultCode.PARAM_DATA_TYPE_CODE_NOT_EXIST);
        }
        int res = deviceParamDataTypeService.deleteByPk(pk);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
    * 根据主键批量删除.
    *
    * @param pkList 要删除实体对象的主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "批量删除", notes = "参数数据类型", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = deviceParamDataTypeService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 根据id更新实体对象.
    *
    * @param domain 要更新的实体对象
    * @return 返回更新的行数。
    */
    @ApiOperation(value = "更新数据", notes = "参数数据类型", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody DeviceParamDataType domain) {
        DeviceParamDataType paramDataType = deviceParamDataTypeService.selectByPk(domain.getId());
        if(paramDataType==null){
            return ResultMessage.fail(ResultCode.PARAM_DATA_TYPE_CODE_NOT_EXIST);
        }
        //根据参数数据类型编码查询参数数据类型是否存在
        DeviceParamDataType data = new DeviceParamDataType();
        data.setOrganizationId(domain.getOrganizationId());
        data.setDataTypeCode(domain.getDataTypeCode());
        List<DeviceParamDataType> deviceParamDataTypes = deviceParamDataTypeService.selectListByDomain(data);
        //如果数据名称存在并且不是当前当前参数数据类型
        if (deviceParamDataTypes != null && deviceParamDataTypes.size() > 0) {
            if (!deviceParamDataTypes.get(0).getId().equals(domain.getId())) {
                return ResultMessage.fail(ResultCode.PARAM_DATA_TYPE_CODE_EXIST);
            }
        }
        int res = deviceParamDataTypeService.updateByPk(domain);
        if (res > 0) {
            return ResultMessage.success(data);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }


    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @ApiOperation(value = "根据主键查询", notes = "参数数据类型", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResultMessage<DeviceParamDataType> selectByPk(@RequestParam BigInteger pk) {
        DeviceParamDataType data = deviceParamDataTypeService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据多个id查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @ApiOperation(value = "根据主键列表查询", notes = "参数数据类型", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    public ResultMessage<List<DeviceParamDataType>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<DeviceParamDataType> data = deviceParamDataTypeService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param whereMap 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @ApiOperation(value = "分页查询", notes = "参数数据类型", httpMethod = "POST")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
        @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
        @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/get/page", method = RequestMethod.POST)
    public ResultMessage<PageInfo<DeviceParamDataType>> pageListByWhere(@RequestParam int start, @RequestParam int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<DeviceParamDataType> data = deviceParamDataTypeService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
    * 根据实体类查询
    *
    * @param domain 实体类
    * @return List<Domain> 查询结果
    */
    @ApiOperation(value = "根据Model查询", notes = "参数数据类型", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage<List<DeviceParamDataType>> selectListByDomain(@RequestBody DeviceParamDataType domain) {
        List<DeviceParamDataType> data = deviceParamDataTypeService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }
}