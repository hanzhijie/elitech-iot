/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service.impl;

import com.elitech.iot.device.domain.DeviceRealTimeData;
import com.elitech.iot.device.dao.DeviceRealTimeDataDao;
import com.elitech.iot.device.service.DeviceRealTimeDataService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 设备实时数据表，存储每个设备最新的一条监测数据.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class DeviceRealTimeDataServiceImpl extends BaseService<DeviceRealTimeData, java.math.BigInteger> implements DeviceRealTimeDataService {
    @Autowired
    private DeviceRealTimeDataDao dao;

    @Override
    protected DeviceRealTimeDataDao getDao() {
        return dao;
    }
}