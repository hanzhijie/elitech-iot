package com.baidu.fsg.uid.worker.dao.impl;

import com.baidu.fsg.uid.worker.dao.WorkerNodeEntityDao;
import com.baidu.fsg.uid.worker.entity.WorkerNodeEntity;
import com.elitech.iot.common.dao.BaseDao;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/14
 */
@Component
public class WorkerNodeEntityDaoImpl extends BaseDao<WorkerNodeEntity, BigInteger> implements WorkerNodeEntityDao {
    @Override
    public WorkerNodeEntity getWorkerNodeByHostPort(String host, String port) {
        Map<String, String> parameterMap = new HashMap<>(2);
        parameterMap.put("host", host);
        parameterMap.put("port", port);

        return this.sqlSessionTemplate.selectOne(this.getQualifiedMethod("getWorkerNodeByHostPort"), parameterMap);
    }

    @Override
    public void addWorkerNode(WorkerNodeEntity workerNodeEntity) {
        this.sqlSessionTemplate.insert(this.getQualifiedMethod("addWorkerNode"), workerNodeEntity);
    }
}
