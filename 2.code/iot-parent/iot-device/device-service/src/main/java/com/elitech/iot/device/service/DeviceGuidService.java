/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.DeviceGuid;

import com.elitech.iot.common.service.IBaseService;
import com.elitech.iot.device.excel.GUIDExportExcel;

import java.math.BigInteger;
import java.util.List;

/**
* 设备guid表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface DeviceGuidService extends IBaseService<DeviceGuid, java.math.BigInteger> {

    /**
     * @MethodName: exportNewDeviceGUID
     * @Description: 导出新的guid表格
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/3/3
    **/
    List<GUIDExportExcel> exportNewDeviceGUID(int num,
                                              String deviceType,
                                              String orderCode,
                                              BigInteger accountId,
                                              BigInteger deviceTypeId,
                                              String userName);
}