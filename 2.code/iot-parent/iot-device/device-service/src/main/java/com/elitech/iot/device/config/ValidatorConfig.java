package com.elitech.iot.device.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.DefaultMessageCodesResolver;
import org.springframework.validation.MessageCodeFormatter;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.validation.Validator;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/21
 */
/*@Configuration
public class ValidatorConfig extends WebMvcConfigurationSupport {
    public ResourceBundleMessageSource getMessageSource() {
        ResourceBundleMessageSource rbms = new ResourceBundleMessageSource();
        rbms.setDefaultEncoding("UTF-8");
        rbms.setBasenames("/i18n/messages", "/i18n/ResultCode", "/i18n/ValidationMessages");
        return rbms;
    }

    @Override
    public Validator getValidator() {
        return validator();
    }

    @Override
    protected MessageCodesResolver getMessageCodesResolver() {
        DefaultMessageCodesResolver defaultMessageCodesResolver = new DefaultMessageCodesResolver();
        defaultMessageCodesResolver.setPrefix("com.elitech.iot.device.domain");
        defaultMessageCodesResolver.setMessageCodeFormatter(DefaultMessageCodesResolver.Format.POSTFIX_ERROR_CODE);
        return defaultMessageCodesResolver;
    }

    @Bean("customValidator")
    public Validator validator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(getMessageSource());
        return validator;
    }
}*/
