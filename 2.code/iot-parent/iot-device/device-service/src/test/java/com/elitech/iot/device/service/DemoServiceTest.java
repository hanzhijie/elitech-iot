package com.elitech.iot.device.service;

import com.elitech.iot.device.domain.Demo;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.elitech.iot.device.DeviceServiceApplication;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.*;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceServiceApplication.class})
public class DemoServiceTest {
    @Autowired
    private DemoService demoService;

    private static Demo demo;

    @BeforeClass
    public static void init() {
        demo = new Demo();
        demo.setDemoBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoBool((byte) 1);
        demo.setDemoBoolean((byte) 0);
        demo.setDemoChar("a");
        demo.setDemoDate(LocalDate.now(ZoneId.of("UTC")));
        demo.setDemoDec(new BigDecimal("-0.0009"));
        demo.setDemoDecimal(new BigDecimal("0.1"));
        demo.setDemoDeleted(false);
        demo.setDemoDouble(1.1);
        demo.setDemoEnum("enum1");
        demo.setDemoFixed(new BigDecimal("9999.9"));
        demo.setDemoFloat(1.0f);
        demo.setDemoInt(new BigInteger("99999999"));
        demo.setDemoInteger(new BigInteger("12345678"));
        demo.setDemoJson("{\"name\": \"Jack\"}");
        demo.setDemoLongBlob(new byte[]{1, 2, 3, 4, 5});
        demo.setDemoLongText("Long text");
        demo.setDemoMediumBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoMediumInt(12345);
        demo.setDemoMediumText("Medium Text");
        demo.setDemoName("Name3");
        demo.setDemoNchar("Nchar");
        demo.setDemoNvarchar("NVarchar");
        demo.setDemoNumeric(new BigDecimal("0.0"));
        demo.setDemoReal(9.123);
        demo.setDemoSet("1");
        demo.setDemoSmallInt(123);
        demo.setDemoText("Text");
        demo.setDemoTime(LocalTime.now(ZoneId.of("UTC")));
        demo.setDemoTimestamp(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setDemoTinyBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoTinyInt(1);
        demo.setDemoTinyText("Tiny Text");
        demo.setDemoVarBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoVarchar("Varchar");
        demo.setGmtCreate(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setId(new BigInteger("3"));
    }

    @Test
    public void insertByPk() {
        int effected = demoService.insert(demo);
        assertEquals(effected, 1);

        Demo selDemo = demoService.selectByPk(demo.getId());
        assertNotNull(selDemo);
    }

    @Test
    public void updateByDomainPk() {
        demo.setId(new BigInteger("2"));
        int effected = demoService.insert(demo);
        assertEquals(effected, 1);

        Demo selDemo = demoService.selectByPk(demo.getId());
        assertNotNull(selDemo);

        selDemo = demoService.selectByPk(demo.getId());
        assertNull(selDemo);
    }

    @Test
    public void transactionTest() {
        BigInteger id = new BigInteger("10");
        demo.setId(id);
        demo.setDemoName("Name10");
        demoService.insertTransaction(demo);

        Demo selDemo = demoService.selectByPk(id);
        assertNull(selDemo);
    }
}
