package com.elitech.iot.device;

import com.alibaba.fastjson.JSON;
import com.elitech.iot.common.base.constant.DeviceConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.device.dao.DeviceTypeDao;
import com.elitech.iot.device.domain.DeviceType;
import com.elitech.iot.device.domain.DeviceTypeParam;
import com.elitech.iot.device.model.DeviceTypeParamModel;
import com.elitech.iot.device.service.DeviceTypeParamService;
import com.elitech.iot.device.service.DeviceTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.List;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/14
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceServiceApplication.class})
@EnableCaching
public class DeviceTypeParamTest {
    @Autowired
    private DeviceTypeParamService deviceTypeParamService;


    /**
     * 查询数据
     */
    @Test
    public void queryDeviceTypeParamTest() {
        List<DeviceTypeParamModel> list = deviceTypeParamService.queryListByDevice(BigInteger.valueOf(100));
        System.out.println(JSON.toJSONString(list));
//        List<DeviceTypeParam> deviceTypeParams = deviceTypeParamService.queryListByDeviceType(BigInteger.valueOf(100));
//        System.out.println(JSON.toJSONString(deviceTypeParams));
    }

    /**
     * 插入数据
     */
    @Test
    public void deviceTypeParamAddTest() {
        DeviceTypeParam deviceTypeParam = new DeviceTypeParam();
       deviceTypeParam.setId(BigInteger.valueOf(104));
       deviceTypeParam.setParamUnit(2);
       deviceTypeParam.setDeviceTypeId(BigInteger.valueOf(100));
       deviceTypeParam.setParamLength(BigInteger.valueOf(10));
       deviceTypeParam.setHasChild((byte)1);
       deviceTypeParam.setParamKey(BigInteger.valueOf(10));
       deviceTypeParam.setParamName("温度4");
       deviceTypeParam.setParamCode("tep10");
       deviceTypeParamService.insert(deviceTypeParam);
    }
}
