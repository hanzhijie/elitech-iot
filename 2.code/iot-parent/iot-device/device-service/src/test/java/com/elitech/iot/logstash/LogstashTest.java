package com.elitech.iot.logstash;

import com.elitech.iot.device.DeviceServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/28
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceServiceApplication.class})
public class LogstashTest {
    Logger log = LoggerFactory.getLogger(LogstashTest.class);

    @Test
    public void test() throws InterruptedException {
        int i = 1000;
        while (i > 0) {
            log.info("********** : " + i);
            i--;
            Thread.sleep(1000);
        }
    }

    @Test
    public void errorTest() throws InterruptedException {
        int i = 100;
        while (i > 0) {
            log.error("********** : " + i);
            i--;
            Thread.sleep(1000);
        }
    }
}
