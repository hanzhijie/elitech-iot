package com.elitech.iot.device;

import com.elitech.iot.common.base.constant.DeviceConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.device.dao.DeviceTypeDao;
import com.elitech.iot.device.domain.DeviceType;
import com.elitech.iot.device.service.DeviceTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/14
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceServiceApplication.class})
public class DeviceTypeTest {
    @Autowired
    private DeviceTypeService deviceTypeService;
    @Autowired
    private DeviceTypeDao deviceTypeDao;

    /**
     * 插入数据
     */
    @Test
    public void deviceTypeAddTest() {
        DeviceType deviceType = new DeviceType();
        deviceType.setAutoSms(DeviceConstant.SMS_AUTO_YES);
        deviceType.setConnectType(DeviceConstant.CONNECT_TYPE_SHORT);
        deviceType.setDefaultSimTraffic(DeviceConstant.DEFAULT_SIM_TRAFFIC_30);
        deviceType.setDeletedFlag(false);
        deviceType.setGmtCreate(DateTimeUtils.getNowZeroTime());
        deviceType.setIsWifi(DeviceConstant.IS_WIFI_NO);
        deviceType.setManualUrl("");
        deviceType.setOrganizationId(BigInteger.valueOf(100));
        deviceType.setMapMode(DeviceConstant.MAP_MODE_DATA_MAP);
        deviceType.setTemperatureUnit(DeviceConstant.TEMPERATURE_UNIT_C);
        deviceType.setTypeCode(100);
        deviceType.setTypeId(BigInteger.valueOf(100));
        deviceType.setTypeName("测试类型");
        deviceType.setWifiType((byte)10);
        deviceTypeDao.insert(deviceType);
    }

    /**
     * 更新数据
     */
    @Test
    public void deviceTypeUpdateTest() {
        DeviceType deviceType = new DeviceType();
        deviceType.setTypeId(BigInteger.valueOf(100));
        DeviceType type = deviceTypeDao.selectByPk(BigInteger.valueOf(100));
        if(type!=null) {
            deviceType.setTypeName("测试类型");
            deviceTypeDao.updateByPk(deviceType);
        }
    }
}
