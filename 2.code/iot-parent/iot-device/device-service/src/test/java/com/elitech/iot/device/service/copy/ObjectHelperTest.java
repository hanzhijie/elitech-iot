package com.elitech.iot.device.service.copy;

import com.elitech.iot.device.domain.Demo;
import com.elitech.iot.device.model.DemoModel;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
public class ObjectHelperTest {
    private static Demo demo;
    private static final int COUNT = 1000000;

    @BeforeClass
    public static void init() {
        demo = new Demo();
        demo.setDemoBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoBool((byte) 1);
        demo.setDemoBoolean((byte) 0);
        demo.setDemoChar("a");
        demo.setDemoDate(LocalDate.now(ZoneId.of("UTC")));
        demo.setDemoDec(new BigDecimal("0.0"));
        demo.setDemoDecimal(new BigDecimal("0.1111"));
        demo.setDemoDeleted(false);
        demo.setDemoDouble(1.1);
        demo.setDemoEnum("enum1");
        demo.setDemoFixed(new BigDecimal("9999.9"));
        demo.setDemoFloat(1.0f);
        demo.setDemoInt(new BigInteger("99999999"));
        demo.setDemoInteger(new BigInteger("12345678"));
        demo.setDemoJson("{\"name\": \"Jack\"}");
        demo.setDemoLongBlob(new byte[]{1, 2, 3, 4, 5});
        demo.setDemoLongText("Long text");
        demo.setDemoMediumBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoMediumInt(12345);
        demo.setDemoMediumText("Medium Text");
        demo.setDemoName("Name");
        demo.setDemoNchar("Nchar");
        demo.setDemoNvarchar("NVarchar");
        demo.setDemoNumeric(new BigDecimal("0.0"));
        demo.setDemoReal(9.123);
        demo.setDemoSet("1");
        demo.setDemoSmallInt(123);
        demo.setDemoText("Text");
        demo.setDemoTime(LocalTime.now(ZoneId.of("UTC")));
        demo.setDemoTimestamp(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setDemoTinyBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoTinyInt(1);
        demo.setDemoTinyText("Tiny Text");
        demo.setDemoVarBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoVarchar("Varchar");
        demo.setGmtCreate(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setId(new BigInteger("1"));
    }

    @Test
    public void springCopier() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < COUNT; i++) {
            DemoModel model = new DemoModel();
            BeanUtils.copyProperties(demo, model);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void micaCopy() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < COUNT; i++) {
            //DemoModel model =  new DemoModel();
            net.dreamlu.mica.core.utils.BeanUtil.copy(demo, DemoModel.class);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void micaCopyProperties() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < COUNT; i++) {
            //DemoModel model =  new DemoModel();
            net.dreamlu.mica.core.utils.BeanUtil.copyProperties(demo, DemoModel.class);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void decimalTest(){
        //System.out.println(demo);
    }
}
