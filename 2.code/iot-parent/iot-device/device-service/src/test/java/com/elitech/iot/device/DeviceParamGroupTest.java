package com.elitech.iot.device;

import com.alibaba.fastjson.JSON;
import com.elitech.iot.common.base.constant.DeviceConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.device.dao.DeviceParamGroupDao;
import com.elitech.iot.device.domain.DeviceParamGroup;
import com.elitech.iot.device.domain.DeviceType;
import com.elitech.iot.device.model.DeviceParamGroupVOModel;
import com.elitech.iot.device.service.DeviceParamGroupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.List;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/14
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceServiceApplication.class})
public class DeviceParamGroupTest {
    @Autowired
    private DeviceParamGroupService deviceParamGroupService;
    @Autowired
    private DeviceParamGroupDao deviceParamGroupDao;

    /**
     * 根据设备id查询分组信息
     */
    @Test
    public void deviceParamGroupQueryDeviceTest() {
        List<DeviceParamGroupVOModel> list = deviceParamGroupService.queryParamGroupForDevice(BigInteger.valueOf(100));
        System.out.println(JSON.toJSON(list));
    }
    /**
     * 插入数据
     */
    @Test
    public void deviceParamGroupAddTest() {
        DeviceParamGroup paramGroup = new DeviceParamGroup();
        paramGroup.setId(BigInteger.valueOf(100));
        paramGroup.setTypeId(BigInteger.valueOf(100));
        paramGroup.setGroupName("参数组1");
        paramGroup.setParamIds("100,101");
        paramGroup.setGmtCreate(DateTimeUtils.getNowZeroTime());
        deviceParamGroupDao.insert(paramGroup);
    }

    /**
     * 更新数据
     */
    @Test
    public void deviceParamGroupUpdateTest(){
        DeviceParamGroup paramGroup = new DeviceParamGroup();
        paramGroup.setId(BigInteger.valueOf(100));
        paramGroup.setTypeId(BigInteger.valueOf(100));
        paramGroup.setGroupName("参数组1");
        paramGroup.setParamIds("100,101");
        paramGroup.setGmtCreate(DateTimeUtils.getNowZeroTime());
        deviceParamGroupDao.updateByPk(paramGroup);
    }
}
