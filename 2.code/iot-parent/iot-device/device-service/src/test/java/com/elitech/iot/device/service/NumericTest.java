package com.elitech.iot.device.service;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
public class NumericTest {
    @Test
    public void floatTest() {
        System.out.println(0.2f + 0.1f);
        System.out.println(0.3f - 0.1f);
        System.out.println(0.2f * 0.1f);
        System.out.println(0.3f / 0.1f);
    }

    @Test
    public void doubleTest() {
        System.out.println(0.2 + 0.1);
        System.out.println(0.3 - 0.1);
        System.out.println(0.2 * 0.1);
        System.out.println(0.3 / 0.1);

        double d1 = 1.1234;
        double d2 = d1 + 0.00000000000000001;

        assertTrue(d1 == d2);
    }

    @Test
    public void decimalTest() {
        BigDecimal d1 = new BigDecimal("1.1234");
        BigDecimal d2 = new BigDecimal("1.12340000000000001");
        assertFalse(d1 == d2);

        BigDecimal d3 = new BigDecimal("1.1234");
        assertTrue(d1.compareTo(d3) == 0);
    }

    @Test
    public void decimalCalcTest() {
        BigDecimal b1 = new BigDecimal("13.14");
        BigDecimal b2 = new BigDecimal("1.11");

        BigDecimal b3 = new BigDecimal("14.25");
        BigDecimal b4 = new BigDecimal("12.03");
        BigDecimal b5 = new BigDecimal("14.5854");
        BigDecimal b6 = new BigDecimal("11.84");

        assertTrue(b1.add(b2).compareTo(b3) == 0);
        assertTrue(b1.subtract(b2).compareTo(b4) == 0);
        assertTrue(b1.multiply(b2).compareTo(b5) == 0);
        assertTrue(b1.divide(b2, 2, RoundingMode.HALF_UP).compareTo(b6)==0);
    }
}
