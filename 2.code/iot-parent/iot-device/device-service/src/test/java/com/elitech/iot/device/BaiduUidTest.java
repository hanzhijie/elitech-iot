package com.elitech.iot.device;

import com.baidu.fsg.uid.impl.CachedUidGenerator;
import com.baidu.fsg.uid.impl.DefaultUidGenerator;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/14
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceServiceApplication.class})
public class BaiduUidTest {
    private static final int COUNT = 1000000;
    private static final int THREAD_COUNT = Runtime.getRuntime().availableProcessors()  ;

    @Autowired
    private CachedUidGenerator cachedUidGenerator;

    @Autowired
    private DefaultUidGenerator defaultUidGenerator;

    @Test
    public void defaultUidGeneratorTest() throws InterruptedException {
        DateTime start = DateTime.now();
        for (int i = 0; i < THREAD_COUNT; i++) {
            Thread th = new Thread() {
                @Override
                public void run() {
                    for (int j = 0; j < COUNT; j++) {
                        long uid = defaultUidGenerator.getUID();
                        //System.out.println(uid);
                    }
                }
            };

            th.start();
            th.join();
        }

        DateTime end = DateTime.now();

        Seconds seconds = Seconds.secondsBetween(start, end);
        System.out.println("cost time(second): " + seconds.getSeconds());
    }

    @Test
    public void cachedUidGeneratorTest() throws InterruptedException {
        DateTime start = DateTime.now();
        for (int i = 0; i < THREAD_COUNT; i++) {
            Thread th = new Thread() {
                @Override
                public void run() {
                    for (int j = 0; j < COUNT; j++) {
                        long uid = cachedUidGenerator.getUID();
                        //System.out.println(uid);
                    }
                }
            };

            th.start();
            th.join();
        }

        DateTime end = DateTime.now();

        Seconds seconds = Seconds.secondsBetween(start, end);
        System.out.println("cost time(second): " + seconds.getSeconds());
    }
}
