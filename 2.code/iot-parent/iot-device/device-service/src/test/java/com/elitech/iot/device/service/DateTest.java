package com.elitech.iot.device.service;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import org.junit.Test;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
public class DateTest {
    @Test
    public void localDate() {
        LocalDate utc = LocalDate.now(ZoneId.of("UTC"));
        LocalDate localDate = LocalDate.now();
        System.out.println(utc);
        System.out.println(localDate);
    }

    @Test
    public void LocalTime() {
        LocalTime utc = LocalTime.now(ZoneId.of("UTC"));
        LocalTime localTime = LocalTime.now();
        System.out.println(utc);
        System.out.println(localTime);
    }

    @Test
    public void LocalDateTime(){
        LocalDateTime utc = LocalDateTime.now(ZoneId.of("UTC"));
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(utc);
        System.out.println(localDateTime);

        Clock clock = Clock.systemUTC();
        System.out.println(clock.getZone());
    }
}
