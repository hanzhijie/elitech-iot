package com.elitech.iot.device.api;

import com.elitech.iot.DeviceApiApplication;
import com.elitech.iot.device.domain.Demo;
import net.dreamlu.mica.core.utils.JsonUtil;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/9
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceApiApplication.class})
public class DemoRestControllerTest {

    @Autowired
    WebApplicationContext webApplicationContext;

    private MockMvc mvc;


    private static Demo demo;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @BeforeClass
    public static void init() {


        demo = new Demo();
        demo.setDemoBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoBool((byte) 1);
        demo.setDemoBoolean((byte) 0);
        demo.setDemoChar("a");
        demo.setDemoDate(LocalDate.now(ZoneId.of("UTC")));
        demo.setDemoDec(new BigDecimal("-0.0009"));
        demo.setDemoDecimal(new BigDecimal("0.1"));
        demo.setDemoDeleted(false);
        demo.setDemoDouble(1.1);
        demo.setDemoEnum("enum1");
        demo.setDemoFixed(new BigDecimal("9999.9"));
        demo.setDemoFloat(1.0f);
        demo.setDemoInt(new BigInteger("99999999"));
        demo.setDemoInteger(new BigInteger("12345678"));
        demo.setDemoJson("{\"name\": \"Jack\"}");
        demo.setDemoLongBlob(new byte[]{1, 2, 3, 4, 5});
        demo.setDemoLongText("Long text");
        demo.setDemoMediumBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoMediumInt(12345);
        demo.setDemoMediumText("Medium Text");
        demo.setDemoName("Name1");
        demo.setDemoNchar("Nchar");
        demo.setDemoNvarchar("NVarchar");
        demo.setDemoNumeric(new BigDecimal("0.0"));
        demo.setDemoReal(9.123);
        demo.setDemoSet("1");
        demo.setDemoSmallInt(123);
        demo.setDemoText("Text");
        demo.setDemoTime(LocalTime.now(ZoneId.of("UTC")));
        demo.setDemoTimestamp(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setDemoTinyBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoTinyInt(1);
        demo.setDemoTinyText("Tiny Text");
        demo.setDemoVarBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoVarchar("Varchar");
        demo.setGmtCreate(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setId(new BigInteger("1"));
    }

    @Test
    public void Test() throws Exception {
        String uri = "/demo/insert/%d";
        int id = 1;
        uri = String.format(uri, id);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(demo))
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

    }
}
