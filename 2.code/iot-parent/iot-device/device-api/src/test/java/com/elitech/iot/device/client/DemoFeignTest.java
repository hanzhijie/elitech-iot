package com.elitech.iot.device.client;

import com.elitech.iot.DeviceApiApplication;
import com.elitech.iot.device.client.DemoClient;
import com.elitech.iot.device.domain.Demo;
import com.elitech.iot.device.model.DemoModel;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNull;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeviceApiApplication.class})
public class DemoFeignTest {
    @Autowired
    private DemoClient demoClient;

    private static Demo demo;

    @BeforeClass
    public static void init() {
        demo = new Demo();
        demo.setDemoBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoBool((byte) 1);
        demo.setDemoBoolean((byte) 0);
        demo.setDemoChar("a");
        demo.setDemoDate(LocalDate.now(ZoneId.of("UTC")));
        demo.setDemoDec(new BigDecimal("-0.0009"));
        demo.setDemoDecimal(new BigDecimal("0.1"));
        demo.setDemoDeleted(false);
        demo.setDemoDouble(1.1);
        demo.setDemoEnum("enum1");
        demo.setDemoFixed(new BigDecimal("9999.9"));
        demo.setDemoFloat(1.0f);
        demo.setDemoInt(new BigInteger("99999999"));
        demo.setDemoInteger(new BigInteger("12345678"));
        demo.setDemoJson("{\"name\": \"Jack\"}");
        demo.setDemoLongBlob(new byte[]{1, 2, 3, 4, 5});
        demo.setDemoLongText("Long text");
        demo.setDemoMediumBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoMediumInt(12345);
        demo.setDemoMediumText("Medium Text");
        demo.setDemoName("Name1");
        demo.setDemoNchar("Nchar");
        demo.setDemoNvarchar("NVarchar");
        demo.setDemoNumeric(new BigDecimal("0.0"));
        demo.setDemoReal(9.123);
        demo.setDemoSet("1");
        demo.setDemoSmallInt(123);
        demo.setDemoText("Text");
        demo.setDemoTime(LocalTime.now(ZoneId.of("UTC")));
        demo.setDemoTimestamp(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setDemoTinyBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoTinyInt(1);
        demo.setDemoTinyText("Tiny Text");
        demo.setDemoVarBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoVarchar("Varchar");
        demo.setGmtCreate(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setId(new BigInteger("1"));
    }

    @Test
    public void insertByPk() {
        BigInteger expected = new BigInteger("1");

        DemoModel model = BeanUtil.copy(demo, DemoModel.class);
        BigInteger actual = demoClient.insert(model).getData().getId();
        assertEquals(expected, actual);

        Demo selDemo = demoClient.selectByPk(demo.getId()).getData();
        assertNotNull(selDemo);

        int deleted = demoClient.deleteByPk(expected).getCode();
        assertEquals(0, deleted);

        selDemo = demoClient.selectByPk(expected).getData();
        assertNull(selDemo);
    }

    @Test
    public void updateByDomainPk() {
        BigInteger expected = new BigInteger("1");

        DemoModel model = BeanUtil.copy(demo, DemoModel.class);
        demoClient.insert(model).getData().getId();

        String val = "HELLO_TEST";
        model.setDemoVarchar(val);
        demoClient.updateByPk(model).getData();

        Demo selDemo = demoClient.selectByPk(demo.getId()).getData();
        assertEquals(val, selDemo.getDemoVarchar());

        int deleted = demoClient.deleteByPk(expected).getCode();
        assertEquals(0, deleted);
    }


}
