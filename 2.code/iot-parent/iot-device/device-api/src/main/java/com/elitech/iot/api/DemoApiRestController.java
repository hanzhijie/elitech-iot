package com.elitech.iot.api;

import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.device.client.DemoClient;
import com.elitech.iot.device.domain.Demo;
import com.elitech.iot.device.model.DemoModel;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/12
 */
@RestController
@RequestMapping
public class DemoApiRestController {
    @Autowired
    private DemoClient demoClient;

    @PostMapping("/insert/{pk}")
    public int insert(@PathVariable("pk") String pk) {
        Demo demo = new Demo();
        demo = new Demo();
        demo.setDemoBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoBool((byte) 1);
        demo.setDemoBoolean((byte) 0);
        demo.setDemoChar("a");
        demo.setDemoDate(LocalDate.now(ZoneId.of("UTC")));
        demo.setDemoDec(new BigDecimal("-0.0009"));
        demo.setDemoDecimal(new BigDecimal("0.1"));
        demo.setDemoDeleted(false);
        demo.setDemoDouble(1.1);
        demo.setDemoEnum("enum1");
        demo.setDemoFixed(new BigDecimal("9999.9"));
        demo.setDemoFloat(1.0f);
        demo.setDemoInt(new BigInteger("99999999"));
        demo.setDemoInteger(new BigInteger("12345678"));
        demo.setDemoJson("{\"name\": \"Jack\"}");
        demo.setDemoLongBlob(new byte[]{1, 2, 3, 4, 5});
        demo.setDemoLongText("Long text");
        demo.setDemoMediumBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoMediumInt(12345);
        demo.setDemoMediumText("Medium Text");
        demo.setDemoName("Name" + pk);
        demo.setDemoNchar("Nchar");
        demo.setDemoNvarchar("NVarchar");
        demo.setDemoNumeric(new BigDecimal("0.0"));
        demo.setDemoReal(9.123);
        demo.setDemoSet("1");
        demo.setDemoSmallInt(123);
        demo.setDemoText("Text");
        demo.setDemoTime(LocalTime.now(ZoneId.of("UTC")));
        demo.setDemoTimestamp(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setDemoTinyBlob(new byte[]{1, 2, 3, 4});
        demo.setDemoTinyInt(1);
        demo.setDemoTinyText("Tiny Text");
        demo.setDemoVarBinary(new byte[]{1, 2, 3, 4});
        demo.setDemoVarchar("Varchar");
        demo.setGmtCreate(LocalDateTime.now(ZoneId.of("UTC")));
        demo.setId(new BigInteger(pk));

        DemoModel model = BeanUtil.copy(demo, DemoModel.class);
        ResultMessage<DemoModel> result = demoClient.insert(model);

        return result.getCode();
    }

    @GetMapping("/get/{pk}")
    public Demo get(@PathVariable("pk") String pk) {
        BigInteger id = new BigInteger(pk);
        ResultMessage<Demo> result = demoClient.selectByPk(id);
        return result.getData();
    }
}
