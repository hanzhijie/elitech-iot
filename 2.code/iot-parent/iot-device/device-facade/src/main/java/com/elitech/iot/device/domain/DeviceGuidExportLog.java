/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 设备guid导出记录表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceGuidExportLog implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    * 订单编码.
    */
    private String orderCode;
    /**
    * 组织id.
    */
    private java.math.BigInteger organizationId;
    /**
    * 组织path.
    */
    private String organizationPath;
    /**
    * 用户账号id.
    */
    private java.math.BigInteger accountId;
    /**
    * 导出者用户名.
    */
    private String userName;
    /**
    * 设备类型Id.
    */
    private java.math.BigInteger deviceTypeId;
    /**
    * 导出数量.
    */
    private java.math.BigInteger guidCount;
    /**
    * 使用数量.
    */
    private java.math.BigInteger usedCount;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}