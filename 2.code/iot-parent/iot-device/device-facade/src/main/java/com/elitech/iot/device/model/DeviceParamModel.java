/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "设备参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。")
public class DeviceParamModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 参数id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.id.NotNUll}")
	@ApiModelProperty(value = "参数id")
    private java.math.BigInteger id;

/**
* 设备id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.deviceId.NotNUll}")
	@ApiModelProperty(value = "设备id")
    private java.math.BigInteger deviceId;

/**
* 参数key.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.paramKey.NotNUll}")
	@ApiModelProperty(value = "参数key")
    private java.math.BigInteger paramKey;

/**
* 用于存储、查询.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceParam.paramCode.Length}")
	@ApiModelProperty(value = "用于存储、查询")
    private String paramCode;

/**
* 参数名称.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.paramName.NotNUll}")
		@Length(max = 40, message = "{com.elitech.iot.device.model.DeviceParam.paramName.Length}")
	@ApiModelProperty(value = "参数名称")
    private String paramName;

/**
* 参数类型id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.paramTypeId.NotNUll}")
	@ApiModelProperty(value = "参数类型id")
    private java.math.BigInteger paramTypeId;

/**
* .
*/
		@ApiModelProperty(value = "")
    private long paramTypeCode;

/**
* 参数长度.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.paramLength.NotNUll}")
	@ApiModelProperty(value = "参数长度")
    private java.math.BigInteger paramLength;

/**
* 参数长度单位：1byte字节、2bit位.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.paramUnit.NotNUll}")
	@ApiModelProperty(value = "参数长度单位：1byte字节、2bit位")
    private int paramUnit;

/**
* 上级参数id.
*/
		@ApiModelProperty(value = "上级参数id")
    private java.math.BigInteger parentId;

/**
* 是否有子参数：1有，0无.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParam.hasChild.NotNUll}")
	@ApiModelProperty(value = "是否有子参数：1有，0无")
    private byte hasChild;

/**
* 告警上限.
*/
			@Length(max = 20, message = "{com.elitech.iot.device.model.DeviceParam.alarmUpper.Length}")
	@ApiModelProperty(value = "告警上限")
    private String alarmUpper;

/**
* 告警下限.
*/
			@Length(max = 20, message = "{com.elitech.iot.device.model.DeviceParam.alarmLower.Length}")
	@ApiModelProperty(value = "告警下限")
    private String alarmLower;

/**
* 参数备注.
*/
			@Length(max = 100, message = "{com.elitech.iot.device.model.DeviceParam.paramDescription.Length}")
	@ApiModelProperty(value = "参数备注")
    private String paramDescription;

/**
* 参数分组id.
*/
		@ApiModelProperty(value = "参数分组id")
    private java.math.BigInteger groupId;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时间")
    private java.time.LocalDateTime gmtModified;
}