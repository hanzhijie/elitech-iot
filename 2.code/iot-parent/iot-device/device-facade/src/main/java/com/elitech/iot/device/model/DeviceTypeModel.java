/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备类型表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "设备类型表")
public class DeviceTypeModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 设备类型id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceType.typeId.NotNUll}")
	@ApiModelProperty(value = "设备类型id")
    private java.math.BigInteger typeId;

/**
* 设备类型编码.
*/
		@ApiModelProperty(value = "设备类型编码")
    private java.math.BigInteger typeCode;

/**
* 设备类型名称.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceType.typeName.Length}")
	@ApiModelProperty(value = "设备类型名称")
    private String typeName;

/**
* 所属组织ID.
*/
		@ApiModelProperty(value = "所属组织ID")
    private java.math.BigInteger organizationId;

/**
* 所属组织路径.
*/
			@Length(max = 200, message = "{com.elitech.iot.device.model.DeviceType.organizationPath.Length}")
	@ApiModelProperty(value = "所属组织路径")
    private String organizationPath;

/**
* 据地图模式：0:DATA_MAP;  1:DATA_NOMAP;  2:NODATA_NOMAP.
*/
		@ApiModelProperty(value = "据地图模式：0:DATA_MAP;  1:DATA_NOMAP;  2:NODATA_NOMAP")
    private int mapMode;

/**
* 连接类型:  0长连接;1短连接.
*/
		@ApiModelProperty(value = "连接类型:  0长连接;1短连接")
    private int connectType;

/**
* 是否wifi设备: 0不是、1是.
*/
		@ApiModelProperty(value = "是否wifi设备: 0不是、1是")
    private byte isWifi;

/**
* wifi类型：不同厂商的设备，WiFi设备时，用于APP配置WiFi !.
*/
		@ApiModelProperty(value = "wifi类型：不同厂商的设备，WiFi设备时，用于APP配置WiFi !")
    private int wifiType;

/**
* 温度单位: 1摄氏度；2华氏度.
*/
		@ApiModelProperty(value = "温度单位: 1摄氏度；2华氏度")
    private int temperatureUnit;

/**
* sim卡默认流量：1.30M、2.100M、3.300M.
*/
		@ApiModelProperty(value = "sim卡默认流量：1.30M、2.100M、3.300M")
    private long defaultSimTraffic;

/**
* 默认开启短信推送：0否、1是；默认值0.
*/
		@ApiModelProperty(value = "默认开启短信推送：0否、1是；默认值0")
    private byte autoSms;

/**
* 说明书地址：存储文件的相对路径.
*/
			@Length(max = 200, message = "{com.elitech.iot.device.model.DeviceType.manualUrl.Length}")
	@ApiModelProperty(value = "说明书地址：存储文件的相对路径")
    private String manualUrl;

/**
* 参数修改后是否推送消息：false不推送，true推送.
*/
		@ApiModelProperty(value = "参数修改后是否推送消息：false不推送，true推送")
    private byte pushParamModified;

/**
* 删除状态: true已删除、false未删除.
*/
		@ApiModelProperty(value = "删除状态: true已删除、false未删除")
    private boolean deletedFlag;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时间")
    private java.time.LocalDateTime gmtModified;
}