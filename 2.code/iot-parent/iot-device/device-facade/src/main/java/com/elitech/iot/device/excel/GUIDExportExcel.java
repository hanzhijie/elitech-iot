package com.elitech.iot.device.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName GUIDExportTemplate
 * @Description: 设备GUID导出模板
 * @Author dongqg
 * @Date 2020/3/3
 * @Version V1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@HeadRowHeight(value = 40)
@ContentRowHeight(value = 20)
public class GUIDExportExcel implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号.
     */
    @ExcelProperty(value = "序号")
    @ColumnWidth(value = 20)
    private Integer id;

    /**
     * 订单编号.
     */
    @ExcelProperty(value = "订单编号")
    @ColumnWidth(value = 20)
    private String orderCode;

    /**
     * 设备id
     */
    @ExcelProperty(value = "设备ID")
    @ColumnWidth(value = 40)
    private String deviceGuid;

    /**
     * 设备id1
     */
    @ExcelProperty(value = "设备ID1")
    @ColumnWidth(value = 40)
    private String deviceGuid1;

    /**
     * 设备类型.
     */
    @ExcelProperty(value = "设备类型")
    @ColumnWidth(value = 20)
    private String deviceType;

    /**
     * 是否使用.
     */
    @ExcelProperty(value = "是否使用")
    @ColumnWidth(value = 20)
    private String isUsed;

    /**
     * 二维码.
     */
    @ExcelProperty(value = "二维码")
    @ColumnWidth(value = 20)
    private String QRCode;
}
