/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* 设备类型表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@HeadRowHeight(value = 40)
@ContentRowHeight(20)
public class DeviceTypeExcel implements Serializable {
private static final long serialVersionUID = 1L;

/**
* 设备类型编码.
 *
*/
	@ExcelProperty(value = "设备类型编码", index = 0)
	@ColumnWidth(value = 20)
    private Long typeCode;

/**
* 设备类型名称.
*/
	@ExcelProperty(value = "设备类型名称", index = 1)
	@ColumnWidth(value = 20)
    private String typeName;

/**
* 所属组织ID.
*/
	@ExcelProperty(value = "所属组织ID", index = 2)
	@ColumnWidth(value = 20)
    private Long organizationId;

/**
* 所属组织路径.
*/
	@ExcelProperty(value = "所属组织路径", index = 3)
	@ColumnWidth(value = 20)
    private String organizationPath;

/**
* 据地图模式：0:DATA_MAP;  1:DATA_NOMAP;  2:NODATA_NOMAP.
*/
	@ExcelProperty(value = "据地图模式", index = 4)
	@ColumnWidth(value = 20)
    private Byte mapMode;

/**
* 连接类型:  0长连接;1短连接.
*/
	@ExcelProperty(value = "连接类型", index = 5)
	@ColumnWidth(value = 20)
    private Byte connectType;

/**
* 是否wifi设备: 0不是、1是.
*/
	@ExcelProperty(value = "是否wifi设备", index = 6)
	@ColumnWidth(value = 20)
    private Byte isWifi;

/**
* wifi类型：不同厂商的设备，WiFi设备时，用于APP配置WiFi !.
*/
	@ExcelProperty(value = "wifi类型", index = 7)
	@ColumnWidth(value = 20)
    private Byte wifiType;

/**
* 温度单位: 1摄氏度；2华氏度.
*/
	@ExcelProperty(value = "温度单位", index = 8)
	@ColumnWidth(value = 20)
    private Byte temperatureUnit;

/**
* sim卡默认流量：1.30M、2.100M、3.300M.
*/
	@ExcelProperty(value = "sim卡默认流量", index = 9)
	@ColumnWidth(value = 20)
    private Integer defaultSimTraffic;

/**
* 默认开启短信推送：0否、1是；默认值0.
*/
	@ExcelProperty(value = "默认开启短信推送", index = 10)
	@ColumnWidth(value = 20)
    private Byte autoSms;

/**
* 说明书地址：存储文件的相对路径.
*/
	@ExcelProperty(value = "说明书地址", index = 11)
	@ColumnWidth(value = 20)
    private String manualUrl;

/**
* 参数修改后是否推送消息：false不推送，true推送.
*/
	@ExcelProperty(value = "参数修改后是否推送消息", index = 12)
	@ColumnWidth(value = 20)
    private Byte pushParamModified;



/**
* 创建时间.
*/
	@ExcelProperty(converter = CustomDateConverter.class,value = "创建时间", index = 13)
	@ColumnWidth(value = 20)
//	@DateTimeFormat("yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat("yyyy年MM月dd日 HH时mm分ss秒")
    private String gmtCreate;

/**
* 最后更新时间.
*/
	@ExcelProperty(converter = CustomDateConverter.class,value = "更新时间",index=14)
	@ColumnWidth(value = 20)
    private String gmtModified;

}