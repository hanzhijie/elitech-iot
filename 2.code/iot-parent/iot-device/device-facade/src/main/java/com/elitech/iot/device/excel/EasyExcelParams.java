package com.elitech.iot.device.excel;

import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Data
public class EasyExcelParams {
    /**
     * excel文件名（不带拓展名)
     */
    private String excelNameWithoutExt;
    /**
     * sheet名称
     */
    private String sheetName ="sheet1";
    /**
     * 是否需要表头
     */
    private boolean needHead = true;
    /**
     * 数据
     */
    private List<?> data;

    /**
     * 数据模型类型
     */
    private Class<?> dataModelClazz;

    /**
     * 响应
     */
    private HttpServletResponse response;


    public EasyExcelParams(String excelNameWithoutExt, String sheetName, List<?> data, Class<? extends BaseRowModel> dataModelClazz, HttpServletResponse response ){
        this.excelNameWithoutExt = excelNameWithoutExt;
        this.sheetName = sheetName;
        this.data = data;
        this.dataModelClazz = dataModelClazz;
        this.response = response;
    }

    public EasyExcelParams(String excelNameWithoutExt, List<?> data, Class<?> dataModelClazz, HttpServletResponse response ){
        this.excelNameWithoutExt = excelNameWithoutExt;
        this.data = data;
        this.dataModelClazz = dataModelClazz;
        this.response = response;
    }

}
