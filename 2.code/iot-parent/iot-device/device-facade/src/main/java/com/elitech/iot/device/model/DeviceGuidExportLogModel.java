/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备guid导出记录表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "设备guid导出记录表")
public class DeviceGuidExportLogModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceGuidExportLog.id.NotNUll}")
	@ApiModelProperty(value = "id")
    private java.math.BigInteger id;

/**
* 订单编码.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceGuidExportLog.orderCode.Length}")
	@ApiModelProperty(value = "订单编码")
    private String orderCode;

/**
* 组织id.
*/
		@ApiModelProperty(value = "组织id")
    private java.math.BigInteger organizationId;

/**
* 组织path.
*/
			@Length(max = 200, message = "{com.elitech.iot.device.model.DeviceGuidExportLog.organizationPath.Length}")
	@ApiModelProperty(value = "组织path")
    private String organizationPath;

/**
* 用户账号id.
*/
		@ApiModelProperty(value = "用户账号id")
    private java.math.BigInteger accountId;

/**
* 导出者用户名.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceGuidExportLog.userName.Length}")
	@ApiModelProperty(value = "导出者用户名")
    private String userName;

/**
* 设备类型Id.
*/
		@ApiModelProperty(value = "设备类型Id")
    private java.math.BigInteger deviceTypeId;

/**
* 导出数量.
*/
		@ApiModelProperty(value = "导出数量")
    private java.math.BigInteger guidCount;

/**
* 使用数量.
*/
		@ApiModelProperty(value = "使用数量")
    private java.math.BigInteger usedCount;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后修改时间")
    private java.time.LocalDateTime gmtModified;
}