/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.excel;

import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ContentRowHeight(20)
@HeadRowHeight(value = 40)
public class DeviceTypeParamExcel implements Serializable {
private static final long serialVersionUID = 1L;



/**
* 设备类型id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceTypeParam.deviceTypeId.NotNUll}")
	@ApiModelProperty(value = "设备类型id")
    private java.math.BigInteger deviceTypeId;

/**
* 参数key.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceTypeParam.paramKey.NotNUll}")
	@ApiModelProperty(value = "参数key")
    private java.math.BigInteger paramKey;

/**
* 用于存储、查询.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceTypeParam.paramCode.Length}")
	@ApiModelProperty(value = "用于存储、查询")
    private String paramCode;

/**
* 参数名称.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceTypeParam.paramName.NotNUll}")
		@Length(max = 40, message = "{com.elitech.iot.device.model.DeviceTypeParam.paramName.Length}")
	@ApiModelProperty(value = "参数名称")
    private String paramName;

/**
* 参数长度.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceTypeParam.paramLength.NotNUll}")
	@ApiModelProperty(value = "参数长度")
    private java.math.BigInteger paramLength;

/**
* 参数长度单位：1byte字节、2bit位.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceTypeParam.paramUnit.NotNUll}")
	@ApiModelProperty(value = "参数长度单位：1byte字节、2bit位")
    private int paramUnit;

/**
* 上级参数id.
*/
		@ApiModelProperty(value = "上级参数id")
    private java.math.BigInteger parentId;

/**
* 是否有子参数：1有，0无.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceTypeParam.hasChild.NotNUll}")
	@ApiModelProperty(value = "是否有子参数：1有，0无")
    private byte hasChild;

/**
* 告警上限.
*/
			@Length(max = 20, message = "{com.elitech.iot.device.model.DeviceTypeParam.alarmUpper.Length}")
	@ApiModelProperty(value = "告警上限")
    private String alarmUpper;

/**
* 告警下限.
*/
			@Length(max = 20, message = "{com.elitech.iot.device.model.DeviceTypeParam.alarnLower.Length}")
	@ApiModelProperty(value = "告警下限")
    private String alarnLower;

/**
* 量程最大值.
*/
			@Length(max = 20, message = "{com.elitech.iot.device.model.DeviceTypeParam.paramMax.Length}")
	@ApiModelProperty(value = "量程最大值")
    private String paramMax;

/**
* 量程最小值.
*/
			@Length(max = 20, message = "{com.elitech.iot.device.model.DeviceTypeParam.paramMin.Length}")
	@ApiModelProperty(value = "量程最小值")
    private String paramMin;

/**
* 默认值.
*/
			@Length(max = 20, message = "{com.elitech.iot.device.model.DeviceTypeParam.paramDefault.Length}")
	@ApiModelProperty(value = "默认值")
    private String paramDefault;

/**
* 参数备注.
*/
			@Length(max = 100, message = "{com.elitech.iot.device.model.DeviceTypeParam.paramDescription.Length}")
	@ApiModelProperty(value = "参数备注")
    private String paramDescription;

/**
* 显示标识：true显示、false不显示.
*/
		@ApiModelProperty(value = "显示标识：true显示、false不显示")
    private boolean displayFlag;

/**
* 设置标识:true可以设置、false不可以设置.
*/
		@ApiModelProperty(value = "设置标识:true可以设置、false不可以设置")
    private boolean setFlag;

/**
* 是系统参数:true是、false否； 系统参数用户可能不需要设置，用户不理解参数意义.
*/
		@ApiModelProperty(value = "是系统参数:true是、false否； 系统参数用户可能不需要设置，用户不理解参数意义")
    private boolean systemFlag;

/**
* 参数分组.
*/
		@ApiModelProperty(value = "参数分组")
    private java.math.BigInteger groupId;

/**
* 排序.
*/
		@ApiModelProperty(value = "排序")
    private java.math.BigInteger sort;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时间")
    private java.time.LocalDateTime gmtModified;
}