/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:29
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "设备表")
public class DeviceModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 设备id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.Device.id.NotNUll}")
	@ApiModelProperty(value = "设备id")
    private java.math.BigInteger id;

/**
* 设备名称.
*/
			@NotNull(message = "{com.elitech.iot.device.model.Device.deviceName.NotNUll}")
		@Length(max = 50, message = "{com.elitech.iot.device.model.Device.deviceName.Length}")
	@ApiModelProperty(value = "设备名称")
    private String deviceName;

/**
* 设备guid.
*/
			@NotNull(message = "{com.elitech.iot.device.model.Device.deviceGuid.NotNUll}")
		@Length(max = 20, message = "{com.elitech.iot.device.model.Device.deviceGuid.Length}")
	@ApiModelProperty(value = "设备guid")
    private String deviceGuid;

/**
* 设备subuid.
*/
		@ApiModelProperty(value = "设备subuid")
    private long subUid;

/**
* 设备hard id.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.Device.hardId.Length}")
	@ApiModelProperty(value = "设备hard id")
    private String hardId;

/**
* sim iccid.
*/
			@Length(max = 30, message = "{com.elitech.iot.device.model.Device.simIccid.Length}")
	@ApiModelProperty(value = "sim iccid")
    private String simIccid;

/**
* 设备类型id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.Device.typeId.NotNUll}")
	@ApiModelProperty(value = "设备类型id")
    private java.math.BigInteger typeId;

/**
* 制造商组织结构ID.
*/
		@ApiModelProperty(value = "制造商组织结构ID")
    private java.math.BigInteger manufacturerOrganizationId;

/**
* 制造商组织path.
*/
			@Length(max = 200, message = "{com.elitech.iot.device.model.Device.manufacturerOrganizationPath.Length}")
	@ApiModelProperty(value = "制造商组织path")
    private String manufacturerOrganizationPath;

/**
* 设备使用状态:true启用、false停用.
*/
			@NotNull(message = "{com.elitech.iot.device.model.Device.useFlag.NotNUll}")
	@ApiModelProperty(value = "设备使用状态:true启用、false停用")
    private boolean useFlag;

/**
* 设备在线状态:true在线、false离线.
*/
		@ApiModelProperty(value = "设备在线状态:true在线、false离线")
    private boolean onlineFlag;

/**
* 删除状态:true删除、false未删除.
*/
		@ApiModelProperty(value = "删除状态:true删除、false未删除")
    private boolean deleteFlag;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
			@NotNull(message = "{com.elitech.iot.device.model.Device.gmtCreate.NotNUll}")
	@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后修改时间")
    private java.time.LocalDateTime gmtModified;

/**
* 通讯会话id、socket通讯会话id.
*/
		@ApiModelProperty(value = "通讯会话id、socket通讯会话id")
    private java.math.BigInteger sessionId;

/**
* 上次通讯时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "上次通讯时间")
    private java.time.LocalDateTime lastSessionTime;
}