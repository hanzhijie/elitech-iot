/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 设备类型表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceType implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 设备类型id.
    */
    private java.math.BigInteger typeId;
    /**
    * 设备类型编码.
    */
    private Integer typeCode;
    /**
    * 设备类型名称.
    */
    private String typeName;
    /**
    * 所属组织ID.
    */
    private java.math.BigInteger organizationId;
    /**
    * 所属组织路径.
    */
    private String organizationPath;
    /**
    * 据地图模式：0:DATA_MAP;  1:DATA_NOMAP;  2:NODATA_NOMAP.
    */
    private Byte mapMode;
    /**
    * 连接类型:  0长连接;1短连接.
    */
    private Byte connectType;
    /**
    * 是否wifi设备: 0不是、1是.
    */
    private Byte isWifi;
    /**
    * wifi类型：不同厂商的设备，WiFi设备时，用于APP配置WiFi !.
    */
    private Byte wifiType;
    /**
    * 温度单位: 1摄氏度；2华氏度.
    */
    private Byte temperatureUnit;
    /**
    * sim卡默认流量：1.30M、2.100M、3.300M.
    */
    private Integer defaultSimTraffic;
    /**
    * 默认开启短信推送：0否、1是；默认值0.
    */
    private Byte autoSms;
    /**
    * 说明书地址：存储文件的相对路径.
    */
    private String manualUrl;
    /**
    * 参数修改后是否推送消息：false不推送，true推送.
    */
    private Byte pushParamModified;
    /**
    * 删除状态: true已删除、false未删除.
    */
    private Boolean deletedFlag;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后更新时间.
    */
    private java.time.LocalDateTime gmtModified;
}