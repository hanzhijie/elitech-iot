/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 设备类型默认参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceTypeParam implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 参数id.
    */
    private java.math.BigInteger id;
    /**
    * 设备类型id.
    */
    private java.math.BigInteger deviceTypeId;
    /**
    * 参数key.
    */
    private java.math.BigInteger paramKey;
    /**
    * 用于存储、查询.
    */
    private String paramCode;
    /**
    * 参数名称.
    */
    private String paramName;
    /**
    * 参数长度.
    */
    private java.math.BigInteger paramLength;
    /**
    * 参数长度单位：1byte字节、2bit位.
    */
    private Integer paramUnit;
    /**
    * 上级参数id.
    */
    private java.math.BigInteger parentId;
    /**
    * 是否有子参数：1有，0无.
    */
    private Byte hasChild;
    /**
    * 告警上限.
    */
    private String alarmUpper;
    /**
    * 告警下限.
    */
    private String alarnLower;
    /**
    * 量程最大值.
    */
    private String paramMax;
    /**
    * 量程最小值.
    */
    private String paramMin;
    /**
    * 默认值.
    */
    private String paramDefault;
    /**
    * 参数备注.
    */
    private String paramDescription;
    /**
    * 显示标识：true显示、false不显示.
    */
    private Boolean displayFlag;
    /**
    * 设置标识:true可以设置、false不可以设置.
    */
    private Boolean setFlag;
    /**
    * 是系统参数:true是、false否； 系统参数用户可能不需要设置，用户不理解参数意义.
    */
    private Boolean systemFlag;
    /**
    * 参数分组.
    */
    private java.math.BigInteger groupId;
    /**
    * 排序.
    */
    private java.math.BigInteger sort;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后更新时间.
    */
    private java.time.LocalDateTime gmtModified;
}