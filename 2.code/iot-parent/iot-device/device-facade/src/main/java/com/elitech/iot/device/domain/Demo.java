/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-20 13:07:49
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 测试表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class Demo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    * 名称.
    */
    private String demoName;
    /**
    * .
    */
    private byte[] demoBinary;
    /**
    * 已删除：1删除、0未删除.
    */
    private boolean demoDeleted;
    /**
    * demo blob.
    */
    private byte[] demoBlob;
    /**
    * demo bool.
    */
    private byte demoBool;
    /**
    * demo boolean.
    */
    private byte demoBoolean;
    /**
    * .
    */
    private String demoChar;
    /**
    * demo date.
    */
    private java.time.LocalDate demoDate;
    /**
    * demo dec.
    */
    private java.math.BigDecimal demoDec;
    /**
    * demo decimal.
    */
    private java.math.BigDecimal demoDecimal;
    /**
    * demo double.
    */
    private double demoDouble;
    /**
    * demo enum.
    */
    private String demoEnum;
    /**
    * demo fixed.
    */
    private java.math.BigDecimal demoFixed;
    /**
    * demo float.
    */
    private float demoFloat;
    /**
    * demo geometry.
    */
    private String demoGeometry;
    /**
    * demo geometry collection.
    */
    private String demoGeometryCollection;
    /**
    * demo int.
    */
    private java.math.BigInteger demoInt;
    /**
    * demo integer.
    */
    private java.math.BigInteger demoInteger;
    /**
    * demo json.
    */
    private String demoJson;
    /**
    * .
    */
    private String demoLineString;
    /**
    * demo lonng blob.
    */
    private byte[] demoLongBlob;
    /**
    * demo long text.
    */
    private String demoLongText;
    /**
    * .
    */
    private byte[] demoMediumBlob;
    /**
    * demo medium int.
    */
    private long demoMediumInt;
    /**
    * .
    */
    private String demoMediumText;
    /**
    * demo multi line string.
    */
    private String demoMultiLineString;
    /**
    * demo multi  point.
    */
    private String demoMultiPoint;
    /**
    * demo multi polygon.
    */
    private String demoMultiPolygon;
    /**
    * demo nchar.
    */
    private String demoNchar;
    /**
    * demo numeric.
    */
    private java.math.BigDecimal demoNumeric;
    /**
    * demo nvarchar.
    */
    private String demoNvarchar;
    /**
    * demo point.
    */
    private String demoPoint;
    /**
    * demo polygon.
    */
    private String demoPolygon;
    /**
    * demo real.
    */
    private double demoReal;
    /**
    * demo set.
    */
    private String demoSet;
    /**
    * demo small int.
    */
    private long demoSmallInt;
    /**
    * demo text.
    */
    private String demoText;
    /**
    * demo time.
    */
    private java.time.LocalTime demoTime;
    /**
    * demo timestamp.
    */
    private java.time.LocalDateTime demoTimestamp;
    /**
    * demo tiny blob.
    */
    private byte[] demoTinyBlob;
    /**
    * demo tiny int.
    */
    private int demoTinyInt;
    /**
    * demo tiny text.
    */
    private String demoTinyText;
    /**
    * demo var binary.
    */
    private byte[] demoVarBinary;
    /**
    * demo varchar.
    */
    private String demoVarchar;
    /**
    * .
    */
    private java.time.LocalDate demoYear;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
}