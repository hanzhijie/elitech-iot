/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-13 16:24:55
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 设备参数分组表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceParamGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 参数分组id.
    */
    private java.math.BigInteger id;
    /**
    * 分组名称.
    */
    private String groupName;
    /**
    * 设备类型id.
    */
    private java.math.BigInteger typeId;
    /**
    * 参数id列表，逗号分隔.
    */
    private String paramIds;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
}