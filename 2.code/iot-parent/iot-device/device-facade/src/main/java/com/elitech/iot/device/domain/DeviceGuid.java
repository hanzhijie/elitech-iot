/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 设备guid表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceGuid implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    * 订单编号.
    */
    private String orderCode;
    /**
    * hard id.
    */
    private String hardId;
    /**
    * 设备guid.
    */
    private String deiceGuid;
    /**
    * 设备厂测模式：1:UNTEST;2:TESTING;3:TESTED;4:USED;5:RETURNED.
    */
    private int testMode;
    /**
    * 设备类型id.
    */
    private java.math.BigInteger deviceTypeId;
    /**
    * 设备类型名称.
    */
    private String deviceTypeName;
    /**
    * 组织id.
    */
    private java.math.BigInteger organizationId;
    /**
    * 组织路径.
    */
    private String organizationPath;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}