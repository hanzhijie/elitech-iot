/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 项目设备设备关联表: 行业应用如果没有项目的概念，就建一个默认的项目；去掉组织机构与设备的关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "项目设备设备关联表: 行业应用如果没有项目的概念，就建一个默认的项目；去掉组织机构与设备的关联表")
public class DeviceProjectDeviceRefModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 主键id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceProjectDeviceRef.id.NotNUll}")
	@ApiModelProperty(value = "主键id")
    private java.math.BigInteger id;

/**
* 项目id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceProjectDeviceRef.projectId.NotNUll}")
	@ApiModelProperty(value = "项目id")
    private java.math.BigInteger projectId;

/**
* 项目路径.
*/
			@Length(max = 200, message = "{com.elitech.iot.device.model.DeviceProjectDeviceRef.projectPath.Length}")
	@ApiModelProperty(value = "项目路径")
    private String projectPath;

/**
* 设备id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceProjectDeviceRef.deviceId.NotNUll}")
	@ApiModelProperty(value = "设备id")
    private java.math.BigInteger deviceId;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后修改时间")
    private java.time.LocalDateTime gmtModified;
}