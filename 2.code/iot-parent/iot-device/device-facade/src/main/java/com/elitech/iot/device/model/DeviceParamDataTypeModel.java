/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 参数数据类型.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "参数数据类型")
public class DeviceParamDataTypeModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParamDataType.id.NotNUll}")
	@ApiModelProperty(value = "id")
    private java.math.BigInteger id;

/**
* 参数数据类型编码.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParamDataType.dataTypeCode.NotNUll}")
	@ApiModelProperty(value = "参数数据类型编码")
    private int dataTypeCode;

/**
* 参数数据类型名称.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParamDataType.dataTypeName.NotNUll}")
		@Length(max = 10, message = "{com.elitech.iot.device.model.DeviceParamDataType.dataTypeName.Length}")
	@ApiModelProperty(value = "参数数据类型名称")
    private String dataTypeName;

/**
* 参数值长度：-1代表变长.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParamDataType.dataLength.NotNUll}")
	@ApiModelProperty(value = "参数值长度：-1代表变长")
    private long dataLength;

/**
* 所属企业id.
*/
		@ApiModelProperty(value = "所属企业id")
    private java.math.BigInteger organizationId;

/**
* 所属企业path.
*/
			@Length(max = 200, message = "{com.elitech.iot.device.model.DeviceParamDataType.organizationPath.Length}")
	@ApiModelProperty(value = "所属企业path")
    private String organizationPath;

/**
* 删状态：true已删除、false未删除.
*/
		@ApiModelProperty(value = "删状态：true已删除、false未删除")
    private boolean deletedFlag;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后修改时间")
    private java.time.LocalDateTime gmtModified;
}