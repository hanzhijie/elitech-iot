/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:29
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 设备表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class Device implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 设备id.
    */
    private java.math.BigInteger id;
    /**
    * 设备名称.
    */
    private String deviceName;
    /**
    * 设备guid.
    */
    private String deviceGuid;
    /**
    * 设备subuid.
    */
    private long subUid;
    /**
    * 设备hard id.
    */
    private String hardId;
    /**
    * sim iccid.
    */
    private String simIccid;
    /**
    * 设备类型id.
    */
    private java.math.BigInteger typeId;
    /**
    * 制造商组织结构ID.
    */
    private java.math.BigInteger manufacturerOrganizationId;
    /**
    * 制造商组织path.
    */
    private String manufacturerOrganizationPath;
    /**
    * 设备使用状态:true启用、false停用.
    */
    private boolean useFlag;
    /**
    * 设备在线状态:true在线、false离线.
    */
    private boolean onlineFlag;
    /**
    * 删除状态:true删除、false未删除.
    */
    private boolean deleteFlag;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
    /**
    * 通讯会话id、socket通讯会话id.
    */
    private java.math.BigInteger sessionId;
    /**
    * 上次通讯时间.
    */
    private java.time.LocalDateTime lastSessionTime;
}