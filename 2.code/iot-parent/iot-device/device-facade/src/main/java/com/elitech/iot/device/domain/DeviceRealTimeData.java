/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 设备实时数据表，存储每个设备最新的一条监测数据.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceRealTimeData implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    * 设备id.
    */
    private java.math.BigInteger deviceId;
    /**
    * 参数id.
    */
    private java.math.BigInteger paramId;
    /**
    * 参数编码.
    */
    private String paramCode;
    /**
    * 参数名称.
    */
    private String paramName;
    /**
    * 参数值.
    */
    private java.math.BigDecimal paramValue;
    /**
    * 监测时间.
    */
    private java.time.LocalDateTime monitorTime;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后更新时间.
    */
    private java.time.LocalDateTime gmtModified;
}