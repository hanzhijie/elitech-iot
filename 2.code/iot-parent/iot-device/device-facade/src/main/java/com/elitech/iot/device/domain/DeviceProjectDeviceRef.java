/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 项目设备设备关联表: 行业应用如果没有项目的概念，就建一个默认的项目；去掉组织机构与设备的关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceProjectDeviceRef implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 主键id.
    */
    private java.math.BigInteger id;
    /**
    * 项目id.
    */
    private java.math.BigInteger projectId;
    /**
    * 项目路径.
    */
    private String projectPath;
    /**
    * 设备id.
    */
    private java.math.BigInteger deviceId;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}