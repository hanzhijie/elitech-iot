/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:48:30
 */

package com.elitech.iot.device.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 设备参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Getter
@Setter
@ApiModel(description = "设备参数，每种设备有多个参数；设备的参数在后台配置，解析服务器根据设备的参数配置信息进行解析，可以配置到位。")
public class DeviceParamGroupVOModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 参数分组id.
     */
    @NotNull(message = "{com.elitech.iot.device.model.DeviceParamGroup.id.NotNUll}")
    @ApiModelProperty(value = "参数分组id")
    private java.math.BigInteger id;
    /**
     * 分组名称.
     */
    @Length(max = 50, message = "{com.elitech.iot.device.model.DeviceParamGroup.groupName.Length}")
    @ApiModelProperty(value = "分组名称")
    private String groupName;
    /**
     * 设备类型id.
     */
    @ApiModelProperty(value = "设备类型id")
    private java.math.BigInteger typeId;
    /**
     * 参数id列表，逗号分隔.
     */
    @ApiModelProperty(value = "参数列表")
    private List<DeviceParamModel> paramList;
}