/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备guid表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "设备guid表")
public class DeviceGuidModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceGuid.id.NotNUll}")
	@ApiModelProperty(value = "id")
    private java.math.BigInteger id;

/**
* 订单编号.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceGuid.orderCode.Length}")
	@ApiModelProperty(value = "订单编号")
    private String orderCode;

/**
* hard id.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceGuid.hardId.Length}")
	@ApiModelProperty(value = "hard id")
    private String hardId;

/**
* 设备guid.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceGuid.deiceGuid.NotNUll}")
		@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceGuid.deiceGuid.Length}")
	@ApiModelProperty(value = "设备guid")
    private String deiceGuid;

/**
* 设备厂测模式：1:UNTEST;2:TESTING;3:TESTED;4:USED;5:RETURNED.
*/
		@ApiModelProperty(value = "设备厂测模式：1:UNTEST;2:TESTING;3:TESTED;4:USED;5:RETURNED")
    private int testMode;

/**
* 设备类型id.
*/
		@ApiModelProperty(value = "设备类型id")
    private java.math.BigInteger deviceTypeId;

/**
* 设备类型名称.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceGuid.deviceTypeName.Length}")
	@ApiModelProperty(value = "设备类型名称")
    private String deviceTypeName;

/**
* 组织id.
*/
		@ApiModelProperty(value = "组织id")
    private java.math.BigInteger organizationId;

/**
* 组织路径.
*/
			@Length(max = 200, message = "{com.elitech.iot.device.model.DeviceGuid.organizationPath.Length}")
	@ApiModelProperty(value = "组织路径")
    private String organizationPath;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后修改时间")
    private java.time.LocalDateTime gmtModified;
}