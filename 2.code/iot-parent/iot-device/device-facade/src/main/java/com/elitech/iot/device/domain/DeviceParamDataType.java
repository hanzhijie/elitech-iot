/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 参数数据类型.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class DeviceParamDataType implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    * 参数数据类型编码.
    */
    private int dataTypeCode;
    /**
    * 参数数据类型名称.
    */
    private String dataTypeName;
    /**
    * 参数值长度：-1代表变长.
    */
    private long dataLength;
    /**
    * 所属企业id.
    */
    private java.math.BigInteger organizationId;
    /**
    * 所属企业path.
    */
    private String organizationPath;
    /**
    * 删状态：true已删除、false未删除.
    */
    private boolean deletedFlag;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}