/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-13 16:24:55
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备参数分组表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "设备参数分组表")
public class DeviceParamGroupModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 参数分组id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceParamGroup.id.NotNUll}")
	@ApiModelProperty(value = "参数分组id")
    private java.math.BigInteger id;

/**
* 分组名称.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceParamGroup.groupName.Length}")
	@ApiModelProperty(value = "分组名称")
    private String groupName;

/**
* 设备类型id.
*/
		@ApiModelProperty(value = "设备类型id")
    private java.math.BigInteger typeId;

/**
* 参数id列表，逗号分隔.
*/
			@Length(max = 2000, message = "{com.elitech.iot.device.model.DeviceParamGroup.paramIds.Length}")
	@ApiModelProperty(value = "参数id列表，逗号分隔")
    private String paramIds;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;
}