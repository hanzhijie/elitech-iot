/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-21 17:46:48
 */

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 测试表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Getter
@Setter
@ApiModel(description = "测试表")
public class DemoModel implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * id.
     */
    @NotNull(message = "{com.elitech.iot.device.model.Demo.id.NotNUll}")
    @ApiModelProperty(value = "id", example = "1")
    private java.math.BigInteger id;

    /**
     * 名称.
     */
    @NotNull(message = "{com.elitech.iot.device.model.Demo.demoName.NotNUll}")
    @Length(max = 50, message = "{com.elitech.iot.device.model.Demo.demoName.Length}")
    @ApiModelProperty(value = "名称", example = "name")
    private String demoName;

    /**
     * .
     */
    @ApiModelProperty(value = "", example = "[1,2,3]")
    private byte[] demoBinary;

    /**
     * 已删除：1删除、0未删除.
     */
    @ApiModelProperty(value = "已删除：1删除、0未删除", example = "false")
    private boolean demoDeleted;

    /**
     * demo blob.
     */
    @ApiModelProperty(value = "demo blob")
    private byte[] demoBlob;

    /**
     * demo bool.
     */
    @ApiModelProperty(value = "demo bool")
    private byte demoBool;

    /**
     * demo boolean.
     */
    @ApiModelProperty(value = "demo boolean")
    private byte demoBoolean;

    /**
     * .
     */
    @Length(max = 20, message = "{com.elitech.iot.device.model.Demo.demoChar.Length}")
    @ApiModelProperty(value = "", example = "A")
    private String demoChar;

    /**
     * demo date.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
    @ApiModelProperty(value = "demo date", example = "2019-12-21")
    private java.time.LocalDate demoDate;

    /**
     * demo dec.
     */
    @ApiModelProperty(value = "demo dec", example = "0.0")
    private java.math.BigDecimal demoDec;

    /**
     * demo decimal.
     */
    @ApiModelProperty(value = "demo decimal", example = "1.2")
    private java.math.BigDecimal demoDecimal;

    /**
     * demo double.
     */
    @ApiModelProperty(value = "demo double")
    private double demoDouble;

    /**
     * demo enum.
     */
    @Length(max = 5, message = "{com.elitech.iot.device.model.Demo.demoEnum.Length}")
    @ApiModelProperty(value = "demo enum")
    private String demoEnum;

    /**
     * demo fixed.
     */
    @ApiModelProperty(value = "demo fixed")
    private java.math.BigDecimal demoFixed;

    /**
     * demo float.
     */
    @ApiModelProperty(value = "demo float")
    private float demoFloat;

    /**
     * demo geometry.
     */
    @Length(max = 255, message = "{com.elitech.iot.device.model.Demo.demoGeometry.Length}")
    @ApiModelProperty(value = "demo geometry")
    private String demoGeometry;

    /**
     * demo geometry collection.
     */
    @ApiModelProperty(value = "demo geometry collection")
    private String demoGeometryCollection;

    /**
     * demo int.
     */
    @ApiModelProperty(value = "demo int")
    private java.math.BigInteger demoInt;

    /**
     * demo integer.
     */
    @ApiModelProperty(value = "demo integer")
    private java.math.BigInteger demoInteger;

    /**
     * demo json.
     */
    @ApiModelProperty(value = "demo json")
    private String demoJson;

    /**
     * .
     */
    @ApiModelProperty(value = "")
    private String demoLineString;

    /**
     * demo lonng blob.
     */
    @ApiModelProperty(value = "demo lonng blob")
    private byte[] demoLongBlob;

    /**
     * demo long text.
     */
    @Length(max = Integer.MAX_VALUE, message = "{com.elitech.iot.device.model.Demo.demoLongText.Length}")
    @ApiModelProperty(value = "demo long text")
    private String demoLongText;

    /**
     * .
     */
    @ApiModelProperty(value = "")
    private byte[] demoMediumBlob;

    /**
     * demo medium int.
     */
    @ApiModelProperty(value = "demo medium int")
    private long demoMediumInt;

    /**
     * .
     */
    @Length(max = 16777215, message = "{com.elitech.iot.device.model.Demo.demoMediumText.Length}")
    @ApiModelProperty(value = "")
    private String demoMediumText;

    /**
     * demo multi line string.
     */
    @ApiModelProperty(value = "demo multi line string")
    private String demoMultiLineString;

    /**
     * demo multi  point.
     */
    @ApiModelProperty(value = "demo multi  point")
    private String demoMultiPoint;

    /**
     * demo multi polygon.
     */
    @ApiModelProperty(value = "demo multi polygon")
    private String demoMultiPolygon;

    /**
     * demo nchar.
     */
    @Length(max = 20, message = "{com.elitech.iot.device.model.Demo.demoNchar.Length}")
    @ApiModelProperty(value = "demo nchar")
    private String demoNchar;

    /**
     * demo numeric.
     */
    @ApiModelProperty(value = "demo numeric")
    private java.math.BigDecimal demoNumeric;

    /**
     * demo nvarchar.
     */
    @Length(max = 255, message = "{com.elitech.iot.device.model.Demo.demoNvarchar.Length}")
    @ApiModelProperty(value = "demo nvarchar")
    private String demoNvarchar;

    /**
     * demo point.
     */
    @ApiModelProperty(value = "demo point")
    private String demoPoint;

    /**
     * demo polygon.
     */
    @ApiModelProperty(value = "demo polygon")
    private String demoPolygon;

    /**
     * demo real.
     */
    @ApiModelProperty(value = "demo real")
    private double demoReal;

    /**
     * demo set.
     */
    @Length(max = 5, message = "{com.elitech.iot.device.model.Demo.demoSet.Length}")
    @ApiModelProperty(value = "demo set")
    private String demoSet;

    /**
     * demo small int.
     */
    @ApiModelProperty(value = "demo small int")
    private long demoSmallInt;

    /**
     * demo text.
     */
    @Length(max = 65535, message = "{com.elitech.iot.device.model.Demo.demoText.Length}")
    @ApiModelProperty(value = "demo text")
    private String demoText;

    /**
     * demo time.
     */
    @DateTimeFormat(pattern = "HH:mm:ss")
    @JsonFormat(pattern = "HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "demo time")
    private java.time.LocalTime demoTime;

    /**
     * demo timestamp.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "demo timestamp")
    private java.time.LocalDateTime demoTimestamp;

    /**
     * demo tiny blob.
     */
    @ApiModelProperty(value = "demo tiny blob")
    private byte[] demoTinyBlob;

    /**
     * demo tiny int.
     */
    @ApiModelProperty(value = "demo tiny int")
    private int demoTinyInt;

    /**
     * demo tiny text.
     */
    @Length(max = 255, message = "{com.elitech.iot.device.model.Demo.demoTinyText.Length}")
    @ApiModelProperty(value = "demo tiny text")
    private String demoTinyText;

    /**
     * demo var binary.
     */
    @ApiModelProperty(value = "demo var binary")
    private byte[] demoVarBinary;

    /**
     * demo varchar.
     */
    @Length(max = 255, message = "{com.elitech.iot.device.model.Demo.demoVarchar.Length}")
    @ApiModelProperty(value = "demo varchar")
    private String demoVarchar;

    /**
     * .
     */
    @ApiModelProperty(value = "")
    private java.time.LocalDate demoYear;

    /**
     * 创建时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;
}