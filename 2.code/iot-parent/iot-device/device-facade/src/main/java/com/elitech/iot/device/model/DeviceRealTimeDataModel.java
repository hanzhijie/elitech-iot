/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:48:30
*/

package com.elitech.iot.device.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 设备实时数据表，存储每个设备最新的一条监测数据.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "设备实时数据表，存储每个设备最新的一条监测数据")
public class DeviceRealTimeDataModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceRealTimeData.id.NotNUll}")
	@ApiModelProperty(value = "id")
    private java.math.BigInteger id;

/**
* 设备id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceRealTimeData.deviceId.NotNUll}")
	@ApiModelProperty(value = "设备id")
    private java.math.BigInteger deviceId;

/**
* 参数id.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceRealTimeData.paramId.NotNUll}")
	@ApiModelProperty(value = "参数id")
    private java.math.BigInteger paramId;

/**
* 参数编码.
*/
			@Length(max = 50, message = "{com.elitech.iot.device.model.DeviceRealTimeData.paramCode.Length}")
	@ApiModelProperty(value = "参数编码")
    private String paramCode;

/**
* 参数名称.
*/
			@NotNull(message = "{com.elitech.iot.device.model.DeviceRealTimeData.paramName.NotNUll}")
		@Length(max = 40, message = "{com.elitech.iot.device.model.DeviceRealTimeData.paramName.Length}")
	@ApiModelProperty(value = "参数名称")
    private String paramName;

/**
* 参数值.
*/
		@ApiModelProperty(value = "参数值")
    private java.math.BigDecimal paramValue;

/**
* 监测时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
			@NotNull(message = "{com.elitech.iot.device.model.DeviceRealTimeData.monitorTime.NotNUll}")
	@ApiModelProperty(value = "监测时间")
    private java.time.LocalDateTime monitorTime;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
			@NotNull(message = "{com.elitech.iot.device.model.DeviceRealTimeData.gmtCreate.NotNUll}")
	@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时间")
    private java.time.LocalDateTime gmtModified;
}