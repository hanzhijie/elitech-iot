/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 产品表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "产品表")
public class ProductModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 申请单产品id.
*/
			@NotNull(message = "{com.elitech.iot.calibration.model.Product.productId.NotNUll}")
	@ApiModelProperty(value = "申请单产品id")
    private java.math.BigInteger productId;

/**
* 申请单id.
*/
			@NotNull(message = "{com.elitech.iot.calibration.model.Product.applicationId.NotNUll}")
	@ApiModelProperty(value = "申请单id")
    private java.math.BigInteger applicationId;

/**
* 产品规格型号.
*/
			@Length(max = 10, message = "{com.elitech.iot.calibration.model.Product.productType.Length}")
	@ApiModelProperty(value = "产品规格型号")
    private String productType;

/**
* 出厂编号.
*/
			@Length(max = 20, message = "{com.elitech.iot.calibration.model.Product.manufacturingCode.Length}")
	@ApiModelProperty(value = "出厂编号")
    private String manufacturingCode;

/**
* 器具名称.
*/
			@Length(max = 50, message = "{com.elitech.iot.calibration.model.Product.productName.Length}")
	@ApiModelProperty(value = "器具名称")
    private String productName;

/**
* 设备编号.
*/
			@Length(max = 50, message = "{com.elitech.iot.calibration.model.Product.productCode.Length}")
	@ApiModelProperty(value = "设备编号")
    private String productCode;

/**
* 证书类型（字典表）.
*/
			@Length(max = 50, message = "{com.elitech.iot.calibration.model.Product.dictCertificateCode.Length}")
	@ApiModelProperty(value = "证书类型（字典表）")
    private String dictCertificateCode;

/**
* 数量.
*/
		@ApiModelProperty(value = "数量")
    private Integer productNum;

/**
* 费用,单位（分）.
*/
		@ApiModelProperty(value = "费用,单位（分）")
    private java.math.BigInteger serviceFee;

/**
* 备注.
*/
			@Length(max = 255, message = "{com.elitech.iot.calibration.model.Product.productRemark.Length}")
	@ApiModelProperty(value = "备注")
    private String productRemark;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}