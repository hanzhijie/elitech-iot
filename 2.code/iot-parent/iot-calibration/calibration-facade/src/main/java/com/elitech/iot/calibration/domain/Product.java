/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
* 产品表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 申请单产品id.
    */
    private java.math.BigInteger productId;
    /**
    * 申请单id.
    */
    private java.math.BigInteger applicationId;
    /**
    * 产品规格型号.
    */
    private String productType;
    /**
    * 出厂编号.
    */
    private String manufacturingCode;
    /**
    * 器具名称.
    */
    private String productName;
    /**
    * 设备编号.
    */
    private String productCode;
    /**
    * 证书类型（字典表）.
    */
    private String dictCertificateCode;
    /**
    * 数量.
    */
    private Integer productNum;
    /**
    * 费用,单位（分）.
    */
    private Integer serviceFee;
    /**
    * 备注.
    */
    private String productRemark;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;

    /**
     * 校准点
     */
    private List<Point> points;
}