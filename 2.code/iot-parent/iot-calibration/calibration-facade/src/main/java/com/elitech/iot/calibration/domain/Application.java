/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:20
*/

package com.elitech.iot.calibration.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
* 校准服务申请单.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class Application implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 申请单id.
    */
    private java.math.BigInteger applicationId;
    /**
    * 订单id.
    */
    private java.math.BigInteger orderId;
    /**
    * 用户id.
    */
    private java.math.BigInteger accountId;

    /**
    * 基础信息id.
    */
    private java.math.BigInteger baseInfoId;
    /**
    * 服务类型，0-普通，1-加急.
    */
    private Boolean serviceType;
    /**
    * 申请单备注.
    */
    private String remark;
    /**
    * 服务费，单位（分）.
    */
    private Integer serviceFee;
    /**
    * 运费（分）.
    */
    private Integer freightFee;
    /**
    * 总价.
    */
    private Integer totalFee;
    /**
    * 申请单状态，1-收件中，2-确认中，3-校验中，4-制作报告中，5-产品送回中，6-完成.
    */
    private int calibrationStatus;
    /**
    * 运单号(寄回快递时).
    */
    private String trackingNumber;
    /**
    * 证书文件地址.
    */
    private String filePath;
    /**
    * 证书到期时间.
    */
    private java.time.LocalDateTime fileExpire;
    /**
    * 结论说明.
    */
    private String resultExplain;
    /**
    * 内部付款凭证（财务填写）.
    */
    private String payEvidence;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}