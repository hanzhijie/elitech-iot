/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-23 10:51:48
*/

package com.elitech.iot.calibration.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
* 费用配置表同一时间只有一条数据生效.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class PriceConf implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    * 生效状态.
    */
    private byte status;
    /**
    * 温度单点价格（分）.
    */
    private Integer temperaturePrice;
    /**
    * 湿度单点价格（分）.
    */
    private Integer humidityPrice;
    /**
    * 加急系数.
    */
    private java.math.BigDecimal urgentCoefficient;
    /**
    * 运费（分）.
    */
    private Integer freightFee;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}