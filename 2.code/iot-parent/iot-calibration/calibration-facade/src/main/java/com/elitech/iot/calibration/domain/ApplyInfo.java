package com.elitech.iot.calibration.domain;

import com.elitech.iot.payment.model.PayOrderInvoiceModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName ApplyInfo
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/3/19
 * @Version V1.0
 **/
@Data
public class ApplyInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 基本信息
     */
    private BaseInfo baseInfo;
    /**
     * 申请单信息
     */
    private Application application;

    /**
     * 产品列表
     */
    private List<Product> products;

    /**
     * 订单发票
     */
    private PayOrderInvoiceModel payOrderInvoiceModel;

}
