/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 校准基本信息.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 申请单id.
    */
    private java.math.BigInteger baseInfoId;
    /**
    * 联系人手机号.
    */
    private String contactPhone;
    /**
    * 联系人姓名.
    */
    private String contactName;
    /**
    * 委托单位名称.
    */
    private String entrustedCompanyName;
    /**
    * 委托单位地址.
    */
    private String entrustedCompanyAddress;
    /**
    * 证书单位名称.
    */
    private String certificateCompanyName;
    /**
    * 证书单位地址.
    */
    private String certificateCompanyAddress;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}