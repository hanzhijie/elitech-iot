/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
* 校准点表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class Point implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 校准点id.
    */
    private java.math.BigInteger pointId;
    /**
    * 产品单id.
    */
    private java.math.BigInteger productId;
    /**
    * 校准点类型（字典表）.
    */
    private String dictPointType;
    /**
    * 单位.
    */
    private String pointUnit;
    /**
    * 误差值.
    */
    private String pointError;
    /**
    * 校准值.
    */
    private java.math.BigDecimal pointValue;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}