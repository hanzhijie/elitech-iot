/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 校准点表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "校准点表")
public class PointModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 校准点id.
*/
			@NotNull(message = "{com.elitech.iot.calibration.model.Point.pointId.NotNUll}")
	@ApiModelProperty(value = "校准点id")
    private java.math.BigInteger pointId;

/**
* 产品单id.
*/
			@NotNull(message = "{com.elitech.iot.calibration.model.Point.productId.NotNUll}")
	@ApiModelProperty(value = "产品单id")
    private java.math.BigInteger productId;

/**
* 校准点类型（字典表）.
*/
			@Length(max = 50, message = "{com.elitech.iot.calibration.model.Point.dictPointType.Length}")
	@ApiModelProperty(value = "校准点类型（字典表）")
    private String dictPointType;

/**
* 单位.
*/
			@Length(max = 5, message = "{com.elitech.iot.calibration.model.Point.pointUnit.Length}")
	@ApiModelProperty(value = "单位")
    private String pointUnit;

/**
* 误差值.
*/
		@ApiModelProperty(value = "误差值")
    private String pointError;

/**
* 校准值.
*/
		@ApiModelProperty(value = "校准值")
    private java.math.BigDecimal pointValue;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}