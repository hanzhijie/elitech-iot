/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 17:51:21
 */

package com.elitech.iot.calibration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * 时间表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
@Getter
@Setter
@ApiModel(description = "时间表")
public class TimeModel implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private BigInteger id;
    /**
     * 校准申请单id.
     */
    @NotNull(message = "{com.elitech.iot.calibration.model.Time.applicationId.NotNUll}")
    @ApiModelProperty(value = "校准申请单id")
    private java.math.BigInteger applicationId;

    /**
     * 状态说明，1-申请时间，2-确认时间，3-校验开始时间，4-校验结束时间（上传报告时间），5-产品快递发出时间，6-完成.
     */
    @ApiModelProperty(value = "状态说明，1-申请时间，2-确认时间，3-校验开始时间，4-校验结束时间（上传报告时间），5-产品快递发出时间，6-完成")
    private int operatorStatus;

    /**
     * 操作时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "操作时间")
    private java.time.LocalDateTime operatorTime;

    /**
     * 操作者.
     */
    @ApiModelProperty(value = "操作者")
    private java.math.BigInteger operatorUserId;

    /**
     * 创建时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

    /**
     * 修改时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}