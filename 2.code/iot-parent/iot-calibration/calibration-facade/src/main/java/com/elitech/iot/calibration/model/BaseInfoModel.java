/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 校准基本信息.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "校准基本信息")
public class BaseInfoModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 申请单id.
*/
			@NotNull(message = "{com.elitech.iot.calibration.model.BaseInfo.baseInfoId.NotNUll}")
	@ApiModelProperty(value = "申请单id")
    private java.math.BigInteger baseInfoId;

/**
* 联系人手机号.
*/
			@Length(max = 20, message = "{com.elitech.iot.calibration.model.BaseInfo.contactPhone.Length}")
	@ApiModelProperty(value = "联系人手机号")
    private String contactPhone;

/**
* 联系人姓名.
*/
			@Length(max = 10, message = "{com.elitech.iot.calibration.model.BaseInfo.contactName.Length}")
	@ApiModelProperty(value = "联系人姓名")
    private String contactName;

/**
* 委托单位名称.
*/
			@Length(max = 100, message = "{com.elitech.iot.calibration.model.BaseInfo.entrustedCompanyName.Length}")
	@ApiModelProperty(value = "委托单位名称")
    private String entrustedCompanyName;

/**
* 委托单位地址.
*/
			@Length(max = 100, message = "{com.elitech.iot.calibration.model.BaseInfo.entrustedCompanyAddress.Length}")
	@ApiModelProperty(value = "委托单位地址")
    private String entrustedCompanyAddress;

/**
* 证书单位名称.
*/
			@Length(max = 100, message = "{com.elitech.iot.calibration.model.BaseInfo.certificateCompanyName.Length}")
	@ApiModelProperty(value = "证书单位名称")
    private String certificateCompanyName;

/**
* 证书单位地址.
*/
			@Length(max = 100, message = "{com.elitech.iot.calibration.model.BaseInfo.certificateCompanyAddress.Length}")
	@ApiModelProperty(value = "证书单位地址")
    private String certificateCompanyAddress;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}