/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 17:51:21
 */

package com.elitech.iot.calibration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 校准服务申请单.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
@Getter
@Setter
@ApiModel(description = "校准服务申请单")
public class ApplicationModel implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 申请单id.
     */
    @NotNull(message = "{com.elitech.iot.calibration.model.Application.applicationId.NotNUll}")
    @ApiModelProperty(value = "申请单id")
    private java.math.BigInteger applicationId;

    /**
     * 订单id.
     */
    @NotNull(message = "{com.elitech.iot.calibration.model.Application.orderId.NotNUll}")
    @ApiModelProperty(value = "订单id")
    private java.math.BigInteger orderId;

    /**
     * 订单id.
     */
    @NotNull(message = "{com.elitech.iot.calibration.model.Application.orderId.NotNUll}")
    @ApiModelProperty(value = "用户id")
    private java.math.BigInteger accountId;

    /**
     * 基础信息id.
     */
    @NotNull(message = "{com.elitech.iot.calibration.model.Application.baseInfoId.NotNUll}")
    @ApiModelProperty(value = "基础信息id")
    private java.math.BigInteger baseInfoId;

    /**
     * 服务类型，0-普通，1-加急.
     */
    @NotNull(message = "{com.elitech.iot.calibration.model.Application.serviceType.NotNUll}")
    @ApiModelProperty(value = "服务类型，0-普通，1-加急")
    private Boolean serviceType;

    /**
     * 申请单备注.
     */
    @Length(max = 200, message = "{com.elitech.iot.calibration.model.Application.remark.Length}")
    @ApiModelProperty(value = "申请单备注")
    private String remark;

    /**
     * 服务费，单位（分）.
     */
    @ApiModelProperty(value = "服务费，单位（分）")
    private java.math.BigInteger serviceFee;

    /**
     * 运费（分）.
     */
    @ApiModelProperty(value = "运费（分）")
    private java.math.BigInteger freightFee;

    /**
     * 总价.
     */
    @ApiModelProperty(value = "总价")
    private java.math.BigInteger totalFee;

    /**
     * 申请单状态，1-收件中，2-确认中，3-校验中，4-制作报告中，5-产品送回中，6-完成.
     */
    @ApiModelProperty(value = "申请单状态，1-收件中，2-确认中，3-校验中，4-制作报告中，5-产品送回中，6-完成")
    private int calibrationStatus;

    /**
     * 运单号(寄回快递时).
     */
    @Length(max = 30, message = "{com.elitech.iot.calibration.model.Application.trackingNumber.Length}")
    @ApiModelProperty(value = "运单号(寄回快递时)")
    private String trackingNumber;

    /**
     * 证书文件地址.
     */
    @Length(max = 100, message = "{com.elitech.iot.calibration.model.Application.filePath.Length}")
    @ApiModelProperty(value = "证书文件地址")
    private String filePath;

    /**
     * 证书到期时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "证书到期时间")
    private java.time.LocalDateTime fileExpire;

    /**
     * 结论说明.
     */
    @Length(max = 50, message = "{com.elitech.iot.calibration.model.Application.resultExplain.Length}")
    @ApiModelProperty(value = "结论说明")
    private String resultExplain;

    /**
     * 内部付款凭证（财务填写）.
     */
    @Length(max = 30, message = "{com.elitech.iot.calibration.model.Application.payEvidence.Length}")
    @ApiModelProperty(value = "内部付款凭证（财务填写）")
    private String payEvidence;

    /**
     * 创建时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

    /**
     * 修改时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}