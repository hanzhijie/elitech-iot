/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-23 10:51:48
*/

package com.elitech.iot.calibration.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 费用配置表同一时间只有一条数据生效.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "费用配置表，同一时间只有一条数据生效")
public class PriceConfModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* id.
*/
			@NotNull(message = "{com.elitech.iot.calibration.model.PriceConf.id.NotNUll}")
	@ApiModelProperty(value = "id")
    private java.math.BigInteger id;

/**
* 生效状态.
*/
		@ApiModelProperty(value = "生效状态")
    private byte status;

/**
* 温度单点价格（分）.
*/
		@ApiModelProperty(value = "温度单点价格（分）")
    private java.math.BigInteger temperaturePrice;

/**
* 湿度单点价格（分）.
*/
		@ApiModelProperty(value = "湿度单点价格（分）")
    private java.math.BigInteger humidityPrice;

/**
* 加急系数.
*/
		@ApiModelProperty(value = "加急系数")
    private java.math.BigDecimal urgentCoefficient;

/**
* 运费（分）.
*/
		@ApiModelProperty(value = "运费（分）")
    private java.math.BigInteger freightFee;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}