package com.elitech.iot.calibration.api;

import com.elitech.iot.calibration.CalibrationApplication;
import com.elitech.iot.calibration.domain.Application;
import com.elitech.iot.calibration.service.ApplicationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;

/**
 * @ClassName ApplicationTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/3/17
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CalibrationApplication.class})
public class ApplicationTest {
    @Autowired
    private ApplicationService applicationService;

    @Test
    public void insertTest(){
        Application application = new Application();
        application.setApplicationId(BigInteger.valueOf(2546));

        application.setBaseInfoId(BigInteger.valueOf(21654));
        application.setServiceType(true);
        int insert = applicationService.insert(application);
        System.out.println(insert);
    }
}
