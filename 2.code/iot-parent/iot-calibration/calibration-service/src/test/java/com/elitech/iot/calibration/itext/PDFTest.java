package com.elitech.iot.calibration.itext;


import com.elitech.iot.calibration.domain.*;
import com.elitech.iot.calibration.util.ItextUtil;
import com.elitech.iot.payment.model.PayOrderInvoiceModel;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName PDFTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/3/27
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
public class PDFTest {

    @Test
    public void pdfTest1() throws IOException, DocumentException {
        ApplyInfo applyInfo = new ApplyInfo();
        Application application = new Application();
        BaseInfo baseInfo = new BaseInfo();
        List<Product> products = new ArrayList<>();
        List<Point> points = new ArrayList<>();
        PayOrderInvoiceModel payOrderInvoiceModel = new PayOrderInvoiceModel();
        application.setApplicationId(BigInteger.valueOf(2154654));
        application.setFreightFee(25342);
        application.setServiceFee(1540);
        application.setTotalFee(2154);
        application.setServiceType(true);
        applyInfo.setApplication(application);
        baseInfo.setCertificateCompanyAddress("dlkeg");
        baseInfo.setCertificateCompanyName("degesf");
        baseInfo.setContactName("ddd");
        baseInfo.setContactPhone("21325464");
        baseInfo.setEntrustedCompanyName("segsfe");
        baseInfo.setEntrustedCompanyAddress("sefsgsefe");
        applyInfo.setBaseInfo(baseInfo);
        Product product = new Product();
        product.setDictCertificateCode("11");
        product.setManufacturingCode("123");
        product.setServiceFee(21654);
        product.setProductType("RCW");
        product.setProductRemark("ddd");
        product.setProductNum(12);
        product.setProductName("ddd");
        product.setProductCode("24654");
        Point point = new Point();
        point.setDictPointType("1");
        point.setPointValue(BigDecimal.valueOf(23));
        point.setPointUnit("℃");
        point.setPointError("±0.5");
        points.add(point);
        Point point1 = new Point();
        point1.setDictPointType("1");
        point1.setPointValue(BigDecimal.valueOf(23));
        point1.setPointUnit("%");
        point1.setPointError("±0.5");
        points.add(point1);
        product.setPoints(points);
        products.add(product);
        applyInfo.setProducts(products);
        payOrderInvoiceModel.setApplyFlag(true);
        payOrderInvoiceModel.setPersonalName("ddd");
        payOrderInvoiceModel.setPhone("1315648648");
        applyInfo.setPayOrderInvoiceModel(payOrderInvoiceModel);
        ItextUtil.createCalibrationOrderView("D://calibration//orderView//", applyInfo);
    }

    @Test
    public void pdfUpdateTest() throws IOException, DocumentException {
        String SRC = "D:/calibration/orderView/202004030333000003.pdf";
        String DEST = "D:/calibration/orderView/202004030333000003.pdf";
        manipulatePdf(SRC, DEST);
    }
    public void manipulatePdf(String src, String dest) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PdfContentByte canvas = stamper.getOverContent(1);
        float height=595;
        System.out.println(canvas.getHorizontalScaling());
        float x,y;
        x= 216;
        y = height -49.09F;
        canvas.saveState();
        canvas.setColorFill(BaseColor.WHITE);
        canvas.rectangle(x, y-5, 43, 15);

        canvas.fill();
        canvas.restoreState();
        //开始写入文本
        canvas.beginText();
        //BaseFont bf = BaseFont.createFont(URLDecoder.decode(CutAndPaste.class.getResource("/AdobeSongStd-Light.otf").getFile()), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        BaseFont bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
        Font font = new Font(bf,10,Font.BOLD);
        //设置字体和大小
        canvas.setFontAndSize(font.getBaseFont(), 10);
        //设置字体的输出位置
        canvas.setTextMatrix(x, y);
        //要输出的text
        canvas.showText("多退少补" );

        //设置字体的输出位置
        canvas.setFontAndSize(font.getBaseFont(), 20);
        canvas.setTextMatrix(x, y-90);
        //要输出的text
        canvas.showText("多退少补" );

        canvas.endText();
        stamper.close();
        reader.close();
    }
    @Test
    public void addContent() throws IOException, DocumentException {
        String filePath = "D:/calibration/orderView/202004030633000001.pdf";
        String savePath = "D:/calibration/orderView/202004030633000001.pdf";
        BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", false);
        Font font = new Font(baseFont,15, Font.BOLD);
        PdfReader reader = new PdfReader(new FileInputStream(filePath));
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(filePath));

        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            PdfContentByte over = stamper.getOverContent(i);
            ColumnText columnText = new ColumnText(over);
            // llx 和 urx  最小的值决定离左边的距离. lly 和 ury 最大的值决定离下边的距离
            columnText.setSimpleColumn(285, 756, 400, 806);
            Chunk chunk = new Chunk("（已支付）");
            chunk.setBackground(BaseColor.WHITE);
            Paragraph elements = new Paragraph(chunk);
            // 设置字体，如果不设置添加的中文将无法显示

            elements.setFont(font);
            columnText.addElement(elements);
            columnText.go();
        }
        stamper.close();
    }

}
