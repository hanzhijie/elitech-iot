package com.elitech.iot.calibration.util;

import com.elitech.iot.calibration.domain.*;
import com.elitech.iot.common.base.constant.CalibrationConstant;
import com.elitech.iot.payment.model.PayOrderInvoiceModel;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName ItextUtil
 * @Description: 生成pdf文件的工具类
 * @Author dongqg
 * @Date 2020/3/30
 * @Version V1.0
 **/
public class ItextUtil {
    public static void createCalibrationOrderView(String path, ApplyInfo applyInfo) throws IOException, DocumentException {
        if (Objects.isNull(applyInfo)) {
            return;
        }

        BaseInfo baseInfo = applyInfo.getBaseInfo();
        Application application = applyInfo.getApplication();
        List<Product> products = applyInfo.getProducts();

        //创建文档
        Document document = new Document();
        //写出流
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(path + application.getApplicationId() + ".pdf"));
        //开始编辑
        document.open();
        //解决中文不能显示问题
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        //字体设置
        //标题字体  15  加粗
        Font titleFont = new Font(bfChinese, 15, Font.BOLD);
        //默认字体
        Font contextFont = new Font(bfChinese);
        //段落文本
        //文档标题
        Paragraph paragraphTitle = new Paragraph("委托单（未支付）", titleFont);
        paragraphTitle.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraphTitle);
        //单号
        Paragraph calibrationCode = new Paragraph("单号:  " + application.getApplicationId(), contextFont);
        calibrationCode.setAlignment(Element.ALIGN_RIGHT);
        calibrationCode.setSpacingBefore(15);
        calibrationCode.setSpacingAfter(15);
        calibrationCode.setIndentationRight(80);
        document.add(calibrationCode);

        //基本信息标题
        document.add(getTitleParagragh("基本信息:", titleFont));

        PdfPTable baseInfoTable = createBaseInfoTable(baseInfo);
        // 把表格加入文档
        document.add(baseInfoTable);

        //产品信息标题
        document.add(getTitleParagragh("产品校准项目信息:", titleFont));

        //产品信息表格
        for (Product product : products) {
            PdfPTable productTable = createProductTable(product);
            document.add(productTable);
        }

        PdfPTable bottomTable = createBottomTable(applyInfo.getPayOrderInvoiceModel(), application);
        // 把表格加入文档
        document.add(bottomTable);

        Paragraph company = new Paragraph("中国江苏精创电气股份有限公司", titleFont);
        company.setAlignment(Element.ALIGN_CENTER);
        document.add(company);

        Paragraph contactAddress = new Paragraph("联系地址：江苏省徐州市北京路10086号", contextFont);
        contactAddress.setAlignment(Element.ALIGN_CENTER);
        document.add(contactAddress);

        Paragraph contactPhone = new Paragraph("联系电话：84847363    73632488   18832355555", contextFont);
        contactPhone.setAlignment(Element.ALIGN_CENTER);
        document.add(contactPhone);
        //编辑完成
        document.close();
    }

    private static Paragraph getTitleParagragh(String title, Font font) {
        Paragraph titleParagragh = new Paragraph(title, font);
        titleParagragh.setIndentationLeft(40);
        titleParagragh.setSpacingAfter(30);
        return titleParagragh;
    }

    private static PdfPCell getPdfPCell(Paragraph paragraph, BaseColor color, int side) {
        PdfPCell pdfPCell = new PdfPCell(paragraph);
        pdfPCell.setBorderColor(color);
        pdfPCell.disableBorderSide(side);
        return pdfPCell;
    }

    private static PdfPCell getPdfPCell(String context, Font font, BaseColor color, int side) {
        Paragraph paragraph = new Paragraph(context, font);
        PdfPCell pdfPCell = new PdfPCell(paragraph);
        pdfPCell.setBorderColor(color);
        pdfPCell.disableBorderSide(side);
        return pdfPCell;
    }

    private static PdfPCell getPdfPCell(String context, Font font, BaseColor color, int side, int padding) {
        PdfPCell pdfPCell = getPdfPCell(context, font, color, side);
        pdfPCell.setPaddingTop(padding);
        pdfPCell.setPaddingBottom(padding);
        pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
        pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return pdfPCell;
    }

    private static PdfPCell getPdfPCell(String context, Font font, BaseColor color, int side, int padding, int colspan) {
        PdfPCell pdfPCell = getPdfPCell(context, font, color, side, padding);
        pdfPCell.setColspan(colspan);
        return pdfPCell;
    }

    private static PdfPTable createBaseInfoTable(BaseInfo baseInfo) throws IOException, DocumentException {
        //解决中文不能显示问题
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        //颜色设置
        //表格边框颜色
        BaseColor baseInfoTableBorder = new BaseColor(215, 217, 224);
        //基本信息字体  9
        Font baseInfoFont = new Font(bfChinese, 9, Font.NORMAL);

        //基本信息表格
        PdfPTable baseInfoTable = new PdfPTable(2);
        //样式
        baseInfoTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 10));
        baseInfoTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 6));

        //基本信息左边内容
        PdfPCell pdfPCellLeft = getPdfPCell("委托单位名称:\u00a0\u00a0" + baseInfo.getEntrustedCompanyName() + "\n\n" +
                        "证书单位名称:\u00a0\u00a0" + baseInfo.getCertificateCompanyName() + "\n\n" +
                        "\u00a0\u00a0 *联系人姓名:\u00a0\u00a0" + baseInfo.getContactName(),
                baseInfoFont,
                baseInfoTableBorder,
                3);
        pdfPCellLeft.setPaddingLeft(30);
        baseInfoTable.addCell(pdfPCellLeft);

        //基本信息右边内容
        PdfPCell pdfPCellRight = getPdfPCell("委托单位地址:\u00a0\u00a0" + baseInfo.getEntrustedCompanyAddress() + "\n\n" +
                        "证书单位地址:\u00a0\u00a0" + baseInfo.getCertificateCompanyAddress() + "\n\n" +
                        "\u00a0\u00a0 *联系人电话:\u00a0\u00a0" + baseInfo.getContactPhone(),
                baseInfoFont,
                baseInfoTableBorder, 3);
        pdfPCellRight.setPaddingLeft(30);
        baseInfoTable.addCell(pdfPCellRight);

        //样式
        baseInfoTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 9));
        baseInfoTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 5));

        // 设置表格下面空白行
        baseInfoTable.setSpacingAfter(30f);
        return baseInfoTable;
    }

    private static PdfPTable createProductTable(Product product) throws DocumentException, IOException {
        //解决中文不能显示问题
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);

        // 8列表格
        PdfPTable productTable = new PdfPTable(8);
        //列比例
        float[] columnWidth = {1.3f, 0.9f, 0.9f, 0.9f, 0.5f, 1, 1.5f, 1};
        productTable.setTotalWidth(columnWidth);
        //产品表格中字体
        BaseColor productTableBorder = new BaseColor(110, 118, 143);
        Font productFont = new Font(bfChinese, 9, Font.NORMAL, productTableBorder);
        //费用字体
        Font serviceFeeFont = new Font(bfChinese, 10, Font.BOLD);

        productTable.addCell(getPdfPCell("计量器具名称", productFont, productTableBorder, 10, 5));
        productTable.addCell(getPdfPCell("规格型号", productFont, productTableBorder, 14, 5));
        productTable.addCell(getPdfPCell("设备编号", productFont, productTableBorder, 14, 5));
        productTable.addCell(getPdfPCell("出厂编号", productFont, productTableBorder, 14, 5));
        productTable.addCell(getPdfPCell("数量", productFont, productTableBorder, 14, 5));
        productTable.addCell(getPdfPCell("证书类型", productFont, productTableBorder, 14, 5));
        productTable.addCell(getPdfPCell("备注", productFont, productTableBorder, 14, 5));
        productTable.addCell(getPdfPCell("费用", productFont, productTableBorder, 6, 5));

        productTable.addCell(getPdfPCell(product.getProductName(), productFont, productTableBorder, 0, 5));
        productTable.addCell(getPdfPCell(product.getProductType(), productFont, productTableBorder, 0, 5));
        productTable.addCell(getPdfPCell(product.getProductCode(), productFont, productTableBorder, 0, 5));
        productTable.addCell(getPdfPCell(product.getManufacturingCode(), productFont, productTableBorder, 0, 5));
        productTable.addCell(getPdfPCell(product.getProductNum().toString(), productFont, productTableBorder, 0, 5));
        productTable.addCell(getPdfPCell(product.getDictCertificateCode(), productFont, productTableBorder, 0, 5));
        productTable.addCell(getPdfPCell(product.getProductRemark(), productFont, productTableBorder, 0, 5));
        String fee = product.getServiceFee() / 100 + "." + product.getServiceFee() % 100 / 10 + product.getServiceFee() % 100 % 10;
        productTable.addCell(getPdfPCell(fee + "元", serviceFeeFont, productTableBorder, 0, 5));
        List<Point> points = product.getPoints();
        String tem = "";
        String hum = "";
        String temError = "";
        String humError = "";
        for (Point point : points) {
            if ("1".equals(point.getDictPointType())) {
                tem += point.getPointValue() + point.getPointUnit() + "，";
                if (temError.length() == 0) {
                    temError = point.getPointError();
                }
            }
            if ("2".equals(point.getDictPointType())) {
                hum += point.getPointValue() + point.getPointUnit() + "，";
                if (humError.length() == 0) {
                    humError = point.getPointError();
                }
            }
        }
        if (tem.length() == 0) {
            hum = hum.substring(0, hum.length() - 1);
            productTable.addCell(getPdfPCell("湿度校准要求（20℃环境下）：" + hum + "\n\n" + "湿度误差要求：" + humError,
                    productFont, productTableBorder, 1, 10, 8));
        }
        if (hum.length() == 0) {
            tem = tem.substring(0, tem.length() - 1);
            productTable.addCell(getPdfPCell("温度度校准要求：" + tem + "\n\n" + "温度误差要求：" + temError,
                    productFont, productTableBorder, 1, 10, 8));
        }
        if (hum.length() > 0 && tem.length() > 0) {
            //点位信息
            hum = hum.substring(0, hum.length() - 1);
            productTable.addCell(getPdfPCell("湿度校准要求（20℃环境下）：" + hum + "\n\n" + "湿度误差要求：" + humError,
                    productFont, productTableBorder, 1, 10, 4));
            tem = tem.substring(0, tem.length() - 1);
            productTable.addCell(getPdfPCell("温度度校准要求：" + tem + "\n\n" + "温度误差要求：" + temError,
                    productFont, productTableBorder, 1, 10, 4));
        }
        productTable.setSpacingAfter(5);
        return productTable;
    }

    private static PdfPTable createBottomTable(PayOrderInvoiceModel payOrderInvoiceModel, Application application) throws IOException, DocumentException {
        //解决中文不能显示问题
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        //费用字体
        Font serviceFeeFont = new Font(bfChinese, 9, Font.BOLD);
        //表格边框颜色
        BaseColor baseInfoTableBorder = new BaseColor(215, 217, 224);
        //基本信息字体  9
        Font baseInfoFont = new Font(bfChinese, 9, Font.NORMAL);
        //底部信息
        PdfPTable bottomTable = new PdfPTable(4);
        //样式
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 10));
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 14));
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 14));
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 6));
        String serviceFee = application.getServiceFee() / 100 + "." + application.getServiceFee() % 100 / 10 + application.getServiceFee() % 100 % 10;
        String freightFee = application.getFreightFee() / 100 + "." + application.getFreightFee() % 100 / 10 + application.getFreightFee() % 100 % 10;
        String totalFee = application.getTotalFee() / 100 + "." + application.getTotalFee() % 100 / 10 + application.getTotalFee() % 100 % 10;
        String flag = application.getServiceType() ? "是" : "否";
        Byte invoiceType = payOrderInvoiceModel.getInvoiceType();
        if (CalibrationConstant.APPLICATION_INVOICE_FLAG.equals(invoiceType)) {
            //不开票
            //底部信息左边内容
            PdfPCell bottomLeft = getPdfPCell(" ",
                    baseInfoFont,
                    baseInfoTableBorder,
                    11);
            bottomLeft.setHorizontalAlignment(Element.ALIGN_RIGHT);
            bottomTable.addCell(bottomLeft);
            //基本信息中间内容
            PdfPCell bottomCenter1 = getPdfPCell("是否加急：\n\n服务费用合计：\n\n快递费用：\n\n总计费用：\n",
                    baseInfoFont,
                    baseInfoTableBorder, 15);
            bottomCenter1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            bottomTable.addCell(bottomCenter1);
            PdfPCell bottomCenter2 = getPdfPCell(flag + "\n\n" +
                            serviceFee + "元\n\n" +
                            freightFee + "元\n\n" +
                            totalFee + "元",
                    serviceFeeFont,
                    baseInfoTableBorder, 15);
            bottomCenter2.setHorizontalAlignment(Element.ALIGN_LEFT);
            bottomTable.addCell(bottomCenter2);
            PdfPCell bottomRight = getPdfPCell(" ",
                    baseInfoFont,
                    baseInfoTableBorder, 7);
            bottomRight.setHorizontalAlignment(Element.ALIGN_LEFT);
            bottomTable.addCell(bottomRight);
        } else if (CalibrationConstant.APPLICATION_INVOICE_TYPE_PERSONAL.equals(invoiceType)) {
            //个人
            //底部信息左边内容
            PdfPCell bottomLeft = getPdfPCell("是否加急：\n\n服务费用合计：\n\n快递费用：\n\n总计费用：\n",
                    baseInfoFont,
                    baseInfoTableBorder,
                    11);
            bottomLeft.setHorizontalAlignment(Element.ALIGN_RIGHT);
            bottomTable.addCell(bottomLeft);

            //基本信息中间内容
            PdfPCell bottomCenter1 = getPdfPCell(flag + "\n\n" +
                            serviceFee + "元\n\n" +
                            freightFee + "元\n\n" +
                            totalFee + "元",
                    serviceFeeFont,
                    baseInfoTableBorder, 7);
            bottomCenter1.setHorizontalAlignment(Element.ALIGN_LEFT);
            bottomTable.addCell(bottomCenter1);
            PdfPCell bottomCenter2 = getPdfPCell("\n\n 姓名：\n\n手机号：",
                    baseInfoFont,
                    baseInfoTableBorder, 11);
            bottomCenter2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            bottomTable.addCell(bottomCenter2);
            PdfPCell bottomRight = getPdfPCell(" \n\n" + payOrderInvoiceModel.getPersonalName()
                            + "\n\n" +
                            payOrderInvoiceModel.getPhone(),
                    baseInfoFont,
                    baseInfoTableBorder, 7);
            bottomRight.setHorizontalAlignment(Element.ALIGN_LEFT);
            bottomTable.addCell(bottomRight);
        } else if (CalibrationConstant.APPLICATION_INVOICE_TYPE_COMPAMY.equals(invoiceType)) {
            //企业
            //底部信息左边内容
            PdfPCell bottomLeft = getPdfPCell("是否加急：\n\n服务费用合计：\n\n快递费用：\n\n总计费用：\n",
                    baseInfoFont,
                    baseInfoTableBorder,
                    11);
            bottomLeft.setHorizontalAlignment(Element.ALIGN_RIGHT);
            bottomTable.addCell(bottomLeft);

            //基本信息中间内容
            PdfPCell bottomCenter1 = getPdfPCell(flag + "\n\n" +
                            serviceFee + "元\n\n" +
                            freightFee + "元\n\n" +
                            totalFee + "元",
                    serviceFeeFont,
                    baseInfoTableBorder, 7);
            bottomCenter1.setHorizontalAlignment(Element.ALIGN_LEFT);
            bottomTable.addCell(bottomCenter1);
            PdfPCell bottomCenter2 = getPdfPCell("企业名称：\n\n税号：\n\n公司地址：\n\n开户行及账号：",
                    baseInfoFont,
                    baseInfoTableBorder, 11);
            bottomCenter2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            bottomTable.addCell(bottomCenter2);
            PdfPCell bottomRight = getPdfPCell(payOrderInvoiceModel.getCompanyName() + "\n\n" +
                            payOrderInvoiceModel.getTaxNumber() + "\n\n" +
                            payOrderInvoiceModel.getCompanyAddress() + "\n\n" +
                            payOrderInvoiceModel.getBankDeposit(),
                    baseInfoFont,
                    baseInfoTableBorder, 7);
            bottomRight.setHorizontalAlignment(Element.ALIGN_LEFT);
            bottomTable.addCell(bottomRight);
        }


        //样式
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 9));
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 13));
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 13));
        bottomTable.addCell(getPdfPCell(new Paragraph(" "), baseInfoTableBorder, 5));

        bottomTable.setSpacingBefore(30f);
        bottomTable.setSpacingAfter(30f);
        return bottomTable;
    }

    public static void addContent(String path , BigInteger applicationId) throws IOException, DocumentException {
        String filePath = path + applicationId + ".pdf";
        BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", false);
        Font font = new Font(baseFont,15, Font.BOLD);
        PdfReader reader = new PdfReader(new FileInputStream(filePath));
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(filePath));

        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            PdfContentByte over = stamper.getOverContent(i);
            ColumnText columnText = new ColumnText(over);
            // llx 和 urx  最小的值决定离左边的距离. lly 和 ury 最大的值决定离下边的距离
            columnText.setSimpleColumn(284, 756, 400, 806);
            Chunk chunk = new Chunk("（已支付）");
            chunk.setBackground(BaseColor.WHITE);
            Paragraph elements = new Paragraph(chunk);
            // 设置字体，如果不设置添加的中文将无法显示

            elements.setFont(font);
            columnText.addElement(elements);
            columnText.go();
        }
        stamper.close();
    }
}
