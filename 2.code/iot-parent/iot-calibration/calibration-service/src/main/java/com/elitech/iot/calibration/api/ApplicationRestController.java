/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 17:51:21
 */
package com.elitech.iot.calibration.api;

import com.elitech.iot.base.client.BaseAccountClient;
import com.elitech.iot.calibration.config.TokenDecode;
import com.elitech.iot.calibration.domain.Application;
import com.elitech.iot.calibration.domain.ApplyInfo;
import com.elitech.iot.calibration.model.ApplicationModel;
import com.elitech.iot.calibration.service.ApplicationService;
import com.elitech.iot.calibration.service.BaseInfoService;
import com.elitech.iot.calibration.util.ItextUtil;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.payment.client.PayOrderClient;
import com.github.pagehelper.PageInfo;
import com.itextpdf.text.DocumentException;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 校准服务申请单 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020-03-18 17:51:21
 * @since v1.0.0
 */
@RestController
@RequestMapping("Application")
@RefreshScope
@Api(tags = {"Application API"})
@CrossOrigin
public class ApplicationRestController {
    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private BaseInfoService baseInfoService;

    @Autowired
    private TokenDecode tokenDecode;

    @Autowired
    private BaseAccountClient baseAccountClient;

    @Autowired
    private PayOrderClient payOrderClient;

    @Value("${com.elitech.iot.calibration.orderView.path}")
    private String calibrationOrderViewPath;

    @Value("${com.elitech.iot.calibration.report.path}")
    private String calibrationReportPath;

    /**
     * 新增数据.
     *
     * @param model 要新增的数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "校准服务申请单", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @PostMapping(path = "/insert")
    public ResultMessage<ApplicationModel> insert(@RequestBody ApplicationModel model) {
        Application domain = BeanUtil.copy(model, Application.class);
        int data = applicationService.insert(domain);
        return ResultMessage.success(model);
    }

    /**
     * 批量新增数据.
     *
     * @param list 要新增的数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "校准服务申请单", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @PostMapping(path = "/insert/list")
    public ResultMessage<List<ApplicationModel>> insertList(@RequestBody List<ApplicationModel> list) {
        List<Application> domainList = BeanUtil.copy(list, Application.class);
        int data = applicationService.insertList(domainList);
        return ResultMessage.success(list);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "校准服务申请单", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @DeleteMapping(path = "/delete")
    public ResultMessage<Integer> deleteByPk(@RequestParam("pk") java.math.BigInteger pk) {
        int data = applicationService.deleteByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "校准服务申请单", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @DeleteMapping(path = "/delete/list")
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = applicationService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param model 要更新的数据
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "校准服务申请单", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @PutMapping(value = "/update")
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody ApplicationModel model) {
        Application domain = BeanUtil.copy(model, Application.class);
        int data = applicationService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "校准服务申请单", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @GetMapping(value = "/get")
    public ResultMessage<Application> selectByPk(@RequestParam("pk") java.math.BigInteger pk) {
        Application data = applicationService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "校准服务申请单", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @PostMapping(value = "/get/list")
    public ResultMessage<List<Application>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<Application> data = applicationService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "校准服务申请单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "query"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @PostMapping(value = "/get/page")
    public ResultMessage<PageInfo<Application>> pageListByWhere(@RequestParam("start") int start, @RequestParam("pageSize") int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<Application> data = applicationService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param model 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "校准服务申请单", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "model", required = true)
    @PostMapping(value = "/get/model")
    public ResultMessage<List<ApplicationModel>> selectListByDomain(@RequestBody ApplicationModel model) {
        Application domain = BeanUtil.copy(model, Application.class);
        List<Application> list = applicationService.selectListByDomain(domain);
        List<ApplicationModel> modelList = BeanUtil.copy(list, ApplicationModel.class);
        return ResultMessage.success(modelList);
    }

    /*******************新增******************************/
    /**
     * 根据实体类查询
     * @param applyInfo 申请信息
     * @return 申请结果
     */
    @ApiOperation(value = "根据Model查询", notes = "校准服务申请单", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "model", required = true)
    @PostMapping(value = "/apply")
    @Transactional
    public ResultMessage applyCalibration( @RequestBody ApplyInfo applyInfo) {
        if (Objects.isNull(applyInfo) ||
                Objects.isNull(applyInfo.getApplication()) ||
                Objects.isNull(applyInfo.getBaseInfo()) ||
                Objects.isNull(applyInfo.getProducts()) ||
                applyInfo.getProducts().size() == 0) {
            return ResultMessage.fail(1001, "参数错误");
        }
        Application application = applicationService.apply(applyInfo);
        if (Objects.isNull(application)) {
            return ResultMessage.fail(500, "注册失败");
        }
        Map resultMap = new HashMap();
        resultMap.put("applicationId",application.getApplicationId().toString());
        resultMap.put("orderId",application.getOrderId().toString());
        return ResultMessage.success(resultMap);
    }

    /**
     * 支付成功后修改校准申请单标志
     * @param applicationId
     * @return
     */
    @PostMapping("/update/orderView")
    public ResultMessage<Boolean> updatePayStatusForOrderView(@RequestParam BigInteger applicationId){
        try {
            ItextUtil.addContent(calibrationOrderViewPath,applicationId);
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }
        return ResultMessage.success(true);
    }

    /**
     * 根据条件分页查询申请单(用户登录后)
     * @return
     */
    @PostMapping(value = "/page/applicationOrderByAccountId")
    public ResultMessage pageSelectCalibrationOrderByAccount(HttpServletRequest request, @RequestParam int start, @RequestParam int pageSize, @RequestBody Application application) {
        String id = tokenDecode.getUserInfo().get("id").toString();
        BigInteger accountId = BigInteger.valueOf(Long.parseLong(id));
        application.setAccountId(accountId);
        PageInfo<Map<String, Object>> pageInfo = applicationService.pageSelectCalibrationOrderByDomain(start, pageSize, application);
        return ResultMessage.success(pageInfo);
    }

    /**
     * 后台客服根据条件分页查询申请单(所有的)
     *
     * @return
     */
    @PostMapping(value = "/page/allApplicationOrder")
    public ResultMessage pageSelectAllCalibrationOrder(HttpServletRequest request, @RequestParam int start, @RequestParam int pageSize, @RequestBody Application application) {

        PageInfo<Map<String, Object>> pageInfo = applicationService.pageSelectCalibrationOrderByDomain(start, pageSize, application);
        return ResultMessage.success(pageInfo);
    }


    /**
     * 后台客服更改订单状态 1 收件 2 确认 3 校验
     *
     * @return
     */
    @PostMapping(value = "/applicationStatus")
    public ResultMessage changeApplicationStatus(@RequestParam int statusType, @RequestParam BigInteger applicationId) {
        String id = tokenDecode.getUserInfo().get("id").toString();
        BigInteger accountId = BigInteger.valueOf(Long.parseLong(id));
        boolean success = applicationService.changeApplicationStatus(accountId, applicationId, statusType);
        if (success) {
            return ResultMessage.success("OK");
        }
        return ResultMessage.fail(500, "服务器错误");
    }

    /**
     * 后台客服上传校准报告
     *
     * @return
     */
    @PostMapping(value = "/upload/report")
    public ResultMessage upload(@RequestParam("file") MultipartFile file, @RequestParam BigInteger applicationId) {
        if (file.isEmpty()) {
            return ResultMessage.fail(1001, "请选择文件");
        }
        //String id = tokenDecode.getUserInfo().get("id").toString();
        //BigInteger accountId = BigInteger.valueOf(Long.parseLong(id));
        BigInteger accountId = BigInteger.valueOf(1234554);
        boolean success = applicationService.upload(file, applicationId, accountId);
        if (success) {
            return ResultMessage.success("OK");
        }
        return ResultMessage.fail(500, "服务器错误");
    }

    @PostMapping(value = "/trackingNumber")
    public ResultMessage trackingNumber(@RequestParam String trackingNumber, @RequestParam BigInteger applicationId) {
        String id = tokenDecode.getUserInfo().get("id").toString();
        BigInteger accountId = BigInteger.valueOf(Long.parseLong(id));
        boolean success = applicationService.trackingNumber(accountId, applicationId, trackingNumber);
        if (success) {
            return ResultMessage.success("OK");
        }
        return ResultMessage.fail(500, "服务器错误");
    }

    /**
     * 后台下载申请单
     * @param calibrationId
     * @param response
     * @return
     */
    @GetMapping("/download/order")
    public ResultMessage downloadOrderView(@RequestParam String calibrationId, HttpServletResponse response){
        String filePath = calibrationOrderViewPath+calibrationId+".pdf";
        File file = new File(filePath);
        if (file.exists()){
            // 设置强制下载不打开
            response.setContentType("application/force-download");
            // 设置文件名
            response.addHeader("Content-Disposition", "attachment;fileName="+calibrationId+".pdf");
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
                return ResultMessage.success("ok");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return ResultMessage.fail(500,"错误");
    }

    @GetMapping("/download/report")
    public void downloadReport(@RequestParam BigInteger calibrationId, HttpServletResponse response){
        Application application = applicationService.selectByPk(calibrationId);
        String filePath = calibrationReportPath+application.getFilePath();

        //String filePath = calibrationOrderViewPath+calibrationId+".pdf";
        File file = new File(filePath);
        if (file.exists()){
            // 设置文件名
            response.setCharacterEncoding("utf8");
            response.setHeader("Content-Type","application/octet-stream");
            //response.setContentType("application/octet-stream");
            response.addHeader("Content-Disposition", "attachment;fileName="+calibrationId+".pdf");

            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @PostMapping("/result/explain")
    public ResultMessage updateProductResult(@RequestBody Application application){
        if (Objects.isNull(application)){
            ResultMessage.fail(1001,"参数错误");
        }
        int i = applicationService.updateByPk(application);
        if (i > 0 ){
            return ResultMessage.success(i);
        }
        return ResultMessage.fail(500,"服务器错误");
    }
}