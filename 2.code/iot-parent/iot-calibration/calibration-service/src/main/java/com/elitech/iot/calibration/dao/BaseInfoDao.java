/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.dao;

import com.elitech.iot.calibration.domain.BaseInfo;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 校准基本信息.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface BaseInfoDao extends IBaseDao<BaseInfo, java.math.BigInteger> {

    /**
     * 根据手机号获取上次填写的基本信息和发票信息
     * @param phone
     * @return
     */

    BaseInfo getBaseInfoByPhone(String phone);
}