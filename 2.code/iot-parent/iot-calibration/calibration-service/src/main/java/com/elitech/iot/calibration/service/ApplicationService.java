/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 17:51:21
 */

package com.elitech.iot.calibration.service;

import com.elitech.iot.calibration.domain.Application;
import com.elitech.iot.calibration.domain.ApplyInfo;
import com.elitech.iot.common.service.IBaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.util.Map;

/**
 * 校准服务申请单.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
public interface ApplicationService extends IBaseService<Application, java.math.BigInteger> {

    /**
     * 申请
     * @param applyInfo
     * @return
     */
    Application apply(ApplyInfo applyInfo);

    /**
     * 获取校准申请单列表
     * @param start
     * @param pageSize
     * @param application
     * @return
     */
    PageInfo<Map<String, Object>> pageSelectCalibrationOrderByDomain(int start, int pageSize, Application application);

    /**
     * 改变申请单状态
     * @param accountId
     * @param applicationId
     * @param statusType
     * @return
     */
    boolean changeApplicationStatus(BigInteger accountId, BigInteger applicationId, int statusType);

    /**
     * 上传报告
     * @param file
     * @param applicationId
     * @return
     */
    boolean upload(MultipartFile file, BigInteger applicationId,BigInteger accountId);

    boolean trackingNumber(BigInteger accountId, BigInteger applicationId, String trackingNumber);
}