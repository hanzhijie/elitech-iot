/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-23 10:51:49
*/

package com.elitech.iot.calibration.service.impl;

import com.elitech.iot.calibration.domain.PriceConf;
import com.elitech.iot.calibration.dao.PriceConfDao;
import com.elitech.iot.calibration.service.PriceConfService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 费用配置表同一时间只有一条数据生效.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class PriceConfServiceImpl extends BaseService<PriceConf, java.math.BigInteger> implements PriceConfService {
    @Autowired
    private PriceConfDao priceConfDao;

    @Override
    protected PriceConfDao getDao() {
        return priceConfDao;
    }

    @Override
    public PriceConf getPriceConf() {
        return priceConfDao.getPriceConf();
    }
}