/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.service.impl;

import com.elitech.iot.calibration.dao.BaseInfoDao;
import com.elitech.iot.calibration.domain.BaseInfo;
import com.elitech.iot.calibration.service.BaseInfoService;
import com.elitech.iot.common.service.BaseService;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Objects;

/**
* 校准基本信息.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseInfoServiceImpl extends BaseService<BaseInfo, java.math.BigInteger> implements BaseInfoService {
    @Autowired
    private BaseInfoDao baseInfoDao;

    @Override
    protected BaseInfoDao getDao() {
        return baseInfoDao;
    }




    private BigInteger createBaseInfoId() {
        while (true) {
            String s = RandomStringUtils.randomNumeric(20);
            BigInteger id = BigInteger.valueOf(Long.parseLong(s));
            BaseInfo baseInfo = baseInfoDao.selectByPk(id);
            if (Objects.isNull(baseInfo)) {
                return id;
            }
        }
    }
}