/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.dao;

import com.elitech.iot.calibration.domain.Product;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 产品表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface ProductDao extends IBaseDao<Product, java.math.BigInteger> {

}