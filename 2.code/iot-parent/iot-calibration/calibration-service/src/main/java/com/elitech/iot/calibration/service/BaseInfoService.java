/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.service;

import com.elitech.iot.calibration.domain.BaseInfo;
import com.elitech.iot.common.service.IBaseService;

/**
* 校准基本信息.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BaseInfoService extends IBaseService<BaseInfo, java.math.BigInteger> {


}