/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-23 10:51:49
*/

package com.elitech.iot.calibration.service;

import com.elitech.iot.calibration.domain.PriceConf;

import com.elitech.iot.common.service.IBaseService;

/**
* 费用配置表,同一时间只有一条数据生效.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface PriceConfService extends IBaseService<PriceConf, java.math.BigInteger> {

    /**
     * 获取价格配置
     * @return
     */
    PriceConf getPriceConf();
}