package com.elitech.iot.calibration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * @ClassName CalibrationApplication
 * @Description: 校准服务启动类
 * @Author dongqg
 * @Date 2020/3/9
 * @Version V1.0
 **/

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.elitech.iot.base.client", "com.elitech.iot.uid.client","com.elitech.iot.payment.client"})
@Configuration
@MapperScan(basePackages = {"com.elitech.iot.calibration.dao"})
@EnableTransactionManagement
public class CalibrationApplication {

    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(CalibrationApplication.class, args);
    }

    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

}
