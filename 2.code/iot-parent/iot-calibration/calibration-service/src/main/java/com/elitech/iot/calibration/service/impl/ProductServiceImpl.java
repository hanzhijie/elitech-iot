/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.service.impl;

import com.elitech.iot.calibration.domain.Product;
import com.elitech.iot.calibration.dao.ProductDao;
import com.elitech.iot.calibration.service.ProductService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 产品表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class ProductServiceImpl extends BaseService<Product, java.math.BigInteger> implements ProductService {
    @Autowired
    private ProductDao productDao;

    @Override
    protected ProductDao getDao() {
        return productDao;
    }
}