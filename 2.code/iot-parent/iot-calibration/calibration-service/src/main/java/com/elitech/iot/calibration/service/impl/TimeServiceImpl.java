/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.service.impl;

import com.elitech.iot.calibration.domain.Time;
import com.elitech.iot.calibration.dao.TimeDao;
import com.elitech.iot.calibration.service.TimeService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 时间表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class TimeServiceImpl extends BaseService<Time, java.math.BigInteger> implements TimeService {
    @Autowired
    private TimeDao timeDao;

    @Override
    protected TimeDao getDao() {
        return timeDao;
    }
}