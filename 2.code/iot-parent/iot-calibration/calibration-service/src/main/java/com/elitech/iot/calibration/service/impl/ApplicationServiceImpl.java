/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 17:51:21
 */

package com.elitech.iot.calibration.service.impl;

import com.elitech.iot.base.client.BaseAccountClient;
import com.elitech.iot.calibration.dao.*;
import com.elitech.iot.calibration.domain.*;
import com.elitech.iot.calibration.service.ApplicationService;
import com.elitech.iot.calibration.service.PriceConfService;
import com.elitech.iot.calibration.util.ItextUtil;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.CalibrationConstant;
import com.elitech.iot.common.service.BaseService;
import com.elitech.iot.payment.client.PayOrderClient;
import com.elitech.iot.payment.model.PayOrderInvoiceModel;
import com.elitech.iot.uid.client.LeafUidClient;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 校准服务申请单.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class ApplicationServiceImpl extends BaseService<Application, java.math.BigInteger> implements ApplicationService {
    @Autowired
    private ApplicationDao applicationDao;
    @Autowired
    private BaseInfoDao baseInfoDao;
    @Autowired
    private PointDao pointDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private TimeDao timeDao;
    @Autowired
    private BaseAccountClient baseAccountClient;
    @Autowired
    private PayOrderClient payOrderClient;
    @Autowired
    private LeafUidClient leafUidClient;
    @Autowired
    private PriceConfService priceConfService;

    @Value("${com.elitech.iot.calibration.report.path}")
    private String calibrationReportPath;

    @Value("${com.elitech.iot.calibration.orderView.path}")
    private String calibrationOrderViewPath;

    @Override
    protected ApplicationDao getDao() {
        return applicationDao;
    }

    @Override
    public Application apply(ApplyInfo applyInfo) {

        //获取手机号
        String contactPhone = applyInfo.getBaseInfo().getContactPhone();
        //查看手机号是否注册
        boolean registered = baseAccountClient.phoneRegistered(contactPhone);
        //没注册的话直接进行注册
        if (!registered) {
            boolean registerSuccess = baseAccountClient.calibrationRegister(contactPhone);
            if (!registerSuccess) {
                return null;
            }
        }
        //根据手机号获取用户id
        BigInteger accountId = baseAccountClient.getAccountIdByPhone(contactPhone);

        //基本信息
        BigInteger baseInfoId = createBaseInfoId();
        BaseInfo baseInfo = applyInfo.getBaseInfo();
        baseInfo.setGmtModified(LocalDateTime.now());
        baseInfo.setGmtCreate(LocalDateTime.now());
        baseInfo.setBaseInfoId(baseInfoId);
        baseInfoDao.insert(baseInfo);

        //application部分
        Application application = applyInfo.getApplication();
        //生成id
        BigInteger applicationId = createApplicationId();

        application.setApplicationId(applicationId);
        application.setAccountId(accountId);
        application.setBaseInfoId(baseInfoId);
        application.setGmtCreate(LocalDateTime.now());
        application.setGmtModified(LocalDateTime.now());

        //产品列表
        List<Product> products = applyInfo.getProducts();
        //获取价格配置
        PriceConf priceConf = priceConfService.getPriceConf();
        //湿度单价
        Integer humidityPrice = priceConf.getHumidityPrice();
        //温度单价
        Integer temperaturePrice = priceConf.getTemperaturePrice();
        //加急系数
        BigDecimal urgentCoefficient = priceConf.getUrgentCoefficient();
        //价格初始化
        Integer serviceFee = 0;
        Integer freightFee = priceConf.getFreightFee();
        Integer totalFee = 0;
        for (Product product : products) {
            //处理单个产品
            BigInteger productId = createProductId();
            product.setProductId(productId);
            product.setApplicationId(applicationId);
            product.setGmtCreate(LocalDateTime.now());
            product.setGmtModified(LocalDateTime.now());
            //点位列表
            List<Point> points = product.getPoints();
            for (Point point : points) {
                //单个点位处理
                String dictPointType = point.getDictPointType();
                if (CalibrationConstant.CALIBRATION_POINT_TEMPERATURE.equals(dictPointType)) {
                    serviceFee += (temperaturePrice * product.getProductNum());
                } else if (CalibrationConstant.CALIBRATION_POINT_HUMIDITY.equals(dictPointType)) {
                    serviceFee += (humidityPrice * product.getProductNum());
                }
                BigInteger pointId = createPointId();
                point.setProductId(productId);
                point.setGmtCreate(LocalDateTime.now());
                point.setGmtModified(LocalDateTime.now());
                point.setPointId(pointId);
            }

            pointDao.insertList(points);
        }
        //点位存储
        int i = productDao.insertList(products);
        //根据服务类型计算总价
        if (application.getServiceType()) {
            serviceFee = urgentCoefficient.multiply(BigDecimal.valueOf(serviceFee)).intValue();
        }
        totalFee = serviceFee + freightFee;
        application.setServiceFee(serviceFee);
        application.setFreightFee(freightFee);
        application.setTotalFee(totalFee);

        //时间表操作
        Time time = new Time();
        time.setId(createTimeId());
        time.setApplicationId(applicationId);
        time.setGmtCreate(LocalDateTime.now());
        time.setGmtModified(LocalDateTime.now());
        time.setOperatorStatus(1);
        time.setOperatorTime(LocalDateTime.now());
        timeDao.insert(time);
        PayOrderInvoiceModel payOrderInvoiceModel = applyInfo.getPayOrderInvoiceModel();
        Byte b = 1;
        payOrderInvoiceModel.setOrderType(b);
        payOrderInvoiceModel.setAccountId(accountId);
        payOrderInvoiceModel.setOrderDescription("校准服务");
        payOrderInvoiceModel.setOrderFee(totalFee);
        payOrderInvoiceModel.setPayFee(totalFee);
        Byte invoiceType = payOrderInvoiceModel.getInvoiceType();
        if (CalibrationConstant.APPLICATION_INVOICE_FLAG.equals(invoiceType)){
            //发票类型如果是0 代表不开票
            payOrderInvoiceModel.setApplyFlag(false);
        }else {
            payOrderInvoiceModel.setApplyFlag(true);
        }
        ResultMessage<BigInteger> resultMessage = payOrderClient.createOrder(payOrderInvoiceModel);
        BigInteger orderId = resultMessage.getData();
        application.setOrderId(orderId);
        int insert = applicationDao.insert(application);
        //生成订单预览页
        try {
            ItextUtil.createCalibrationOrderView(calibrationOrderViewPath, applyInfo);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return application;
    }


    @Override
    public PageInfo<Map<String, Object>> pageSelectCalibrationOrderByDomain(int start, int pageSize, Application application) {
        PageHelper.startPage(start, pageSize);
        List<Application> calibrationOrders = applicationDao.selectListByDomain(application);
        PageInfo pageInfo = new PageInfo(calibrationOrders);
        return pageInfo;
    }

    @Override
    public boolean changeApplicationStatus(BigInteger accountId, BigInteger applicationId, int statusType) {
        Application application = applicationDao.selectByPk(applicationId);
        application.setGmtModified(LocalDateTime.now());
        application.setCalibrationStatus(statusType + 1);
        int i = applicationDao.updateByPk(application);

        Time time = new Time();
        time.setOperatorTime(LocalDateTime.now());
        time.setGmtModified(LocalDateTime.now());
        time.setGmtCreate(LocalDateTime.now());
        time.setOperatorStatus(statusType);
        time.setApplicationId(applicationId);
        time.setOperatorUserId(accountId);
        int insert = timeDao.insert(time);
        if (i > 0 && insert > 0) {
            return true;
        }
        return false;
    }

    /**
     * 校准报告上传
     *
     * @param file
     * @param applicationId
     * @return
     */
    @Override
    public boolean upload(MultipartFile file, BigInteger applicationId, BigInteger accountId) {
        String originalFileName = file.getOriginalFilename();
        String fileSuffix = originalFileName.substring(originalFileName.lastIndexOf(".") + 1).toLowerCase();
        String fileName = applicationId.toString() + "." + fileSuffix;

        File destFile = new File(calibrationReportPath + fileName);
        try {
            file.transferTo(destFile);
            System.out.println(destFile.getAbsolutePath());
            Application application = applicationDao.selectByPk(applicationId);
            application.setGmtModified(LocalDateTime.now());
            application.setCalibrationStatus(5);
            application.setFilePath(originalFileName);
            int i = applicationDao.updateByPk(application);

            Time time = new Time();
            time.setOperatorTime(LocalDateTime.now());
            time.setGmtModified(LocalDateTime.now());
            time.setGmtCreate(LocalDateTime.now());
            time.setOperatorStatus(4);
            time.setApplicationId(applicationId);
            time.setOperatorUserId(accountId);
            int insert = timeDao.insert(time);
            return i > 0 && insert > 0;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 填写快递单号，更改订单状态
     *
     * @param accountId
     * @param applicationId
     * @param trackingNumber
     * @return
     */
    @Override
    public boolean trackingNumber(BigInteger accountId, BigInteger applicationId, String trackingNumber) {
        Application application = applicationDao.selectByPk(applicationId);
        application.setGmtModified(LocalDateTime.now());
        application.setCalibrationStatus(6);
        int i = applicationDao.updateByPk(application);

        Time time = new Time();
        time.setOperatorTime(LocalDateTime.now());
        time.setGmtModified(LocalDateTime.now());
        time.setGmtCreate(LocalDateTime.now());
        time.setOperatorStatus(5);
        time.setApplicationId(applicationId);
        time.setOperatorUserId(accountId);
        int insert = timeDao.insert(time);
        return i > 0 && insert > 0;
    }

    private BigInteger createBaseInfoId() {
        while (true) {
            ResultMessage<Long> calibrationBaseInfo = leafUidClient.getSegmentKey("calibration_base_info");
            BigInteger id = BigInteger.valueOf(calibrationBaseInfo.getData());
            System.out.println(id);

            BaseInfo baseInfo = baseInfoDao.selectByPk(id);
            if (Objects.isNull(baseInfo)) {
                return id;
            }
        }
    }

    private BigInteger createApplicationId() {
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"));
        long calibrationId = Long.parseLong(time);
        calibrationId *= 1000000;
        while (true) {
            calibrationId++;
            BigInteger id = BigInteger.valueOf(calibrationId);
            Application application = applicationDao.selectByPk(id);
            if (Objects.isNull(application)) {
                return id;
            }
        }
    }

    private BigInteger createProductId() {
        while (true) {
            ResultMessage<Long> calibrationProduct = leafUidClient.getSegmentKey("calibration_product");
            BigInteger id = BigInteger.valueOf(calibrationProduct.getData());
            System.out.println(id);

            Product product = productDao.selectByPk(id);
            if (Objects.isNull(product)) {
                return id;
            }
        }
    }

    private BigInteger createPointId() {
        while (true) {
            ResultMessage<Long> calibrationPoint = leafUidClient.getSegmentKey("calibration_point");
            BigInteger id = BigInteger.valueOf(calibrationPoint.getData());
            System.out.println(id);
            Point point = pointDao.selectByPk(id);
            if (Objects.isNull(point)) {
                return id;
            }
        }
    }


    private BigInteger createTimeId() {
        while (true) {
            ResultMessage<Long> calibrationTime = leafUidClient.getSegmentKey("calibration_time");
            BigInteger id = BigInteger.valueOf(calibrationTime.getData());
            System.out.println(id);
            Time time = timeDao.selectByPk(id);
            if (Objects.isNull(time)) {
                return id;
            }
        }
    }
}