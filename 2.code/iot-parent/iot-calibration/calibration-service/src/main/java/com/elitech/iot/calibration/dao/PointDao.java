/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.dao;

import com.elitech.iot.calibration.domain.Point;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 校准点表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface PointDao extends IBaseDao<Point, java.math.BigInteger> {

}