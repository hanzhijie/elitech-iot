/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/
package com.elitech.iot.calibration.api;

import com.elitech.iot.calibration.domain.Point;
import com.elitech.iot.calibration.model.PointModel;
import com.elitech.iot.calibration.service.PointService;

import com.elitech.iot.common.base.api.ResultMessage;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
* 校准点表 API.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2020-03-18 17:51:21
* @since v1.0.0
*/
@RestController
@RequestMapping("Point")
@RefreshScope
@Api(tags = {"Point API"})
public class PointRestController {
    @Autowired
    private PointService calibrationPointService;

   /**
    * 新增数据.
    *
    * @param model 要新增的数据
    * @return 返回插入行数
    */
    @ApiOperation(value = "新增数据", notes = "校准点表", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @PostMapping(path = "/insert")
    public ResultMessage<PointModel> insert(@RequestBody PointModel model) {
        Point domain = BeanUtil.copy(model, Point.class);
        int data = calibrationPointService.insert(domain);
        return ResultMessage.success(model);
    }

    /**
    * 批量新增数据.
    *
    * @param list 要新增的数据列表
    * @return 返回插入行数
    */
    @ApiOperation(value = "批量新增", notes = "校准点表", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @PostMapping(path = "/insert/list")
    public ResultMessage<List<PointModel>> insertList(@RequestBody List<PointModel> list) {
        List<Point> domainList = BeanUtil.copy(list, Point.class);
        int data = calibrationPointService.insertList(domainList);
        return ResultMessage.success(list);
    }

    /**
    * 根据主键删除实体对象.
    *
    * @param pk 主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "删除数据", notes = "校准点表", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @DeleteMapping(path = "/delete")
    public ResultMessage<Integer> deleteByPk(@RequestParam("pk") java.math.BigInteger pk) {
        int data = calibrationPointService.deleteByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键批量删除.
    *
    * @param pkList 要删除实体对象的主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "批量删除", notes = "校准点表", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @DeleteMapping(path = "/delete/list")
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = calibrationPointService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 根据id更新实体对象.
    *
    * @param model 要更新的数据
    * @return 返回更新的行数。
    */
    @ApiOperation(value = "更新数据", notes = "校准点表", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @PutMapping(value = "/update")
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody PointModel model) {
        Point domain = BeanUtil.copy(model, Point.class);
        int data = calibrationPointService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @ApiOperation(value = "根据主键查询", notes = "校准点表", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @GetMapping(value = "/get")
    public ResultMessage<Point> selectByPk(@RequestParam("pk")  java.math.BigInteger pk) {
        Point data = calibrationPointService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据多个id查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @ApiOperation(value = "根据主键列表查询", notes = "校准点表", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @PostMapping(value = "/get/list")
    public ResultMessage<List<Point>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<Point> data = calibrationPointService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param whereMap 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @ApiOperation(value = "分页查询", notes = "校准点表", httpMethod = "POST")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "query"),
        @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "query"),
        @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @PostMapping(value = "/get/page")
    public ResultMessage<PageInfo<Point>> pageListByWhere(@RequestParam("start") int start, @RequestParam("pageSize") int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<Point> data = calibrationPointService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
    * 根据实体类查询
    *
    * @param model 实体类
    * @return List<Domain> 查询结果
    */
    @ApiOperation(value = "根据Model查询", notes = "校准点表", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "model", required = true)
    @PostMapping(value = "/get/model")
    public ResultMessage<List<PointModel>> selectListByDomain(@RequestBody PointModel model) {
        Point domain = BeanUtil.copy(model, Point.class);
        List<Point> list = calibrationPointService.selectListByDomain(domain);
        List<PointModel> modelList = BeanUtil.copy(list, PointModel.class);
        return ResultMessage.success(modelList);
    }
}