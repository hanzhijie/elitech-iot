/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 17:51:21
*/

package com.elitech.iot.calibration.service.impl;

import com.elitech.iot.calibration.domain.Point;
import com.elitech.iot.calibration.dao.PointDao;
import com.elitech.iot.calibration.service.PointService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 校准点表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class PointServiceImpl extends BaseService<Point, java.math.BigInteger> implements PointService {
    @Autowired
    private PointDao pointDao;

    @Override
    protected PointDao getDao() {
        return pointDao;
    }
}