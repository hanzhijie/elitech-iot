/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-23 10:51:48
*/

package com.elitech.iot.calibration.dao;

import com.elitech.iot.calibration.domain.PriceConf;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 费用配置表同一时间只有一条数据生效.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface PriceConfDao extends IBaseDao<PriceConf, java.math.BigInteger> {
    PriceConf getPriceConf();
}