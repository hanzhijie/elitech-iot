/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-23 10:51:49
*/
package com.elitech.iot.calibration.api;

import com.elitech.iot.calibration.domain.PriceConf;
import com.elitech.iot.calibration.model.PriceConfModel;
import com.elitech.iot.calibration.service.PriceConfService;

import com.elitech.iot.common.base.api.ResultMessage;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
* 费用配置表同一时间只有一条数据生效 API.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2020-03-23 10:51:49
* @since v1.0.0
*/
@RestController
@RequestMapping("PriceConf")
@RefreshScope
@Api(tags = {"PriceConf API"})
@CrossOrigin
public class PriceConfRestController {
    @Autowired
    private PriceConfService calibrationPriceConfService;

   /**
    * 新增数据.
    *
    * @param model 要新增的数据
    * @return 返回插入行数
    */
    @ApiOperation(value = "新增数据", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @PostMapping(path = "/insert")
    public ResultMessage<PriceConfModel> insert(@RequestBody PriceConfModel model) {
        PriceConf domain = BeanUtil.copy(model, PriceConf.class);
        int data = calibrationPriceConfService.insert(domain);
        return ResultMessage.success(model);
    }

    /**
    * 批量新增数据.
    *
    * @param list 要新增的数据列表
    * @return 返回插入行数
    */
    @ApiOperation(value = "批量新增", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @PostMapping(path = "/insert/list")
    public ResultMessage<List<PriceConfModel>> insertList(@RequestBody List<PriceConfModel> list) {
        List<PriceConf> domainList = BeanUtil.copy(list, PriceConf.class);
        int data = calibrationPriceConfService.insertList(domainList);
        return ResultMessage.success(list);
    }

    /**
    * 根据主键删除实体对象.
    *
    * @param pk 主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "删除数据", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @DeleteMapping(path = "/delete")
    public ResultMessage<Integer> deleteByPk(@RequestParam("pk") BigInteger pk) {
        int data = calibrationPriceConfService.deleteByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键批量删除.
    *
    * @param pkList 要删除实体对象的主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "批量删除", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @DeleteMapping(path = "/delete/list")
    public ResultMessage<BigInteger> deleteByPkList(@RequestBody List<BigInteger> pkList) {
        int data = calibrationPriceConfService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 根据id更新实体对象.
    *
    * @param model 要更新的数据
    * @return 返回更新的行数。
    */
    @ApiOperation(value = "更新数据", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @PutMapping(value = "/update")
    public ResultMessage<BigInteger> updateByPk(@RequestBody PriceConfModel model) {
        PriceConf domain = BeanUtil.copy(model, PriceConf.class);
        int data = calibrationPriceConfService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @ApiOperation(value = "根据主键查询", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @GetMapping(value = "/get")
    public ResultMessage<PriceConf> selectByPk(@RequestParam("pk")  BigInteger pk) {
        PriceConf data = calibrationPriceConfService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据多个id查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @ApiOperation(value = "根据主键列表查询", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @PostMapping(value = "/get/list")
    public ResultMessage<List<PriceConf>> selectByPkList(@RequestBody List<BigInteger> pkList) {
        List<PriceConf> data = calibrationPriceConfService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param whereMap 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @ApiOperation(value = "分页查询", notes = "费用配置表同一时间只有一条数据生效", httpMethod = "POST")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "query"),
        @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "query"),
        @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @PostMapping(value = "/get/page")
    public ResultMessage<PageInfo<PriceConf>> pageListByWhere(@RequestParam("start") int start, @RequestParam("pageSize") int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<PriceConf> data = calibrationPriceConfService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
    * 根据实体类查询
    *
    * @param model 实体类
    * @return List<Domain> 查询结果
    */
    @ApiOperation(value = "根据Model查询", notes = "费用配置表同一时间只有一条数据生效", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "model", required = true)
    @PostMapping(value = "/get/model")
    public ResultMessage<List<PriceConfModel>> selectListByDomain(@RequestBody PriceConfModel model) {
        PriceConf domain = BeanUtil.copy(model, PriceConf.class);
        List<PriceConf> list = calibrationPriceConfService.selectListByDomain(domain);
        List<PriceConfModel> modelList = BeanUtil.copy(list, PriceConfModel.class);
        return ResultMessage.success(modelList);
    }

    /************************新增******************************/
    /**
     * 获取价格配置
     *
     *
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "获取价格配置", notes = "费用配置表，同一时间只有一条数据生效", tags = "", httpMethod = "GET")
    @GetMapping(value = "/get/price")
    public ResultMessage getPriceConf(){
        PriceConf priceConf = calibrationPriceConfService.getPriceConf();
        if (priceConf == null ){
            return ResultMessage.fail(500,"服务内部错误");
        }
        return ResultMessage.success(priceConf);
    }
}