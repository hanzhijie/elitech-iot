/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/

package com.elitech.iot.payment.service;

import com.elitech.iot.payment.domain.PayOrder;

import com.elitech.iot.common.service.IBaseService;
import com.elitech.iot.payment.model.PayOrderInvoiceModel;

import java.math.BigInteger;

/**
* 订单表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface PayOrderService extends IBaseService<PayOrder, java.math.BigInteger> {
    /**
     * 创建订单
     * @param model
     * @return
     */
    BigInteger createOrder(PayOrderInvoiceModel model);
}