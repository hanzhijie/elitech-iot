package com.elitech.iot.payment.config;

import com.elitech.iot.common.base.util.IOUtils;
import lombok.Data;
import org.apache.http.ssl.SSLContexts;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;

@Data
@Component
@ConfigurationProperties(prefix = "wxpayconfig")
/**
 * 微信支付配置
 * @author st
 * @date 2020-03-18
 */
public class WxpayConfig {

    /**
     * 公众账号ID
     */
    private String appId;
    /**
     * 公众账号秘钥
     */
    private String appSecret;
    /**
     * 商户号
     */
    private String mchId;
    /**
     * 商户的key【API密匙】
     */
    private String key;
    /**
     * 服务器异步通知页面路径
     */
    private String notifyUrl;
    /**
     * 服务器同步通知页面路径
     */
    private String returnUrl;
    /**
     * API证书绝对路径 (本项目放在了 resources/cert/wechat_pay_cert.p12")
     */
    private String certPath;

    /**
     * 证书内容
     */
    private SSLContext sslContext;

    /**
     * 初始化证书
     * @return
     */
    public SSLContext initSSLContext() {
        FileInputStream inputStream = null;
        try {
            ClassPathResource classPathResource = new ClassPathResource(this.certPath);
            inputStream = new FileInputStream(classPathResource.getFile());
        } catch (IOException e) {
            throw new RuntimeException("读取微信商户证书文件出错", e);
        }

        try {
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            char[] partnerId2charArray = mchId.toCharArray();
            keystore.load(inputStream, partnerId2charArray);
            this.sslContext = SSLContexts.custom().loadKeyMaterial(keystore, partnerId2charArray).build();
            return this.sslContext;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("证书文件有问题，请核实！", e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }
    private String wxPackage;
}
