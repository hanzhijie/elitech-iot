package com.elitech.iot.payment.service;

import com.alipay.api.AlipayApiException;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.payment.config.PayType;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface PayService {
    /**
     * 统一下单
     * @param outTradeNo 订单编号
     * @param totalFee 订单金额
     * @param payDesc 订单描述
     * @return
     */
    ResultMessage createOrderWechat( PayType payType, String outTradeNo, Integer totalFee, String payDesc) throws Exception;

    /**
     * 验证回调参数
     * @return
     */
    String weChatNotify();

    /**
     * 退款（微信）
     * @param outTradeNo 订单编号
     * @param totalFee 订单总金额
     * @param refundFee 退款金额
     * @return
     */
    Map<String,String> refundOrderWechat(String outTradeNo, String outRefundNo, Integer totalFee, Integer refundFee) throws Exception;

    /**
     * @Description: 创建支付宝订单
     * @param orderNo: 订单编号
     * @param amount: 实际支付金额
     * @param body: 订单描述
     * @Author:
     * @Date: 2019/8/1
     * @return
     */
    String createOrderAli(String orderNo, Integer amount, String body) throws AlipayApiException;

    /**
     * @Description:
     * @param tradeStatus: 支付宝交易状态(异步)
     * @param orderNo: 订单编号
     * @param tradeNo: 支付宝订单号
     * @Author:
     * @Date: 2019/8/1
     * @return
     */
    boolean notifyAli(String tradeStatus, String orderNo, String tradeNo,String sellerId);
    /**
     * @Description:
     * @param tradeStatus: 支付宝交易状态(同步)
     * @param orderNo: 订单编号
     * @param tradeNo: 支付宝订单号
     * @Author:
     * @Date: 2019/8/1
     * @return
     */
    boolean returnAli(String tradeStatus, String orderNo, String tradeNo);

    /**
     * @Description: 校验签名
     * @Author:
     * @Date: 2019/8/1
     * @return
     */
    boolean rsaCheckAli()throws AlipayApiException, UnsupportedEncodingException;

    /**
     * @Description: 退款（阿里）
     * @param orderNo: 订单编号
     * @param amount: 实际支付金额
     * @param refundReason: 退款原因
     * @Author: XCK
     * @Date: 2019/8/6
     * @return
     */
    ResultMessage refundOrderAli(String orderNo, Integer amount, String refundReason);

    /**
     * 退款申请
     * @param orderCode 订单编号
     * @param refundFee 退款金额
     * @param refundReason 退款原因
     * @return
     */
    ResultMessage refundApply(String orderCode,Integer refundFee,String refundReason);

    /**
     * 退款
     * @param refundCode
     * @return
     */
    ResultMessage refunOrder(String refundCode);
}
