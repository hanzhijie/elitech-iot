/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 13:14:38
 */
package com.elitech.iot.payment.api;

import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.payment.domain.PayInvoice;
import com.elitech.iot.payment.model.PayInvoiceModel;
import com.elitech.iot.payment.service.PayInvoiceService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * 发票表 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020-03-18 13:14:38
 * @since v1.0.0
 */
@RestController
@RequestMapping("PayInvoice")
@RefreshScope
@Api(tags = {"PayInvoice API"})
public class PayInvoiceRestController {
    @Autowired
    private PayInvoiceService payInvoiceService;

    /**
     * 根据发票id更新开票状态.
     *
     * @param invoiceId 发票id
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "根据发票id更新开票状态.", notes = "根据发票id更新开票状态.", httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "invoiceId", value = "发票id", required = true, dataType = "int", defaultValue = "0", paramType = "query")})
    @PutMapping(value = "/updateInvoiceStatus")
    public ResultMessage<java.math.BigInteger> updateInvoiceStatus(@RequestParam BigInteger invoiceId) {
        return payInvoiceService.updateInvoiceStatus(invoiceId);
    }

    /**
     * 新增数据.
     *
     * @param model 要新增的数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "发票表", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @PostMapping(path = "/insert")
    public ResultMessage<PayInvoiceModel> insert(@RequestBody PayInvoiceModel model) {
        PayInvoice domain = BeanUtil.copy(model, PayInvoice.class);
        int data = payInvoiceService.insert(domain);
        return ResultMessage.success(model);
    }

    /**
     * 批量新增数据.
     *
     * @param list 要新增的数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "发票表", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @PostMapping(path = "/insert/list")
    public ResultMessage<List<PayInvoiceModel>> insertList(@RequestBody List<PayInvoiceModel> list) {
        List<PayInvoice> domainList = BeanUtil.copy(list, PayInvoice.class);
        int data = payInvoiceService.insertList(domainList);
        return ResultMessage.success(list);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "发票表", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @DeleteMapping(path = "/delete")
    public ResultMessage<Integer> deleteByPk(@RequestParam("pk") java.math.BigInteger pk) {
        int data = payInvoiceService.deleteByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "发票表", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @DeleteMapping(path = "/delete/list")
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = payInvoiceService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param model 要更新的数据
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "发票表", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @PutMapping(value = "/update")
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody PayInvoiceModel model) {
        PayInvoice domain = BeanUtil.copy(model, PayInvoice.class);
        int data = payInvoiceService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "发票表", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @GetMapping(value = "/get")
    public ResultMessage<PayInvoice> selectByPk(@RequestParam("pk") java.math.BigInteger pk) {
        PayInvoice data = payInvoiceService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "发票表", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @PostMapping(value = "/get/list")
    public ResultMessage<List<PayInvoice>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<PayInvoice> data = payInvoiceService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "发票表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "query"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @PostMapping(value = "/get/page")
    public ResultMessage<PageInfo<PayInvoice>> pageListByWhere(@RequestParam("start") int start, @RequestParam("pageSize") int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<PayInvoice> data = payInvoiceService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param model 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "发票表", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "model", required = true)
    @PostMapping(value = "/get/model")
    public ResultMessage<List<PayInvoiceModel>> selectListByDomain(@RequestBody PayInvoiceModel model) {
        PayInvoice domain = BeanUtil.copy(model, PayInvoice.class);
        List<PayInvoice> list = payInvoiceService.selectListByDomain(domain);
        List<PayInvoiceModel> modelList = BeanUtil.copy(list, PayInvoiceModel.class);
        return ResultMessage.success(modelList);
    }
}