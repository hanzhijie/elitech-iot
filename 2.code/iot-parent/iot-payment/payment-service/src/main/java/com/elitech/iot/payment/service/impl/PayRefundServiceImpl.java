/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-24 13:11:07
*/

package com.elitech.iot.payment.service.impl;

import com.elitech.iot.payment.domain.PayRefund;
import com.elitech.iot.payment.dao.PayRefundDao;
import com.elitech.iot.payment.service.PayRefundService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 退款表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class PayRefundServiceImpl extends BaseService<PayRefund, java.math.BigInteger> implements PayRefundService {
    @Autowired
    private PayRefundDao dao;

    @Override
    protected PayRefundDao getDao() {
        return dao;
    }
}