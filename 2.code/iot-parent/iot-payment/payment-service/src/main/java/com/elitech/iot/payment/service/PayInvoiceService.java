/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/

package com.elitech.iot.payment.service;

import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.payment.domain.PayInvoice;

import com.elitech.iot.common.service.IBaseService;

import java.math.BigInteger;

/**
* 发票表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface PayInvoiceService extends IBaseService<PayInvoice, java.math.BigInteger> {

    /**
     * 确认开票
     * @param invoiceId
     * @return
     */
    ResultMessage<BigInteger> updateInvoiceStatus(BigInteger invoiceId);
}