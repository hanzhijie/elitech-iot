/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/

package com.elitech.iot.payment.service;

import com.elitech.iot.payment.domain.PayRecord;

import com.elitech.iot.common.service.IBaseService;

/**
* 支付记录.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface PayRecordService extends IBaseService<PayRecord, java.math.BigInteger> {

}