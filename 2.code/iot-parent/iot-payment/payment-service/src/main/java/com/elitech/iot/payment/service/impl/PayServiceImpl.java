package com.elitech.iot.payment.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.PayConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.common.base.util.HttpKit;
import com.elitech.iot.payment.config.AlipayConfig;
import com.elitech.iot.payment.config.PayType;
import com.elitech.iot.payment.config.WxpayConfig;
import com.elitech.iot.payment.dao.PayOrderDao;
import com.elitech.iot.payment.dao.PayRecordDao;
import com.elitech.iot.payment.dao.PayRefundDao;
import com.elitech.iot.payment.domain.PayOrder;
import com.elitech.iot.payment.domain.PayRecord;
import com.elitech.iot.payment.domain.PayRefund;
import com.elitech.iot.payment.service.PayService;
import com.elitech.iot.payment.utils.WXSignUtils;
import com.github.wxpay.sdk.WXPayUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class PayServiceImpl implements PayService {
    private static final Logger logger = LoggerFactory.getLogger(PayServiceImpl.class);
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private WxpayConfig wxpayConfig;
    @Autowired
    private AlipayConfig alipayConfig;
    @Autowired
    private AlipayClient alipayClient;
    @Autowired
    private PayOrderDao payOrderDao;
    @Autowired
    private PayRecordDao payRecordDao;
    @Autowired
    private PayRefundDao payRefundDao;
    /**
     * 微信统一下单地址
     */
    private static String unifiedOrderUrl = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    /**
     * 申请退款地址（微信）
     */
    public static String refundUrl = "https://api.mch.weixin.qq.com/secapi/pay/refund";
    /**
     * 微信回调通知成功，微信不会再回调
     */
    private static String RETURNSUCCESS = "SUCCESS";

    @Override
    public ResultMessage createOrderWechat(PayType payType, String outTradeNo, Integer totalFee, String payDesc) throws Exception {
        Map<String, String> reqParams = new HashMap<>();
        //微信分配的小程序ID
        reqParams.put("appid", wxpayConfig.getAppId());
        //微信支付分配的商户号
        reqParams.put("mch_id", wxpayConfig.getMchId());
        String nonce_str = RandomStringUtils.randomAlphanumeric(32);
        //随机字符串
        reqParams.put("nonce_str", nonce_str);
        //签名类型
        reqParams.put("sign_type", "MD5");
        //充值订单 商品描述
        reqParams.put("body", payDesc);

        //商户订单号
        reqParams.put("out_trade_no", outTradeNo);
        //订单总金额，单位为分
        reqParams.put("total_fee", totalFee + "");
        //终端IP
        reqParams.put("spbill_create_ip", WXSignUtils.getRemortIP(request));
        //通知地址
        reqParams.put("notify_url", wxpayConfig.getNotifyUrl());
        //交易类型
        reqParams.put("trade_type", payType.getValue());
        //用户标识
//        reqParams.put("openid", openid);
        //签名
        String sign = WXSignUtils.createSign(reqParams, wxpayConfig.getKey());
        reqParams.put("sign", sign);
        /*
            调用支付定义下单API,返回预付单信息 prepay_id
         */
        String xmlResult = HttpKit.post(unifiedOrderUrl, WXSignUtils.toXml(reqParams));
        Map<String, String> result = WXSignUtils.xmlToMap(xmlResult);
        if (result != null) {
            if (PayConstant.WECHAT_STATUS_SUCCESS.equalsIgnoreCase(result.get("return_code")) && PayConstant.WECHAT_STATUS_SUCCESS.equalsIgnoreCase(result.get("result_code"))) {
                //预付单信息
                String prepay_id = result.get("prepay_id");
                Map<String, String> packageParams = new HashMap<String, String>();
                packageParams.put("appId", wxpayConfig.getAppId());
                packageParams.put("timeStamp", System.currentTimeMillis() / 1000 + "");
                packageParams.put("nonceStr", nonce_str);
                packageParams.put("package", wxpayConfig.getWxPackage());
                packageParams.put("prepayid", prepay_id);
                packageParams.put("partnerid", wxpayConfig.getMchId());
                packageParams.put("qrCode", result.get("code_url"));
                String packageSign = WXPayUtil.generateSignature(packageParams, wxpayConfig.getKey());
                packageParams.put("paySign", packageSign);
                ResultMessage resultMessage = ResultMessage.success(packageParams);
                return resultMessage;
            }
            logger.info("createOrderWechat is eeror;message:{}", result.get("err_code_des"));
            return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }

        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    @Override
    public Map<String, String> refundOrderWechat(String outTradeNo, String outRefundNo, Integer totalFee, Integer refundFee) throws Exception {
        Map<String, String> reqParams = new HashMap<>();
        //微信分配的appID
        reqParams.put("appid", wxpayConfig.getAppId());
        //微信支付分配的商户号
        reqParams.put("mch_id", wxpayConfig.getMchId());
        String nonceStr = RandomStringUtils.randomAlphanumeric(32);
        //随机字符串
        reqParams.put("nonce_str", nonceStr);
        //签名类型
        reqParams.put("sign_type", "MD5");
        //商户订单号
        reqParams.put("out_trade_no", outTradeNo);
        //退款单号
        reqParams.put("out_refund_no", outRefundNo);
        //订单总金额，单位为分
        reqParams.put("total_fee", totalFee + "");
        //退款金额，单位为分
        reqParams.put("refund_fee", refundFee + "");
        //签名
        String sign = WXSignUtils.createSign(reqParams, wxpayConfig.getKey());
        reqParams.put("sign", sign);
//        wxPay.refund()

        if (wxpayConfig.getSslContext() == null) {
            wxpayConfig.initSSLContext();
        }
        //调用退款
        String xmlResult = HttpKit.postRefund(refundUrl, WXSignUtils.toXml(reqParams), wxpayConfig.getSslContext().getSocketFactory());
        Map<String, String> result = WXSignUtils.xmlToMap(xmlResult);
        return result;
    }

    @Override
    public String weChatNotify() {
        Map<String, String> map = new HashMap<>();
        String xmlMsg = HttpKit.readData(request);
        Map<String, String> resultMap = WXSignUtils.xmlToMap(xmlMsg);
        try {
            boolean b = WXSignUtils.verifyNotify(resultMap, wxpayConfig.getKey());
            if (!b) {
                map.put("return_code", "FAIL");
                map.put("return_msg", "return_code不正确");
                return WXSignUtils.toXml(map);
            }
        } catch (Exception e) {
            map.put("return_code", "FAIL");
            map.put("return_msg", "return_code不正确");
            return WXSignUtils.toXml(map);
        }
        if (RETURNSUCCESS.equals(resultMap.get("return_code"))) {
            String orderNo = resultMap.get("out_trade_no");
            //用户标识
            String openId = resultMap.get("openid");
            //微信订单id
            String transactionId = resultMap.get("transaction_id");

            /**
             *  通过订单号 修改数据库中的记录
             */
            PayOrder payOrder = updatePayOrder(orderNo, PayConstant.PAY_TYPE_WECHAT, openId);
            if (payOrder != null) {
                saveRecord(payOrder, xmlMsg, transactionId);
                map.put("return_code", "SUCCESS");
                map.put("return_msg", "OK");
                return WXSignUtils.toXml(map);
            }
            map.put("return_code", "error");
            map.put("return_msg", "更新订单失败");
            return WXSignUtils.toXml(map);
        }
        map.put("return_code", "error");
        map.put("return_msg", "支付失败");
        return WXSignUtils.toXml(map);
    }

    /**
     * 更新订单状态
     *
     * @param orderNo
     * @param payType
     */
    private PayOrder updatePayOrder(String orderNo, Byte payType, String tradeUser) {
        PayOrder payOrder = new PayOrder();
        payOrder.setOrderCode(orderNo);
        List<PayOrder> payOrders = payOrderDao.selectListByDomain(payOrder);
        if (payOrders != null && payOrders.size() > 0) {
            LocalDateTime nowZeroTime = DateTimeUtils.getNowZeroTime();
            payOrder = payOrders.get(0);
            payOrder.setPayStatus(PayConstant.PAY_STATUS_PAID);
            payOrder.setPayTime(nowZeroTime);
            payOrder.setPayType(payType);
            payOrder.setGmtModified(nowZeroTime);
            payOrder.setPayAccount(tradeUser);
            //修改订单状态
            int res = payOrderDao.updateByPk(payOrder);
            return payOrder;
        } else {
            //订单不存在
            return null;
        }
    }

    /**
     * 保存支付记录
     *
     * @param payOrder 订单信息
     * @param body     返回信息
     * @return
     */
    private int saveRecord(PayOrder payOrder, String body, String tradeNo) {
        LocalDateTime nowZeroTime = DateTimeUtils.getNowZeroTime();
        PayRecord record = new PayRecord();
        record.setId(BigInteger.valueOf(100));
        record.setGmtCreate(nowZeroTime);
        record.setOrderId(payOrder.getOrderId());
        record.setTradeNo(tradeNo);
        record.setResultBody(body);
        //保存支付记录
        int res = payRecordDao.insert(record);
        return res;
    }

    @Override
    public String createOrderAli(String orderNo, Integer amount, String body) throws AlipayApiException {
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setSubject(body);
        model.setOutTradeNo(orderNo);
        String totalAmount = String.valueOf(new BigDecimal(amount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
        model.setTotalAmount(totalAmount);
        model.setProductCode("QUICK_MSECURITY_PAY");
        model.setPassbackParams("公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数");

        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradeAppPayRequest ali_request = new AlipayTradeAppPayRequest();
        ali_request.setBizModel(model);
        // 回调地址
        ali_request.setNotifyUrl(alipayConfig.getNotifyUrl());
        ali_request.setReturnUrl(alipayConfig.getReturnUrl());
        //web版调用
        AlipayTradeAppPayResponse ali_response = alipayClient.pageExecute(ali_request);
        //app调用
        // AlipayTradeAppPayResponse ali_response = alipayClient.sdkExecute(ali_request);
        //就是orderString 可以直接给客户端请求，无需再做处理。

        PayOrder payOrder = new PayOrder();
        payOrder.setOrderCode(orderNo);
        List<PayOrder> payOrders = payOrderDao.selectListByDomain(payOrder);
        //保存支付记录
        if (payOrders != null && payOrders.size() > 0) {
            saveRecord(payOrders.get(0), ali_response.toString(), ali_response.getTradeNo());
        }
        return ali_response.getBody();
    }

    @Override
    public boolean notifyAli(String tradeStatus, String orderNo, String tradeNo, String sellerId) {
        if (PayConstant.TRADE_STATUS_SUCCESS.equals(tradeStatus)) {
            //修改订单状态并返回结果，不需要验证业务状态
            PayOrder payOrder = updatePayOrder(orderNo, PayConstant.PAY_TYPE_ALI, sellerId);
            if (payOrder != null) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean returnAli(String tradeStatus, String orderNo, String tradeNo) {
        PayOrder payOrder = new PayOrder();
        payOrder.setOrderCode(orderNo);
        List<PayOrder> payOrders = payOrderDao.selectListByDomain(payOrder);
        if (payOrders != null && payOrders.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean rsaCheckAli() throws AlipayApiException, UnsupportedEncodingException {
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            //乱码解决
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean verifyResult = AlipaySignature.rsaCheckV1(params, alipayConfig.getAlipayPublicKey(), alipayConfig.getCharset(), alipayConfig.getSignType());
        return verifyResult;
    }

    @Override
    public ResultMessage refundOrderAli(String orderNo, Integer amount, String refundReason) {

        AlipayTradeRefundModel model = new AlipayTradeRefundModel();
        // 商户订单号
        model.setOutTradeNo(orderNo);
        // 退款金额
        model.setRefundAmount(String.valueOf(amount));
        // 退款原因
        model.setRefundReason(refundReason);
        //创建请求对象
        AlipayTradeRefundRequest alipayRequest = new AlipayTradeRefundRequest();
        alipayRequest.setBizModel(model);
        //返回对象
        AlipayTradeRefundResponse alipayResponse = null;
        try {
            alipayResponse = alipayClient.execute(alipayRequest);
        } catch (AlipayApiException e) {
        }
        if (alipayResponse != null) {
            String code = alipayResponse.getCode();
            String subCode = alipayResponse.getSubCode();
            String subMsg = alipayResponse.getSubMsg();
            if (PayConstant.ALI_COMMON_CODE_SUCCESS.equals(code)
                    && StringUtils.isBlank(subCode)
                    && StringUtils.isBlank(subMsg)) {
                // 表示退款申请接受成功，结果通过退款查询接口查询
                // 修改用户订单状态为退款
                return ResultMessage.success("订单退款成功");
            }
            return ResultMessage.fail(ResultCode.PAY_RESULT_ERROR);
        }
        return ResultMessage.fail(ResultCode.PAY_REFUND_ERROR);
    }

    @Override
    public ResultMessage refundApply(String orderCode, Integer refundFee, String refundReason) {
        PayOrder payOrder = new PayOrder();
        payOrder.setOrderCode(orderCode);
        ResultMessage resultMessage = ResultMessage.success(null);
        //根据订单号查询订单信息
        List<PayOrder> payOrders = payOrderDao.selectListByDomain(payOrder);
        //订单号不存在，直接返回
        if (payOrders == null || payOrders.isEmpty()) {
            return ResultMessage.fail(ResultCode.PAY_ORDER_NOT_EXIST);
        }
        payOrder = payOrders.get(0);
        PayRefund refund = new PayRefund();
        refund.setOrderId(payOrder.getOrderId());
        //查询退款申请是否已创建
        List<PayRefund> payRefunds = payRefundDao.selectListByDomain(refund);
        if (payRefunds != null && payRefunds.size() > 0) {
            return ResultMessage.fail(ResultCode.PAY_REFUND_APPLY_EXIST);
        }
        //创建退款订单
        refund.setGmtCreate(DateTimeUtils.getNowZeroTime());
        refund.setRefundCode(RandomStringUtils.randomAlphanumeric(32));
        refund.setRefundFee(payOrder.getPayFee());
        refund.setRefundReason(refundReason);
        refund.setRefundStatus(PayConstant.REFUND_STATUS_NO);
        refund.setRefundId(BigInteger.valueOf(100));
        int res = payRefundDao.insert(refund);
        if (res > 0) {
            return resultMessage;
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    @Override
    public ResultMessage refunOrder(String refundCode) {
        PayRefund refund = new PayRefund();
        refund.setRefundCode(refundCode);
        //查询是否有退款申请
        List<PayRefund> payRefunds = payRefundDao.selectListByDomain(refund);
        if (payRefunds == null || payRefunds.isEmpty()) {
            return ResultMessage.fail(ResultCode.PAY_REFUND_APPLY_NOT_EXIST);
        }
        //退款订单
        refund = payRefunds.get(0);
        //验证是否已退款
        if (PayConstant.REFUND_STATUS_YES.equals(refund.getRefundStatus())) {
            return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }
        PayOrder payOrder = payOrderDao.selectByPk(refund.getOrderId());
        ResultMessage resultMessage = ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        if (payOrder != null) {
            Byte payType = payOrder.getPayType();
            try {
                //微信退款
                if (PayConstant.PAY_TYPE_WECHAT.equals(payType)) {
                    Map<String, String> json = this.refundOrderWechat(payOrder.getOrderCode(), refund.getRefundCode(), payOrder.getPayFee(), refund.getRefundFee());
                    if (PayConstant.WECHAT_STATUS_SUCCESS.equals(json.get("return_code")) && PayConstant.WECHAT_STATUS_SUCCESS.equals(json.get("result_code"))) {
                        resultMessage = ResultMessage.success("微信退款成功");
                    }
                } else if (PayConstant.PAY_TYPE_ALI.equals(payType)) {
                    //阿里退款
                    resultMessage = this.refundOrderAli(payOrder.getOrderCode(), refund.getRefundFee(), refund.getRefundReason());
                }
                //退款成功更新订单与退款申请状态
                if (resultMessage.isSuccess()) {
                    refund.setRefundStatus(PayConstant.REFUND_STATUS_YES);
                    refund.setRefundTime(DateTimeUtils.getNowZeroTime());
                    refund.setGmtModified(DateTimeUtils.getNowZeroTime());
                    payRefundDao.updateByPk(refund);
                    payOrder.setPayStatus(PayConstant.PAY_STATUS_REFUND);
                    payOrderDao.updateByPk(payOrder);
                    return ResultMessage.success("退款成功");
                }
            } catch (Exception e) {
                return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
            }
        }
        return resultMessage;
    }

}
