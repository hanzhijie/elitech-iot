/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:37
*/

package com.elitech.iot.payment.dao;

import com.elitech.iot.payment.domain.PayRecord;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 支付记录.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface PayRecordDao extends IBaseDao<PayRecord, java.math.BigInteger> {

}