/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/

package com.elitech.iot.payment.dao;

import com.elitech.iot.payment.domain.PayOrder;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 订单表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface PayOrderDao extends IBaseDao<PayOrder, java.math.BigInteger> {

}