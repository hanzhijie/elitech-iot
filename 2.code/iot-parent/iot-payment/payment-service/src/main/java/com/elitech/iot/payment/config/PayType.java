package com.elitech.iot.payment.config;

import lombok.Getter;

@Getter
/**
 * 支付类型
 */
public enum PayType {
    //微信网页端支付
    WECHAT_GONGZHONGHAO("JSAPI"),
    //微信网页端扫码支付
    WECHAT_SWEEPCODE("NATIVE"),
    //微信app支付
    WECHAT_APP("APP"),
    //支付宝网页端支付
    ALI_WEB("aliweb"),
    //支付宝app端支付
    ALI_APP("aliapp"),
    //paypal支付
    PAYPAI("PAYPAI"),
    //谷歌支付
    GOOGLEPAY("GOOGLEPAY"),
    //苹果支付
    APPLEPAY("APPLEPAY"),
    //paypal支付手机版
    APPPAYPAI("APP_PAYPAI"),
    //谷歌支付手机版
    APPGOOGLEPAY("APP_GOOGLEPAY"),
    //苹果支付手机版
    APPAPPLEPAY("APP_APPLEPAY");

    /**
     * 支付类型值
     */
    private String value;

    PayType(String value) {
        this.value = value;
    }
}
