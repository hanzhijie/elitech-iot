/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/

package com.elitech.iot.payment.service.impl;

import com.elitech.iot.payment.domain.PayRecord;
import com.elitech.iot.payment.dao.PayRecordDao;
import com.elitech.iot.payment.service.PayRecordService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 支付记录.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class PayRecordServiceImpl extends BaseService<PayRecord, java.math.BigInteger> implements PayRecordService {
    @Autowired
    private PayRecordDao dao;

    @Override
    protected PayRecordDao getDao() {
        return dao;
    }
}