/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 13:14:38
 */

package com.elitech.iot.payment.service.impl;

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.common.service.BaseService;
import com.elitech.iot.payment.dao.PayInvoiceDao;
import com.elitech.iot.payment.dao.PayOrderDao;
import com.elitech.iot.payment.domain.PayInvoice;
import com.elitech.iot.payment.domain.PayOrder;
import com.elitech.iot.payment.service.PayInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

/**
 * 发票表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class PayInvoiceServiceImpl extends BaseService<PayInvoice, java.math.BigInteger> implements PayInvoiceService {
    @Autowired
    private PayInvoiceDao dao;
    @Autowired
    private PayOrderDao payOrderDao;

    @Override
    protected PayInvoiceDao getDao() {
        return dao;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultMessage<BigInteger> updateInvoiceStatus(BigInteger invoiceId) {
        //查询是否存在发票信息
        PayInvoice payInvoice = dao.selectByPk(invoiceId);
        if (payInvoice == null) {
            return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }
        PayOrder order = new PayOrder();
        order.setInvoiceId(invoiceId);
        //查询订单信息
        List<PayOrder> payOrders = payOrderDao.selectListByDomain(order);
        if (payOrders != null && payOrders.size() > 0) {
            PayOrder payOrder = payOrders.get(0);
            payOrder.setInvoiceStatus(true);
            payInvoice.setInvoiceTime(DateTimeUtils.getNowZeroTime());
            //更新开票时间
            int data = dao.updateByPk(payInvoice);
            //更新开票状态
            int res = payOrderDao.updateByPk(payOrder);
            if (data + res > 1) {
                return ResultMessage.success(res);
            }
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }
}