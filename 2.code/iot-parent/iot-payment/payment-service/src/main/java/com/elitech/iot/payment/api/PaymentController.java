package com.elitech.iot.payment.api;

import com.alipay.api.AlipayApiException;
import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.PayConstant;
import com.elitech.iot.payment.config.AlipayConfig;
import com.elitech.iot.payment.config.PayType;
import com.elitech.iot.payment.domain.PayOrder;
import com.elitech.iot.payment.model.WechatPayResult;
import com.elitech.iot.payment.service.PayOrderService;
import com.elitech.iot.payment.service.PayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * 支付接口
 */
@RestController
@RequestMapping("payment")
@RefreshScope
@Api(tags = {"Payment API"})
public class PaymentController {
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    @Autowired
    private PayService payService;
    @Autowired
    private PayOrderService payOrderService;
    @Autowired
    private AlipayConfig alipayConfig;

    @ApiOperation(value = "微信充值,扫码支付", notes = "微信充值")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderCode", value = "账单编号", required = true, dataType = "String")
           })
    @PostMapping(value = "wechatPay")
    public ResultMessage<WechatPayResult> wechatWebPay(HttpServletRequest request, @RequestParam String orderCode) {
        List<PayOrder> list = getPayOrders(orderCode);
        if (list == null || list.isEmpty()) {
          return ResultMessage.fail(ResultCode.PAY_ORDER_NOT_EXIST);
        }
        PayOrder payOrder = list.get(0);
        /*
            生成订单...
         */
        ResultMessage resultMessage;
        try {
            resultMessage= payService.createOrderWechat(PayType.WECHAT_SWEEPCODE, orderCode, payOrder.getPayFee(),  payOrder.getOrderDescription());
        } catch (Exception e) {
            resultMessage = ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }
        return resultMessage;
    }

    @ApiOperation(value = "创建阿里支付", notes = "创建阿里支付,返回form对象直接输出页面")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderCode", value = "账单编号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orderDesc", value = "订单描述", required = true, dataType = "String"),
            @ApiImplicitParam(name = "totalFee", value = "订单金额，单位（分）", dataType = "int")})
    @PostMapping(value = "aliWebPay")
    public void aliWebPay(HttpServletResponse response, @RequestParam String orderCode, @RequestParam String orderDesc, @RequestParam Integer totalFee) {
        List<PayOrder> list = getPayOrders(orderCode);
        if (list != null && list.size() > 0) {
            PayOrder payOrder = list.get(0);
            //生成订单...
            try {
                String form = payService.createOrderAli(orderCode, payOrder.getPayFee(), payOrder.getOrderDescription());
                response.setContentType("text/html;charset=" + alipayConfig.getCharset());
                //直接将完整的表单html输出到页面
                response.getWriter().write(form);
                response.getWriter().flush();
                response.getWriter().close();
            } catch (Exception e) {
                logger.error("aliWebPay is eeror;message:{}",e.getMessage(),e);
            }
        }
    }

    private List<PayOrder> getPayOrders(String orderCode) {
        PayOrder order = new PayOrder();
        order.setOrderCode(orderCode);
        //根据订单编码查询订单信息
        return payOrderService.selectListByDomain(order);
    }


    @ApiOperation(value = "申请退款", notes = "申请退款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderCode", value = "订单编号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "refundFee", value = "退款金额", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "refundReason", value = "退款原因", required = true, dataType = "String")})
    @PostMapping(value = "refundApply")
    public ResultMessage<Object> refundApply(@RequestParam String orderCode, @RequestParam Integer refundFee, @RequestParam String refundReason) {
        /*
            退款申请...
         */
        ResultMessage resultMessage = payService.refundApply(orderCode, refundFee, refundReason);
        return resultMessage;
    }

    @ApiOperation(value = "退款", notes = "退款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "refundCode", value = "退款单号", required = true, dataType = "String")})
    @PostMapping(value = "refundOrder")
    public ResultMessage<Object> refundOrder(@RequestParam String refundCode) {
        /*
            退款...
         */
        ResultMessage resultMessage = payService.refunOrder(refundCode);
        return resultMessage;
    }

    @ApiOperation(value = "微信支付回调接口", notes = "微信支付回调接口")
    @PostMapping(value = "wechatCallBack")
    public String wechatCallBack(HttpServletRequest request) {
        String ntify = payService.weChatNotify();
        return ntify;
    }

    @ApiOperation(value = "支付宝支付回调接口(异步)", notes = "支付宝支付回调接口（异步）")
    @PostMapping(value = "aliNotifyBack")
    public String aliNotifyBack(HttpServletRequest request) {
        try {
            //验签，确认交易合法性
            boolean rs = payService.rsaCheckAli();
            if (rs) {
                // 交易状态
                String tradeStatus = request.getParameter("trade_status");
                // 商户订单号
                String outTradeNo = request.getParameter("out_trade_no");
                // 支付宝订单号
                String tradeNo = request.getParameter("trade_no");
                //收款支付宝账号对应的支付宝唯一用户号
                String sellerId = request.getParameter("seller_id");
                boolean notify = payService.notifyAli(tradeStatus, outTradeNo, tradeNo, sellerId);
                if (notify) {
                    return "success";
                }
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "fail";
    }

    @ApiOperation(value = "支付宝支付回调接口(同步)", notes = "支付宝支付回调接口（同步）")
    @PostMapping(value = "aliReturnBack")
    public String aliReturnBack(HttpServletRequest request) {
        try {
            //验签，确认交易合法性
            boolean rs = payService.rsaCheckAli();
            if (rs) {
                return "success";
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "fail";
    }
}
