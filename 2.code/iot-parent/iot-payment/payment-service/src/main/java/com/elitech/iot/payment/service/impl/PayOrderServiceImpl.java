/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 13:14:38
 */

package com.elitech.iot.payment.service.impl;

import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.PayConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.common.service.BaseService;
import com.elitech.iot.payment.dao.PayInvoiceDao;
import com.elitech.iot.payment.dao.PayOrderDao;
import com.elitech.iot.payment.domain.PayInvoice;
import com.elitech.iot.payment.domain.PayOrder;
import com.elitech.iot.payment.model.PayOrderInvoiceModel;
import com.elitech.iot.payment.service.PayOrderService;
import com.elitech.iot.uid.client.LeafUidClient;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Objects;

/**
 * 订单表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class PayOrderServiceImpl extends BaseService<PayOrder, java.math.BigInteger> implements PayOrderService {
    @Autowired
    private PayOrderDao payOrderDao;
    @Autowired
    private PayInvoiceDao invoiceDao;
    @Autowired
    private LeafUidClient leafUidClient;

    @Override
    protected PayOrderDao getDao() {
        return payOrderDao;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BigInteger createOrder(PayOrderInvoiceModel model) {
        PayOrder domain = BeanUtil.copy(model, PayOrder.class);
        //生成订单id
        BigInteger orderId = createOrderId();
        domain.setOrderId(orderId);
        //设置订单编号
        domain.setOrderCode(RandomStringUtils.randomNumeric(32));
        //设置为未支付
        domain.setPayStatus(PayConstant.PAY_STATUS_UNPAD);
        domain.setGmtCreate(DateTimeUtils.getNowZeroTime());
        //是否需要开票
        if (domain.getApplyFlag()) {
            PayInvoice invoice = BeanUtil.copy(model, PayInvoice.class);
            //生成发票id
            BigInteger invoiceId = createInvoiceId();
            invoice.setInvoiceId(invoiceId);
            invoice.setGmtCreate(DateTimeUtils.getNowZeroTime());
            //新增发票信息
            invoiceDao.insert(invoice);
            //设置发票id
            domain.setInvoiceId(invoiceId);
        }

        int res = payOrderDao.insert(domain);
        return orderId;
    }

    private BigInteger createOrderId() {
        while (true) {
            ResultMessage<Long> paymentOrder = leafUidClient.getSegmentKey("payment_order");
            BigInteger id = BigInteger.valueOf(paymentOrder.getData());
            System.out.println(id);
            PayOrder payOrder = payOrderDao.selectByPk(id);
            if (Objects.isNull(payOrder)) {
                return id;
            }
        }
    }

    private BigInteger createInvoiceId() {
        while (true) {
            ResultMessage<Long> paymentInvoice = leafUidClient.getSegmentKey("payment_invoice");
            BigInteger id = BigInteger.valueOf(paymentInvoice.getData());
            System.out.println(id);
            PayInvoice payInvoice = invoiceDao.selectByPk(id);
            if (Objects.isNull(payInvoice)) {
                return id;
            }
        }
    }
}