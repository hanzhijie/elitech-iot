/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-24 13:11:06
*/

package com.elitech.iot.payment.dao;

import com.elitech.iot.payment.domain.PayRefund;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 退款表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface PayRefundDao extends IBaseDao<PayRefund, java.math.BigInteger> {

}