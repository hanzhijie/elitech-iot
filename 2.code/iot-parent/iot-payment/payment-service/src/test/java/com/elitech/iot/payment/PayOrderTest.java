package com.elitech.iot.payment;

import com.elitech.iot.payment.model.PayOrderInvoiceModel;
import com.elitech.iot.payment.service.PayOrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/14
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PaymentApplication.class})
public class PayOrderTest {

    @Autowired
    private PayOrderService orderService;
    /**
     * 插入数据
     */
    @Test
    public void createOrderTest() {
        PayOrderInvoiceModel model = new PayOrderInvoiceModel();
        model.setOrderCode("weqweqwe123wewqeqwe12321321ewe");
        model.setOrderId(BigInteger.valueOf(100));
        BigInteger order = orderService.createOrder(model);

    }


}
