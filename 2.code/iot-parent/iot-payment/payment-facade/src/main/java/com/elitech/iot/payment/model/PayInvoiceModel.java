/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 13:14:38
 */

package com.elitech.iot.payment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * 发票表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
@Getter
@Setter
@ApiModel(description = "发票表")
public class PayInvoiceModel implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 发票id.
     */
    @NotNull(message = "{com.elitech.iot.payment.model.PayInvoice.invoiceId.NotNUll}")
    @ApiModelProperty(value = "发票id")
    private java.math.BigInteger invoiceId;

    /**
     * 开票类型，1-个人，2-企业.
     */
    @NotNull(message = "{com.elitech.iot.payment.model.PayInvoice.invoiceType.NotNUll}")
    @ApiModelProperty(value = "开票类型，1-个人，2-企业")
    private Byte invoiceType;

    /**
     * 个人姓名.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.personalName.Length}")
    @ApiModelProperty(value = "个人姓名")
    private String personalName;

    /**
     * 公司名称.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.companyName.Length}")
    @ApiModelProperty(value = "公司名称")
    private String companyName;

    /**
     * 纳税人识别号.
     */
    @Length(max = 20, message = "{com.elitech.iot.payment.model.PayInvoice.taxNumber.Length}")
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNumber;

    /**
     * 公司地址.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.companyAddress.Length}")
    @ApiModelProperty(value = "公司地址")
    private String companyAddress;

    /**
     * 电话号码.
     */
    @Length(max = 20, message = "{com.elitech.iot.payment.model.PayInvoice.phone.Length}")
    @ApiModelProperty(value = "电话号码")
    private String phone;

    /**
     * 开户银行.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.bankDeposit.Length}")
    @ApiModelProperty(value = "开户银行")
    private String bankDeposit;

    /**
     * 银行帐号.
     */
    @Length(max = 20, message = "{com.elitech.iot.payment.model.PayInvoice.bankAccount.Length}")
    @ApiModelProperty(value = "银行帐号")
    private String bankAccount;

    /**
     * 开票时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "开票时间")
    private java.time.LocalDateTime invoiceTime;

    /**
     * 发票金额，单位（分）.
     */
    @ApiModelProperty(value = "发票金额，单位（分）")
    private Integer invoiceAmount;

    /**
     * 币种（对应字典表）.
     */
    @Length(max = 50, message = "{com.elitech.iot.payment.model.PayInvoice.dictCurrency.Length}")
    @ApiModelProperty(value = "币种（对应字典表）")
    private String dictCurrency;

    /**
     * 创建时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

    /**
     * 修改时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private BigInteger accountId;
}