/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-24 13:11:06
 */

package com.elitech.iot.payment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 退款表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Getter
@Setter
@ApiModel(description = "退款表")
public class PayRefundModel implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 创建时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

    /**
     * 修改时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;

    /**
     * 订单id.
     */
    @NotNull(message = "{com.elitech.iot.payment.model.PayRefund.orderId.NotNUll}")
    @ApiModelProperty(value = "订单id")
    private java.math.BigInteger orderId;

    /**
     * 退款编码.
     */
    @Length(max = 32, message = "{com.elitech.iot.payment.model.PayRefund.refundCode.Length}")
    @ApiModelProperty(value = "退款编码")
    private String refundCode;

    /**
     * 退款金额(分).
     */
    @ApiModelProperty(value = "退款金额(分)")
    private Integer refundFee;

    /**
     * 退款id.
     */
    @NotNull(message = "{com.elitech.iot.payment.model.PayRefund.refundId.NotNUll}")
    @ApiModelProperty(value = "退款id")
    private java.math.BigInteger refundId;

    /**
     * 退款原因.
     */
    @Length(max = 200, message = "{com.elitech.iot.payment.model.PayRefund.refundReason.Length}")
    @ApiModelProperty(value = "退款原因")
    private String refundReason;

    /**
     * 退款状态，0-未退款，1-已退款.
     */
    @ApiModelProperty(value = "退款状态，0-未退款，1-已退款")
    private byte refundStatus;

    /**
     * 退款时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "退款时间")
    private java.time.LocalDateTime refundTime;
}