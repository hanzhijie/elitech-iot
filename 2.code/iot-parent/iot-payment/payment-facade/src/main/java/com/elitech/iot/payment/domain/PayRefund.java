/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-24 13:11:06
*/

package com.elitech.iot.payment.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 退款表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class PayRefund implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
    /**
    * 订单id.
    */
    private java.math.BigInteger orderId;
    /**
    * 退款编码.
    */
    private String refundCode;
    /**
    * 退款金额(分).
    */
    private Integer refundFee;
    /**
    * 退款id.
    */
    private java.math.BigInteger refundId;
    /**
    * 退款原因.
    */
    private String refundReason;
    /**
    * 退款状态，0-未退款，1-已退款.
    */
    private byte refundStatus;
    /**
    * 退款时间.
    */
    private java.time.LocalDateTime refundTime;
}