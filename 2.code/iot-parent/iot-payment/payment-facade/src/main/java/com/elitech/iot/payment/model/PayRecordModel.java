/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 13:14:37
 */

package com.elitech.iot.payment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 支付记录.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Getter
@Setter
@ApiModel(description = "支付记录")
public class PayRecordModel implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * .
     */
    @NotNull(message = "{com.elitech.iot.payment.model.PayRecord.id.NotNUll}")
    @ApiModelProperty(value = "")
    private java.math.BigInteger id;

    /**
     * 订单id.
     */
    @ApiModelProperty(value = "订单id")
    private java.math.BigInteger orderId;

    /**
     * 支付方订单编号.
     */
    @Length(max = 30, message = "{com.elitech.iot.payment.model.PayRecord.tradeNo.Length}")
    @ApiModelProperty(value = "支付方订单编号")
    private String tradeNo;

    /**
     * 支付返回结果.
     */
    @Length(max = 5000, message = "{com.elitech.iot.payment.model.PayRecord.resultBody.Length}")
    @ApiModelProperty(value = "支付返回结果")
    private String resultBody;

    /**
     * 创建时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @NotNull(message = "{com.elitech.iot.payment.model.PayRecord.gmtCreate.NotNUll}")
    @ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

    /**
     * 修改时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}