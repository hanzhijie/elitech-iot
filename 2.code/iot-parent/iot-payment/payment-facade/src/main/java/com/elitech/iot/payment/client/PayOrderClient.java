/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/
package com.elitech.iot.payment.client;

import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.payment.domain.PayOrder;
import com.elitech.iot.payment.model.PayOrderInvoiceModel;
import com.elitech.iot.payment.model.PayOrderModel;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;




/**
* 订单表 FeignClient.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2020-03-18 13:14:38
* @since v1.0.0
*/
@FeignClient(name = "payment-service", path="/payment")
@Component
public interface PayOrderClient {

    /**
     * 创建订单
     * @param payOrderInvoiceModel
     * @return
     */
    @PostMapping(value = "/PayOrder/createOrder", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResultMessage<BigInteger> createOrder(@RequestBody PayOrderInvoiceModel payOrderInvoiceModel);
    /**
    *  新增订单表.
    *
    * @param model 要新增的数据。
    * @return 返回是否新增成功。
    */
    @PostMapping(path = "/insert")
    ResultMessage<PayOrderModel> insert(@RequestBody PayOrderModel model);

    /**
    *  批量新增订单表.
    *
    * @param list
    *            要新增的实体对象列表
    * @return 返回是否新增成功（如果成功则全部成功，否则全部失败）。
    */
    @PostMapping(path = "/insert/list")
    ResultMessage<List<PayOrderModel>> insertList(@RequestBody List<PayOrderModel> list);

    /**
        *  删除订单表.
        *
    * @param pk
    *            要删除的数据主键
    * @return 返回删除的行数。
    */
    @DeleteMapping(path = "/delete")
    ResultMessage<Integer> deleteByPk(@RequestParam("pk") java.math.BigInteger pk);

    /**
    * 根据id批量删除.
    *
    * @param pkList
    *            要删除数据的主键列表
    * @return
    */
    @DeleteMapping(path = "/delete/list")
    ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList);

    /**
    * 根据主键更新实体对象.
    *
    * @param domain 要更新的实体对象
    * @return 返回更新的行数。
    */
    @PutMapping(value = "/update")
    ResultMessage<java.math.BigInteger> updateByPk(@RequestBody PayOrderModel domain);

    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @GetMapping(value = "/get")
    ResultMessage<PayOrder> selectByPk(@RequestParam("pk")  BigInteger pk);

    /**
    * 根据id列表查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @PostMapping(value = "/get/list")
    ResultMessage<List<PayOrder>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList);

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param whereMap 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @PostMapping(value = "/get/page")
    ResultMessage<PageInfo<PayOrder>> pageListByWhere(@RequestParam("start") int start, @RequestParam("pageSize") int pageSize, @RequestBody Map<String, Object> whereMap);
}
