/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/

package com.elitech.iot.payment.domain;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 发票表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class PayInvoice implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 发票id.
    */
    private java.math.BigInteger invoiceId;
    /**
    * 开票类型，1-个人，2-企业.
    */
    private Byte invoiceType;
    /**
    * 个人姓名.
    */
    private String personalName;
    /**
    * 公司名称.
    */
    private String companyName;
    /**
    * 纳税人识别号.
    */
    private String taxNumber;
    /**
    * 公司地址.
    */
    private String companyAddress;
    /**
    * 电话号码.
    */
    private String phone;
    /**
    * 开户银行.
    */
    private String bankDeposit;
    /**
    * 银行帐号.
    */
    private String bankAccount;
    /**
    * 开票时间.
    */
    private java.time.LocalDateTime invoiceTime;
    /**
    * 发票金额，单位（分）.
    */
    private Integer invoiceAmount;
    /**
    * 币种（对应字典表）.
    */
    private String dictCurrency;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
    /**
     * 用户id
     */
    private BigInteger accountId;
}