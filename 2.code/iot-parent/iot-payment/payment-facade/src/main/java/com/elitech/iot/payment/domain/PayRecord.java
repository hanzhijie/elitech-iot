/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:37
*/

package com.elitech.iot.payment.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 支付记录.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class PayRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * .
    */
    private java.math.BigInteger id;
    /**
    * 订单id.
    */
    private java.math.BigInteger orderId;
    /**
    * 支付方订单编号.
    */
    private String tradeNo;
    /**
    * 支付返回结果.
    */
    private String resultBody;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}