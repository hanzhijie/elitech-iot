/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-03-18 13:14:38
*/

package com.elitech.iot.payment.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;

/**
* 订单表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class PayOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 订单id.
    */
    private java.math.BigInteger orderId;
    /**
    * 订单类型1校准服务2XXX.
    */
    private Byte orderType;
    /**
    * 订单编号.
    */
    private String orderCode;
    /**
    * 订单金额（分）.
    */
    private Integer orderFee;
    /**
    * 订单描述.
    */
    private String orderDescription;
    /**
    * 币种（对应字典表）.
    */
    private String dictCurrency;
    /**
    * 支付类型1.微信支付，2-支付宝支付
    */
    private Byte payType;
    /**
    * 支付账户.
    */
    private String payAccount;
    /**
    * 支付时间.
    */
    private java.time.LocalDateTime payTime;
    /**
    * 订单状态.订单状态：1-未支付，2-已支付，3-已退款
    */
    private Byte payStatus;
    /**
    * 支付金额(分).
    */
    private Integer payFee;
    /**
    * 是否开票,0-不开票，1-开票
    */
    private Boolean applyFlag;
    /**
    * 发票id.
    */
    private java.math.BigInteger invoiceId;
    /**
    * 开票状态:0-未开票,1-已开票
    */
    private Boolean invoiceStatus;
    /**
    * 收据编号.
    */
    private String receiptCode;
    /**
    * 账单地址.
    */
    private String billAddress;
    /**
    * 国家码.
    */
    private String countryCode;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
     * 用户id
     */
    private BigInteger accountId;
}