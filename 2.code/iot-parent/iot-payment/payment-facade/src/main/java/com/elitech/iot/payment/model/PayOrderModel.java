/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 13:14:38
 */

package com.elitech.iot.payment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * 订单表.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Getter
@Setter
@ApiModel(description = "订单表")
public class PayOrderModel implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 订单id.
     */
    @NotNull(message = "{com.elitech.iot.payment.model.PayOrder.orderId.NotNUll}")
    @ApiModelProperty(value = "订单id")
    private java.math.BigInteger orderId;

    /**
     * 订单类型1校准服务2XXX.
     */
    @ApiModelProperty(value = "订单类型1校准服务2XXX")
    private Byte orderType;

    /**
     * 订单编号.
     */
    @Length(max = 32, message = "{com.elitech.iot.payment.model.PayOrder.orderCode.Length}")
    @ApiModelProperty(value = "订单编号")
    private String orderCode;

    /**
     * 订单金额（分）.
     */
    @ApiModelProperty(value = "订单金额（分）")
    private Integer orderFee;

    /**
     * 订单描述.
     */
    @Length(max = 200, message = "{com.elitech.iot.payment.model.PayOrder.orderDescription.Length}")
    @ApiModelProperty(value = "订单描述")
    private String orderDescription;

    /**
     * 币种（对应字典表）.
     */
    @Length(max = 50, message = "{com.elitech.iot.payment.model.PayOrder.dictCurrency.Length}")
    @ApiModelProperty(value = "币种（对应字典表）")
    private String dictCurrency;

    /**
     * 支付类型1.微信支付，2-支付宝支付
     */
    @ApiModelProperty(value = "支付类型1.微信支付，2-支付宝支付")
    private Byte payType;

    /**
     * 支付账户.
     */
    @Length(max = 50, message = "{com.elitech.iot.payment.model.PayOrder.payAccount.Length}")
    @ApiModelProperty(value = "支付账户")
    private String payAccount;

    /**
     * 支付时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "支付时间")
    private java.time.LocalDateTime payTime;

    /**
     * 订单状态.订单状态：1-未支付，2-已支付，3-已退款
     */
    @ApiModelProperty(value = "订单状态.订单状态：1-未支付，2-已支付，3-已退款")
    private Byte payStatus;

    /**
     * 支付金额(分).
     */
    @ApiModelProperty(value = "支付金额(分)")
    private Integer payFee;

    /**
     * 是否开票,0-不开票，1-开票
     */
    @ApiModelProperty(value = "是否开票,0-不开票，1-开票")
    private boolean applyFlag;

    /**
     * 发票id.
     */
    @ApiModelProperty(value = "发票id")
    private java.math.BigInteger invoiceId;

    /**
     * 开票状态:0-未开票,1-已开票
     */
    @ApiModelProperty(value = "开票状态:0-未开票,1-已开票")
    private boolean invoiceStatus;

    /**
     * 收据编号.
     */
    @Length(max = 20, message = "{com.elitech.iot.payment.model.PayOrder.receiptCode.Length}")
    @ApiModelProperty(value = "收据编号")
    private String receiptCode;

    /**
     * 账单地址.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayOrder.billAddress.Length}")
    @ApiModelProperty(value = "账单地址")
    private String billAddress;

    /**
     * 国家码.
     */
    @Length(max = 10, message = "{com.elitech.iot.payment.model.PayOrder.countryCode.Length}")
    @ApiModelProperty(value = "国家码")
    private String countryCode;

    /**
     * 修改时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;

    /**
     * 创建时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private BigInteger accountId;
}