package com.elitech.iot.payment.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName WechatPayResult
 * @Description: 微信支付返回参数
 * @Author user
 * @Date 2020/3/17
 * @Version V1.0
 **/
@Data
@ApiModel(description = "微信支付返回参数")
public class WechatPayResult {
    /**
     * 微信支付二维码
     */
    @ApiModelProperty(value = "支付二维码")
    private String qrCode;
}
