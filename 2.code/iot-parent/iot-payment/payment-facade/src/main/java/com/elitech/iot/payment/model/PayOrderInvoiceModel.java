/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-03-18 13:14:38
 */

package com.elitech.iot.payment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * 订单发票对象.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Getter
@Setter
@ApiModel(description = "订单发票对象")
public class PayOrderInvoiceModel extends PayOrderModel implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 开票类型，1-个人，2-企业.
     */
    @ApiModelProperty(value = "开票类型，1-个人，2-企业")
    private Byte invoiceType;
    /**
     * 个人姓名.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.personalName.Length}")
    @ApiModelProperty(value = "个人姓名")
    private String personalName;

    /**
     * 公司名称.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.companyName.Length}")
    @ApiModelProperty(value = "公司名称")
    private String companyName;

    /**
     * 纳税人识别号.
     */
    @Length(max = 20, message = "{com.elitech.iot.payment.model.PayInvoice.taxNumber.Length}")
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNumber;

    /**
     * 公司地址.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.companyAddress.Length}")
    @ApiModelProperty(value = "公司地址")
    private String companyAddress;

    /**
     * 电话号码.
     */
    @Length(max = 20, message = "{com.elitech.iot.payment.model.PayInvoice.phone.Length}")
    @ApiModelProperty(value = "电话号码")
    private String phone;

    /**
     * 开户银行.
     */
    @Length(max = 100, message = "{com.elitech.iot.payment.model.PayInvoice.bankDeposit.Length}")
    @ApiModelProperty(value = "开户银行")
    private String bankDeposit;

    /**
     * 银行帐号.
     */
    @Length(max = 20, message = "{com.elitech.iot.payment.model.PayInvoice.bankAccount.Length}")
    @ApiModelProperty(value = "银行帐号")
    private String bankAccount;

    /**
     * 开票时间.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    @ApiModelProperty(value = "开票时间")
    private java.time.LocalDateTime invoiceTime;

    /**
     * 发票金额，单位（分）.
     */
    @ApiModelProperty(value = "发票金额，单位（分）")
    private Integer invoiceAmount;

}