package com.elitech.iot.data.cassandra;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
@ConditionalOnExpression("'${database.ts.type}'=='cassandra'")
public @interface NoSqlDao {

}
