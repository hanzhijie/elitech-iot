package com.elitech.iot.data.cassandra;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
@Profile("install")
@NoSqlDao
@Component("CassandraInstallCluster")
public class CassandraInstallCluster extends AbstractCassandraCluster {
    @PostConstruct
    public void init() {
        super.init(null);
    }
}
