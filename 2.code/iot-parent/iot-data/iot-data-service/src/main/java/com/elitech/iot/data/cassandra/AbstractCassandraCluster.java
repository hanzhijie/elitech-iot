package com.elitech.iot.data.cassandra;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.util.StringUtils;

import javax.annotation.PreDestroy;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
public abstract class AbstractCassandraCluster {
    /**
     * 集群列表分隔符(逗号).
     */
    private static final String COMMA = ",";

    /**
     * 服务器地址、端口分隔符(分号).
     */
    private static final String COLON = ":";

    /**
     * Cassandra配置.
     */
    @Autowired
    private CassandraConfigurationProperties cassandraConfigurationProperties;

    /**
     * Cassandra socket配置.
     */
    @Autowired
    private CassandraSocketOptions socketOptions;

    /**
     * Cassandra 查询配置.
     */
    @Autowired
    private CassandraQueryOptions queryOptions;

    /**
     * 环境变量.
     */
    @Autowired
    private Environment environment;

    /**
     * Cassandra 集群.
     */
    private Cluster cluster;

    /**
     * Cassandra 集群构建器.
     */
    private Cluster.Builder clusterBuilder;

    /**
     * Cassandra 连接会话.
     */
    private Session session;

    /**
     *
     */
    private MappingManager mappingManager;

    /**
     * @param clz
     * @param <T>
     * @return
     */
    public <T> Mapper<T> getMapper(Class<T> clz) {
        return mappingManager.mapper(clz);
    }

    /**
     * keyspace名称.
     */
    private String keyspaceName;

    /**
     * @return
     */
    public String getKeyspaceName() {
        return this.keyspaceName;
    }

    /**
     * 初始化cassandra 集群.
     *
     * @param keyspaceName keyspace名称
     */
    protected void init(String keyspaceName) {
        this.keyspaceName = keyspaceName;
        PoolingOptions poolingOpts = new PoolingOptions().setMaxConnectionsPerHost(HostDistance.LOCAL, cassandraConfigurationProperties.getMaxRequestsLocal())
                .setMaxRequestsPerConnection(HostDistance.REMOTE, cassandraConfigurationProperties.getMaxRequestsRemote());

        this.clusterBuilder = Cluster.builder()
                .addContactPointsWithPorts(getContactPoints(cassandraConfigurationProperties.getUrl()))
                .withClusterName(cassandraConfigurationProperties.getClusterName())
                .withSocketOptions(socketOptions.getOpts())
                .withPoolingOptions(poolingOpts)
                .withQueryOptions(queryOptions.getOpts())
                .withCompression(StringUtils.isEmpty(cassandraConfigurationProperties.getCompression()) ? ProtocolOptions.Compression.NONE : ProtocolOptions.Compression.valueOf(cassandraConfigurationProperties.getCompression().toUpperCase()));
        if (cassandraConfigurationProperties.isSsl()) {
            this.clusterBuilder.withSSL();
        }

        if (!cassandraConfigurationProperties.isJmx()) {
            this.clusterBuilder.withoutJMXReporting();
        }

        if (!cassandraConfigurationProperties.isMetrics()) {
            this.clusterBuilder.withoutMetrics();
        }

        if (cassandraConfigurationProperties.isCredentials()) {
            this.clusterBuilder.withCredentials(cassandraConfigurationProperties.getUsername(), cassandraConfigurationProperties.getPassword());
        }

        if (!isInstall()) {
            initSession();
        }
    }

    public Session getSession() {
        if (!isInstall()) {
            return this.session;
        } else {
            if (this.session == null) {
                initSession();
            }

            return this.session;
        }
    }

    private void initSession() {
        long endTime = System.currentTimeMillis() + cassandraConfigurationProperties.getInitTimeoutMs();
        while (System.currentTimeMillis() < endTime) {
            try {
                cluster = clusterBuilder.build();
                cluster.init();

                if (this.keyspaceName != null) {
                    session = cluster.connect(this.keyspaceName);
                } else {
                    session = cluster.connect();
                }

                DefaultPropertyMapper propertyMapper = new DefaultPropertyMapper();
                propertyMapper.setPropertyAccessStrategy(PropertyAccessStrategy.FIELDS);
                MappingConfiguration configuration = MappingConfiguration.builder().withPropertyMapper(propertyMapper).build();
                mappingManager = new MappingManager(session, configuration);
                break;
            } catch (Exception ex) {
                try {
                    Thread.sleep(cassandraConfigurationProperties.getInitRetryInterval());
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    private boolean isInstall() {
        return environment.acceptsProfiles(Profiles.of("install"));
    }

    private Collection<InetSocketAddress> getContactPoints(String url) {
        List<InetSocketAddress> socketAddressList;
        if (StringUtils.isEmpty(url)) {
            socketAddressList = Collections.emptyList();
        } else {
            socketAddressList = new ArrayList<>();
            for (String hostPort : url.split(COMMA)) {
                String host = hostPort.split(COLON)[0];
                Integer port = Integer.valueOf(hostPort.split(COLON)[1]);
                socketAddressList.add(new InetSocketAddress(host, port));
            }
        }

        return socketAddressList;
    }

    @PreDestroy
    public void close() {
        if (cluster != null) {
            cluster.close();
        }
    }


    /**
     * 获取默认的读一致性等级.
     *
     * @return
     */
    public ConsistencyLevel getDefaultReadConsistencyLevel() {
        return queryOptions.getDefaultReadConsistencyLevel();
    }

    /**
     * 获取默认的写一致性等级.
     *
     * @return
     */
    public ConsistencyLevel getDefaultWriteConsistencyLevel() {
        return queryOptions.getDefaultWriteConsistencyLevel();
    }
}
