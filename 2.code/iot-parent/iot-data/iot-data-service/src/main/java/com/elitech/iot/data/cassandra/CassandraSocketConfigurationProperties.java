package com.elitech.iot.data.cassandra;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "cassandra.socket")
public class CassandraSocketConfigurationProperties {
    /**
     * 连接超时(毫秒).
     */
    private int connectTimeoutMillis;

    /**
     * 连接超时(毫秒).
     */
    private int readTimeoutMillis;

    /**
     * 是否保持连接.
     */
    private Boolean keepAlive;

    /**
     * 是否复用地址.
     */
    private Boolean reuseAddress;

    /**
     *
     */
    private Integer soLinger;

    /**
     *
     */
    private Boolean tcpNoDelay;

    /**
     * 接收缓冲区大小.
     */
    private Integer receiveBufferSize;

    /**
     * 发送缓冲区大小.
     */
    private Integer sendBufferSize;
}
