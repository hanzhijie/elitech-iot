package com.elitech.iot.data.cassandra;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
@Component("CassandraCluster")
public class CassandraCluster extends AbstractCassandraCluster {
    @Value("${cassandra.keyspaceName}")
    public String keyspaceName;

    @PostConstruct
    public void init() {
        super.init(keyspaceName);
    }
}
