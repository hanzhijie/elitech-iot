package com.elitech.iot.data.cassandra;

import com.datastax.driver.core.SocketOptions;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
@Component
@Configuration
@Data
@NoSqlDao
public class CassandraSocketOptions {
    @Autowired
    private CassandraSocketConfigurationProperties configurationProperties;

    private SocketOptions opts;

    @PostConstruct
    public void initOpts() {
        opts = new SocketOptions();
        opts.setConnectTimeoutMillis(configurationProperties.getConnectTimeoutMillis());
        opts.setReadTimeoutMillis(configurationProperties.getReadTimeoutMillis());
        if (configurationProperties.getKeepAlive() != null) {
            opts.setKeepAlive(configurationProperties.getKeepAlive());
        }
        if (configurationProperties.getReuseAddress() != null) {
            opts.setReuseAddress(configurationProperties.getReuseAddress());
        }

        if (configurationProperties.getSoLinger() != null) {
            opts.setSoLinger(configurationProperties.getSoLinger());
        }

        if (configurationProperties.getTcpNoDelay() != null) {
            opts.setTcpNoDelay(configurationProperties.getTcpNoDelay());
        }

        if (configurationProperties.getReceiveBufferSize() != null) {
            opts.setReceiveBufferSize(configurationProperties.getReceiveBufferSize());
        }

        if (configurationProperties.getSendBufferSize() != null) {
            opts.setSendBufferSize(configurationProperties.getSendBufferSize());
        }
    }
}
