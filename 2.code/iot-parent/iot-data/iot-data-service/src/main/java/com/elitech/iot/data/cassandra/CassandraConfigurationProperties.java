package com.elitech.iot.data.cassandra;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "cassandra")
public class CassandraConfigurationProperties {
    /**
     * 集群名称.
     */
    private String clusterName;

    /**
     * 集群地址(逗号分隔的地址端口).
     */
    private String url;

    /**
     * 压缩方式(如：).
     */
    private String compression;

    /**
     * 是否使用ssl.
     */
    private boolean ssl;

    /**
     * 是否启用JMX.
     */
    private boolean jmx;

    /**
     * 是否启用指标监测.
     */
    private boolean metrics;

    /**
     * 是否需要认证.
     */
    private boolean credentials;

    /**
     * 用户名.
     */
    private String username;

    /**
     * 密码.
     */
    private String password;

    /**
     * 初始化超时时间(ms).
     */
    private int initTimeoutMs;

    /**
     * 初始化重试次数.
     */
    private int initRetryInterval;

    /**
     * 本地最大请求数.
     */
    private int maxRequestsLocal;

    /**
     * 远程最大请求数.
     */
    private int maxRequestsRemote;
}
