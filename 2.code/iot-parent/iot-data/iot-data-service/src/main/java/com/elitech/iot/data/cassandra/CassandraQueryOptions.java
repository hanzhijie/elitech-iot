package com.elitech.iot.data.cassandra;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.QueryOptions;
import jnr.ffi.annotations.In;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/16
 */
@Component
@Configuration
@Data
@NoSqlDao
public class CassandraQueryOptions {

    @Setter
    @Getter
    @Component
    @ConfigurationProperties(prefix = "cassandra.query")
    public class CassandraQueryConfigurationProperties {
        /**
         * 认查询分页大小.
         */
        private Integer defaultFetchSize;

        private String readConsistencyLevel;

        private String writeConsistencyLevel;
    }

    private QueryOptions opts;
    private ConsistencyLevel defaultReadConsistencyLevel;
    private ConsistencyLevel defaultWriteConsistencyLevel;

    @Autowired
    private CassandraQueryConfigurationProperties queryConfigurationProperties;

    @PostConstruct
    public void initOpts(){
        opts = new QueryOptions();
        opts.setFetchSize(queryConfigurationProperties.getDefaultFetchSize());
    }

    protected ConsistencyLevel getDefaultReadConsistencyLevel() {
        if (defaultReadConsistencyLevel == null) {
            if (queryConfigurationProperties.getReadConsistencyLevel() != null) {
                defaultReadConsistencyLevel = ConsistencyLevel.valueOf(queryConfigurationProperties.getReadConsistencyLevel().toUpperCase());
            } else {
                defaultReadConsistencyLevel = ConsistencyLevel.ONE;
            }
        }
        return defaultReadConsistencyLevel;
    }

    protected ConsistencyLevel getDefaultWriteConsistencyLevel() {
        if (defaultWriteConsistencyLevel == null) {
            if (queryConfigurationProperties.getWriteConsistencyLevel() != null) {
                defaultWriteConsistencyLevel = ConsistencyLevel.valueOf(queryConfigurationProperties.getWriteConsistencyLevel().toUpperCase());
            } else {
                defaultWriteConsistencyLevel = ConsistencyLevel.ONE;
            }
        }
        return defaultWriteConsistencyLevel;
    }
}
