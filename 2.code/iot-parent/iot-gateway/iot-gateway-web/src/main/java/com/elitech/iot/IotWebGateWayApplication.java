package com.elitech.iot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 物联网云平台网关.
 * <p>兼容2020前的v1、v2协议.</p>
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@SpringBootApplication
@Configuration
public class IotWebGateWayApplication {
    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(IotWebGateWayApplication.class, args);
    }

    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}