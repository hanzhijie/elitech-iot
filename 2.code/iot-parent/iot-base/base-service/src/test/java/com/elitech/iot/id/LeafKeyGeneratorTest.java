package com.elitech.iot.id;

import com.elitech.iot.BaseServiceApplication;
import com.elitech.iot.uid.leaf.LeafProperties;
import io.opensharding.keygen.leaf.LeafSegmentKeyGenerator;
import io.opensharding.keygen.leaf.LeafSnowflakeKeyGenerator;
import org.apache.shardingsphere.spi.algorithm.keygen.ShardingKeyGeneratorServiceLoader;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Properties;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/5
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
public class LeafKeyGeneratorTest {
    ShardingKeyGenerator snowflakeShardingKeyGenerator;
    ShardingKeyGenerator segmentShardingKeyGenerator;

    @Autowired
    private LeafProperties leafProperties;

    private Properties properties;

    String bizTag = "device";


    @Before
    public void init() {
        properties = new Properties();
        properties.setProperty("leaf.jdbc.url", leafProperties.getJdbcUrl());
        properties.setProperty("leaf.jdbc.username", leafProperties.getJdbcUsername());
        properties.setProperty("leaf.jdbc.password", leafProperties.getPassword());
        properties.setProperty("leaf.zk.list", leafProperties.getZkList());
        properties.setProperty("leaf.key", bizTag);

        snowflakeShardingKeyGenerator = new ShardingKeyGeneratorServiceLoader().newService(LeafSnowflakeKeyGenerator.TYPE, properties);
        segmentShardingKeyGenerator = new ShardingKeyGeneratorServiceLoader().newService(LeafSegmentKeyGenerator.TYPE, properties);
    }

    @Test
    public void snowflakeTest() {
        for (int i = 0; i < 10000; i++) {
            Comparable<?> key = snowflakeShardingKeyGenerator.generateKey();
            System.out.println(key);
        }
    }

    @Test
    public void segmentTest() {

        for (int i = 0; i < 10; i++) {
            Comparable<?> key = segmentShardingKeyGenerator.generateKey();
            System.out.println(key);
        }
    }
}
