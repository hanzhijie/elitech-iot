package com.elitech.iot.base;

import com.elitech.iot.BaseServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName RedisTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/10
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
@EnableAutoConfiguration
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;


    @Test
    public void redisTest1(){
        redisTemplate.opsForValue().set("dddddd", "d153", 600, TimeUnit.SECONDS);
    }

}
