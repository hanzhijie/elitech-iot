package com.elitech.iot.id;

import com.elitech.iot.BaseServiceApplication;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.uid.client.LeafUidClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/6
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
public class LeafUidClientTest {
    @Autowired
    private LeafUidClient leafUidClient;

    private int count = 10;

    @Test
    public void segmentUidTest() {
        String bizTag = "calibration_application";
        for (int i = 0; i < count; i++) {
            ResultMessage<Long> result = leafUidClient.getSegmentKey(bizTag);
            System.out.println(result.getMessage() + ": " + result.getData());
        }
        for (int i = 0; i < count; i++) {
            bizTag = "calibration_point";
            ResultMessage<Long> result = leafUidClient.getSegmentKey(bizTag);
            System.out.println(result.getMessage() + ": " + result.getData());
        }
        for (int i = 0; i < count; i++) {
            bizTag = "calibration_base_info";
            ResultMessage<Long> result = leafUidClient.getSegmentKey(bizTag);
            System.out.println(result.getMessage() + ": " + result.getData());
        }
    }

    @Test
    public void snowflakeUidTest(){
        for (int i = 0; i < count; i++) {
            ResultMessage<Long> result = leafUidClient.getSnowflakeKey();
            System.out.println(result.getMessage() + ": " + result.getData());
        }
    }
}
