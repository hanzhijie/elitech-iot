package com.elitech.iot.feign;

import com.elitech.iot.BaseServiceApplication;
import com.elitech.iot.auth.client.AuthClient;
import com.elitech.iot.auth.domain.AuthToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName FeignTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/11
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
public class FeignTest {
    @Autowired
    private AuthClient authClient;

    @Test
    public void feignTest() {
        AuthToken buyusan = authClient.getToken("budsfe123", "202cb962ac59075b964b07152d234b70");
        System.out.println(buyusan);
    }
}
