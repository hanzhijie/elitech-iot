package com.elitech.iot;

import com.elitech.iot.base.mail.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
public class SendMailTest
{
    @Autowired
    private MailService mailService;

    /**
     * @MethodName: sendMail
     * @Description: 发送邮件测试
     * @Param: []
     * @Return: void
     * @Author: dongqg
     * @Date: 2020/1/8
    **/
    @Test
    public void sendMail(){
        mailService.sendSimpleMail("65222780@qq.com","test","验证码：123456");
    }
}
