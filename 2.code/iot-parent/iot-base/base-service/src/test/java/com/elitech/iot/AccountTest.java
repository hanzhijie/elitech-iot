package com.elitech.iot;

import com.alibaba.fastjson.JSON;
import com.elitech.iot.base.dao.BaseAccountDao;
import com.elitech.iot.base.domain.BaseAccount;
import com.elitech.iot.base.service.BaseAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @ClassName AccountTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/1/8
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
@EnableAutoConfiguration
public class AccountTest {
    @Autowired
    private BaseAccountService baseAccountService;

    @Autowired
    private BaseAccountDao baseAccountDao;

    /**
     * 插入数据
     */
    @Test
    public void AccountAddTest() {
        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setId(BigInteger.valueOf(1314));
        baseAccount.setUserName("buyusan");
        baseAccount.setAccountPassword("abc");
        baseAccount.setAccountState(1);
        baseAccount.setOrganizationId(BigInteger.valueOf(2));
        baseAccount.setOrganizationPath("dd");
        baseAccount.setGmtCreate(LocalDateTime.now());
        baseAccountService.register(baseAccount);
    }

    //测试查询数据
    @Test
    public void queryAccountByIdTest(){
        BaseAccount baseAccount = baseAccountDao.selectByPk(BigInteger.valueOf(1314));
        System.out.println(JSON.toJSON(baseAccount));
    }

    //根据domain查询测试
    @Test
    public void queryByDomainTest(){
        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setEmail("719592616@qq.com");
        //baseAccount.setAccountState(1);
        List<BaseAccount> baseAccounts = baseAccountDao.selectListByDomain(baseAccount);
        System.out.println(baseAccounts);
    }

    //注册测试
    @Test
    public void accountRegisterTest(){
        BaseAccount baseAccount = new BaseAccount();
        String userName = "slkjgoie";
        String phone = "13655656656";
        //String phoneCode = "546564";
        String password = "df7a3c9867d7537f0400c9c302d06282";
        baseAccount.setUserName(userName);
        baseAccount.setPhone(phone);
        baseAccount.setAccountPassword(password);
        int register = baseAccountService.register(baseAccount);
    }

    @Test
    public void forgotPasswordTest(){
        baseAccountService.resetPassword("9445365753","df7a3c9867d7537f0400c9c302d06282");
    }
}
