package com.elitech.iot;

import com.alibaba.fastjson.JSON;
import com.elitech.iot.base.dao.BaseOrganizationDao;
import com.elitech.iot.base.domain.BaseOrganization;
import com.elitech.iot.base.domain.BaseTree;
import com.elitech.iot.base.service.BaseOrganizationService;
import com.elitech.iot.common.base.constant.SymbolConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName OrganizationTest
 * @Description: TODO
 * @Author songtao
 * @Date 2020/2/4
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
@EnableAutoConfiguration
public class OrganizationTest {
    @Autowired
    private BaseOrganizationService baseOrganizationService;

    @Autowired
    private BaseOrganizationDao baseOrganizationDao;

    /**
     * 插入数据
     */
    @Test
    public void projectAddTest() {
        BaseOrganization organization = new BaseOrganization();
        organization.setCountryCode("cn");
        organization.setDetailAddress("江苏省徐州市铜山区");
        organization.setGmtCreate(DateTimeUtils.getNowZeroTime());
        organization.setOrganizationCode("test004");
        organization.setParentId(BigInteger.valueOf(0));
//        organization.setOrganizationPath(BigInteger.valueOf(100) + SymbolConstant.COMMA_NAME);
        organization.setOrganizationName("精创电气4");
        organization.setOrganizationTypeId(1);
        baseOrganizationService.insert(organization);
    }

    /**
     * 删除组织机构
     */
    @Test
    public void organizationDelTest() {
        BaseOrganization data = baseOrganizationService.selectByPk(BigInteger.valueOf(101));
        if (data != null) {
            data.setDeletedFlag(true);
            int res = baseOrganizationService.updateByPk(data);
        }
    }

    //测试根据用户查询组织机构
    @Test
    public void queryOrganizationTest() {
        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("accountId", 100);
        List<BaseTree> baseProjects = baseOrganizationService.queryOrganizationTree(map);
        System.out.println(JSON.toJSON(baseProjects));
    }

    //根据domain查询测试
    @Test
    public void queryByDomainTest() {
        BaseOrganization organization = new BaseOrganization();
        organization.setOrganizationName("精创电气");
        List<BaseOrganization> baseOrganizations = baseOrganizationDao.selectListByDomain(organization);
        System.out.println(baseOrganizations);
    }


}
