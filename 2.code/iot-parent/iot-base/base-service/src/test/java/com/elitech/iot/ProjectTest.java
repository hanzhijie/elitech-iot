package com.elitech.iot;

import com.alibaba.fastjson.JSON;
import com.elitech.iot.base.dao.BaseProjectAccountDao;
import com.elitech.iot.base.dao.BaseProjectDao;
import com.elitech.iot.base.domain.BaseProject;
import com.elitech.iot.base.domain.BaseProjectAccount;
import com.elitech.iot.base.service.BaseProjectService;
import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.util.DateTimeUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ProjectTest
 * @Description: TODO
 * @Author songtao
 * @Date 2020/2/4
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceApplication.class})
@EnableAutoConfiguration
public class ProjectTest {
    @Autowired
    private BaseProjectService baseProjectService;
    @Autowired
    private BaseProjectDao baseProjectDao;
    @Autowired
    private BaseProjectAccountDao baseProjectAccountDao;

    /**
     * 插入数据
     */
    @Test
    public void projectAddTest() {
        BaseProject project = new BaseProject();
        project.setCountryCode("cn");
        project.setDetailAdress("江苏省徐州市铜山区");
        project.setGmtCreate(DateTimeUtils.getNowZeroTime());
        project.setOrganizationId(null);
        project.setId(BigInteger.valueOf(101));
        project.setProjectName("精创电气");
        project.setProjectDescription("精创电气");
        baseProjectDao.insert(project);
        BaseProjectAccount projectAccount = new BaseProjectAccount();
        projectAccount.setId(BigInteger.valueOf(100));
        projectAccount.setProjectId(BigInteger.valueOf(101));
        projectAccount.setAccountId(BigInteger.valueOf(100));
        baseProjectAccountDao.insert(projectAccount);
    }

    /**
     * 删除项目
     */
    @Test
    public void projectDelTest(){
        BaseProject data = baseProjectService.selectByPk(BigInteger.valueOf(101));
        if (data != null) {
            data.setDeletedFlag(true);
            int res = baseProjectService.updateByPk(data);
        }
    }

    //测试根据用户查询项目
    @Test
    public void queryProjectAccountByIdTest(){
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("accountId",100);
        List<BaseProject> baseProjects = baseProjectService.queryProjectByAccount(map);
        System.out.println(JSON.toJSON(baseProjects));
    }

    //根据domain查询测试
    @Test
    public void queryByDomainTest(){
        BaseProject project = new BaseProject();
        project.setProjectName("精创电气");
        //baseAccount.setAccountState(1);
        List<BaseProject> BaseProjects = baseProjectDao.selectListByDomain(project);
        System.out.println(BaseProjects);
    }


}
