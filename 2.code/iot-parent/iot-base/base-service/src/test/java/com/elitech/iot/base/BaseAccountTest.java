package com.elitech.iot.base;

import com.elitech.iot.BaseServiceApplication;
import com.elitech.iot.base.service.BaseAccountService;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName BaseAccountTest
 * @Description: 测试用户相关功能
 * @Author dongqg
 * @Date 2020/1/13
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment =SpringBootTest.WebEnvironment.MOCK,classes = {BaseServiceApplication.class})
@AutoConfigureMockMvc
public class BaseAccountTest {

    @Autowired
    private BaseAccountService baseAccountService;

    @Test
    public void updatePasswordTest(){
        baseAccountService.resetPassword("buyusan123","12466548");
    }
}
