package com.elitech.iot.base.sms;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.elitech.iot.common.base.constant.AccountConstant;

/**
 * @ClassName SmsAliUtil
 * @Description: 阿里大鱼发送短信
 * @Author dongqg
 * @Date 2020/1/7
 * @Version V1.0
 **/
public class SmsAliUtil {
    /**
     * 阿里大鱼app key
     */
    public final static String ALI_DAYU_KEY = "LTAIrVCv02BMvWQ2";
    /**
     * 阿里大鱼App Secret
     */
    public final static String ALI_DAYU_SECRET = "qUF2qlw1rtJuws9HKSpkC6FmEpTnIM";
    /**
     * 短信API产品名称（短信产品名固定，无需修改）
     */
    public final static String ALI_DAYU_PRODUCT = "Dysmsapi";
    /**
     * 短信API产品域名（接口地址固定，无需修改）
     */
    public final static String ALI_DAYU_DOMAIN = "dysmsapi.aliyuncs.com";
    /**
     * 短信发送地区
     */
    public final static String ALI_DAYU_REGION_ID = "cn-hangzhou";
    /**
     * signName
     */
    public final static String ALI_DAYU_INTERNATIONAL_SIGN_NAME = "Elitech";
    /**
     * signName
     */
    public final static String ALI_DAYU_SIGN_NAME = "精创冷云";
    /**
     * 用户注册模板编码(EN)
     */
    public final static String ALI_DAYU_SMS_157650122 = "SMS_157650122";
    /**
     * 设备告警模板编码(EN)
     */
    public final static String ALI_DAYU_SMS_158345122 = "SMS_158345122";
    /**
     * 设备告警模板编码(CN)
     */
    public final static String ALI_DAYU_SMS_158445602 = "SMS_158445602";
    /**
     * 高级用户设备报警通知值班人员
     */
    public final static String ALI_DAYU_SMS_158490189 = "SMS_158490189";
    /**
     * 短信余量告警(CN)
     */
    public final static String ALI_DAYU_SMS_158490510 = "SMS_158490510";
    /**
     * 设备参数修改(CN)
     */
    public final static String ALI_DAYU_SMS_158490683 = "SMS_158490683";
    /**
     * 设备状态告警(CN)
     */
    public final static String ALI_DAYU_SMS_158491355 = "SMS_158491355";
    /**
     * pod sim卡流量告警
     */
    public final static String ALI_DAYU_SMS_174813742 = "SMS_174813742";

    /**
     * 登录确认验证码
     */
    public final static String ALI_DAYU_SMS_157650124 = "SMS_157650124";


    public static final String ALI_DAYU_SMS_157650121 = "SMS_157650121";

    /**
     * @MethodName: sendCheckCode
     * @Description: 发送验证码方法，根据type选择不同的模板进行发送
     * type == 1 : login
     * type == 2 : register
     * type == 3 : forgot
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/14
     **/
    public static JSONObject sendCheckCode(String phone, String checkCode, String type) {
        DefaultProfile profile = DefaultProfile.getProfile(ALI_DAYU_REGION_ID, ALI_DAYU_KEY, ALI_DAYU_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain(ALI_DAYU_DOMAIN);
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", ALI_DAYU_REGION_ID);
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", ALI_DAYU_SIGN_NAME);
        if (AccountConstant.PHONE_CHECKCODE_LOGIN.equals(type)) {
            //登录
            request.putQueryParameter("TemplateCode", ALI_DAYU_SMS_157650124);
        }
        if (AccountConstant.PHONE_CHECKCODE_REGISTER.equals(type)) {
            //注册
            request.putQueryParameter("TemplateCode", ALI_DAYU_SMS_157650122);
        }
        if (AccountConstant.PHONE_CHECKCODE_FORGOT.equals(type)) {
            //重置密码
            request.putQueryParameter("TemplateCode", ALI_DAYU_SMS_157650121);
        }
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + checkCode + "\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            if (response != null) {
                return JSON.parseObject(response.getData());
            }
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

}
