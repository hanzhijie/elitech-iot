package com.elitech.iot.uid.leaf;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/5
 */
@Component
@RefreshScope
@Setter
@Getter
public class LeafProperties {
    @Value("${leaf.keyGenerator}")
    private String keyGenerator;

    @Value("${leaf.segment.jdbc.url}")
    private String jdbcUrl;

    @Value("${leaf.segment.jdbc.username}")
    private String jdbcUsername;

    @Value("${leaf.segment.jdbc.password}")
    private String password;

    @Value("${leaf.snowflake.zkList}")
    private String zkList;

    @Value("${leaf.snowflake.port: 8089}")
    private int port;

}
