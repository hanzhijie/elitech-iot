/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.dao;

import com.elitech.iot.base.domain.BaseProject;

import com.elitech.iot.common.dao.IBaseDao;

import java.util.List;
import java.util.Map;

/**
 * 项目.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface BaseProjectDao extends IBaseDao<BaseProject, java.math.BigInteger> {
    /**
     * 根据用户id和项目路径查询项目列表
     * @return
     */
    List<BaseProject> selectProjectByAccount(Map<String,Object> map);
}