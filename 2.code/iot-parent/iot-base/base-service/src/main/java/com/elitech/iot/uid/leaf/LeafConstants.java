package com.elitech.iot.uid.leaf;

/**
 * Leal 常量类.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/5
 */
public class LeafConstants {
    /**
     * 用于读取bizTag名称.
     */
    public final static String LEAF_KEY = "leaf.key";

    /**
     * leaf Jdbc Url 属性名称（用于Segment算法）.
     */
    public final static String LEAF_JDBC_URL = "leaf.jdbc.url";

    /**
     * leaf Jdbc username 属性名称（用于Segment算法）.
     */
    public final static String LEAF_JDBC_USERNAME = "leaf.jdbc.username";

    /**
     * leaf Jdbc password 属性名称（用于Segment算法）.
     */
    public final static String LEAF_JDBC_PASSWORD = "leaf.jdbc.password";

    /**
     * leaf zk list 属性名称（用于LeafSnowflake算法）.
     */
    public final static String LEAF_ZK_LIST = "leaf.zk.list";

    /**
     *
     */
    public final static String LEAF_SNOWFLAKE_PORT = "leaf.snowflake.port";

    public final static String SEGMENT_GENERATOR_NAME = "LEAF_SEGMENT";

    public final static String SNOWFLAKE_GENERATOR_NAME = "LEAF_SNOWFLAKE";
}

