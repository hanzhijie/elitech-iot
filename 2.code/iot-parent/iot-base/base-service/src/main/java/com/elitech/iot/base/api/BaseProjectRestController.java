/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */
package com.elitech.iot.base.api;

import com.elitech.iot.base.domain.BaseProject;
import com.elitech.iot.base.model.BaseProjectModel;
import com.elitech.iot.base.service.BaseProjectService;
import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.SymbolConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-27 09:21:38
 * @since v1.0.0
 */
@RestController
@RequestMapping("BaseProject")
@RefreshScope
@Api(tags = {"BaseProject API"})
public class BaseProjectRestController {
    @Autowired
    private BaseProjectService baseProjectService;


    /**
     * 根据用户id查询用户项目信息
     *
     * @param accountId 用户id
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据用户id查询用户项目信息", notes = "用户id查询项目", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "accountId", value = "用户id", required = true, dataType = "BigInteger"),
            @ApiImplicitParam(name = "projectPath", value = "项目路径,查询子级传此参数", required = false, dataType = "String")})
    @RequestMapping(value = "/queryProjectByAccount", method = RequestMethod.POST)
    public ResultMessage<List<BaseProject>> queryProjectByAccount(@RequestParam BigInteger accountId, @RequestParam(value = "projectPath", required = false) String projectPath) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("accountId", accountId);
        map.put("projectPath", projectPath);
        List<BaseProject> data = baseProjectService.queryProjectByAccount(map);
        return ResultMessage.success(data);
    }

    /**
     * 新增数据.
     *
     * @param model 要新增数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "项目", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @ApiResponses({@ApiResponse(code = 2201, message = "项目已存在")})
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insert(@RequestBody BaseProjectModel model) {
        BaseProject domain = BeanUtil.copy(model, BaseProject.class);
        //int data = baseProjectService.insert(domain);
        //根据项目名称查询项目是否存在
        BaseProject project = new BaseProject();
        project.setProjectName(model.getProjectName());
        project.setDeletedFlag(false);
        List<BaseProject> baseProjects = baseProjectService.selectListByDomain(project);
        if (baseProjects != null && baseProjects.size() > 0) {
            return ResultMessage.fail(ResultCode.PROJECT_EXIST);
        }
        BigInteger accountId = BigInteger.valueOf(100);
        int res = baseProjectService.addProject(domain, accountId);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 批量新增数据.
     *
     * @param list 数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "项目", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<BaseProject> list) {
        if (list == null || list.size() == 0) {
            return ResultMessage.fail(ResultCode.PARAM_ERROR);
        }
        //设置创建时间为零时区时间
        for (BaseProject project : list) {
            //获取全局id
            BigInteger id = BigInteger.valueOf(1);
            project.setId(id);
            setParentPath(project);
            project.setGmtCreate(DateTimeUtils.getNowZeroTime());
        }
        int data = baseProjectService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
     * 公用设置父节点路径方法
     *
     * @param project
     */
    private void setParentPath(BaseProject project) {
        if (project.getParentId() != null) {
            BaseProject parentProject = baseProjectService.selectByPk(project.getParentId());
            //如果父级节点不为空，将父级节点拼接上父级id作为当前节点的路径。
            if (parentProject != null && parentProject.getProjectPath() != null) {
                project.setProjectPath(parentProject.getProjectPath() + project.getParentId() + SymbolConstant.COMMA_NAME);
            } else {
                //如果父级节点为空，将父级id作为当前节点的路径。
                project.setProjectPath(project.getParentId() + SymbolConstant.COMMA_NAME);
            }
        }else {
            project.setParentId(BigInteger.valueOf(0));
        }
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "项目", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPk(@RequestParam BigInteger pk) {
        BaseProject data = baseProjectService.selectByPk(pk);
        //验证当前用户是否为项目创建者,需用户认证完成
        //TODO
        if (data == null) {
            return ResultMessage.fail(ResultCode.PARAM_ERROR);
        }
        data.setDeletedFlag(true);
        int res = baseProjectService.updateByPk(data);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param domain 要更新的实体对象
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "项目", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @ApiResponses({@ApiResponse(code = 2201, message = "项目已存在")})
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody BaseProject domain) {
        BaseProject data = baseProjectService.selectByPk(domain.getId());
        //验证当前用户是否为项目创建者,需用户认证完成
        //TODO
        //根据项目名称查询项目是否存在
        BaseProject project = new BaseProject();
        project.setProjectName(domain.getProjectName());
        project.setDeletedFlag(false);
        List<BaseProject> baseProjects = baseProjectService.selectListByDomain(project);
        //如果项目存在并且与当前项目名称不一致
        if (baseProjects != null && baseProjects.size() > 0) {
            if (!baseProjects.get(0).getProjectName().equals(domain.getProjectName())) {
                return ResultMessage.fail(ResultCode.PROJECT_EXIST);
            }
        }
        //更新项目
        if (data != null) {
            setParentPath(domain);
            domain.setGmtModified(DateTimeUtils.getNowZeroTime());
            int res = baseProjectService.updateByPk(domain);
            if (res > 0) {
                return ResultMessage.success(res);
            }
            return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }
        return ResultMessage.fail(ResultCode.PARAM_ERROR);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "项目", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResultMessage<BaseProject> selectByPk(@RequestParam BigInteger pk) {
        BaseProject data = baseProjectService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "项目", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    public ResultMessage<List<BaseProject>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<BaseProject> data = baseProjectService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "项目", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/get/page", method = RequestMethod.POST)
    public ResultMessage<PageInfo<BaseProject>> pageListByWhere(@RequestParam int start, @RequestParam int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<BaseProject> data = baseProjectService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param domain 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "项目", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage<List<BaseProject>> selectListByDomain(@RequestBody BaseProject domain) {
        List<BaseProject> data = baseProjectService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }
}