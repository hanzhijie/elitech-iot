/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service;

import com.elitech.iot.base.domain.BaseUserProfile;

import com.elitech.iot.common.service.IBaseService;

/**
* 用户偏好.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BaseUserProfileService extends IBaseService<BaseUserProfile, java.math.BigInteger> {

}