/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.dao;

import com.elitech.iot.base.domain.BaseRole;
import com.elitech.iot.common.dao.IBaseDao;

import java.math.BigInteger;
import java.util.List;

/**
 * 角色表：用于权限分组.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface BaseRoleDao extends IBaseDao<BaseRole, java.math.BigInteger> {

    /**
     * @MethodName:
     * @Description: 根据用户id查询角色列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
     **/
    List<BaseRole> queryRolesByAccountId(BigInteger id);

}