/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.domain.BaseProjectAccount;
import com.elitech.iot.base.dao.BaseProjectAccountDao;
import com.elitech.iot.base.service.BaseProjectAccountService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 项目账号关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseProjectAccountServiceImpl extends BaseService<BaseProjectAccount, java.math.BigInteger> implements BaseProjectAccountService {
    @Autowired
    private BaseProjectAccountDao dao;

    @Override
    protected BaseProjectAccountDao getDao() {
        return dao;
    }
}