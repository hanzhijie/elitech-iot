/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service;

import com.elitech.iot.base.domain.BaseRole;

import com.elitech.iot.common.service.IBaseService;

import java.util.Map;

/**
* 角色表：用于权限分组.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BaseRoleService extends IBaseService<BaseRole, java.math.BigInteger> {

    /**
     * @MethodName:
     * @Description: 新增角色
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
    **/
    int insertRole(Map map);
}