/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.dao.BasePermissionDao;
import com.elitech.iot.base.domain.BasePermission;
import com.elitech.iot.base.service.BasePermissionService;
import com.elitech.iot.common.service.BaseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* 权限列表：包括菜单、按钮、接口等权限.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BasePermissionServiceImpl extends BaseService<BasePermission, java.math.BigInteger> implements BasePermissionService {
    @Autowired
    private BasePermissionDao dao;

    @Override
    protected BasePermissionDao getDao() {
        return dao;
    }

    @Override
    public PageInfo<BasePermission> pageListByPermission(int start, int pageSize, BasePermission basePermission) {
        PageHelper.startPage(start,pageSize);
        List<BasePermission> basePermissions = dao.selectListByDomain(basePermission);
        PageInfo pageInfo = new PageInfo(basePermissions);
        return pageInfo;
    }
}