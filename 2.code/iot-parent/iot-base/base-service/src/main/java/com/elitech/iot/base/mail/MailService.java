package com.elitech.iot.base.mail;

public interface MailService {

    /**
     * 发送邮件
     * @param to 收件人
     * @param subject 标题
     * @param content 内容
     * @return 是否成功
     */
    boolean sendSimpleMail(String to, String subject, String content);

}
