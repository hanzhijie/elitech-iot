package com.elitech.iot.base.mail.impl;

import com.elitech.iot.base.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * @ClassName MailServiceImpl
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/1/7
 * @Version V1.0
 **/
@Service
public class MailServiceImpl implements MailService {

    @Value("${mail.from}")
    private String from;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public boolean sendSimpleMail(String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        //收信人
        message.setTo(to);
        //主题
        message.setSubject(subject);
        //内容
        message.setText(content);
        //发信人
        message.setFrom(from);

        try {
            mailSender.send(message);
            return true;
        } catch (MailException e) {
            e.printStackTrace();
            return false;
        }
    }
}
