/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */

package com.elitech.iot.base.dao;

import com.elitech.iot.base.domain.BaseOrganization;
import com.elitech.iot.base.domain.BaseTree;
import com.elitech.iot.common.dao.IBaseDao;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * 组织机构.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
public interface BaseOrganizationDao extends IBaseDao<BaseOrganization, java.math.BigInteger> {
    /**
     * 根据用户id和组织机构路径查询组织机构列表
     *
     * @return
     */
    List<BaseOrganization> selectOrganizationByAccount(Map<String, Object> map);

    /**
     * 根据用户id查询组织机构
     * @param id
     * @return
     */
    List<BaseOrganization> selectOrganizationByAccountId(BigInteger id);

    /**
     * 查询组织机构树
     * @param map
     * @return
     */
    List<BaseTree> selectOrganizationTree(Map<String, Object> map);
}