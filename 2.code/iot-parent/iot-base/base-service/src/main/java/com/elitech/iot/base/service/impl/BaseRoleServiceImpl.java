/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.dao.BaseRoleDao;
import com.elitech.iot.base.dao.BaseRolePermissionRelDao;
import com.elitech.iot.base.domain.BaseRole;
import com.elitech.iot.base.domain.BaseRolePermissionRel;
import com.elitech.iot.base.service.BaseRoleService;
import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 角色表：用于权限分组.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
@Service
public class BaseRoleServiceImpl extends BaseService<BaseRole, java.math.BigInteger> implements BaseRoleService {
    @Autowired
    private BaseRoleDao dao;

    @Autowired
    private BaseRolePermissionRelDao relDao;
    @Override
    protected BaseRoleDao getDao() {
        return dao;
    }

    @Override
    public int insertRole(Map map) {
        BaseRole baseRole = (BaseRole) map.get("baseRole");
        List<BigInteger> permissions = (List) map.get("permissions");
        BigInteger organizationId = (BigInteger) map.get("organizationId");
        //生成id
        StringBuilder id = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            id.append(random.nextInt(10));
        }
        baseRole.setRoleId(BigInteger.valueOf(Integer.parseInt(id.toString())));
        baseRole.setOrganizationId(organizationId);
        baseRole.setSystemFlag(false);
        baseRole.setGmtCreate(LocalDateTime.now());
        baseRole.setGmtModified(LocalDateTime.now());
        int insert = dao.insert(baseRole);
        BaseRolePermissionRel baseRolePermissionRel = new BaseRolePermissionRel();
        baseRolePermissionRel.setGmtCreate(LocalDateTime.now());
        baseRolePermissionRel.setGmtModified(LocalDateTime.now());
        baseRolePermissionRel.setRoleId(BigInteger.valueOf(Integer.parseInt(id.toString())));
        for (BigInteger permission : permissions) {
            //生成id
            StringBuilder id1 = new StringBuilder();

            for (int i = 0; i < 6; i++) {
                id1.append(random.nextInt(10));
            }
            baseRolePermissionRel.setId(BigInteger.valueOf(Integer.parseInt(id1.toString())));
            baseRolePermissionRel.setPermissionId(permission);
            int insert1 = relDao.insert(baseRolePermissionRel);
        }
        return insert;
    }
}