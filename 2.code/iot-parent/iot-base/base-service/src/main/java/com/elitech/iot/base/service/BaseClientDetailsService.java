/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service;

import com.elitech.iot.base.domain.BaseClientDetails;

import com.elitech.iot.common.service.IBaseService;

/**
* OAuth2.0客户端信息： oauth_access_token、oauth_refresh_token信息存储在re.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BaseClientDetailsService extends IBaseService<BaseClientDetails, java.math.BigInteger> {

}