/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */

package com.elitech.iot.base.dao;

import com.elitech.iot.base.domain.*;
import com.elitech.iot.common.dao.IBaseDao;

import java.math.BigInteger;
import java.util.List;

/**
 * 用户帐户.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface BaseAccountDao extends IBaseDao<BaseAccount, java.math.BigInteger> {

    /**
     * 根据实体类查询
     *
     * @param username 登录账号
     * @return List<BaseAccount> 查询结果
     */
    List<BaseAccount> selectByEmailOrPhoneOrUsername(String username);


    /**
     * @MethodName:
     * @Description: 根据用户id查询权限列表(只需按钮权限和菜单权限即可)
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
    **/
    List<BasePermission> queryPermissionsByAccountId(BigInteger id);

    /**
     * @MethodName:
     * @Description: 逻辑删除
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/21
    **/
    int cancelByPkList(List<BigInteger> pkList);

    /**
     * @MethodName:
     * @Description: 逻辑删除
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/21
    **/
    int cancelByPk(BigInteger pk);


    /**
     * @MethodName:
     * @Description:
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
    **/
    int insertInvite(AccountInviteOrganization accountInviteOrganization);


    /**
     * @MethodName:
     * @Description: 根据组织id查询申请列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/25
    **/
    List<AccountInviteOrganization> selectInviteInfoByDomain(AccountInviteOrganization accountInviteOrganization);

    /**
     * @MethodName:
     * @Description: 根据用户名查找
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/27
    **/
    BaseAccount selectByUserName(String userName);

    /**
     * @MethodName:
     * @Description: 根据邮箱查找
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/27
    **/
    BaseAccount selectByEmail(String email);

    /**
     * @MethodName:
     * @Description: 根据手机号查找
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/27
    **/
    BaseAccount selectByPhone(String phone);

    /**
     * @MethodName:
     * @Description: 根据id查找组织列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/27
    **/
    List<BaseOrganization> selectOrganizationByAccountId(BigInteger id);
}