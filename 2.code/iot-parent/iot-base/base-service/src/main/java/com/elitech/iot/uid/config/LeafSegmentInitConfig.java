package com.elitech.iot.uid.config;

import com.elitech.iot.uid.leaf.LeafConstants;
import com.elitech.iot.uid.leaf.LeafKeyGeneratorFactory;
import com.elitech.iot.uid.leaf.LeafProperties;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * Leaf segment 提前初始化.
 * <p>
 * 提前初始化，减少因为网络等初始化延迟导致生成主键key失败.
 * </p>
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/4/1
 */
@Component
public class LeafSegmentInitConfig implements InitializingBean {
    @Autowired
    private LeafProperties leafProperties;

    @Override
    public void afterPropertiesSet() throws Exception {
        Properties newProperties = new Properties();
        newProperties.setProperty(LeafConstants.LEAF_JDBC_URL, leafProperties.getJdbcUrl());
        newProperties.setProperty(LeafConstants.LEAF_JDBC_USERNAME, leafProperties.getJdbcUsername());
        newProperties.setProperty(LeafConstants.LEAF_JDBC_PASSWORD, leafProperties.getPassword());


        LeafKeyGeneratorFactory.createSnowflakeKeyGenerator(newProperties);
        LeafKeyGeneratorFactory.createSegmentKeyGenerator(newProperties);
    }
}
