/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.dao;

import com.elitech.iot.base.domain.BaseAccountRoleRel;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 用户角色关联表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface BaseAccountRoleRelDao extends IBaseDao<BaseAccountRoleRel, java.math.BigInteger> {

}