package com.elitech.iot.uid.leaf;

import io.opensharding.keygen.leaf.LeafSegmentKeyGenerator;
import io.opensharding.keygen.leaf.LeafSnowflakeKeyGenerator;
import org.apache.shardingsphere.spi.algorithm.keygen.ShardingKeyGeneratorServiceLoader;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/5
 */
public class LeafKeyGeneratorFactory {
    /**
     * segment key生成器缓存.
     */
    //private static ConcurrentHashMap<String, ShardingKeyGenerator> segmentGenMap = new ConcurrentHashMap<>(50);

    /**
     * snowflake key 生成器（由于是根据机器码、时间戳等生成因此不需要区分业务类型bizTag）.
     */
    private static LeafSnowflakeKeyGenerator snowflakeGenerator;

    private static LeafSegmentKeyGenerator segmentKeyGenerator;

    /**
     * SPI 服务加载器.
     */
    private static ShardingKeyGeneratorServiceLoader loader = new ShardingKeyGeneratorServiceLoader();

    /**
     * 获取segment key生成器.
     *
     * @return
     */
    public static LeafSegmentKeyGenerator getSegmentKeyGenerator() {
        return segmentKeyGenerator;
    }

    /**
     * 获取snowflake key 生成器.
     *
     * @return
     */
    public static LeafSnowflakeKeyGenerator getSnowflakeKeyGenerator() {
        return snowflakeGenerator;
    }

    /**
     * 获取segment key生成器.
     *
     * @param properties segment 数据库配置属性
     * @return LeafSegmentKeyGenerator
     */
    public static synchronized LeafSegmentKeyGenerator createSegmentKeyGenerator(Properties properties) {
       /* if (segmentGenMap.containsKey(bizTag)) {
            return segmentGenMap.get(bizTag);
        } else {
            ShardingKeyGenerator gen = loader.newService(LeafSegmentKeyGenerator.TYPE, properties);
            segmentGenMap.putIfAbsent(bizTag, gen);
            ((LeafSegmentKeyGenerator)gen).generateKey()

            return gen;
        }*/

        if (segmentKeyGenerator == null) {
            segmentKeyGenerator = (LeafSegmentKeyGenerator) loader.newService(LeafSegmentKeyGenerator.TYPE, properties);
            segmentKeyGenerator.initDataSourceAndIDGen(properties);
        }

        return segmentKeyGenerator;
    }

    /**
     * @param properties snowflake zookeeper 配置属性
     * @return LeafSnowflakeKeyGenerator
     */
    public static synchronized LeafSnowflakeKeyGenerator createSnowflakeKeyGenerator(Properties properties) {
        if (snowflakeGenerator == null) {
            snowflakeGenerator = (LeafSnowflakeKeyGenerator) loader.newService(LeafSnowflakeKeyGenerator.TYPE, properties);
        }

        return snowflakeGenerator;
    }
}
