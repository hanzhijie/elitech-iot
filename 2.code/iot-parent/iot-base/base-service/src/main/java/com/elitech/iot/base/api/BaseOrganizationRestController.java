/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */
package com.elitech.iot.base.api;

import com.elitech.iot.base.domain.BaseOrganization;
import com.elitech.iot.base.domain.BaseTree;
import com.elitech.iot.base.model.BaseOrganizationModel;
import com.elitech.iot.base.service.BaseOrganizationService;
import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.SymbolConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.github.pagehelper.PageInfo;
import io.netty.util.internal.ThreadLocalRandom;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 组织机构 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-27 09:21:38
 * @since v1.0.0
 */
@RestController
@RequestMapping("BaseOrganization")
@RefreshScope
@CrossOrigin
@Api(tags = {"BaseOrganization API"})
public class BaseOrganizationRestController {
    @Autowired
    private BaseOrganizationService baseOrganizationService;

    /**
     * 根据组织机构名称查询组织机构信息树.
     *
     * @param organizationId   组织机构路径,查询子级传此参数
     * @param organizationName 组织结构名称
     * @return 返回满足条件的数据。
     */
    @ApiOperation(value = "查询组织机构树", notes = "组织机构树", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "accountId", value = "用户id", required = false, dataType = "BigInteger"),
            @ApiImplicitParam(name = "organizationName", value = "组织结构名称", required = false, dataType = "String"),
            @ApiImplicitParam(name = "organizationId", value = "组织机构Id,查询子级", required = false, dataType = "String")})
    @RequestMapping(value = "/queryOrganizationTree", method = RequestMethod.POST)
    public ResultMessage<List<BaseTree>> queryOrganizationTree(@RequestParam(required = false) BigInteger accountId, @RequestParam(required = false) BigInteger organizationId, @RequestParam(required = false) String organizationName) {
        Map<String, Object> map = new HashMap<String, Object>();
        if (accountId != null) {
            map.put("accountId", accountId);
        }
        if (StringUtils.isNotBlank(organizationName)) {
            map.put("organizationName", organizationName);
        }
        //查询子级数据
        if (organizationId != null) {
            BaseOrganization baseOrganization = baseOrganizationService.selectByPk(organizationId);
            if (baseOrganization != null) {
                map.put("organizationPath", baseOrganization.getOrganizationPath());
                map.put("organizationId",organizationId);
            }
        }
        //查询树列表
        List<BaseTree> baseOrganizations = baseOrganizationService.queryOrganizationTree(map);
        return ResultMessage.success(baseOrganizations);
    }


    /**
     * 新增数据.
     *
     * @param model 要新增数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "组织机构", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insert(@RequestBody BaseOrganizationModel model) {
        BaseOrganization domain = BeanUtil.copy(model, BaseOrganization.class);
        //根据组织机构名称查询组织机构是否存在
        BaseOrganization organization = new BaseOrganization();
        organization.setOrganizationName(model.getOrganizationName());
        organization.setDeletedFlag(false);
        List<BaseOrganization> baseOrganizations = baseOrganizationService.selectListByDomain(organization);
        if (baseOrganizations != null && baseOrganizations.size() > 0) {
            return ResultMessage.fail(ResultCode.ORGANIZATION_EXIST);
        }
        //插入组织机构信息
        int res = baseOrganizationService.insert(domain);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 批量新增数据.
     *
     * @param list 数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "组织机构", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<BaseOrganization> list) {
        if (list == null || list.size() == 0) {
            return ResultMessage.fail(ResultCode.PARAM_ERROR);
        }
        //设置创建时间为零时区时间
        for (BaseOrganization organization : list) {
            //获取全局id
            BigInteger id = BigInteger.valueOf(1);
            organization.setId(id);
            organization = baseOrganizationService.setParentPath(organization);
            organization.setGmtCreate(DateTimeUtils.getNowZeroTime());
        }
        int data = baseOrganizationService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "组织机构", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPk(@RequestParam BigInteger pk) {
        BaseOrganization organization = baseOrganizationService.selectByPk(pk);
        //验证当前用户是否为组织机构项目创建者,需用户认证完成
        //TODO
        if (organization == null) {
            return ResultMessage.fail(ResultCode.PARAM_ERROR);
        }
        organization.setDeletedFlag(true);
        int res = baseOrganizationService.updateByPk(organization);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "组织机构", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = baseOrganizationService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param domain 要更新的实体对象
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "组织机构", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody BaseOrganization domain) {
        BaseOrganization data = baseOrganizationService.selectByPk(domain.getId());
        //根据项目名称查询项目是否存在
        BaseOrganization organization = new BaseOrganization();
        organization.setOrganizationName(domain.getOrganizationName());
        organization.setDeletedFlag(false);
        List<BaseOrganization> baseProjects = baseOrganizationService.selectListByDomain(organization);
        //如果项目存在并且与当前项目名称不一致
        if (baseProjects != null && baseProjects.size() > 0) {
            if (!baseProjects.get(0).getOrganizationName().equals(domain.getOrganizationName())) {
                return ResultMessage.fail(ResultCode.PROJECT_EXIST);
            }
        }
        if (data != null) {
            domain = baseOrganizationService.setParentPath(domain);
            domain.setGmtModified(DateTimeUtils.getNowZeroTime());
            int res = baseOrganizationService.updateByPk(domain);
            if (res > 0) {
                return ResultMessage.success(res);
            }
            return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
        }
        return ResultMessage.fail(ResultCode.PARAM_ERROR);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "组织机构", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResultMessage<BaseOrganization> selectByPk(@RequestParam BigInteger pk) {
        BaseOrganization data = baseOrganizationService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "组织机构", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @PostMapping(value = "/get/list")
    public ResultMessage<List<BaseOrganization>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<BaseOrganization> data = baseOrganizationService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "组织机构", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/get/page", method = RequestMethod.POST)
    public ResultMessage<PageInfo<BaseOrganization>> pageListByWhere(@RequestParam int start, @RequestParam int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<BaseOrganization> data = baseOrganizationService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param domain 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "组织机构", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage<List<BaseOrganization>> selectListByDomain(@RequestBody BaseOrganization domain) {
        List<BaseOrganization> data = baseOrganizationService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }
}