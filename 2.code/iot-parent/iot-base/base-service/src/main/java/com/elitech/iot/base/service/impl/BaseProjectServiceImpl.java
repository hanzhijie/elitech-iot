/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.dao.BaseProjectAccountDao;
import com.elitech.iot.base.domain.BaseProject;
import com.elitech.iot.base.dao.BaseProjectDao;
import com.elitech.iot.base.domain.BaseProjectAccount;
import com.elitech.iot.base.service.BaseProjectService;

import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.common.service.BaseService;
import io.netty.util.internal.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * 项目.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
@Service
public class BaseProjectServiceImpl extends BaseService<BaseProject, java.math.BigInteger> implements BaseProjectService {
    @Autowired
    private BaseProjectDao dao;
    @Autowired
    private BaseProjectAccountDao baseProjectAccountDao;

    @Override
    protected BaseProjectDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public int addProject(BaseProject project, BigInteger accountId) {
        //获取全局id
        BigInteger id = BigInteger.valueOf(ThreadLocalRandom.current().nextInt(100000));
        project.setId(id);
        if (project.getParentId() != null) {
            project.setProjectPath(project.getParentId() + "," + project.getId() + ",");
        } else {
            project.setProjectPath(project.getId() + ",");
        }
        project.setGmtCreate(DateTimeUtils.getNowZeroTime());
        //插入项目信息
        int res = dao.insert(project);
        BaseProjectAccount projectAccount = new BaseProjectAccount();
        projectAccount.setAccountId(accountId);
        projectAccount.setProjectId(id);
        //插入项目用户关系
        int res1 = baseProjectAccountDao.insert(projectAccount);
        return (res + res1) > 1 ? 1 : 0;
    }


    @Override
    public List<BaseProject> queryProjectByAccount(Map<String, Object> map) {
        return dao.selectProjectByAccount(map);
    }

}