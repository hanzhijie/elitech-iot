/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.domain.BaseAccountRoleRel;
import com.elitech.iot.base.dao.BaseAccountRoleRelDao;
import com.elitech.iot.base.service.BaseAccountRoleRelService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 用户角色关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseAccountRoleRelServiceImpl extends BaseService<BaseAccountRoleRel, java.math.BigInteger> implements BaseAccountRoleRelService {
    @Autowired
    private BaseAccountRoleRelDao dao;

    @Override
    protected BaseAccountRoleRelDao getDao() {
        return dao;
    }
}