/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.dao.BaseOrganizationDao;
import com.elitech.iot.base.domain.BaseOrganization;
import com.elitech.iot.base.domain.BaseTree;
import com.elitech.iot.base.service.BaseOrganizationService;
import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.SymbolConstant;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.elitech.iot.common.service.BaseService;
import io.netty.util.internal.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 组织机构.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class BaseOrganizationServiceImpl extends BaseService<BaseOrganization, java.math.BigInteger> implements BaseOrganizationService {
    @Autowired
    private BaseOrganizationDao dao;

    @Override
    protected BaseOrganizationDao getDao() {
        return dao;
    }

    @Override
    public int insert(BaseOrganization baseOrganization) {
        //获取全局id
        BigInteger id = BigInteger.valueOf(ThreadLocalRandom.current().nextInt(100000));
        baseOrganization.setId(id);
        baseOrganization = setParentPath(baseOrganization);
        baseOrganization.setGmtCreate(DateTimeUtils.getNowZeroTime());
        return dao.insert(baseOrganization);
    }

    @Override
    public List<BaseOrganization> queryOrganizationByAccount(Map<String, Object> map) {
        return dao.selectOrganizationByAccount(map);
    }

    @Override
    public List<BaseTree> queryOrganizationTree(Map<String, Object> map) {
        List<BaseTree> treeList = dao.selectOrganizationTree(map);
        if (treeList != null && treeList.size() > 0) {
            //获取查询节点
            Object orgId = map.get("organizationId");
            //默认查询根节点
            BigInteger organizationId = new BigInteger("0");
            if (orgId != null) {
                organizationId = new BigInteger(orgId.toString());
            }
            //将数据转换成树
            treeList = toTree(treeList, organizationId);
        }
        return treeList;
    }

    /**
     * list转换tree
     *
     * @param list
     * @return
     */
    private static List<BaseTree> toTree(List<BaseTree> list, BigInteger organizationId) {
        List<BaseTree> treeList = new ArrayList<BaseTree>();
        //遍历数据，查询子级时去除当前的数据
        for (BaseTree tree : list) {
            if (tree.getParentId().compareTo(BigInteger.valueOf(0)) == 0 && organizationId.compareTo(tree.getParentId()) == 0) {
                treeList.add(tree);
            }
        }
        //如果不是根节点的查询，以查询的节点为根节点
        if (treeList.size() < 1) {
            for (BaseTree tree : list) {
                if (organizationId.compareTo(tree.getParentId()) == 0) {
                    treeList.add(tree);
                }
            }
        }
        for (BaseTree tree : list) {
            toTreeChildren(treeList, tree);
        }
        return treeList;
    }

    /**
     * 递归查询下级节点
     *
     * @param treeList
     * @param tree
     */
    private static void toTreeChildren(List<BaseTree> treeList, BaseTree tree) {
        for (BaseTree node : treeList) {
            if (tree.getParentId().compareTo(node.getId()) == 0) {
                if (node.getChildren() == null) {
                    node.setChildren(new ArrayList<BaseTree>());
                }
                node.getChildren().add(tree);
            }
            if (node.getChildren() != null) {
                toTreeChildren(node.getChildren(), tree);
            }
        }
    }

    /**
     * 公用设置父节点路径方法
     *
     * @param domain
     */
    @Override
    public BaseOrganization setParentPath(BaseOrganization domain) {
        if (domain.getParentId() != null) {
            //查询父级的节点路径
            BaseOrganization parentOrganization = this.selectByPk(domain.getParentId());
            //如果父级节点不为空，将父级节点拼接上父级id作为当前节点的路径。
            if (parentOrganization != null && parentOrganization.getOrganizationPath() != null) {
                domain.setOrganizationPath(parentOrganization.getOrganizationPath() + domain.getId() + SymbolConstant.COMMA_NAME);
            } else {
                //如果父级节点为空，将父级id作为当前节点的路径。
                domain.setOrganizationPath(domain.getId() + SymbolConstant.COMMA_NAME);
            }
        } else {
            domain.setParentId(BigInteger.valueOf(0));
            domain.setOrganizationPath(domain.getId() + SymbolConstant.COMMA_NAME);
        }
        return domain;
    }

}