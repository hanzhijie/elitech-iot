/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-17 15:15:48
*/

package com.elitech.iot.base.dao;

import com.elitech.iot.base.domain.BaseDict;

import com.elitech.iot.common.dao.IBaseDao;

/**
 * 系统字典表.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 *
 */
public interface BaseDictDao extends IBaseDao<BaseDict, java.math.BigInteger> {

}