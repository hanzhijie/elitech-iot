/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service;

import com.elitech.iot.base.domain.BaseOrganization;
import com.elitech.iot.base.domain.BaseTree;
import com.elitech.iot.common.service.IBaseService;

import java.util.List;
import java.util.Map;

/**
* 组织机构.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BaseOrganizationService extends IBaseService<BaseOrganization, java.math.BigInteger> {
    /**
     * 根据用户id和组织机构路径查询组织机构列表
     * @return
     */
    List<BaseOrganization> queryOrganizationByAccount(Map<String,Object> map);

    /**
     * 查询组织机构树
     * @param map
     * @return
     */
    List<BaseTree> queryOrganizationTree(Map<String,Object> map);

    /**
     * 设置组织机构路径
     * @param domain
     * @return
     */
    BaseOrganization setParentPath(BaseOrganization domain);
}