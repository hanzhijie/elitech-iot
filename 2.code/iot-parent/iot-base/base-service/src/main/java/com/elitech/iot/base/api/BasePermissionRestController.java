/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/
package com.elitech.iot.base.api;

import com.elitech.iot.base.domain.BasePermission;
import com.elitech.iot.base.model.BasePermissionModel;
import com.elitech.iot.base.service.BasePermissionService;
import com.elitech.iot.common.base.api.ResultMessage;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

/**
* 权限列表：包括菜单、按钮、接口等权限 API.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2019-12-27 09:21:38
* @since v1.0.0
*/
@RestController
@RequestMapping("BasePermission")
@RefreshScope
@Api(tags = {"BasePermission API"})
public class BasePermissionRestController {
    @Autowired
    private BasePermissionService basePermissionService;

   /**
    * 新增权限.
    *
    * @param model 要新增数据
    * @return 返回插入行数
    */
    @ApiOperation(value = "新增数据", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<java.math.BigInteger> insert(@RequestBody BasePermissionModel model) {
        BasePermission domain = BeanUtil.copy(model, BasePermission.class);
        int data = basePermissionService.insert(domain);
        return ResultMessage.success(data);
    }

    /**
    * 批量新增数据.
    *
    * @param list 数据列表
    * @return 返回插入行数
    */
    @ApiOperation(value = "批量新增", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<BasePermission> list) {
        int data = basePermissionService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键删除实体对象.
    *
    * @param pk 主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "删除数据", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete/{pk}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<java.math.BigInteger> deleteByPk(@PathVariable BigInteger pk) {
        int data = basePermissionService.deleteByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键批量删除.
    *
    * @param pkList 要删除实体对象的主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "批量删除", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = basePermissionService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 根据id更新实体对象.
    *
    * @param domain 要更新的实体对象
    * @return 返回更新的行数。
    */
    @ApiOperation(value = "更新数据", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody BasePermission domain) {
        int data = basePermissionService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @ApiOperation(value = "根据主键查询", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get/{pk}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<BasePermission> selectByPk(@PathVariable BigInteger pk) {
        BasePermission data = basePermissionService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据多个id查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @ApiOperation(value = "根据主键列表查询", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<List<BasePermission>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<BasePermission> data = basePermissionService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param basePermission 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @ApiOperation(value = "分页查询", notes = "权限列表：包括菜单、按钮、接口等权限", httpMethod = "POST")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
        @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
        @ApiImplicitParam(name = "basePermission", value = "查询参数", dataType = "BasePermission", paramType = "body")})
    @RequestMapping(value = "/get/page", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<PageInfo<BasePermission>> pageListByWhere(@PathVariable int start, @PathVariable int pageSize, @RequestBody BasePermission basePermission) {
        PageInfo<BasePermission> data = basePermissionService.pageListByPermission(start, pageSize, basePermission);
        return ResultMessage.success(data);
    }

    /**
    * 根据实体类查询
    *
    * @param domain 实体类
    * @return List<Domain> 查询结果
    */
    @ApiOperation(value = "根据Model查询", notes = "权限列表：包括菜单、按钮、接口等权限", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('INSERT_PERMISSION')")
    public ResultMessage<List<BasePermission>> selectListByDomain(@RequestBody BasePermission domain) {
        List<BasePermission> data = basePermissionService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }
}