/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2020-02-17 15:15:49
 */
package com.elitech.iot.base.api;

import com.elitech.iot.base.domain.BaseDict;
import com.elitech.iot.base.domain.BaseOrganization;
import com.elitech.iot.base.model.BaseDictModel;
import com.elitech.iot.base.service.BaseDictService;

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.util.DateTimeUtils;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统字典表 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020-02-17 15:15:49
 * @since v1.0.0
 */
@RestController
@RequestMapping("BaseDict")
@RefreshScope
@Api(tags = {"BaseDict API"})
public class BaseDictRestController {
    @Autowired
    private BaseDictService baseDictService;

    /**
     * 根据字典类型查询字典数据
     *
     * @param dataType 字典类型
     * @return 返回指定字典类型查询字典数据。
     */
    @ApiOperation(value = "根据字典类型查询字典数据", notes = "查询字典信息", httpMethod = "POST")
    @RequestMapping(value = "/queryDictByType", method = RequestMethod.POST)
    public ResultMessage<List<BaseDictModel>> queryDictByType(@ApiParam(value = "字典类型", required = true) @RequestParam String dataType) {
        List<BaseDict> list = baseDictService.queryDictByType(dataType);
        List<BaseDictModel> modelList = BeanUtil.copy(list, BaseDictModel.class);
        return ResultMessage.success(modelList);
    }
    /**
     * 新增数据.
     *
     * @param model 要新增的数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "系统字典表", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @PostMapping(path = "/insert")
    public ResultMessage<BaseDictModel> insert(@RequestBody BaseDictModel model) {
        BaseDict domain = BeanUtil.copy(model, BaseDict.class);
        BaseDict baseDict = new BaseDict();
        baseDict.setDataType(model.getDataType());
        baseDict.setDataCode(model.getDataCode());
        List<BaseDict> baseDicts = baseDictService.selectListByDomain(baseDict);
        if (baseDicts != null && baseDicts.size() > 0) {
            return ResultMessage.fail(ResultCode.DICT_EXIST);
        }
        domain.setGmtCreate(DateTimeUtils.getNowZeroTime());
        int res = baseDictService.insert(domain);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 批量新增数据.
     *
     * @param list 要新增的数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "系统字典表", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @PostMapping(path = "/insert/list")
    public ResultMessage<List<BaseDictModel>> insertList(@RequestBody List<BaseDictModel> list) {
        List<BaseDict> domainList = BeanUtil.copy(list, BaseDict.class);
        int data = baseDictService.insertList(domainList);
        return ResultMessage.success(list);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "系统字典表", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @DeleteMapping(path = "/delete")
    ResultMessage<Integer> deleteByPk(@RequestParam("pk") BigInteger pk) {
        BaseDict baseDict = baseDictService.selectByPk(pk);
        if(baseDict==null){
            return ResultMessage.fail(ResultCode.DICT_NOT_EXIST);
        }
        baseDict.setDeletedFlag(true);
        int res = baseDictService.updateByPk(baseDict);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "系统字典表", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @DeleteMapping(path = "/delete/list")
    public ResultMessage<BigInteger> deleteByPkList(@RequestBody List<BigInteger> pkList) {
        int data = baseDictService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param model 要更新的数据
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "系统字典表", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @PutMapping(value = "/update")
    public ResultMessage<BigInteger> updateByPk(@RequestBody BaseDictModel model) {
        BaseDict domain = BeanUtil.copy(model, BaseDict.class);
        BaseDict baseDict = baseDictService.selectByPk(domain.getId());
        if(baseDict==null){
            return ResultMessage.fail(ResultCode.DICT_NOT_EXIST);
        }
        domain.setGmtModified(DateTimeUtils.getNowZeroTime());
        int res = baseDictService.updateByPk(domain);
        if (res > 0) {
            return ResultMessage.success(res);
        }
        return ResultMessage.fail(ResultCode.UNKNOWN_ERROR);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "根据主键查询", notes = "系统字典表", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @GetMapping(value = "/get")
    ResultMessage<BaseDict> selectByPk(@RequestParam("pk") BigInteger pk) {
        BaseDict data = baseDictService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "系统字典表", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @PostMapping(value = "/get/list")
    public ResultMessage<List<BaseDict>> selectByPkList(@RequestBody List<BigInteger> pkList) {
        List<BaseDict> data = baseDictService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "系统字典表", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "query"),
            @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @PostMapping(value = "/get/page")
    ResultMessage<PageInfo<BaseDict>> pageListByWhere(@RequestParam("start") int start, @RequestParam("pageSize") int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<BaseDict> data = baseDictService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
     * 根据实体类查询
     *
     * @param model 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "系统字典表", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "model", required = true)
    @PostMapping(value = "/get/model")
    public ResultMessage<List<BaseDictModel>> selectListByDomain(@RequestBody BaseDictModel model) {
        BaseDict domain = BeanUtil.copy(model, BaseDict.class);
        List<BaseDict> list = baseDictService.selectListByDomain(domain);
        List<BaseDictModel> modelList = BeanUtil.copy(list, BaseDictModel.class);
        return ResultMessage.success(modelList);
    }
}