/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/
package com.elitech.iot.base.api;

import com.elitech.iot.base.config.TokenDecode;
import com.elitech.iot.base.domain.BaseUserProfile;
import com.elitech.iot.base.model.BaseUserProfileModel;
import com.elitech.iot.base.service.BaseUserProfileService;

import com.elitech.iot.common.base.api.ResultMessage;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* 用户偏好 API.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2019-12-27 09:21:38
* @since v1.0.0
*/
@RestController
@RequestMapping("BaseUserProfile")
@RefreshScope
@Api(tags = {"BaseUserProfile API"})
public class BaseUserProfileRestController {
    @Autowired
    private BaseUserProfileService baseUserProfileService;
    @Autowired
    private TokenDecode tokenDecode;

   /**
    * 完善修改用户偏好信息.
    *
    * @param model 要新增数据
    * @return 返回插入行数
    */
    @ApiOperation(value = "完善修改用户偏好信息", notes = "用户偏好", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('user')")
    public ResultMessage<java.math.BigInteger> insert(@RequestBody BaseUserProfileModel model) {
        BaseUserProfile domain = BeanUtil.copy(model, BaseUserProfile.class);
        if (Objects.isNull(domain) || Objects.isNull(domain.getAccountId())){
            return ResultMessage.fail(1001,"参数格式不正确");
        }
        String id = tokenDecode.getUserInfo().get("id").toString();
        if (!id.equals(domain.getAccountId().toString())){
            return ResultMessage.fail(1001,"信息与登录账号不一致");
        }
        domain.setGmtCreate(LocalDateTime.now());
        domain.setGmtModified(LocalDateTime.now());

        int data = baseUserProfileService.insert(domain);
        return ResultMessage.success(data);
    }

    /**
    * 批量新增数据.
    *
    * @param list 数据列表
    * @return 返回插入行数
    */
    @ApiOperation(value = "批量新增", notes = "用户偏好", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage<java.math.BigInteger> insertList(@RequestBody List<BaseUserProfile> list) {
        int data = baseUserProfileService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键删除实体对象.
    *
    * @param pk 主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "删除数据", notes = "用户偏好", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete/{pk}", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPk(@PathVariable BigInteger pk) {
        int data = baseUserProfileService.deleteByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据主键批量删除.
    *
    * @param pkList 要删除实体对象的主键
    * @return 返回删除的行数
    */
    @ApiOperation(value = "批量删除", notes = "用户偏好", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    public ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        int data = baseUserProfileService.deleteByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 根据id更新用户偏好信息.
    *
    * @param domain 要更新的实体对象
    * @return 返回更新的行数。
    */
    @ApiOperation(value = "更新用户偏好", notes = "用户偏好", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage<java.math.BigInteger> updateByPk(@RequestBody BaseUserProfile domain) {
        String id = tokenDecode.getUserInfo().get("id").toString();
        if (!id.equals(domain.getAccountId().toString())){
            return ResultMessage.fail(1001,"信息与登录账号不一致");
        }
        domain.setGmtModified(LocalDateTime.now());
        int data = baseUserProfileService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @ApiOperation(value = "根据主键查询", notes = "用户偏好", httpMethod = "GET")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/get/{pk}", method = RequestMethod.GET)
    public ResultMessage<BaseUserProfile> selectByPk(@PathVariable BigInteger pk) {
        BaseUserProfile data = baseUserProfileService.selectByPk(pk);
        return ResultMessage.success(data);
    }

    /**
    * 根据多个id查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @ApiOperation(value = "根据主键列表查询", notes = "用户偏好", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    public ResultMessage<List<BaseUserProfile>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<BaseUserProfile> data = baseUserProfileService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param whereMap 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @ApiOperation(value = "分页查询", notes = "用户偏好", httpMethod = "POST")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
        @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
        @ApiImplicitParam(name = "whereMap", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/get/page/{start}/{pageSize}", method = RequestMethod.POST)
    public ResultMessage<PageInfo<BaseUserProfile>> pageListByWhere(@PathVariable int start, @PathVariable int pageSize, @RequestBody Map<String, Object> whereMap) {
        PageInfo<BaseUserProfile> data = baseUserProfileService.pageListByWhere(start, pageSize, whereMap);
        return ResultMessage.success(data);
    }

    /**
    * 根据实体类查询
    *
    * @param domain 实体类
    * @return List<Domain> 查询结果
    */
    @ApiOperation(value = "根据Model查询", notes = "用户偏好", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage<List<BaseUserProfile>> selectListByDomain(@RequestBody BaseUserProfile domain) {
        List<BaseUserProfile> data = baseUserProfileService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }
}