/**
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */

package com.elitech.iot.base.service;

import com.elitech.iot.auth.domain.AuthToken;
import com.elitech.iot.base.domain.*;
import com.elitech.iot.common.service.IBaseService;
import com.github.pagehelper.PageInfo;

import java.math.BigInteger;
import java.util.List;

/**
 * 用户帐户.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
public interface BaseAccountService extends IBaseService<BaseAccount, java.math.BigInteger> {

    /**
     * @MethodName: login
     * @Description: 用户名注册
     * @Param: [domain]
     * @Return: int
     * @Author: dongqg
     * @Date: 2020/1/6
     **/
    int register(BaseAccount domain);

    /**
     * @MethodName: isPhoneRegister
     * @Description: 手机号是否被注册
     * @Param: [phone]
     * @Return: boolean true注册, false未注册
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    boolean isPhoneRegister(String phone);

    /**
     * @MethodName: isUserNameRegister
     * @Description: 用户名是否被注册
     * @Param: [userName]
     * @Return: boolean true注册, false未注册
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    boolean isUserNameRegister(String userName);

    /**
     * @MethodName: isEmailRegister
     * @Description: 邮箱是否已被注册
     * @Param: [email]
     * @Return: boolean true注册, false未注册
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    boolean isEmailRegister(String email);



    /**
     * @MethodName: login
     * @Description: TODO
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
    **/
    BaseAccount login(BaseAccount account,Long ip);

    /**
     * @MethodName:
     * @Description: 根据用户id查询角色列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
    **/
    List<BaseRole> queryRolesByAccountId(BigInteger id);

    /**
     * @MethodName:
     * @Description: 根据用户id查询权限列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
    **/
    List<BasePermission> queryPermissionsByAccountId(BigInteger id);

    /**
     * @MethodName:
     * @Description: 重置密码
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/14
    **/
    boolean resetPassword(String userName, String password);

    /**
     * @MethodName: queryOrgnizationByAccountId
     * @Description: 根据id查找组织机构
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/19
     **/
    List<BaseOrganization> queryOrgnizationByAccountId(BigInteger id);

    /**
     * @MethodName:
     * @Description: 批量注销
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
    **/
    int cancelByPkList(List<BigInteger> pkList);

    /**
     * @MethodName:
     * @Description: 注销用户
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
    **/
    int cancelByPk(BigInteger pk);


    /**
     * @MethodName:
     * @Description: 查询所有用户信息（后台管理员操作）
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
    **/
    PageInfo<BaseAccount> pageListByAccount(int start, int pageSize, BaseAccount baseAccount);

    /**
     * @MethodName:
     * @Description: 个人申请加入组织
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
    **/
    int joinToOrganization(BigInteger pk, String orCode);

    /**
     * @MethodName:
     * @Description: 组织邀请个人加入
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
    **/
    int inviteJoinToOr(BigInteger pk, String userName);

    /**
     * @MethodName: 
     * @Description: 根据用户id查询邀请列表
     * @Param: 
     * @Return: 
     * @Author: dongqg
     * @Date: 2020/2/25
    **/
    List<AccountInviteOrganization> queryInviteByAccountId(BigInteger id);
    
    /**
     * @MethodName:
     * @Description: 根据组织id查询申请列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/25
    **/
    List<AccountInviteOrganization> queryApplyByOrganizationId(BigInteger id);


    /**
     * @MethodName:
     * @Description: 切换当前组织
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/27
    **/
    AuthToken switchCurrentOrganization(BigInteger pk, BigInteger organizationId);


    /**
     * 校准服务的注册调用
     * @param baseAccount
     * @return
     */
    int calibrationRegister(BaseAccount baseAccount);

    /**
     * 根据手机号获取用户id，校准服务调用
     * @param phone
     * @return
     */
    BaseAccount getAccountIdByPhone(String phone);
}