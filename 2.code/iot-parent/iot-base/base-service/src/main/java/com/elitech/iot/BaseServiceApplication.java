package com.elitech.iot;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 基础服务模块应用程序.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-27 08:35:12
 * @since v1.0.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.elitech.iot.auth.client", "com.elitech.iot.uid.client"})
@Configuration
@MapperScan(basePackages = {"com.elitech.iot.base.dao"})
public class BaseServiceApplication {
    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(BaseServiceApplication.class, args);
    }

    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

}
