/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service;

import com.elitech.iot.base.domain.BasePermission;

import com.elitech.iot.common.service.IBaseService;
import com.github.pagehelper.PageInfo;

/**
* 权限列表：包括菜单、按钮、接口等权限.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BasePermissionService extends IBaseService<BasePermission, java.math.BigInteger> {

    PageInfo<BasePermission> pageListByPermission(int start, int pageSize, BasePermission basePermission);
}