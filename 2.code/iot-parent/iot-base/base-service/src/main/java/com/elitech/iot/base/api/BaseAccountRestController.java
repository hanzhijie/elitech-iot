package com.elitech.iot.base.api;

import com.alibaba.fastjson.JSONObject;
import com.elitech.iot.auth.client.AuthClient;
import com.elitech.iot.auth.domain.AuthToken;
import com.elitech.iot.base.config.TokenDecode;
import com.elitech.iot.base.domain.*;
import com.elitech.iot.base.mail.MailService;
import com.elitech.iot.base.model.BaseAccountModel;
import com.elitech.iot.base.service.BaseAccountService;
import com.elitech.iot.base.service.BaseOrganizationService;
import com.elitech.iot.base.sms.SmsAliUtil;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.base.constant.AccountConstant;
import com.elitech.iot.common.base.util.CheckCodeUtil;
import com.elitech.iot.common.base.util.StringUtil;
import com.elitech.iot.common.base.util.VerifyCodeUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import net.dreamlu.mica.core.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * 用户帐户 API.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-27 09:21:38
 * @since v1.0.0
 */
@CrossOrigin
@RestController
@RequestMapping("BaseAccount")
@RefreshScope
@Api(tags = {"BaseAccount API"})
public class BaseAccountRestController {
    @Autowired
    private BaseAccountService baseAccountService;
    @Autowired
    private BaseOrganizationService baseOrganizationService;
    @Autowired
    private MailService mailService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private AuthClient authClient;
    @Autowired
    private TokenDecode tokenDecode;
    /**
     * 测试模式（可跳过一些短信发送，验证码等）
     */
    @Value("${com.elitech.iot.base.isTest}")
    private boolean isTest;


    /**
     * @MethodName: login
     * @Description: 登录接口
     * 该接口支持手机验证码登录，用户密码登录，通过参数type区分，1表示手机登录，2表示用户名密码
     * 用户名密码登录时，手机，邮箱，用户名都可以作为用户名，用参数username表示
     * 手机验证码登录时，需要将手机号通过phone参数和手机验证码一起传递
     * 用户名密码登录时，需要图形验证码校验，忽略大小写
     * @Param: [username, password, verCode, phone,phoneCode, type, request, response]
     * @Return: com.elitech.iot.common.base.api.ResultMessage
     * @Author: dongqg
     * @Date: 2020/2/13
     **/
    @ApiOperation(value = "用户登录", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "userName,password,verCode", required = true)
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResultMessage login(@RequestParam String userName,
                               @RequestParam String password,
                               @RequestParam String verCode,
                               HttpServletRequest request) {
        //创建登录信息对象，封装登录信息
        LoginInfo loginInfo = new LoginInfo();
        //创建用户对象，准备封装用户信息
        BaseAccount account = new BaseAccount();

        //获取ip
        Long LongId = getRemoteIP(request);
        //图片验证码校对
        //测试环境不需要验证码
        if (!isTest) {
            //验证码是否为空
            if (StringUtil.isEmpty(verCode)) {
                return ResultMessage.fail(1001, "图片验证码不能为空");
            }
            String redisVerifyCode =
                    (String) redisTemplate.opsForValue().get(AccountConstant.VERIFY_CODE_IP + LongId);
            //删除redis验证码
            redisTemplate.delete(AccountConstant.VERIFY_CODE_IP + LongId);
            //验证码是否正确
            if (StringUtil.isEmpty(redisVerifyCode) || !redisVerifyCode.equalsIgnoreCase(verCode)) {
                return ResultMessage.fail(2009, "图片验证码错误");
            }
        }

        //检查用户名格式
        String userNameStr = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
        String phonePattern = "^1[3456789]\\d{9}$";
        String emailPattern = "^[A-Za-z0-9\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
        if (!Pattern.compile(userNameStr).matcher(userName).matches() &&
                !Pattern.compile(phonePattern).matcher(userName).matches() &&
                !Pattern.compile(emailPattern).matcher(userName).matches()) {
            return ResultMessage.fail(1001, "用户名格式错误");
        }

        //检查用户名是否存在
        if (!baseAccountService.isUserNameRegister(userName) &&
                !baseAccountService.isPhoneRegister(userName) &&
                !baseAccountService.isEmailRegister(userName)) {
            return ResultMessage.fail(2001, "用户名不存在");
        }

        //判断密码格式(至少包含数字和英文，长度6-20)已加密，不需再次加密
        //String str1 = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
        if (StringUtil.isEmpty(password) || password.length() != AccountConstant.PASSWORD_LENGH) {
            return ResultMessage.fail(1001, "密码格式有误");
        }
        account.setUserName(userName);
        account.setAccountPassword(password);

        account = baseAccountService.login(account, LongId);

        if (Objects.isNull(account)) {
            //用户名密码错误
            return ResultMessage.fail(2003, "密码错误");
        }
        BaseOrganization baseOrganization = baseOrganizationService.selectByPk(account.getOrganizationId());
        List<BasePermission> permissionList = baseAccountService.queryPermissionsByAccountId(account.getId());
        AuthToken authToken = authClient.getToken(account.getUserName(), account.getAccountPassword());
        if (Objects.isNull(authToken)) {
            return ResultMessage.fail(500, "授权服务器发生错误");
        }
        redisTemplate.opsForValue().set(account.getId().toString(), authToken.getAccessToken(), 7200, TimeUnit.SECONDS);
        loginInfo.setToken(authToken.getAccessToken());
        loginInfo.setUserName(account.getUserName());
        loginInfo.setPhone(account.getPhone());
        loginInfo.setEmail(account.getEmail());
        loginInfo.setAccountPhoto(account.getAccountPhoto());
        loginInfo.setLastLoginTime(account.getLastLoginTime());
        loginInfo.setPermissions(permissionList);
        loginInfo.setBaseOrganization(baseOrganization);
        return ResultMessage.success(loginInfo);
    }
    /**
     * @MethodName: getAccountInfo
     * @Description: 获取用户信息
     * 根据令牌获取用户信息，包括用户角色权限，组织机构等信息。
     * @Param: [username]
     * @Return: com.elitech.iot.common.base.api.ResultMessage
     * @Author: dongqg
     * @Date: 2020/2/13
     **/
    @ApiOperation(value = "获取用户信息", notes = "用户帐户", httpMethod = "GET")
    @ApiParam(name = "userName", required = true)
    @RequestMapping(value = "/accountInfo", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('GET_LOGIN_INFO')")
    public ResultMessage getAccountInfo(HttpServletRequest request) {
        String accountId = tokenDecode.getUserInfo().get("id").toString();
        if (StringUtil.isEmpty(accountId)) {
            return ResultMessage.fail(2010, "未登录");
        }
        String authorization = request.getHeader("Authorization");
        String token = authorization.substring(7);
        String redisToken = (String) redisTemplate.opsForValue().get(accountId);
        if (!token.equals(redisToken)) {
            return ResultMessage.fail(2010, "token错误");
        }
        BigInteger id = BigInteger.valueOf(Long.parseLong(accountId));
        BaseAccount baseAccount = baseAccountService.selectByPk(id);
        List<BaseRole> roleList = baseAccountService.queryRolesByAccountId(id);
        List<BasePermission> permissionList = baseAccountService.queryPermissionsByAccountId(id);
        List<BaseOrganization> organizations = baseAccountService.queryOrgnizationByAccountId(id);
        AccountInfo accountInfo = new AccountInfo();
        /*
         * 封装用户信息
         * */
        accountInfo.setBaseAccount(baseAccount);
        accountInfo.setBaseRoles(roleList);
        accountInfo.setBasePermissions(permissionList);
        accountInfo.setBaseOrganizations(organizations);
        return ResultMessage.success(accountInfo);
    }

    /**
     * @MethodName: getNewVerify
     * @Description: 获取图片验证码
     * @Param: [request, response]
     * @Return: void
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    @ApiOperation(value = "生成图片验证码", notes = "用户帐户", httpMethod = "GET")
    @RequestMapping(value = "/newVerify", method = RequestMethod.GET)
    public void getNewVerify(HttpServletRequest request, HttpServletResponse response) {
        //生成随机字串
        String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
        //验证码存入redis
        //获取ip
        Long LongId = getRemoteIP(request);

        redisTemplate.opsForValue().set(AccountConstant.VERIFY_CODE_IP + LongId, verifyCode);

        // 设置相应类型,告诉浏览器输出的内容为图片
        response.setContentType("image/jpeg");
        // 设置响应头信息，告诉浏览器不要缓存此内容
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        //生成图片
        int w = 100, h = 30;
        try {
            VerifyCodeUtil.outputImage(w, h, response.getOutputStream(), verifyCode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * @MethodName: resetPassword
     * @Description: 重置密码
     * 根据type判断重置依据
     * type == 1 : phone
     * type == 2 : email
     * @Param: [type, userName]
     * @Return: com.elitech.iot.common.base.api.ResultMessage
     * @Author: dongqg
     * @Date: 2020/2/14
     **/
    @ApiOperation(value = "密码重置", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "type,username", required = true)
    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('FORGOT_PASSWORD')")
    public ResultMessage resetPassword(@RequestParam String type,
                                       @RequestParam String userName,
                                       @RequestParam String password,
                                       @RequestParam String verCode) {
        //判断类型
        if (AccountConstant.FORGOT_TYPE_PHONE.equals(type)) {
            //手机重置
            //直接去redis查找，找到说明手机号没问题
            //判断验证码合法性
            String redisCode = (String) redisTemplate.opsForValue().get(AccountConstant.FORGOT_CHECKCODE + userName);

            //校验手机验证码是否正确
            if (Objects.isNull(redisCode) || !redisCode.equals(verCode)) {
                return ResultMessage.fail(2004, "验证码错误");
            }
        } else if (AccountConstant.FORGOT_TYPE_EMAIL.equals(type)) {
            //邮件重置
            //获取redis中验证码
            String redisMailCode =
                    (String) redisTemplate.opsForValue().get(AccountConstant.FORGOT_CHECKCODE + userName);
            //比较判断
            if (StringUtil.isEmpty(redisMailCode) || !redisMailCode.equals(verCode)) {
                return ResultMessage.fail(2004, "验证码错误");
            }
        } else {
            return ResultMessage.fail(1001, "类型不存在");
        }
        boolean isSuccess = baseAccountService.resetPassword(userName, password);
        if (isSuccess) {
            return ResultMessage.success("重置成功");
        } else {
            return ResultMessage.fail(500, "服务器内部错误");
        }
    }

    /**
     * @MethodName: accountRegister
     * @Description: 用户注册api
     * 根据type判断注册类型，1表示手机注册，2表示邮箱注册
     * userName为必填项，另外手机注册需要填写手机号，注册验证码。
     * 邮箱注册需要填写邮箱地址，密码，用户名
     * @Param: [type, userName, email, password, phone, phoneCode]
     * @Return: com.elitech.iot.common.base.api.ResultMessage
     * @Author: dongqg
     * @Date: 2020/2/12
     **/
    @ApiOperation(value = "用户注册", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "type，userName", required = true)
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResultMessage accountRegister(@RequestParam String type,
                                         @RequestParam String userName,
                                         @RequestParam String password,
                                         @RequestParam(required = false) String email,
                                         @RequestParam(required = false) String phone,
                                         @RequestParam(required = false) String phoneCode,
                                         @RequestParam(required = false) String emailCode,
                                         HttpServletRequest request
    ) {
        //检查用户名格式
        String userNamePattern = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
        if (!Pattern.compile(userNamePattern).matcher(userName).matches()) {
            return ResultMessage.fail(1001, "用户名格式错误");
        }

        //是否已占用
        if (baseAccountService.isUserNameRegister(userName)) {
            return ResultMessage.fail(2002, "用户名已占用！");
        }
        //判断密码格式(至少包含数字和英文，长度6-20)已加密，不需再次加密
        //String str1 = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
        if (!StringUtil.isEmpty(password) && password.length() != AccountConstant.PASSWORD_LENGH) {
            return ResultMessage.fail(1001, "密码格式有误");
        }

        //判断注册类型
        if (AccountConstant.REGISTER_TYPE_PHONE.equals(type)) {
            //手机注册
            if (!StringUtil.isEmpty(email) ||
                    !StringUtil.isEmpty(emailCode)) {
                return ResultMessage.fail(1001, "参数不匹配");
            }
            //校验手机验证码是否正确
            if (!isTest) {
                //判断验证码合法性
                String redisCode = (String) redisTemplate.opsForValue().get(AccountConstant.REGISTER_CHECKCODE + phone);
                if (Objects.isNull(redisCode)) {
                    return ResultMessage.fail(2008, "验证码已过期");
                }
                if (!redisCode.equals(phoneCode)) {
                    return ResultMessage.fail(2004, "短信验证码错误");
                }
            }
            //校验手机号格式
            String phonePattern = "^1[3456789]\\d{9}$";
            if (!Pattern.compile(phonePattern).matcher(phone).matches()) {
                return ResultMessage.fail(1001, "手机号格式错误");
            }
            //判断手机号是否已被注册
            boolean isRegister = baseAccountService.isPhoneRegister(phone);
            if (isRegister) {
                return ResultMessage.fail(2002, "手机号已注册！");
            }
        } else if (AccountConstant.REGISTER_TYPE_EMAIL.equals(type)) {
            //邮箱注册
            if (!StringUtil.isEmpty(phone) ||
                    !StringUtil.isEmpty(phoneCode)) {
                return ResultMessage.fail(1001, "参数不匹配");
            }
            if (!isTest) {
                //获取redis中验证码
                String redisMailCode =
                        (String) redisTemplate.opsForValue().get(AccountConstant.REGISTER_CHECKCODE + email);
                //比较判断
                if (StringUtil.isEmpty(redisMailCode) || !redisMailCode.equalsIgnoreCase(emailCode)) {
                    return ResultMessage.fail(2009, "验证码错误");
                }
            }
            //判断邮箱格式
            String emailPattern = "^[A-Za-z0-9\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
            if (!Pattern.compile(emailPattern).matcher(email).matches()) {
                return ResultMessage.fail(1001, "邮箱格式错误");
            }
            //判断邮箱是否可用
            if (baseAccountService.isEmailRegister(email)) {
                return ResultMessage.fail(2002, "邮箱已被注册");
            }
        } else {
            return ResultMessage.fail(1001, "不存在的类型");
        }

        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setUserName(userName);
        baseAccount.setPhone(phone);
        baseAccount.setEmail(email);
        baseAccount.setAccountPassword(password);
        int isSeccess = baseAccountService.register(baseAccount);
        if (isSeccess == 0) {
            return ResultMessage.fail(500, "注册失败，服务器错误");
        }
        return ResultMessage.success("注册成功.");
    }


    /**
     * @MethodName: sendCheckCodeByPhone
     * @Description: 发送手机验证码统一接口，传参手机号和type
     * type == 1 : login
     * type == 2 : register
     * type == 3 : forgot
     * @Param: [phone, type]
     * @Return: com.elitech.iot.common.base.api.ResultMessage
     * @Author: dongqg
     * @Date: 2020/2/14
     **/
    @ApiOperation(value = "获取手机验证码", notes = "用户帐户", httpMethod = "GET")
    @ApiParam(name = "phone，type", required = true)
    @RequestMapping(value = "/code/phone", method = RequestMethod.GET)
    public ResultMessage sendCheckCodeByPhone(@RequestParam String phone, @RequestParam String type) {
        //校验手机号格式
        String phonePattern = "^1[3456789]\\d{9}$";
        if (!Pattern.compile(phonePattern).matcher(phone).matches()) {
            return ResultMessage.fail(1001, "手机号格式错误");
        }
        //标记，判断验证码获取是否过于频繁
        boolean hasCode = false;
        //判断短信类型
        if (AccountConstant.PHONE_CHECKCODE_LOGIN.equals(type)) {
            //登录
            //验证该手机号是否已注册
            boolean isRegister = baseAccountService.isPhoneRegister(phone);
            if (!isRegister) {
                return ResultMessage.fail(2001, "该手机未注册");
            }
            //判断验证码是否获取过于频繁
            hasCode = redisTemplate.hasKey(AccountConstant.LOGIN_CHECKCODE + phone);
            if (hasCode) {
                return ResultMessage.fail(2007, "验证码获取过于频繁！");
            }
            //生成验证码
            String checkCode = CheckCodeUtil.getCheckCodeNum(6);
            //验证码存入redis,过期时间60s
            redisTemplate.opsForValue().set(AccountConstant.LOGIN_CHECKCODE + phone, checkCode, 60, TimeUnit.SECONDS);
            JSONObject result = SmsAliUtil.sendCheckCode(phone, checkCode, type);
            if (result == null) {
                return ResultMessage.fail(500, "验证码发送失败,短信服务出现错误");
            }
            return "OK".equals(result.get("Code")) ?
                    ResultMessage.success("验证码发送成功") :
                    ResultMessage.fail(500, "验证码发送失败,短信服务出现错误");
        } else if (AccountConstant.PHONE_CHECKCODE_REGISTER.equals(type)) {
            //注册
            //判断手机号是否已被注册
            boolean isRegister = baseAccountService.isPhoneRegister(phone);
            if (isRegister) {
                return ResultMessage.fail(2002, "手机号已注册！");
            }
            //判断验证码是否获取过于频繁
            hasCode = redisTemplate.hasKey(AccountConstant.REGISTER_CHECKCODE + phone);
            if (hasCode) {
                return ResultMessage.fail(2007, "验证码获取过于频繁！");
            }
            //生成验证码（6位）
            String checkCode = CheckCodeUtil.getCheckCodeNum(6);
            //验证码存入redis,过期时间60s
            redisTemplate.opsForValue().set(AccountConstant.REGISTER_CHECKCODE + phone, checkCode, 60, TimeUnit.SECONDS);
            //发送验证码短信
            JSONObject result = SmsAliUtil.sendCheckCode(phone, checkCode, type);

            if (result == null) {
                return ResultMessage.fail(500, "验证码发送失败,短信服务出现错误");
            }
            return "OK".equals(result.get("Code")) ?
                    ResultMessage.success("验证码发送成功") :
                    ResultMessage.fail(500, "验证码发送失败,短信服务出现错误");
        } else if (AccountConstant.PHONE_CHECKCODE_FORGOT.equals(type)) {
            //密码重置
            //验证该手机号是否已注册
            boolean isRegister = baseAccountService.isPhoneRegister(phone);
            if (!isRegister) {
                return ResultMessage.fail(2001, "该手机未注册");
            }
            //判断验证码是否获取过于频繁
            hasCode = redisTemplate.hasKey(AccountConstant.FORGOT_CHECKCODE + phone);
            if (hasCode) {
                return ResultMessage.fail(2007, "验证码获取过于频繁！");
            }
            //生成验证码（6位）
            String checkCode = CheckCodeUtil.getCheckCodeNum(6);
            //验证码存入redis,过期时间60s
            redisTemplate.opsForValue().set(AccountConstant.FORGOT_CHECKCODE + phone, checkCode, 60, TimeUnit.SECONDS);
            //发送验证码短信
            JSONObject result = SmsAliUtil.sendCheckCode(phone, checkCode, type);

            if (result == null) {
                return ResultMessage.fail(500, "验证码发送失败,短信服务出现错误");
            }
            return "OK".equals(result.get("Code")) ?
                    ResultMessage.success("验证码发送成功") :
                    ResultMessage.fail(500, "验证码发送失败,短信服务出现错误");
        } else {

            return ResultMessage.fail(1001, "类型不存在");
        }
    }

    @ApiOperation(value = "获取邮箱验证码", notes = "用户帐户", httpMethod = "GET")
    @ApiParam(name = "phone", required = true)
    @RequestMapping(value = "/code/email", method = RequestMethod.GET)
    /**
     * @MethodName: sendCheckCodeByEmail
     * @Description: 发送邮件验证码统一接口，传参手机号和type
     * type == xx : login(废弃)
     * type == 1 : register
     * type == 2 : forgot
     * @Param: [email, type]
     * @Return: com.elitech.iot.common.base.api.ResultMessage
     * @Author: dongqg
     * @Date: 2020/2/14
     **/
    public ResultMessage sendCheckCodeByEmail(@RequestParam String email, @RequestParam String type) {
        //判断邮箱格式
        String emailPattern = "^[A-Za-z0-9\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
        if (!Pattern.compile(emailPattern).matcher(email).matches()) {
            return ResultMessage.fail(1001, "邮箱格式错误");
        }
        if (AccountConstant.EMAIL_CHECKCODE_REGISTER.equals(type)) {
            //注册
            //判断邮箱是否已被注册
            boolean isRegister = baseAccountService.isEmailRegister(email);
            if (isRegister) {
                return ResultMessage.fail(2001, "邮箱已注册！");
            }
            //判断验证码是否获取过于频繁（同一邮箱一分钟内只可以获取一次）
            long expireTime = redisTemplate.getExpire(
                    AccountConstant.REGISTER_CHECKCODE + email,
                    TimeUnit.SECONDS);
            if (expireTime >= 540) {
                return ResultMessage.fail(2007, "验证码获取过于频繁！");
            }
            //生成验证码（6位）
            String checkCode = CheckCodeUtil.getCheckCodeNum(6);
            //验证码存入redis,过期时间60s
            redisTemplate.opsForValue().set(
                    AccountConstant.REGISTER_CHECKCODE + email,
                    checkCode,
                    600, TimeUnit.SECONDS);
            //发送邮件
            boolean isSuccess = mailService.sendSimpleMail(email,
                    AccountConstant.EMAIL_REGISTER_SUBJECT,
                    AccountConstant.EMAIL_REGISTER_CONTENT_PREFIX +
                            checkCode +
                            AccountConstant.EMAIL_REGISTER_CONTENT_SUFFIX);

            if (!isSuccess) {
                return ResultMessage.fail(500, "验证码发送失败,邮件服务出现错误");
            }
            return ResultMessage.success("验证码发送成功");
        } else if (AccountConstant.EMAIL_CHECKCODE_FORGOT.equals(type)) {
            //重置密码
            //判断邮箱是否已被注册
            boolean isRegister = baseAccountService.isEmailRegister(email);
            if (!isRegister) {
                return ResultMessage.fail(2001, "邮箱未注册！");
            }
            //判断验证码是否获取过于频繁（同一邮箱一分钟内只可以获取一次）
            long expireTime = redisTemplate.getExpire(
                    AccountConstant.FORGOT_CHECKCODE + email,
                    TimeUnit.SECONDS);
            if (expireTime >= 540) {
                return ResultMessage.fail(2007, "验证码获取过于频繁！");
            }
            //生成验证码（6位）
            String checkCode = CheckCodeUtil.getCheckCodeNum(6);
            //验证码存入redis,过期时间60s
            redisTemplate.opsForValue().set(
                    AccountConstant.FORGOT_CHECKCODE + email,
                    checkCode,
                    600, TimeUnit.SECONDS);
            //发送邮件
            boolean isSuccess = mailService.sendSimpleMail(email,
                    AccountConstant.EMAIL_FORGOT_SUBJECT,
                    AccountConstant.EMAIL_FORGOT_CONTENT_PREFIX +
                            checkCode +
                            AccountConstant.EMAIL_FORGOT_CONTENT_SUFFIX);

            if (isSuccess) {
                return ResultMessage.fail(500, "验证码发送失败,邮件服务出现错误");
            }
            return ResultMessage.success("验证码发送成功");

        } else {
            return ResultMessage.fail(1001, "不存在的类型");
        }
    }

    /**
     * 添加子用户.
     * <p>
     * 企业管理员可以添加子用户，子用户必须是未注册的，只能单个添加
     * 添加子用户需要设置用户名和密码，子用户默认属于该组织
     *
     * @param model 要新增数据
     * @return 返回插入行数
     */
    @ApiOperation(value = "新增数据", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('')")
    public ResultMessage insert(@RequestBody BaseAccountModel model) {
        BaseAccount domain = BeanUtil.copy(model, BaseAccount.class);
        int data = baseAccountService.insert(domain);
        return ResultMessage.success(data);
    }

    /**
     * 批量新增数据.
     *
     * @param list 数据列表
     * @return 返回插入行数
     */
    @ApiOperation(value = "批量新增", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "list", required = true)
    @RequestMapping(value = "/insert/list", method = RequestMethod.POST)
    public ResultMessage insertList(@RequestBody List<BaseAccount> list) {
        int data = baseAccountService.insertList(list);
        return ResultMessage.success(data);
    }

    /**
     * 删除用户（后台超级管理员操作，物理删除）
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "删除数据", notes = "用户帐户", httpMethod = "DELETE")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyAuthority('admin')")
    public ResultMessage deleteByPk(@RequestParam BigInteger pk) {
        if (Objects.isNull(pk)) {
            return ResultMessage.fail(1001, "参数不能为空");
        }
        int data = baseAccountService.deleteByPk(pk);
        if (data >= 0) {
            return ResultMessage.success(data);
        }
        return ResultMessage.fail(500, "删除失败");
    }

    /**
     * 根据主键批量删除(后台管理员操作).
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量删除", notes = "用户帐户", httpMethod = "DELETE")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/delete/list", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyAuthority('admin')")
    public ResultMessage deleteByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        if (Objects.isNull(pkList) || pkList.size() == 0) {
            return ResultMessage.fail(1001, "参数错误");
        }
        int data = baseAccountService.deleteByPkList(pkList);
        if (data >= 0) {
            return ResultMessage.success(data);
        }
        return ResultMessage.fail(500, "删除失败");
    }


    /**
     * 注销用户（可以注销自己，可以注销组织机构下的子权限用户）
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "注销用户", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('user','admin')")
    public ResultMessage cancelByPk(@RequestParam BigInteger pk) {
        if (Objects.isNull(pk)) {
            return ResultMessage.fail(1001, "参数不能为空");
        }
        int data = baseAccountService.cancelByPk(pk);
        if (data >= 0) {

            return ResultMessage.success(data);
        }
        return ResultMessage.fail(500, "注销失败");
    }

    /**
     * 根据主键批量注销(只能注销组织机构下的账号).
     * 组织内管理员权限
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    @ApiOperation(value = "批量注销", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/cancel/list", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('admin')")
    public ResultMessage cancelByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        if (Objects.isNull(pkList) || pkList.size() == 0) {
            return ResultMessage.fail(1001, "参数错误");
        }
        int data = baseAccountService.cancelByPkList(pkList);
        if (data >= 0) {
            return ResultMessage.success(data);
        }
        return ResultMessage.fail(500, "删除失败");
    }

    /**
     * 根据id更新实体对象.
     *
     * @param domain 要更新的实体对象
     * @return 返回更新的行数。
     */
    @ApiOperation(value = "更新数据", notes = "用户帐户", httpMethod = "PUT")
    @ApiParam(name = "domain", required = true)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultMessage updateByPk(@RequestBody BaseAccount domain) {
        int data = baseAccountService.updateByPk(domain);
        return ResultMessage.success(data);
    }


    /**
     * 个人申请加入组织.
     *
     * @param pk 主键 orPk
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "个人申请加入组织", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/joinToOr", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('user')")
    public ResultMessage joinToOrganization(@RequestParam BigInteger pk, @RequestParam String orCode) {

        int i = baseAccountService.joinToOrganization(pk, orCode);
        return ResultMessage.success(i);
    }

    /**
     * 组织邀请个人加入.
     *
     * @param pk 主键 orPk
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @ApiOperation(value = "组织邀请个人加入", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "pk", required = true)
    @RequestMapping(value = "/invite", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('organization_admin')")
    public ResultMessage inviteJoinToOr(@RequestParam BigInteger pk, @RequestParam String userName) {

        int i = baseAccountService.inviteJoinToOr(pk, userName);
        return ResultMessage.success(i);
    }

    /**
     * @MethodName:
     * @Description: 切换当前组织，返回新的token
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/27
     **/
    @RequestMapping(value = "/switchCurrentOrganization", method = RequestMethod.POST)
    public ResultMessage switchCurrentOrganization(@RequestParam BigInteger pk,
                                                   @RequestParam BigInteger organizationId,
                                                   HttpServletRequest request) {
        String accountId = tokenDecode.getUserInfo().get("id").toString();
        if (StringUtil.isEmpty(accountId)) {
            return ResultMessage.fail(2010, "未登录");
        }
        String authorization = request.getHeader("Authorization");
        String token = authorization.substring(7);
        String redisToken = (String) redisTemplate.opsForValue().get(accountId);
        if (!token.equals(redisToken)) {
            return ResultMessage.fail(2010, "token错误");
        }
        AuthToken authToken = baseAccountService.switchCurrentOrganization(pk, organizationId);
        if (Objects.isNull(authToken)) {
            return ResultMessage.fail(500, "服务器错误");
        }
        return ResultMessage.success(authToken);
    }


    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    @ApiOperation(value = "根据主键列表查询", notes = "用户帐户", httpMethod = "POST")
    @ApiParam(name = "pkList", required = true)
    @RequestMapping(value = "/get/list", method = RequestMethod.POST)
    public ResultMessage selectByPkList(@RequestBody List<java.math.BigInteger> pkList) {
        List<BaseAccount> data = baseAccountService.selectByPkList(pkList);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     * 后台管理员查看所有用户信息
     *
     * @param start       数据库查询记录偏移值
     * @param pageSize    每页数据条数
     * @param baseAccount 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "用户帐户", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
            @ApiImplicitParam(name = "baseAccount", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/admin/getAccount/page", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('admin')")
    public ResultMessage pageListByAccountAdmin(@RequestParam int start, @RequestParam int pageSize, @RequestBody BaseAccount baseAccount) {

        PageInfo<BaseAccount> data = baseAccountService.pageListByAccount(start, pageSize, baseAccount);
        return ResultMessage.success(data);
    }

    /**
     * 条件分页查询.
     * 组织机构管理员查看组织的用户信息
     *
     * @param start       数据库查询记录偏移值
     * @param pageSize    每页数据条数
     * @param baseAccount 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @ApiOperation(value = "分页查询", notes = "用户帐户", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页起始位置", required = true, dataType = "int", defaultValue = "0", paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, dataType = "int", defaultValue = "10", paramType = "path"),
            @ApiImplicitParam(name = "baseAccount", value = "查询参数", dataType = "Map", paramType = "body")})
    @RequestMapping(value = "/getAccount/page", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('organization_admin')")
    public ResultMessage pageListByAccountOrganization(@RequestParam int start, @RequestParam int pageSize, @RequestBody BaseAccount baseAccount) {
        //获取id和组织id
        BigInteger id = BigInteger.valueOf(Long.parseLong(tokenDecode.getUserInfo().get("id").toString()));
        if (!id.equals(baseAccount.getId())) {
            return ResultMessage.fail(1001, "参数和token不匹配");
        }
        if (Objects.isNull(baseAccount.getOrganizationId())) {
            BaseAccount baseAccount1 = baseAccountService.selectByPk(id);
            baseAccount.setOrganizationId(baseAccount1.getOrganizationId());
        }
        PageInfo<BaseAccount> data = baseAccountService.pageListByAccount(start, pageSize, baseAccount);
        return ResultMessage.success(data);
    }

    /**
     * @MethodName:
     * @Description: 获取个人申请列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/25
     **/
    @GetMapping(value = "/getApplyList")
    @PreAuthorize("hasAnyAuthority('admin','organization_admin')")
    public ResultMessage ApplyToJoinOrganizationList() {
        //获取管理员id
        BigInteger id = BigInteger.valueOf(Long.parseLong(tokenDecode.getUserInfo().get("id").toString()));
        //获取管理员用户信息
        BaseAccount baseAccount = baseAccountService.selectByPk(id);
        //根据组织id查询申请列表
        List<AccountInviteOrganization> applyList = baseAccountService.queryApplyByOrganizationId(baseAccount.getOrganizationId());
        List<BaseAccount> baseAccounts = new ArrayList<>();
        //根据邀请信息的组织id查询组织信息
        for (AccountInviteOrganization accountInviteOrganization : applyList) {
            baseAccount = baseAccountService.selectByPk(accountInviteOrganization.getAccountId());
            baseAccount.setAccountPassword(null);
            baseAccounts.add(baseAccount);
        }
        return ResultMessage.success(baseAccounts);
    }

    /**
     * @MethodName:
     * @Description: 获取邀请组织列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/25
     **/
    @GetMapping(value = "/getInviteList")
    @PreAuthorize("hasAnyAuthority('admin','user')")
    public ResultMessage InviteInfoList() {
        //获取id和组织id
        BigInteger id = BigInteger.valueOf(Long.parseLong(tokenDecode.getUserInfo().get("id").toString()));
        List<AccountInviteOrganization> inviteList = baseAccountService.queryInviteByAccountId(id);
        List<BaseOrganization> organizationList = new ArrayList<>();
        for (AccountInviteOrganization accountInviteOrganization : inviteList) {
            BaseOrganization baseOrganization = baseOrganizationService.selectByPk(accountInviteOrganization.getOrganizationId());
            organizationList.add(baseOrganization);
        }
        return ResultMessage.success(organizationList);
    }

    /**
     * 根据实体类查询
     *
     * @param domain 实体类
     * @return List<Domain> 查询结果
     */
    @ApiOperation(value = "根据Model查询", notes = "用户帐户", tags = "", httpMethod = "POST")
    @ApiImplicitParam(name = "domain", required = true)
    @RequestMapping(value = "/get/domain", method = RequestMethod.POST)
    public ResultMessage selectListByDomain(@RequestBody BaseAccount domain) {
        List<BaseAccount> data = baseAccountService.selectListByDomain(domain);
        return ResultMessage.success(data);
    }


    /****************************校准服务调用*****************************/

    /**
     * 查看手机号是否被注册
     *
     * @param phone
     * @return
     */
    @GetMapping(value = "/calibration/phoneRegistered")
    public boolean phoneRegistered(@RequestParam String phone) {
        return baseAccountService.isPhoneRegister(phone);
    }

    /**
     * 校准服务的注册
     *
     * @param phone
     * @return
     */
    @PostMapping("/calibration/register")
    public boolean calibrationRegister(@RequestParam String phone) {
        //判断手机号是否已被注册
        boolean isRegister = baseAccountService.isPhoneRegister(phone);
        if (isRegister) {
            return false;
        }
        //校验手机号格式
        String phonePattern = "^1[3456789]\\d{9}$";
        if (!Pattern.compile(phonePattern).matcher(phone).matches()) {
            return false;
        }
        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setPhone(phone);

        int isSeccess = baseAccountService.calibrationRegister(baseAccount);
        if (isSeccess == 0) {
            return false;
        }
        return true;
    }

    /**
     * 根据手机号获取id
     * @param phone
     * @return
     */
    @GetMapping(value = "/calibration/getAccountIdByPhone")
    public BigInteger getAccountIdByPhone(@RequestParam String phone) {
        boolean phoneRegister = baseAccountService.isPhoneRegister(phone);
        if (phoneRegister) {
            BaseAccount baseAccount = baseAccountService.getAccountIdByPhone(phone);
            if (!Objects.isNull(baseAccount)){
                return baseAccount.getId();
            }
        }
        return null;
    }

    /**************************************************************/
    /**
     * @MethodName: getRemoteIP
     * @Description: 获取请求头的ip
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
     **/
    private Long getRemoteIP(HttpServletRequest request) {
        String ip = null;
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip != null) {
            //对于通过多个代理的情况，最后IP为客户端真实IP,多个IP按照','分割
            int position = ip.indexOf(",");
            if (position > 0) {
                ip = ip.substring(0, position);
            }
        }
        String[] ipNums = ip.split("\\.");
        Long LongId = (Long.parseLong(ipNums[0]) << 24)
                + (Long.parseLong(ipNums[1]) << 16)
                + (Long.parseLong(ipNums[2]) << 8)
                + (Long.parseLong(ipNums[3]));
        return LongId;
    }

}