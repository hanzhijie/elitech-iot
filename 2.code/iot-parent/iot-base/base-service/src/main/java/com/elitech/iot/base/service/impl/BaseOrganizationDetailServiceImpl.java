/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.domain.BaseOrganizationDetail;
import com.elitech.iot.base.dao.BaseOrganizationDetailDao;
import com.elitech.iot.base.service.BaseOrganizationDetailService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 企业详细信息.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseOrganizationDetailServiceImpl extends BaseService<BaseOrganizationDetail, java.math.BigInteger> implements BaseOrganizationDetailService {
    @Autowired
    private BaseOrganizationDetailDao dao;

    @Override
    protected BaseOrganizationDetailDao getDao() {
        return dao;
    }
}