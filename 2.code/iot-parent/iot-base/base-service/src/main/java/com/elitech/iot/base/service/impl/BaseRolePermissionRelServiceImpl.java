/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.domain.BaseRolePermissionRel;
import com.elitech.iot.base.dao.BaseRolePermissionRelDao;
import com.elitech.iot.base.service.BaseRolePermissionRelService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 角色权限关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseRolePermissionRelServiceImpl extends BaseService<BaseRolePermissionRel, java.math.BigInteger> implements BaseRolePermissionRelService {
    @Autowired
    private BaseRolePermissionRelDao dao;

    @Override
    protected BaseRolePermissionRelDao getDao() {
        return dao;
    }
}