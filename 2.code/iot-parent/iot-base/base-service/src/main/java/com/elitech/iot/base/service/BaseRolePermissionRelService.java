/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service;

import com.elitech.iot.base.domain.BaseRolePermissionRel;

import com.elitech.iot.common.service.IBaseService;

/**
* 角色权限关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BaseRolePermissionRelService extends IBaseService<BaseRolePermissionRel, java.math.BigInteger> {

}