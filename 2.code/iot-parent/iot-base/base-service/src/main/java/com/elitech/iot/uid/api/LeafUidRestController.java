package com.elitech.iot.uid.api;

import com.elitech.iot.common.base.api.ResultCode;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.uid.leaf.LeafConstants;
import com.elitech.iot.uid.leaf.LeafKeyGeneratorFactory;
import com.elitech.iot.uid.leaf.LeafProperties;
import io.opensharding.keygen.leaf.LeafSegmentKeyGenerator;
import io.opensharding.keygen.leaf.LeafSnowflakeKeyGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Properties;

/**
 * 基于美团Leaf的分布式主键生成服务.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/6
 */
@CrossOrigin
@RestController
@RequestMapping(path = "leaf")
@Slf4j
@Api(value = "Leaf Uid API", tags = {"Leaf Uid API"})
public class LeafUidRestController {
    @Autowired
    private LeafProperties leafProperties;

    @ApiOperation(value = "获取主键", notes = "Leaf Segment算法，基于Mysql数据库", httpMethod = "GET")
    @ApiParam(name = "bizTag", value = "业务类型编码", required = true)
    @GetMapping("segment")
    public ResultMessage<Long> getSegmentKey(@RequestParam String bizTag) {
        try {
            LeafSegmentKeyGenerator gen = LeafKeyGeneratorFactory.getSegmentKeyGenerator();
            Comparable<Long> key = (Comparable<Long>) gen.generateKey(bizTag);

            if (key.compareTo(0L) <= 0) {
                return ResultMessage.fail(ResultCode.GENERATE_SEGMENT_ID_ERROR);
            } else {
                return ResultMessage.success(key);
            }

        } catch (
                Exception ex) {
            log.error("生成Leaf Segment key失败：", ex);
            return ResultMessage.fail(ResultCode.GENERATE_SEGMENT_ID_ERROR);
        }

    }

    @ApiOperation(value = "获取主键", notes = "Leaf Snowflake算法，基于zookeeper", httpMethod = "GET")
    @GetMapping("/snow")
    public ResultMessage<Long> getSnowflakeKey() {
        try {
            LeafSnowflakeKeyGenerator gen = LeafKeyGeneratorFactory.getSnowflakeKeyGenerator();
            Comparable<Long> key = (Comparable<Long>) gen.generateKey();
            return ResultMessage.success(key);

        } catch (Exception ex) {
            log.error("生成Leaf Segment key失败：", ex);
            return ResultMessage.fail(ResultCode.GENERATE_SNOWFLAKE_ID_ERROR);
        }
    }
}
