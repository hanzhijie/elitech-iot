/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-17 15:15:48
*/

package com.elitech.iot.base.service;

import com.elitech.iot.base.domain.BaseDict;

import com.elitech.iot.common.service.IBaseService;

import java.util.List;

/**
* 系统字典表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
public interface BaseDictService extends IBaseService<BaseDict, java.math.BigInteger> {

    /**
     * 根据字典类型查询字典项
     * @param dataType
     * @return
     */
    List<BaseDict> queryDictByType(String dataType);
}