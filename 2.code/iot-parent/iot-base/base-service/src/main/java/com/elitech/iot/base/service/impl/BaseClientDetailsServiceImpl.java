/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.domain.BaseClientDetails;
import com.elitech.iot.base.dao.BaseClientDetailsDao;
import com.elitech.iot.base.service.BaseClientDetailsService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* OAuth2.0客户端信息： oauth_access_token、oauth_refresh_token信息存储在re.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseClientDetailsServiceImpl extends BaseService<BaseClientDetails, java.math.BigInteger> implements BaseClientDetailsService {
    @Autowired
    private BaseClientDetailsDao dao;

    @Override
    protected BaseClientDetailsDao getDao() {
        return dao;
    }
}