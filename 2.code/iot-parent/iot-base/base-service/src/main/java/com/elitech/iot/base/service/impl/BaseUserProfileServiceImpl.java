/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.domain.BaseUserProfile;
import com.elitech.iot.base.dao.BaseUserProfileDao;
import com.elitech.iot.base.service.BaseUserProfileService;

import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* 用户偏好.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseUserProfileServiceImpl extends BaseService<BaseUserProfile, java.math.BigInteger> implements BaseUserProfileService {
    @Autowired
    private BaseUserProfileDao dao;

    @Override
    protected BaseUserProfileDao getDao() {
        return dao;
    }
}