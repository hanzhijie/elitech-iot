/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-17 15:15:48
*/

package com.elitech.iot.base.service.impl;

import com.elitech.iot.base.dao.BaseDictDao;
import com.elitech.iot.base.domain.BaseDict;
import com.elitech.iot.base.service.BaseDictService;
import com.elitech.iot.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

/**
* 系统字典表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Service
public class BaseDictServiceImpl extends BaseService<BaseDict, java.math.BigInteger> implements BaseDictService {
    @Autowired
    private BaseDictDao dao;

    @Override
    protected BaseDictDao getDao() {
        return dao;
    }

    @Override
    @Cacheable(value = "BaseDict",key = "#dataType")
    public List<BaseDict> queryDictByType(String dataType) {
        BaseDict dict = new BaseDict();
        dict.setDataType(dataType);
        dict.setDeletedFlag(false);
        List<BaseDict> baseDicts = dao.selectListByDomain(dict);
        return baseDicts;
    }

    @Override
    @CacheEvict(value="BaseDict",key = "#domain.dataType")
    public int insert(BaseDict domain) {
        return getDao().insert(domain);
    }

    @Override
    @CacheEvict(value="BaseDict",key = "#domain.dataType")
    public int updateByPk(BaseDict domain) {
        return getDao().updateByPk(domain);
    }

    @Override
    @CacheEvict(value="BaseDict",key = "#domain.dataType")
    public int deleteByPk(BigInteger pk) {
        return getDao().deleteByPk(pk);
    }

}