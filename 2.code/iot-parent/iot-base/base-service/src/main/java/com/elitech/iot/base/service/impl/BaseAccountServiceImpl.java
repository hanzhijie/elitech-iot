/*
 * Copyright © 2020-2120 . All Rights Reserved.
 * CreateTime：  2019-12-27 09:21:38
 */

package com.elitech.iot.base.service.impl;

import com.elitech.iot.auth.client.AuthClient;
import com.elitech.iot.auth.domain.AuthToken;
import com.elitech.iot.base.dao.*;
import com.elitech.iot.base.domain.*;
import com.elitech.iot.base.service.BaseAccountService;
import com.elitech.iot.common.base.api.ResultMessage;
import com.elitech.iot.common.service.BaseService;
import com.elitech.iot.uid.client.LeafUidClient;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * 用户帐户.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@Service
public class BaseAccountServiceImpl extends BaseService<BaseAccount, java.math.BigInteger> implements BaseAccountService {
    @Autowired
    private BaseAccountDao baseAccountDao;
    @Autowired
    private BaseRoleDao baseRoleDao;
    @Autowired
    private BasePermissionDao basePermissionDao;
    @Autowired
    private BaseOrganizationDao baseOrganizationDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private AuthClient authClient;
    @Autowired
    private BaseAccountRoleRelDao roleRelDao;
    @Autowired
    private BaseUserProfileDao userProfileDao;
    @Autowired
    private LeafUidClient leafUidClient;

    @Override
    protected BaseAccountDao getDao() {
        return baseAccountDao;
    }


    /**
     * @MethodName: register
     * @Description: 注册
     * @Param: [domain]
     * @Return: int  1成功  0已被注册  -1 错误
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    @Override
    public int register(BaseAccount baseAccount) {
        //生成id
        BigInteger id = createAccountId();
        baseAccount.setId(id);
        //默认所属组织机构
        baseAccount.setOrganizationId(BigInteger.valueOf(0));
        baseAccount.setOrganizationPath("0");
        //设置状态
        baseAccount.setAccountState(1);
        //设置删除状态
        baseAccount.setDeletedFlag(false);
        //设置默认头像
        //设置创建时间
        baseAccount.setGmtCreate(LocalDateTime.now());
        //赋予用户默认角色
        //赋予用户默认项目
        BaseAccountRoleRel roleRel = new BaseAccountRoleRel();
        roleRel.setId(createAccountRoleRelId());
        roleRel.setAccountId(id);
        roleRel.setRoleId(BigInteger.valueOf(4));
        roleRel.setGmtCreate(LocalDateTime.now());
        roleRel.setGmtModified(LocalDateTime.now());
        roleRelDao.insert(roleRel);
        //用户偏好信息初始化
        BaseUserProfile baseUserProfile = new BaseUserProfile();
        baseUserProfile.setGmtCreate(LocalDateTime.now());
        baseUserProfile.setGmtModified(LocalDateTime.now());
        baseUserProfile.setAccountId(id);
        userProfileDao.insert(baseUserProfile);
        return baseAccountDao.insert(baseAccount);
    }

    /**
     * 校准服务注册（只有手机号）
     *
     * @param baseAccount
     * @return
     */
    @Override
    public int calibrationRegister(BaseAccount baseAccount) {
        //生成id
        BigInteger id = createAccountId();
        baseAccount.setId(id);
        //用户名
        String userName = createCalibrationUserName();
        baseAccount.setUserName(userName);
        baseAccount.setAccountPassword("calibration");
        //默认所属组织机构
        baseAccount.setOrganizationId(BigInteger.valueOf(0));
        baseAccount.setOrganizationPath("0");
        //设置状态
        baseAccount.setAccountState(1);
        //设置删除状态
        baseAccount.setDeletedFlag(false);
        //设置默认头像
        //设置创建时间
        baseAccount.setGmtCreate(LocalDateTime.now());
        //赋予用户默认角色
        //赋予用户默认项目
        BaseAccountRoleRel roleRel = new BaseAccountRoleRel();
        roleRel.setId(createAccountRoleRelId());
        roleRel.setAccountId(id);
        roleRel.setRoleId(BigInteger.valueOf(4));
        roleRel.setGmtCreate(LocalDateTime.now());
        roleRel.setGmtModified(LocalDateTime.now());
        roleRelDao.insert(roleRel);
        //用户偏好信息初始化
        BaseUserProfile baseUserProfile = new BaseUserProfile();
        baseUserProfile.setGmtCreate(LocalDateTime.now());
        baseUserProfile.setGmtModified(LocalDateTime.now());
        baseUserProfile.setAccountId(id);
        userProfileDao.insert(baseUserProfile);
        return baseAccountDao.insert(baseAccount);
    }

    @Override
    public BaseAccount getAccountIdByPhone(String phone) {
        return baseAccountDao.selectByPhone(phone);
    }


    /**
     * @MethodName: isPhoneRegister
     * @Description: 手机号是否被注册
     * @Param: [phone]
     * @Return: boolean true注册, false未注册
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    @Override
    public boolean isPhoneRegister(String phone) {
        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setPhone(phone);
        baseAccount.setDeletedFlag(false);
        List<BaseAccount> baseAccounts = baseAccountDao.selectListByDomain(baseAccount);
        return baseAccounts.size() > 0;
    }

    /**
     * @MethodName: isUserNameRegister
     * @Description: 用户名是否被注册
     * @Param: [userName]
     * @Return: boolean true注册, false未注册
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    @Override
    public boolean isUserNameRegister(String userName) {
        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setUserName(userName);
        baseAccount.setDeletedFlag(false);
        List<BaseAccount> baseAccounts = baseAccountDao.selectListByDomain(baseAccount);
        return baseAccounts.size() > 0;
    }

    /**
     * @MethodName: isEmailRegister
     * @Description: 邮箱是否已被注册
     * @Param: [email]
     * @Return: boolean true注册, false未注册
     * @Author: dongqg
     * @Date: 2020/1/8
     **/
    @Override
    public boolean isEmailRegister(String email) {
        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setEmail(email);
        List<BaseAccount> baseAccounts = baseAccountDao.selectListByDomain(baseAccount);
        return baseAccounts.size() > 0;
    }


    /**
     * @MethodName: login
     * @Description: 登录  进行用户名密码验证，若手机登录，直接查询即可
     * @Param:
     * @Return: BaseAccount 成功返回用户信息，密码错误返回null
     * @Author: dongqg
     * @Date: 2020/2/13
     **/
    @Override
    public BaseAccount login(BaseAccount account, Long ip) {
        if (Objects.isNull(account.getPhone())) {
            //用户名密码登录（此时username值可能为邮箱，可能为手机号，可能为用户名）
            BaseAccount baseAccount = null;
            if (isUserNameRegister(account.getUserName())) {
                baseAccount = baseAccountDao.selectByUserName(account.getUserName());
            }
            if (isEmailRegister(account.getUserName())) {
                baseAccount = baseAccountDao.selectByEmail(account.getUserName());
            }
            if (isPhoneRegister(account.getUserName())) {
                baseAccount = baseAccountDao.selectByPhone(account.getUserName());
            }
            //比对密码
            if (baseAccount.getAccountPassword().equals(account.getAccountPassword())) {
                //获取上次登录时间
                LocalDateTime lastLoginTime = baseAccount.getLastLoginTime();
                BigInteger lastLoginIp = baseAccount.getLastLoginIp();
                //更新最新登录的时间
                baseAccount.setLastLoginTime(LocalDateTime.now());
                baseAccount.setLastLoginIp(BigInteger.valueOf(ip));
                baseAccount.setLastLoginType(1);
                int i = baseAccountDao.updateByPk(baseAccount);
                if (i == 1) {
                    baseAccount.setLastLoginTime(lastLoginTime);
                    baseAccount.setLastLoginIp(lastLoginIp);
                    return baseAccount;
                }
            }
        }

        if (!Objects.isNull(account.getPhone())) {
            List<BaseAccount> baseAccounts = baseAccountDao.selectListByDomain(account);
            if (baseAccounts.size() == 1) {
                BaseAccount baseAccount = baseAccounts.get(0);
                return baseAccount;
            }
        }
        return null;
    }

    /**
     * @MethodName:
     * @Description: 根据用户id查询角色列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
     **/
    @Override
    public List<BaseRole> queryRolesByAccountId(BigInteger id) {
        return baseRoleDao.queryRolesByAccountId(id);
    }

    /**
     * @MethodName:
     * @Description: 根据用户id查询权限列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/13
     **/
    @Override
    public List<BasePermission> queryPermissionsByAccountId(BigInteger id) {
        return basePermissionDao.queryPermissionsByAccountId(id);
    }

    /**
     * @MethodName:
     * @Description: 重置密码，根据手机或者邮箱
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/14
     **/
    @Override
    public boolean resetPassword(String userName, String password) {
        //用户名密码登录（此时username值可能为邮箱，可能为手机号）
        BaseAccount baseAccount = null;
        if (isEmailRegister(userName)) {
            baseAccount = baseAccountDao.selectByEmail(userName);
        }
        if (isPhoneRegister(userName)) {
            baseAccount = baseAccountDao.selectByPhone(userName);
        }
        if (Objects.isNull(baseAccount)) {
            return false;
        }
        baseAccount.setAccountPassword(password);
        int i = baseAccountDao.updateByPk(baseAccount);
        return i > 0;
    }

    /**
     * @MethodName:
     * @Description: 根据用户id查询组织
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
     **/
    @Override
    public List<BaseOrganization> queryOrgnizationByAccountId(BigInteger id) {
        return baseOrganizationDao.selectOrganizationByAccountId(id);
    }

    /**
     * @MethodName:
     * @Description: 批量注销
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
     **/
    @Override
    public int cancelByPkList(List<BigInteger> pkList) {
        return baseAccountDao.cancelByPkList(pkList);
    }

    /**
     * @MethodName:
     * @Description: 注销
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
     **/
    @Override
    public int cancelByPk(BigInteger pk) {
        return baseAccountDao.cancelByPk(pk);
    }

    /**
     * @MethodName:
     * @Description: 分页查询
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
     **/
    @Override
    public PageInfo<BaseAccount> pageListByAccount(int start, int pageSize, BaseAccount baseAccount) {
        PageHelper.startPage(start, pageSize);
        List<BaseAccount> baseAccounts = baseAccountDao.selectListByDomain(baseAccount);
        PageInfo pageInfo = new PageInfo(baseAccounts);
        //Page<BaseAccount> baseAccounts1 = (Page<BaseAccount>) baseAccounts;
        return pageInfo;
    }

    /**
     * @MethodName:
     * @Description: 个人申请加入组织
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
     **/
    @Override
    public int joinToOrganization(BigInteger pk, String orCode) {
        AccountInviteOrganization accountInviteOrganization = new AccountInviteOrganization();
        //生成id
        StringBuilder id = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            id.append(random.nextInt(10));
        }
        accountInviteOrganization.setId(BigInteger.valueOf(Integer.parseInt(id.toString())));
        BaseOrganization baseOrganization = new BaseOrganization();
        baseOrganization.setOrganizationCode(orCode);
        List<BaseOrganization> organizationList = baseOrganizationDao.selectListByDomain(baseOrganization);
        accountInviteOrganization.setOrganizationId(organizationList.get(0).getId());
        accountInviteOrganization.setAccountId(pk);
        accountInviteOrganization.setInviteType(2);
        accountInviteOrganization.setGmtCreate(LocalDateTime.now());
        accountInviteOrganization.setGmtModified(LocalDateTime.now());
        int i = baseAccountDao.insertInvite(accountInviteOrganization);
        return i;
    }

    /**
     * @MethodName:
     * @Description: 组织邀请个人加入
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/22
     **/
    @Override
    public int inviteJoinToOr(BigInteger pk, String userName) {
        BaseAccount baseAccount = new BaseAccount();
        baseAccount.setUserName(userName);
        List<BaseAccount> baseAccounts = baseAccountDao.selectListByDomain(baseAccount);
        if (baseAccounts.size() > 0) {
            BigInteger accountId = baseAccounts.get(0).getId();
            AccountInviteOrganization accountInviteOrganization = new AccountInviteOrganization();
            //生成id
            StringBuilder id = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < 6; i++) {
                id.append(random.nextInt(10));
            }
            accountInviteOrganization.setId(BigInteger.valueOf(Integer.parseInt(id.toString())));
            accountInviteOrganization.setOrganizationId(pk);
            accountInviteOrganization.setAccountId(accountId);
            accountInviteOrganization.setInviteType(1);
            accountInviteOrganization.setGmtCreate(LocalDateTime.now());
            accountInviteOrganization.setGmtModified(LocalDateTime.now());
            int i = baseAccountDao.insertInvite(accountInviteOrganization);
            return i;
        }
        return 1;
    }

    /**
     * @MethodName:
     * @Description: 根据用户id查询邀请列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/25
     **/
    @Override
    public List<AccountInviteOrganization> queryInviteByAccountId(BigInteger id) {
        AccountInviteOrganization accountInviteOrganization = new AccountInviteOrganization();
        accountInviteOrganization.setAccountId(id);
        return baseAccountDao.selectInviteInfoByDomain(accountInviteOrganization);
    }

    /**
     * @MethodName:
     * @Description: 根据组织id查询申请列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/25
     **/
    @Override
    public List<AccountInviteOrganization> queryApplyByOrganizationId(BigInteger id) {
        AccountInviteOrganization accountInviteOrganization = new AccountInviteOrganization();
        accountInviteOrganization.setOrganizationId(id);
        return baseAccountDao.selectInviteInfoByDomain(accountInviteOrganization);
    }

    @Override
    public AuthToken switchCurrentOrganization(BigInteger pk, BigInteger organizationId) {
        BaseAccount baseAccount = baseAccountDao.selectByPk(pk);
        baseAccount.setOrganizationId(organizationId);
        int i = baseAccountDao.updateByPk(baseAccount);
        if (i > 0) {
            AuthToken token = authClient.getToken(baseAccount.getUserName(), baseAccount.getAccountPassword());
            if (!Objects.isNull(token)) {
                redisTemplate.opsForValue().set(baseAccount.getId().toString(), token.getAccessToken());
                return token;
            }
            return null;
        }
        return null;
    }


    private BigInteger createAccountId() {
        while (true) {
            ResultMessage<Long> calibration = leafUidClient.getSegmentKey("base_account");
            BigInteger id = BigInteger.valueOf(calibration.getData());
            BaseAccount baseAccount = baseAccountDao.selectByPk(id);
            if (Objects.isNull(baseAccount)) {
                return id;
            }
        }
    }

    private BigInteger createAccountRoleRelId() {
        while (true) {
            ResultMessage<Long> calibration = leafUidClient.getSegmentKey("base_role");
            BigInteger id = BigInteger.valueOf(calibration.getData());
            BaseAccountRoleRel roleRel = roleRelDao.selectByPk(id);
            if (Objects.isNull(roleRel)) {
                return id;
            }
        }
    }

    private String createCalibrationUserName() {
        while (true) {
            String s = RandomStringUtils.randomNumeric(6);
            String username = "calibration_" + s;

            BaseAccount baseAccount = baseAccountDao.selectByUserName(username);
            if (Objects.isNull(baseAccount)) {
                return username;
            }
        }
    }
}