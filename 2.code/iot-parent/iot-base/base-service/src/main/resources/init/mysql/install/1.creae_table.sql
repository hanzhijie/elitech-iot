DROP TABLE IF EXISTS `base_dict`;
CREATE TABLE `base_dict` (
                           `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
                           `pid` int(11) DEFAULT '0' COMMENT ' 父ID ',
                           `data_type` varchar(50) NOT NULL COMMENT ' 数据类别,首字母大写 ',
                           `data_code` varchar(50) NOT NULL COMMENT ' 数据编码 ',
                           `data_value` varchar(200) NOT NULL COMMENT ' 数据名称/值 ',
                           `sort_no` int(11) unsigned DEFAULT '1' COMMENT ' 顺序 ',
                           `deleted_flag` bit(1) DEFAULT b'0' COMMENT '删状态：true已删除、false未删除.',
                           `data_desc` varchar(400) DEFAULT NULL COMMENT '数据描述',
                           `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
                           `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `G_SYS_DICT_PK` (`id`),
                           KEY `idx_dc_dt` (`data_type`,`data_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表';


--
-- Create table `leaf_alloc`
--
CREATE TABLE leaf_alloc (
  biz_tag varchar(128) NOT NULL DEFAULT '' COMMENT '业务类型',
  max_id bigint(20) NOT NULL DEFAULT 1 COMMENT '当前最大值',
  step int(11) NOT NULL COMMENT '步长',
  description varchar(256) DEFAULT NULL COMMENT '描述',
  update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (biz_tag)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci,
COMMENT = 'Leaf Segment分布式主键记录表';