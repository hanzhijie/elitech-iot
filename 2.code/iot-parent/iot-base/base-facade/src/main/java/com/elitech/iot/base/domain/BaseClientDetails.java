/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* OAuth2.0客户端信息： oauth_access_token、oauth_refresh_token信息存储在re.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseClientDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 客户端信息主键 id.
    */
    private java.math.BigInteger id;
    /**
    * 客户端id.
    */
    private String clientId;
    /**
    * 客户密钥.
    */
    private String clientSecret;
    /**
    * 客户端类型：1web云平台、2IOS、3Android、4微信小程序、5微信公众号、100第三方、1000设备.
    */
    private int clientType;
    /**
    * 资源id.
    */
    private String resourceIds;
    /**
    * 作用范围.
    */
    private String scope;
    /**
    * 授权类型.
    */
    private String authorizedGrantTypes;
    /**
    * 重定向地址.
    */
    private String webServerRedirectUri;
    /**
    * 角色.
    */
    private String authorities;
    /**
    * access_token有效时间.
    */
    private long accessTokenValidity;
    /**
    * refresh_token有效时间.
    */
    private long refreshTokenValidity;
    /**
    * 自动授权.
    */
    private int autoApprove;
    /**
    * 附加信息.
    */
    private String additionalInformation;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}