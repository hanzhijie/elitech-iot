package com.elitech.iot.base.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * @ClassName AccountInviteOrganization
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/22
 * @Version V1.0
 **/
@Data
public class AccountInviteOrganization implements Serializable {
    /**
     * 主键id
     */
    private BigInteger id;

    /**
     * 账号id
     */
    private BigInteger accountId;

    /**
     * 组织id
     */
    private BigInteger organizationId;

    /**
     * 类型 1企业邀请 2主动申请
     */
    private Integer inviteType;

    /**
     * 审核时间
     */
    private LocalDateTime aduitTime;

    /**
     * 处理结果1同意 0 不同意
     */
    private Integer dealResult;

    /**
     * 过期时间
     */
    private LocalDateTime outTime;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;
}
