/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 组织机构.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseOrganization implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 组织机构id.
    */
    private java.math.BigInteger id;
    /**
    * 组织机构名称.
    */
    private String organizationName;
    /**
    * 上级组织机构id.
    */
    private java.math.BigInteger parentId;
    /**
    * 组织机构路径.
    */
    private String organizationPath;
    /**
    * 组织机构编码.
    */
    private String organizationCode;
    /**
    * 组织机构类型：1生产企业、2经销商、3终端企业、4监管机构.
    */
    private long organizationTypeId;
    /**
    * 组织机构logo.
    */
    private String organizationLogo;
    /**
    * 国家编码.
    */
    private String countryCode;
    /**
    * 所属地域编码.
    */
    private String zoneCode;
    /**
    * 所属区域.
    */
    private String zoneAddress;
    /**
    * 详细地址.
    */
    private String detailAddress;
    /**
    * 删状态：true已删除、false未删除.
    */
    private boolean deletedFlag;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后更新时间.
    */
    private java.time.LocalDateTime gmtModified;
}