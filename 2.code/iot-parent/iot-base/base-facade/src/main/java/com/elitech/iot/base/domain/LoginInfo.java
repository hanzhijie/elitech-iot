package com.elitech.iot.base.domain;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @ClassName LoginInfo
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/13
 * @Version V1.0
 **/
@Data
public class LoginInfo {
    /**
     * 令牌token
     */
    private String token;
    /**
     * 用户信息
     */
    private String phone;
    private String userName;
    private String email;
    private String accountPhoto;
    private LocalDateTime lastLoginTime;
    private BaseOrganization baseOrganization;
    private List<BasePermission> permissions;

}
