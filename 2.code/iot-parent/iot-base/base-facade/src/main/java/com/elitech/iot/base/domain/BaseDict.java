/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-17 15:15:48
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 系统字典表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseDict implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    *  父ID .
    */
    private java.math.BigInteger pid;
    /**
    *  数据类别,首字母大写 .
    */
    private String dataType;
    /**
    *  数据编码 .
    */
    private String dataCode;
    /**
    *  数据名称/值 .
    */
    private String dataValue;
    /**
    *  顺序 .
    */
    private java.math.BigInteger sortNo;
    /**
    * 删状态：true已删除、false未删除.
    */
    private Boolean deletedFlag;
    /**
    * 数据描述.
    */
    private String dataDesc;
    /**
     * 创建时间.
     */
    private java.time.LocalDateTime gmtCreate;
    /**
     * 最后更新时间.
     */
    private java.time.LocalDateTime gmtModified;
}