package com.elitech.iot.uid.client;

import com.elitech.iot.common.base.api.ResultMessage;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/6
 */
@FeignClient(name = "base-service", path="/base")
@RequestMapping("leaf")
public interface LeafUidClient {
    /**
     * 根据Leaf Segment 算法生成key.
     *
     * @param bizTag 业务类型
     * @return 主键id
     */
    @GetMapping("segment")
    ResultMessage<Long> getSegmentKey(@RequestParam String bizTag);

    /**
     * 根据Leaf Snowflake 算法生成key.
     * @return
     */
    @GetMapping("/snow")
    ResultMessage<Long> getSnowflakeKey();
}
