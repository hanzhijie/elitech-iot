/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 项目.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "项目")
public class BaseProjectModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseProject.id.NotNUll}")
	@ApiModelProperty(value = "id")
    private java.math.BigInteger id;

/**
* 项目名称.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseProject.projectName.NotNUll}")
		@Length(max = 50, message = "{com.elitech.iot.base.model.BaseProject.projectName.Length}")
	@ApiModelProperty(value = "项目名称")
    private String projectName;

/**
* 项目类型id.
*/
		@ApiModelProperty(value = "项目类型id")
    private java.math.BigInteger projectTypeId;

/**
* 所属国家编码.
*/
			@Length(max = 20, message = "{com.elitech.iot.base.model.BaseProject.countryCode.Length}")
	@ApiModelProperty(value = "所属国家编码")
    private String countryCode;

/**
* 地址编码：便于统计分析.
*/
			@Length(max = 50, message = "{com.elitech.iot.base.model.BaseProject.zoneCode.Length}")
	@ApiModelProperty(value = "地址编码：便于统计分析")
    private String zoneCode;

/**
* 所属区域.
*/
			@Length(max = 100, message = "{com.elitech.iot.base.model.BaseProject.zoneAddress.Length}")
	@ApiModelProperty(value = "所属区域")
    private String zoneAddress;

/**
* 详细地址.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseProject.detailAdress.Length}")
	@ApiModelProperty(value = "详细地址")
    private String detailAdress;

/**
* 所属组织机构id.
*/
		@ApiModelProperty(value = "所属组织机构id")
    private java.math.BigInteger organizationId;

/**
* 所属组织路径.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseProject.organizationName.Length}")
	@ApiModelProperty(value = "所属组织路径")
    private String organizationName;

/**
* 上级ID.
*/
		@ApiModelProperty(value = "上级ID")
    private java.math.BigInteger parentId;

/**
* 项目路径.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseProject.projectPath.Length}")
	@ApiModelProperty(value = "项目路径")
    private String projectPath;

/**
* 项目描述.
*/
			@Length(max = 500, message = "{com.elitech.iot.base.model.BaseProject.projectDescription.Length}")
	@ApiModelProperty(value = "项目描述")
    private String projectDescription;

/**
* 是否自动创建项目: 在某些场景下不需要项目的概念，系统自动创建一个项目便于统一数据模型，在展示的时候不需要显示项目。直接在组织机构下面显示设备管理.
*/
		@ApiModelProperty(value = "是否自动创建项目: 在某些场景下不需要项目的概念，系统自动创建一个项目便于统一数据模型，在展示的时候不需要显示项目。直接在组织机构下面显示设备管理")
    private boolean autoFlag;

/**
* 删状态：true已删除、false未删除.
*/
		@ApiModelProperty(value = "删状态：true已删除、false未删除")
    private boolean deletedFlag;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后修改时间")
    private java.time.LocalDateTime gmtModified;
}