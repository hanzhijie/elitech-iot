/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
* 用户帐户.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseAccount implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 账号id.
    */
    private java.math.BigInteger id;
    /**
    * 手机号国家码.
    */
    private String phoneCountyCode;
    /**
    * 手机号.
    */
    private String phone;
    /**
    * 邮箱.
    */
    private String email;
    /**
    * 密码：32位MD5大写字符串.
    */
    private String accountPassword;
    /**
    * 用户名可以用来登录：手机号、邮箱注册时可以填写用户名，登录可以使用用户名登录.
    */
    private String userName;
    /**
    * 账号头像，相对url地址.
    */
    private String accountPhoto;
    /**
    * 账号状态：0已注册、1已激活、2已冻结、3已删除.
    */
    private Integer accountState;
    /**
    * 微信openid.
    */
    private String wechat;
    /**
    * qq.
    */
    private String qq;
    /**
    * facebook.
    */
    private String facebook;
    /**
    * twitter.
    */
    private String twitter;
    /**
    * linkedin.
    */
    private String linkedin;
    /**
    * 组织机构id.
    */
    private java.math.BigInteger organizationId;
    /**
    * 组织路径.
    */
    private String organizationPath;
    /**
    * 创建时间（格林尼治时间）.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后更新时（格林尼治时间）.
    */
    private java.time.LocalDateTime gmtModified;
    /**
    * 最后登录时间（格林尼治时间）.
    */
    private java.time.LocalDateTime lastLoginTime;
    /**
    * 字符串转换成为long类型.
    */
    private java.math.BigInteger lastLoginIp;
    /**
    * 最后登录方式：1web、2IOS、3Android、4微信小程序、5微信公众号.
    */
    private Integer lastLoginType;
    /**
    * 删状态：true已删除、false未删除.
    */
    private boolean deletedFlag;
}