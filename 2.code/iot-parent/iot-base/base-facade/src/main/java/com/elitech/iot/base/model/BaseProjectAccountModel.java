/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 项目账号关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "项目账号关联表")
public class BaseProjectAccountModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 主键id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseProjectAccount.id.NotNUll}")
	@ApiModelProperty(value = "主键id")
    private java.math.BigInteger id;

/**
* 账号id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseProjectAccount.accountId.NotNUll}")
	@ApiModelProperty(value = "账号id")
    private java.math.BigInteger accountId;

/**
* 项目id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseProjectAccount.projectId.NotNUll}")
	@ApiModelProperty(value = "项目id")
    private java.math.BigInteger projectId;

/**
* 项目path.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseProjectAccount.projectPath.Length}")
	@ApiModelProperty(value = "项目path")
    private String projectPath;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时间")
    private java.time.LocalDateTime gmtModified;
}