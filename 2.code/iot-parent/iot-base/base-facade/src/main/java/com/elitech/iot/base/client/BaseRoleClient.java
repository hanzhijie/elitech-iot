/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-12 16:48:26
*/
package com.elitech.iot.base.client;

import com.elitech.iot.base.domain.BaseRole;
import com.elitech.iot.base.model.BaseRoleModel;
import com.elitech.iot.common.base.api.ResultMessage;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;




/**
* 角色表：用于权限分组 FeignClient.
*
* @author wangjiangmin <wjm@e-elitech.com>
* @date 2020-02-12 16:48:26
* @since v1.0.0
*/
@FeignClient(name = "base-service", path="/base")
@RequestMapping("BaseRole")
public interface BaseRoleClient {
    /**
    *  新增角色表：用于权限分组.
    *
    * @param model 要新增的数据。
    * @return 返回是否新增成功。
    */
    @PostMapping(path = "/insert")
    ResultMessage<BaseRoleModel> insert(@RequestBody BaseRoleModel model);

    /**
    *  批量新增角色表：用于权限分组.
    *
    * @param list
    *            要新增的实体对象列表
    * @return 返回是否新增成功（如果成功则全部成功，否则全部失败）。
    */
    @PostMapping(path = "/insert/list")
    ResultMessage<List<BaseRoleModel>> insertList(@RequestBody List<BaseRoleModel> list);

    /**
        *  删除角色表：用于权限分组.
        *
    * @param pk
    *            要删除的数据主键
    * @return 返回删除的行数。
    */
    @DeleteMapping(path = "/delete/{pk}")
    ResultMessage<Integer> deleteByPk(@PathVariable java.math.BigInteger pk);

    /**
    * 根据id批量删除.
    *
    * @param pkList
    *            要删除数据的主键列表
    * @return
    */
    @DeleteMapping(path = "/delete/list")
    ResultMessage<java.math.BigInteger> deleteByPkList(@RequestBody List<java.math.BigInteger> pkList);

    /**
    * 根据主键更新实体对象.
    *
    * @param domain 要更新的实体对象
    * @return 返回更新的行数。
    */
    @PutMapping(value = "/update")
    ResultMessage<java.math.BigInteger> updateByPk(@RequestBody BaseRoleModel domain);

    /**
    * 根据id查询.
    *
    * @param pk 主键
    * @return 返回指定id的实体对象，如果不存在则返回null。
    */
    @GetMapping(value = "/get/{pk}")
    ResultMessage<BaseRole> selectByPk(@PathVariable BigInteger pk);

    /**
    * 根据id列表查询.
    *
    * @param pkList 主键列表
    * @return 返回指定主键的实体对象列表。
    */
    @GetMapping(value = "/get/list")
    ResultMessage<List<BaseRole>> selectByPkList(@RequestBody List<java.math.BigInteger> pkList);

    /**
    * 条件分页查询.
    *
    * @param start    数据库查询记录偏移值
    * @param pageSize 每页数据条数
    * @param whereMap 查询条件。
    * @return 返回满足条件的分页数据 ,及数据条数。
    */
    @GetMapping(value = "/get/page/{start}/{pageSize}")
    ResultMessage<PageInfo<BaseRole>> pageListByWhere(@PathVariable int start, @PathVariable int pageSize, @RequestBody Map<String, Object> whereMap);
}
