/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 组织机构.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "组织机构")
public class BaseOrganizationModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 组织机构id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseOrganization.id.NotNUll}")
	@ApiModelProperty(value = "组织机构id")
    private java.math.BigInteger id;

/**
* 组织机构名称.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseOrganization.organizationName.NotNUll}")
		@Length(max = 100, message = "{com.elitech.iot.base.model.BaseOrganization.organizationName.Length}")
	@ApiModelProperty(value = "组织机构名称")
    private String organizationName;

/**
* 上级组织机构id.
*/
		@ApiModelProperty(value = "上级组织机构id")
    private java.math.BigInteger parentId;

/**
* 组织机构路径.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseOrganization.organizationPath.NotNUll}")
		@Length(max = 200, message = "{com.elitech.iot.base.model.BaseOrganization.organizationPath.Length}")
	@ApiModelProperty(value = "组织机构路径")
    private String organizationPath;

/**
* 组织机构编码.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseOrganization.organizationCode.NotNUll}")
		@Length(max = 20, message = "{com.elitech.iot.base.model.BaseOrganization.organizationCode.Length}")
	@ApiModelProperty(value = "组织机构编码")
    private String organizationCode;

/**
* 组织机构类型：1生产企业、2经销商、3终端企业、4监管机构.
*/
		@ApiModelProperty(value = "组织机构类型：1生产企业、2经销商、3终端企业、4监管机构")
    private long organizationTypeId;

/**
* 组织机构logo.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseOrganization.organizationLogo.Length}")
	@ApiModelProperty(value = "组织机构logo")
    private String organizationLogo;

/**
* 国家编码.
*/
			@Length(max = 10, message = "{com.elitech.iot.base.model.BaseOrganization.countryCode.Length}")
	@ApiModelProperty(value = "国家编码")
    private String countryCode;

/**
* 所属地域编码.
*/
			@Length(max = 50, message = "{com.elitech.iot.base.model.BaseOrganization.zoneCode.Length}")
	@ApiModelProperty(value = "所属地域编码")
    private String zoneCode;

/**
* 所属区域.
*/
			@Length(max = 100, message = "{com.elitech.iot.base.model.BaseOrganization.zoneAddress.Length}")
	@ApiModelProperty(value = "所属区域")
    private String zoneAddress;

/**
* 详细地址.
*/
			@Length(max = 100, message = "{com.elitech.iot.base.model.BaseOrganization.detailAddress.Length}")
	@ApiModelProperty(value = "详细地址")
    private String detailAddress;

/**
* 删状态：true已删除、false未删除.
*/
		@ApiModelProperty(value = "删状态：true已删除、false未删除")
    private boolean deletedFlag;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时间")
    private java.time.LocalDateTime gmtModified;
}