/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 用户偏好.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseUserProfile implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 账号id.
    */
    private java.math.BigInteger accountId;
    /**
    * 语言.
    */
    private String locale;
    /**
    * 温度单位: 1摄氏度，2华氏度.
    */
    private int temperatureUnit;
    /**
    * 时区.
    */
    private String timeZone;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后更新时间.
    */
    private java.time.LocalDateTime gmtModified;
}