/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 用户角色关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "用户角色关联表")
public class BaseAccountRoleRelModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 主键id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccountRoleRel.id.NotNUll}")
	@ApiModelProperty(value = "主键id")
    private java.math.BigInteger id;

/**
* 账号id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccountRoleRel.accountId.NotNUll}")
	@ApiModelProperty(value = "账号id")
    private java.math.BigInteger accountId;

/**
* 角色id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccountRoleRel.roleId.NotNUll}")
	@ApiModelProperty(value = "角色id")
    private java.math.BigInteger roleId;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccountRoleRel.gmtCreate.NotNUll}")
	@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "修改时间")
    private java.time.LocalDateTime gmtModified;
}