/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 用户帐户.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "用户帐户")
public class BaseAccountModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 账号id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccount.id.NotNUll}")
	@ApiModelProperty(value = "账号id")
    private java.math.BigInteger id;

/**
* 手机号国家码.
*/
			@Length(max = 5, message = "{com.elitech.iot.base.model.BaseAccount.phoneCountyCode.Length}")
	@ApiModelProperty(value = "手机号国家码")
    private String phoneCountyCode;

/**
* 手机号.
*/
			@Length(max = 20, message = "{com.elitech.iot.base.model.BaseAccount.phone.Length}")
	@ApiModelProperty(value = "手机号")
    private String phone;

/**
* 邮箱.
*/
			@Length(max = 100, message = "{com.elitech.iot.base.model.BaseAccount.email.Length}")
	@ApiModelProperty(value = "邮箱")
    private String email;

/**
* 密码：32位MD5大写字符串.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccount.accountPassword.NotNUll}")
		@Length(max = 32, message = "{com.elitech.iot.base.model.BaseAccount.accountPassword.Length}")
	@ApiModelProperty(value = "密码：32位MD5大写字符串")
    private String accountPassword;

/**
* 用户名可以用来登录：手机号、邮箱注册时可以填写用户名，登录可以使用用户名登录.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccount.userName.NotNUll}")
		@Length(max = 100, message = "{com.elitech.iot.base.model.BaseAccount.userName.Length}")
	@ApiModelProperty(value = "用户名可以用来登录：手机号、邮箱注册时可以填写用户名，登录可以使用用户名登录")
    private String userName;

/**
* 账号头像，相对url地址.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseAccount.accountPhoto.Length}")
	@ApiModelProperty(value = "账号头像，相对url地址")
    private String accountPhoto;

/**
* 账号状态：0已注册、1已激活、2已冻结、3已删除.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccount.accountState.NotNUll}")
	@ApiModelProperty(value = "账号状态：0已注册、1已激活、2已冻结、3已删除")
    private int accountState;

/**
* 微信openid.
*/
			@Length(max = 50, message = "{com.elitech.iot.base.model.BaseAccount.wechat.Length}")
	@ApiModelProperty(value = "微信openid")
    private String wechat;

/**
* qq.
*/
			@Length(max = 50, message = "{com.elitech.iot.base.model.BaseAccount.qq.Length}")
	@ApiModelProperty(value = "qq")
    private String qq;

/**
* facebook.
*/
			@Length(max = 50, message = "{com.elitech.iot.base.model.BaseAccount.facebook.Length}")
	@ApiModelProperty(value = "facebook")
    private String facebook;

/**
* twitter.
*/
			@Length(max = 50, message = "{com.elitech.iot.base.model.BaseAccount.twitter.Length}")
	@ApiModelProperty(value = "twitter")
    private String twitter;

/**
* linkedin.
*/
			@Length(max = 50, message = "{com.elitech.iot.base.model.BaseAccount.linkedin.Length}")
	@ApiModelProperty(value = "linkedin")
    private String linkedin;

/**
* 组织机构id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccount.organizationId.NotNUll}")
	@ApiModelProperty(value = "组织机构id")
    private java.math.BigInteger organizationId;

/**
* 组织路径.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccount.organizationPath.NotNUll}")
		@Length(max = 200, message = "{com.elitech.iot.base.model.BaseAccount.organizationPath.Length}")
	@ApiModelProperty(value = "组织路径")
    private String organizationPath;

/**
* 创建时间（格林尼治时间）.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
			@NotNull(message = "{com.elitech.iot.base.model.BaseAccount.gmtCreate.NotNUll}")
	@ApiModelProperty(value = "创建时间（格林尼治时间）")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时（格林尼治时间）.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时（格林尼治时间）")
    private java.time.LocalDateTime gmtModified;

/**
* 最后登录时间（格林尼治时间）.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后登录时间（格林尼治时间）")
    private java.time.LocalDateTime lastLoginTime;

/**
* 字符串转换成为long类型.
*/
		@ApiModelProperty(value = "字符串转换成为long类型")
    private java.math.BigInteger lastLoginIp;

/**
* 最后登录方式：1web、2IOS、3Android、4微信小程序、5微信公众号.
*/
		@ApiModelProperty(value = "最后登录方式：1web、2IOS、3Android、4微信小程序、5微信公众号")
    private int lastLoginType;

/**
* 删状态：true已删除、false未删除.
*/
		@ApiModelProperty(value = "删状态：true已删除、false未删除")
    private boolean deletedFlag;
}