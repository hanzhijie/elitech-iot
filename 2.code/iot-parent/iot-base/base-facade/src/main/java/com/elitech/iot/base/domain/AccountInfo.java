package com.elitech.iot.base.domain;

import lombok.Data;

import java.util.List;

/**
 * @ClassName AccountInfo
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/19
 * @Version V1.0
 **/
@Data
public class AccountInfo {
    private BaseAccount baseAccount;

    private List<BaseRole> baseRoles;

    private List<BasePermission> basePermissions;

    private List<BaseOrganization> baseOrganizations;
}
