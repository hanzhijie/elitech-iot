/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 用户角色关联表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseAccountRoleRel implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 主键id.
    */
    private java.math.BigInteger id;
    /**
    * 账号id.
    */
    private java.math.BigInteger accountId;
    /**
    * 角色id.
    */
    private java.math.BigInteger roleId;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}