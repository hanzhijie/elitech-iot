/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 用户偏好.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "用户偏好")
public class BaseUserProfileModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 账号id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseUserProfile.accountId.NotNUll}")
	@ApiModelProperty(value = "账号id")
    private java.math.BigInteger accountId;

/**
* 语言.
*/
			@Length(max = 10, message = "{com.elitech.iot.base.model.BaseUserProfile.locale.Length}")
	@ApiModelProperty(value = "语言")
    private String locale;

/**
* 温度单位: 1摄氏度，2华氏度.
*/
		@ApiModelProperty(value = "温度单位: 1摄氏度，2华氏度")
    private int temperatureUnit;

/**
* 时区.
*/
			@Length(max = 10, message = "{com.elitech.iot.base.model.BaseUserProfile.timeZone.Length}")
	@ApiModelProperty(value = "时区")
    private String timeZone;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后更新时间")
    private java.time.LocalDateTime gmtModified;
}