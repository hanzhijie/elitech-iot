/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 权限列表：包括菜单、按钮、接口等权限.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BasePermission implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 权限id.
    */
    private java.math.BigInteger permissionId;
    /**
    * 权限名称.
    */
    private String permissionName;
    /**
    * 权限编码，用于权限校验.
    */
    private String permissionCode;
    /**
    * 权限类型：1菜单、2按钮、3接口.
    */
    private int permissionType;
    /**
    * 权限级别:1一级菜单、2二级菜单、3三级菜单.
    */
    private int permissionLevel;
    /**
    * 权限图标：菜单按钮显示的css 图标.
    */
    private String permissionIcon;
    /**
    * 上级ID.
    */
    private java.math.BigInteger parentId;
    /**
    * 逗号分隔的权限id列表，代表权限菜单的上下级关系，便于查询子菜单.
    */
    private String permissionPath;
    /**
    * 权限排序，主要用于菜单显示的顺序控制.
    */
    private long permissionSort;
    /**
    * 权限url.
    */
    private String permissionUrl;
    /**
    * 权限状态：0未启用、1已启用、2已停用.
    */
    private int permissionState;
    /**
    * 权限所属的服务模块.
    */
    private String businessModule;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}