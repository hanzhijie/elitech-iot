/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2020-02-17 15:15:48
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 系统字典表.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "系统字典表")
public class BaseDictModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseDict.id.NotNUll}")
	@ApiModelProperty(value = "id")
    private java.math.BigInteger id;

/**
*  父ID .
*/
		@ApiModelProperty(value = " 父ID ")
    private java.math.BigInteger pid;

/**
*  数据类别,首字母大写 .
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseDict.dataType.NotNUll}")
		@Length(max = 50, message = "{com.elitech.iot.base.model.BaseDict.dataType.Length}")
	@ApiModelProperty(value = " 数据类别,首字母大写 ")
    private String dataType;

/**
*  数据编码 .
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseDict.dataCode.NotNUll}")
		@Length(max = 50, message = "{com.elitech.iot.base.model.BaseDict.dataCode.Length}")
	@ApiModelProperty(value = " 数据编码 ")
    private String dataCode;

/**
*  数据名称/值 .
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseDict.dataValue.NotNUll}")
		@Length(max = 200, message = "{com.elitech.iot.base.model.BaseDict.dataValue.Length}")
	@ApiModelProperty(value = " 数据名称/值 ")
    private String dataValue;

/**
*  顺序 .
*/
		@ApiModelProperty(value = " 顺序 ")
    private java.math.BigInteger sortNo;

/**
* 0正常,1删除.
*/
		@ApiModelProperty(value = "0正常,1删除")
    private int status;

/**
* 数据描述.
*/
			@Length(max = 400, message = "{com.elitech.iot.base.model.BaseDict.dataDesc.Length}")
	@ApiModelProperty(value = "数据描述")
    private String dataDesc;

/**
* .
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "")
    private java.time.LocalDateTime updateTime;
}