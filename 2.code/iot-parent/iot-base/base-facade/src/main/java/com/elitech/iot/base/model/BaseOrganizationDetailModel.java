/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 企业详细信息.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "企业详细信息")
public class BaseOrganizationDetailModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* .
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseOrganizationDetail.organizationId.NotNUll}")
	@ApiModelProperty(value = "")
    private java.math.BigInteger organizationId;

/**
* .
*/
			@Length(max = 100, message = "{com.elitech.iot.base.model.BaseOrganizationDetail.organizationName.Length}")
	@ApiModelProperty(value = "")
    private String organizationName;
}