/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 企业详细信息.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseOrganizationDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * .
    */
    private java.math.BigInteger organizationId;
    /**
    * .
    */
    private String organizationName;
}