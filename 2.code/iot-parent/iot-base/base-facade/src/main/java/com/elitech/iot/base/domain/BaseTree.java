package com.elitech.iot.base.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

/**
 * @ClassName BaseTree
 * @Description: 树结构
 * @Author st
 * @Date 2020/3/25
 * @Version V1.0
 **/
@Getter
@Setter
@ToString
public class BaseTree implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 树节点id
     */
    private BigInteger id;
    /**
     * 树节点显示
     */
    private String title;
    /**
     * 父节点id
     */
    private BigInteger parentId;
    /**
     * 子节点列表
     */
    private List<BaseTree> children;
}
