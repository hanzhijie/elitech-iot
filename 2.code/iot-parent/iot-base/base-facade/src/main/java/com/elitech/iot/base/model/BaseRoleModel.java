/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 角色表：用于权限分组.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "角色表：用于权限分组")
public class BaseRoleModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 角色id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseRole.roleId.NotNUll}")
	@ApiModelProperty(value = "角色id")
    private java.math.BigInteger roleId;

/**
* 角色名称.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseRole.roleName.NotNUll}")
		@Length(max = 20, message = "{com.elitech.iot.base.model.BaseRole.roleName.Length}")
	@ApiModelProperty(value = "角色名称")
    private String roleName;

/**
* 角色编码.
*/
			@Length(max = 20, message = "{com.elitech.iot.base.model.BaseRole.roleCode.Length}")
	@ApiModelProperty(value = "角色编码")
    private String roleCode;

/**
* 角色描述.
*/
			@Length(max = 100, message = "{com.elitech.iot.base.model.BaseRole.roleDescription.Length}")
	@ApiModelProperty(value = "角色描述")
    private String roleDescription;

/**
* 所属组织id: 0系统默认角色.
*/
		@ApiModelProperty(value = "所属组织id: 0系统默认角色")
    private java.math.BigInteger organizationId;

/**
* 所属组织path：逗号分隔的organization_id，便于查询.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseRole.organizationPath.Length}")
	@ApiModelProperty(value = "所属组织path：逗号分隔的organization_id，便于查询")
    private String organizationPath;

/**
* 是否系统角色：true是，false不是.
*/
		@ApiModelProperty(value = "是否系统角色：true是，false不是")
    private boolean systemFlag;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 更新时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "更新时间")
    private java.time.LocalDateTime gmtModified;
}