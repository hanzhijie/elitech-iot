/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* OAuth2.0客户端信息： oauth_access_token、oauth_refresh_token信息存储在re.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ApiModel(description = "OAuth2.0客户端信息： oauth_access_token、oauth_refresh_token信息存储在re")
public class BaseClientDetailsModel implements Serializable {
private static final long serialVersionUID = 1L;


/**
* 客户端信息主键 id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.id.NotNUll}")
	@ApiModelProperty(value = "客户端信息主键 id")
    private java.math.BigInteger id;

/**
* 客户端id.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.clientId.NotNUll}")
		@Length(max = 32, message = "{com.elitech.iot.base.model.BaseClientDetails.clientId.Length}")
	@ApiModelProperty(value = "客户端id")
    private String clientId;

/**
* 客户密钥.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.clientSecret.NotNUll}")
		@Length(max = 32, message = "{com.elitech.iot.base.model.BaseClientDetails.clientSecret.Length}")
	@ApiModelProperty(value = "客户密钥")
    private String clientSecret;

/**
* 客户端类型：1web云平台、2IOS、3Android、4微信小程序、5微信公众号、100第三方、1000设备.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.clientType.NotNUll}")
	@ApiModelProperty(value = "客户端类型：1web云平台、2IOS、3Android、4微信小程序、5微信公众号、100第三方、1000设备")
    private int clientType;

/**
* 资源id.
*/
			@Length(max = 256, message = "{com.elitech.iot.base.model.BaseClientDetails.resourceIds.Length}")
	@ApiModelProperty(value = "资源id")
    private String resourceIds;

/**
* 作用范围.
*/
			@Length(max = 256, message = "{com.elitech.iot.base.model.BaseClientDetails.scope.Length}")
	@ApiModelProperty(value = "作用范围")
    private String scope;

/**
* 授权类型.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.authorizedGrantTypes.NotNUll}")
		@Length(max = 256, message = "{com.elitech.iot.base.model.BaseClientDetails.authorizedGrantTypes.Length}")
	@ApiModelProperty(value = "授权类型")
    private String authorizedGrantTypes;

/**
* 重定向地址.
*/
			@Length(max = 200, message = "{com.elitech.iot.base.model.BaseClientDetails.webServerRedirectUri.Length}")
	@ApiModelProperty(value = "重定向地址")
    private String webServerRedirectUri;

/**
* 角色.
*/
			@Length(max = 256, message = "{com.elitech.iot.base.model.BaseClientDetails.authorities.Length}")
	@ApiModelProperty(value = "角色")
    private String authorities;

/**
* access_token有效时间.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.accessTokenValidity.NotNUll}")
	@ApiModelProperty(value = "access_token有效时间")
    private long accessTokenValidity;

/**
* refresh_token有效时间.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.refreshTokenValidity.NotNUll}")
	@ApiModelProperty(value = "refresh_token有效时间")
    private long refreshTokenValidity;

/**
* 自动授权.
*/
			@NotNull(message = "{com.elitech.iot.base.model.BaseClientDetails.autoApprove.NotNUll}")
	@ApiModelProperty(value = "自动授权")
    private int autoApprove;

/**
* 附加信息.
*/
			@Length(max = 65535, message = "{com.elitech.iot.base.model.BaseClientDetails.additionalInformation.Length}")
	@ApiModelProperty(value = "附加信息")
    private String additionalInformation;

/**
* 创建时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "创建时间")
    private java.time.LocalDateTime gmtCreate;

/**
* 最后修改时间.
*/
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
		@ApiModelProperty(value = "最后修改时间")
    private java.time.LocalDateTime gmtModified;
}