/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 项目.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseProject implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * id.
    */
    private java.math.BigInteger id;
    /**
    * 项目名称.
    */
    private String projectName;
    /**
    * 项目类型id.
    */
    private java.math.BigInteger projectTypeId;
    /**
    * 所属国家编码.
    */
    private String countryCode;
    /**
    * 地址编码：便于统计分析.
    */
    private String zoneCode;
    /**
    * 所属区域.
    */
    private String zoneAddress;
    /**
    * 详细地址.
    */
    private String detailAdress;
    /**
    * 所属组织机构id.
    */
    private java.math.BigInteger organizationId;
    /**
    * 所属组织路径.
    */
    private String organizationName;
    /**
    * 上级ID.
    */
    private java.math.BigInteger parentId;
    /**
    * 项目路径.
    */
    private String projectPath;
    /**
    * 项目描述.
    */
    private String projectDescription;
    /**
    * 是否自动创建项目: 在某些场景下不需要项目的概念，系统自动创建一个项目便于统一数据模型，在展示的时候不需要显示项目。直接在组织机构下面显示设备管理.
    */
    private boolean autoFlag;
    /**
    * 删状态：true已删除、false未删除.
    */
    private boolean deletedFlag;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 最后修改时间.
    */
    private java.time.LocalDateTime gmtModified;
}