/**
*  Copyright © 2020-2120 . All Rights Reserved.
*  CreateTime：  2019-12-27 09:21:38
*/

package com.elitech.iot.base.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
* 角色表：用于权限分组.
* @author wangjiangmin <wjm@e-elitech.com>
* @since v1.0.0
*
*/
@Getter
@Setter
@ToString
public class BaseRole implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
    * 角色id.
    */
    private java.math.BigInteger roleId;
    /**
    * 角色名称.
    */
    private String roleName;
    /**
    * 角色编码.
    */
    private String roleCode;
    /**
    * 角色描述.
    */
    private String roleDescription;
    /**
    * 所属组织id: 0系统默认角色.
    */
    private java.math.BigInteger organizationId;
    /**
    * 所属组织path：逗号分隔的organization_id，便于查询.
    */
    private String organizationPath;
    /**
    * 是否系统角色：true是，false不是.
    */
    private boolean systemFlag;
    /**
    * 创建时间.
    */
    private java.time.LocalDateTime gmtCreate;
    /**
    * 更新时间.
    */
    private java.time.LocalDateTime gmtModified;
}