package com.elitech.iot.lock;

import com.elitech.iot.lock.aop.MethodInterceptorHolder;
import com.elitech.iot.lock.util.ExpressionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

/**
 * 锁处理器.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
@Slf4j
public class LockProcessor<A extends Annotation, L> {
    private A lockAnnotation;

    private MethodInterceptorHolder methodInterceptorHolder;

    private Function<A, String[]> lockNameGetter;

    private Function<String, L> lockGetter;

    private LockAccepter<L> lockAccepter;

    private LockAccepter<L> unlockAccepter;

    private Map<String, L> lockStore = new HashMap<>();

    private LockProcessor() {
    }

    public static <A extends Annotation, L> LockProcessor<A, L> build(A annotation, MethodInterceptorHolder holder) {
        LockProcessor<A, L> alLockProcessor = new LockProcessor<>();
        alLockProcessor.lockAnnotation = annotation;
        alLockProcessor.methodInterceptorHolder = holder;
        return alLockProcessor;
    }

    public LockProcessor<A, L> lockNameIs(Function<A, String[]> lockNameGetter) {
        this.lockNameGetter = lockNameGetter;
        return this;
    }

    public LockProcessor<A, L> lockIs(Function<String, L> lockGetter) {
        this.lockGetter = lockGetter;
        return this;
    }

    public LockProcessor<A, L> lock(LockAccepter<L> lockAccepter) {
        this.lockAccepter = lockAccepter;
        return this;
    }

    public LockProcessor<A, L> unlock(LockAccepter<L> unlockAccepter) {
        this.unlockAccepter = unlockAccepter;
        return this;
    }

    public LockProcessor<A, L> init() {
        Objects.requireNonNull(lockAnnotation);
        Objects.requireNonNull(methodInterceptorHolder);
        Objects.requireNonNull(lockNameGetter);
        String[] lockNameArr = lockNameGetter.apply(lockAnnotation);
        if (lockNameArr.length == 0) {
            String name = createLockName(null);
            lockStore.put(name, lockGetter.apply(name));
        } else {
            for (String expression : lockNameArr) {
                String name = createLockName(expression);
                lockStore.put(name, lockGetter.apply(name));
            }
        }
        return this;
    }

    protected String createLockName(String expression) {
        try {
            if (StringUtils.isEmpty(expression)) {
                return methodInterceptorHolder.getMethod().getName().concat("_").concat(methodInterceptorHolder.getKey());
            }
            return ExpressionUtil.analytical(expression, methodInterceptorHolder.getArgs(), String.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<L> successLock = new ArrayList<>();

    public Throwable doLock() {
        Throwable lockError = null;
        for (Map.Entry<String, L> lock : lockStore.entrySet()) {
            try {
                boolean success = lockAccepter.accept(lock.getValue());
                if (!success) {
                    return new TimeoutException("try lock " + lock.getKey() + " error");
                }
                successLock.add(lock.getValue());
            } catch (Throwable throwable) {
                lockError = throwable;
            }
        }
        return lockError;
    }

    public void doUnlock() {
        for (L lock : successLock) {
            try {
                unlockAccepter.accept(lock);
            } catch (Throwable error) {
                log.error("unlock {} error", methodInterceptorHolder.getMethod(), error);
            }
        }
    }
}
