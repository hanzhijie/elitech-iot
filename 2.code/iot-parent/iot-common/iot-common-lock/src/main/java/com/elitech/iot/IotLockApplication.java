package com.elitech.iot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * Hello world!
 */
@SpringBootApplication
@Configuration
@EnableCaching
public class IotLockApplication {
    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(IotLockApplication.class, args);
    }


    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
