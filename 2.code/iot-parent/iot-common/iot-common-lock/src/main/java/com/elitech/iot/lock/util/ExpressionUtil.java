package com.elitech.iot.lock.util;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * spel 表达式工具类.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public class ExpressionUtil {
    private static final Map<String, Expression> CACHE = new ConcurrentHashMap<>();

    private static final ExpressionParser PARSER = new SpelExpressionParser();

    /**
     * 计算表达式.
     *
     * @param expressionString  表达式字符串
     * @param argMap            参数表
     * @param desiredResultType
     * @param <T>
     * @return 表达式的值
     */
    public static <T> T analytical(String expressionString, Map<String, Object> argMap, Class<T> desiredResultType) {
        Expression expression = CACHE.get(expressionString);
        StandardEvaluationContext context = new StandardEvaluationContext(argMap);
        for (Map.Entry<String, Object> entry : argMap.entrySet()) {
            context.setVariable(entry.getKey(), entry.getValue());
        }
        if (expression == null) {
            expression = PARSER.parseExpression(expressionString);
            CACHE.putIfAbsent(expressionString, expression);
        }
        return expression.getValue(context, desiredResultType);
    }
}
