package com.elitech.iot.lock.aop;

import com.elitech.iot.lock.MethodInterceptorContext;
import com.elitech.iot.lock.util.AopUtil;
import com.elitech.iot.lock.util.ThreadLocalUtil;
import lombok.Getter;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.support.AopUtils;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.util.DigestUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
@Getter
public class MethodInterceptorHolder {
    public static final ParameterNameDiscoverer NAME_DISCOVERER = new LocalVariableTableParameterNameDiscoverer();

    private String key;

    private Method method;

    private Object target;

    private Map<String, Object> args;

    public MethodInterceptorHolder(String key, Method method, Object target, Map<String, Object> args) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(method);
        Objects.requireNonNull(target);
        Objects.requireNonNull(args);

        this.key = key;
        this.method = method;
        this.target = target;
        this.args = args;
    }

    /**
     * @return
     */
    public static MethodInterceptorHolder current() {
        return ThreadLocalUtil.get(MethodInterceptorHolder.class.getName());
    }

    public static MethodInterceptorHolder create(MethodInvocation methodInvocation) {
        String key = DigestUtils.md5DigestAsHex(String.valueOf(methodInvocation.getMethod().hashCode()).getBytes());
        String[] argNames = NAME_DISCOVERER.getParameterNames(methodInvocation.getMethod());
        Object[] args = methodInvocation.getArguments();
        Map<String, Object> argMap = new LinkedHashMap<>();
        for (int i = 0; i < args.length; i++) {
            String name = (argNames == null || argNames[i] == null) ? "arg_" + i : argNames[i];
            argMap.put(name, args[i]);
        }

        return new MethodInterceptorHolder(key, methodInvocation.getMethod(), methodInvocation.getThis(), argMap);
    }

    public MethodInterceptorHolder set() {
        MethodInterceptorHolder.setCurrent(this);
        return this;
    }

    public static MethodInterceptorHolder clear() {
        return ThreadLocalUtil.getAndRemove(MethodInterceptorHolder.class.getName());
    }

    public static MethodInterceptorHolder setCurrent(MethodInterceptorHolder holder) {
        return ThreadLocalUtil.put(MethodInterceptorHolder.class.getName(), holder);
    }

    public <T extends Annotation> T findMethodAnnotation(Class<T> annClass) {
        return AopUtil.findMethodAnnotation(annClass, method, annClass);
    }

    public <T extends Annotation> T findClassAnnotation(Class<T> annClass) {
        return AopUtil.findAnnotation(target.getClass(), annClass);
    }

    public <T extends Annotation> T findAnnotation(Class<T> annClass) {
        return AopUtil.findAnnotation(target.getClass(), method, annClass);
    }

    public MethodInterceptorContext createParamContext() {
        return createParamContext(null);
    }

    public MethodInterceptorContext createParamContext(Object invokeResult) {
        return new MethodInterceptorContext() {
            private static final long serialVersionUID = -4102787561601219273L;

            @Override
            public Object getTarget() {
                return target;
            }

            @Override
            public Method getMethod() {
                return method;
            }

            @Override
            public <T> Optional<T> getParameter(String name) {
                if (args == null) {
                    return Optional.empty();
                }
                return Optional.of((T) args.get(name));
            }

            @Override
            public <T extends Annotation> T getAnnotation(Class<T> annClass) {
                return findAnnotation(annClass);
            }

            @Override
            public Map<String, Object> getParams() {
                return getArgs();
            }

            @Override
            public Object getInvokeResult() {
                return invokeResult;
            }
        };
    }
}
