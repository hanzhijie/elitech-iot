package com.elitech.iot.lock.redis;

import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Redis 分布式锁自动配置.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/13
 */
@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class RedisLockAutoConfiguration {
    @Autowired
    private RedissonClient client;

    @Bean
    public RedisLockFactory redisLockFactory() {
        return new RedisLockFactory(client);
    }
}
