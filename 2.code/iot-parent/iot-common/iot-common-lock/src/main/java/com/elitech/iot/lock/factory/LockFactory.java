package com.elitech.iot.lock.factory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * 锁的工厂类.
 * <p>用于获取锁。</p>
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public interface LockFactory {
    /**
     * 根据锁名称获取锁.
     * <p>相同的名称,则锁也相同</p>
     *
     * @param lockName 锁的名称
     * @return 锁对象
     * @see Lock
     */
    Lock getLock(String lockName);

    /**
     * 根据锁名称获取读写锁.
     * <p>相同的名称,则锁也相同</p>
     *
     * @param lockName 锁的名称
     * @return 读写锁对象
     * @see ReadWriteLock
     */
    ReadWriteLock getReadWriteLock(String lockName);
}
