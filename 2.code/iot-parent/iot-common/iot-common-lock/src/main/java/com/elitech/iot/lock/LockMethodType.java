package com.elitech.iot.lock;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public enum LockMethodType {
    /**
     * 失败的方法.
     */
    FAILED_METHOD("Failed_Method"),

    /**
     * 成功完成的方法.
     */
    SUCCESSED_METHOD("Succeed_Method"),

    /**
     * 已加锁的方法.
     */
    LOCKED_METHOD("Locked_Method");


    private String type;

    private LockMethodType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
