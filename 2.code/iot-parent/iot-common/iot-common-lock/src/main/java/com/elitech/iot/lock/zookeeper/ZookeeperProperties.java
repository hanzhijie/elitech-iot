package com.elitech.iot.lock.zookeeper;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/13
 */
@Getter
@Setter
@Component
@RefreshScope
//@ConfigurationProperties
//@ConfigurationProperties(prefix = "")
public class ZookeeperProperties {
    /**
     * zookeeper 地址.
     */
    @Value("${lock.zookeeper.zkList}")
    private String zkList;

    /**
     * 重试次数.
     */
    @Value("${lock.zookeeper.retryCount}")
    private int retryCount;

    /**
     * 重试间隔时间.
     */
    @Value("${lock.zookeeper.elapsedTimeMs}")
    private int elapsedTimeMs;

    /**
     * session超时时间.
     */
    @Value("${lock.zookeeper.sessionTimeoutMs}")
    private int sessionTimeoutMs;

    /**
     * 连接超时时间.
     */
    @Value("${lock.zookeeper.connectionTimeoutMs}")
    private int connectionTimeoutMs;
}
