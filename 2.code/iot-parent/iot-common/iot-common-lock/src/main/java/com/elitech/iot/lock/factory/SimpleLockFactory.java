package com.elitech.iot.lock.factory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 基于jvm的简单锁工程类.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public class SimpleLockFactory extends AbstractLockFactory {
    @Override
    protected Lock createLock(String lockName) {
        return new ReentrantLock();
    }

    @Override
    protected ReadWriteLock createReadWriteLock(String lockName) {
        return new ReentrantReadWriteLock();
    }
}
