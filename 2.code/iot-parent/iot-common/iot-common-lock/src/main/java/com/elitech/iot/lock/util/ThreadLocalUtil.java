package com.elitech.iot.lock.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public class ThreadLocalUtil {
    private static final ThreadLocal<Map<String, Object>> THREAD_LOCAL = ThreadLocal.withInitial(HashMap::new);

    private ThreadLocalUtil() {
    }

    /**
     * 获取ThreadLocal中的全部值.
     *
     * @return
     */
    public static Map<String, Object> getAll() {
        return THREAD_LOCAL.get();
    }


    public static <T> T put(String key, T value) {
        THREAD_LOCAL.get().put(key, value);
        return value;
    }

    /**
     * 根据key删除.
     *
     * @param key
     */
    public static void remove(String key) {
        THREAD_LOCAL.get().remove(key);
    }

    /**
     * 清除数据.
     */
    public static void clear() {
        THREAD_LOCAL.get().clear();
    }

    /**
     * 根据key查询数据.
     *
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T get(String key) {
        return (T) THREAD_LOCAL.get().get(key);
    }

    /**
     * 根据key查询数据.
     *
     * @param key
     * @param supplierOnNull
     * @param <T>
     * @return
     */
    public static <T> T get(String key, Supplier<T> supplierOnNull) {
        return ((T) THREAD_LOCAL.get().computeIfAbsent(key, k -> supplierOnNull.get()));
    }

    /**
     * 根据key获取并删除数据.
     *
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T getAndRemove(String key) {
        try {
            return get(key);
        } finally {
            remove(key);
        }
    }
}
