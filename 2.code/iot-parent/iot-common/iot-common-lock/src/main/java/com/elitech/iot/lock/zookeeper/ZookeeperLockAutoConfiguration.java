package com.elitech.iot.lock.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/13
 */
@Configuration
//@EnableConfigurationProperties(ZookeeperProperties.class)
public class ZookeeperLockAutoConfiguration {
    @Autowired
    private ZookeeperProperties zookeeperProperties;

    /**
     * 创建一个CuratorFramework 客户端(Zookeeper 客户端).
     *
     * @return
     */
    @Bean(initMethod = "start")
    public CuratorFramework curatorFramework() {
        Objects.requireNonNull(zookeeperProperties.getZkList(), "zookeeper list cannot be null");

        System.out.println("************");
        System.out.println("Zookeeper List: " + zookeeperProperties.getZkList());

        return CuratorFrameworkFactory.newClient(
                zookeeperProperties.getZkList(),
                zookeeperProperties.getSessionTimeoutMs(),
                zookeeperProperties.getConnectionTimeoutMs(),
                new RetryNTimes(zookeeperProperties.getRetryCount(), zookeeperProperties.getElapsedTimeMs()));
    }

    @Bean
    public ZookeeperLockFactory zookeeperLockFactory() {
        return new ZookeeperLockFactory(curatorFramework());
    }

}
