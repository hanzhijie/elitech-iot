package com.elitech.iot.lock.redis;

import com.elitech.iot.lock.factory.AbstractLockFactory;
import org.redisson.api.RedissonClient;
import org.redisson.spring.data.connection.RedissonConnectionFactory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * redis 分布式锁.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/12
 */
public class RedisLockFactory extends AbstractLockFactory {

    /**
     * Redis 连接工厂.
     */
    private RedissonConnectionFactory connectionFactory;

    private RedissonClient redissonClient;

    /**
     * 创建一个RedisLockFactory类的实例.
     * @param redissonClient Redission 客户端
     */
    public RedisLockFactory(RedissonClient redissonClient) {
        if (null == redissonClient) {
            throw new NullPointerException();
        }
        this.redissonClient = redissonClient;
    }

    @Override
    protected Lock createLock(String lockName) {
        return redissonClient.getFairLock(lockName);
    }

    @Override
    protected ReadWriteLock createReadWriteLock(String lockName) {
        return redissonClient.getReadWriteLock(lockName);
    }
}
