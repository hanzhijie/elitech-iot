package com.elitech.iot.lock;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public interface LockAccepter<T> {
    boolean accept(T t) throws Throwable;
}
