package com.elitech.iot.lock.factory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * 抽象分布式锁工厂类.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public abstract class AbstractLockFactory implements LockFactory {
    private final ConcurrentHashMap<String, Lock> lockMap = new ConcurrentHashMap<>(128);
    private final ConcurrentHashMap<String, ReadWriteLock> readWriteLockMap = new ConcurrentHashMap<>(128);

    @Override
    public Lock getLock(String lockName) {
        Lock lock = lockMap.get(lockName);
        if (lock == null) {
            return lockMap.computeIfAbsent(lockName, this::createLock);
        }
        return lock;
    }

    @Override
    public ReadWriteLock getReadWriteLock(String lockName) {
        ReadWriteLock lock = readWriteLockMap.get(lockName);
        if (lock == null) {
            return readWriteLockMap.computeIfAbsent(lockName, this::createReadWriteLock);
        }
        return lock;
    }

    /**
     * 创建锁.
     *
     * @param lockName 锁的名称
     * @return 锁
     */
    protected abstract Lock createLock(String lockName);

    /**
     * 创建读写锁.
     *
     * @param lockName 锁的名称
     * @return 读写锁
     */
    protected abstract ReadWriteLock createReadWriteLock(String lockName);
}
