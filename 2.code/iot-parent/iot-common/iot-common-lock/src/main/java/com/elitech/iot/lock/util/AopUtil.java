package com.elitech.iot.lock.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public class AopUtil {
    private AopUtil() {
    }

    /**
     * 查找方法上的注解.
     *
     * @param targetClass
     * @param method
     * @param annotationClass
     * @param <T>
     * @return
     */
    public static <T extends Annotation> T findMethodAnnotation(Class targetClass, Method method, Class<T> annotationClass) {
        Method m = method;
        T annotation = AnnotationUtils.findAnnotation(m, annotationClass);
        if (annotation != null) {
            return annotation;
        }
        m = ClassUtils.getMostSpecificMethod(m, targetClass);
        annotation = AnnotationUtils.findAnnotation(m, annotationClass);
        if (annotation == null) {
            List<Class> supers = new ArrayList<>();
            supers.addAll(Arrays.asList(targetClass.getInterfaces()));
            if (targetClass.getSuperclass() != Object.class) {
                supers.add(targetClass.getSuperclass());
            }

            for (Class aClass : supers) {
                if (aClass == null) {
                    continue;
                }
                Method ims[] = new Method[1];

                ReflectionUtils.doWithMethods(aClass, im -> {
                    if (im.getName().equals(method.getName()) && im.getParameterCount() == method.getParameterCount()) {
                        ims[0] = im;
                    }
                });

                if (ims[0] != null) {
                    annotation = findMethodAnnotation(aClass, ims[0], annotationClass);
                    if (annotation != null) {
                        return annotation;
                    }
                }
            }
        }
        return annotation;
    }

    /**
     * 查找目标类上的注解.
     *
     * @param targetClass     目标类
     * @param annotationClass 注解类
     * @param <T>
     * @return
     */
    public static <T extends Annotation> T findAnnotation(Class targetClass, Class<T> annotationClass) {
        return AnnotationUtils.findAnnotation(targetClass, annotationClass);
    }

    /**
     * 查找注解.
     *
     * @param targetClass     目标类
     * @param method          目标方法
     * @param annotationClass 注解类
     * @param <T>
     * @return
     */
    public static <T extends Annotation> T findAnnotation(Class targetClass, Method method, Class<T> annotationClass) {
        T methodAnnotation = findMethodAnnotation(targetClass, method, annotationClass);
        if (methodAnnotation != null) {
            return methodAnnotation;
        }
        return findAnnotation(targetClass, annotationClass);
    }

    /**
     * 查找注解.
     *
     * @param joinPoint       注入点
     * @param annotationClass 注解类
     * @param <T>
     * @return
     */
    public static <T extends Annotation> T findAnnotation(JoinPoint joinPoint, Class<T> annotationClass) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Class<?> targetClass = joinPoint.getTarget().getClass();
        return findAnnotation(targetClass, method, annotationClass);
    }

    /**
     * 获取方法签名字符串.
     * <p>
     * 如: doSomThing(String key, int value)
     * </p>
     *
     * @param joinPoint 注入点
     * @return
     */
    public static String getMethodBody(JoinPoint joinPoint) {
        StringBuilder methodName = new StringBuilder(joinPoint.getSignature().getName()).append("(");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String[] names = signature.getParameterNames();
        Class[] args = signature.getParameterTypes();
        for (int i = 0, len = args.length; i < len; i++) {
            if (i != 0) {
                methodName.append(",");
            }
            methodName.append(args[i].getSimpleName()).append(" ").append(names[i]);
        }
        return methodName.append(")").toString();
    }

    /**
     * 获取参数表.
     *
     * @param joinPoint 注入点
     * @return 参数键值对的LinkedHashMap
     */
    public static Map<String, Object> getArgsMap(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Map<String, Object> args = new LinkedHashMap<>();
        String[] names = signature.getParameterNames();
        for (int i = 0, len = names.length; i < len; i++) {
            args.put(names[i], joinPoint.getArgs()[i]);
        }
        return args;
    }
}
