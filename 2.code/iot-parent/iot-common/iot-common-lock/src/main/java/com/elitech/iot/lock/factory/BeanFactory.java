package com.elitech.iot.lock.factory;

public interface BeanFactory {

    /**
     * 获得bean
     *
     * @param var1
     * @return T
     */
    <T> T getBean(Class<T> var1);

    /**
     * 判断是否是clazz的工厂类
     *
     * @param clazz
     * @return boolean
     */
    <T> boolean isFactory(Class<T> clazz);

}
