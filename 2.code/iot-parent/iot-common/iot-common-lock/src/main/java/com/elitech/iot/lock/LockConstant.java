package com.elitech.iot.lock;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
public class LockConstant {
    /**
     * 分布式锁的根地址.
     */
    public static final String ROOT_LOCK = "/elitech_lock/";
}
