package com.elitech.iot.lock.aop;

import com.elitech.iot.lock.LockProcessor;
import com.elitech.iot.lock.annotation.Lock;
import com.elitech.iot.lock.annotation.ReadLock;
import com.elitech.iot.lock.annotation.WriteLock;
import com.elitech.iot.lock.factory.LockFactory;
import com.elitech.iot.lock.util.AopUtil;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
@Slf4j
public class LockPointcutAdvisor extends StaticMethodMatcherPointcutAdvisor {


    public LockPointcutAdvisor(LockFactory lockFactory) {
        Objects.requireNonNull(lockFactory);

        // new MethodInterceptor 改写成lambda
        setAdvice((MethodInterceptor) invocation -> {
            MethodInterceptorHolder holder = MethodInterceptorHolder.create(invocation);
            // 查找方法上的Lock注解
            Lock lockAnnotation = holder.findMethodAnnotation(Lock.class);

            // 查找方法上的 ReqdLock注解
            ReadLock readLockAnnotation = holder.findMethodAnnotation(ReadLock.class);

            // 查找方法上的WriteLock注解
            WriteLock writeLockAnnotation = holder.findMethodAnnotation(WriteLock.class);

            List<LockProcessor> lockProcessorList = new ArrayList<>();
            if (lockAnnotation != null) {
                LockProcessor<Lock, java.util.concurrent.locks.Lock> processor = LockProcessor.<Lock, java.util.concurrent.locks.Lock>build(lockAnnotation, holder)
                        .lockNameIs(Lock::name)
                        .lockIs(lockFactory::getLock);
                LockProcessor<Lock, java.util.concurrent.locks.Lock> lockInfo = initLockInfo(lockAnnotation.timeout(), lockAnnotation.timeUnit(), processor);

                lockProcessorList.add(lockInfo);
            }

            if (readLockAnnotation != null) {
                LockProcessor<ReadLock, java.util.concurrent.locks.Lock> processor = LockProcessor.<ReadLock, java.util.concurrent.locks.Lock>build(readLockAnnotation, holder)
                        .lockNameIs(ReadLock::name)
                        .lockIs(name -> lockFactory.getReadWriteLock(name).readLock());
                LockProcessor<ReadLock, java.util.concurrent.locks.Lock> lockInfo = initLockInfo(readLockAnnotation.timeout(), readLockAnnotation.timeUnit(), processor);

                lockProcessorList.add(lockInfo);
            }

            if (writeLockAnnotation != null) {
                LockProcessor<WriteLock, java.util.concurrent.locks.Lock> processor = LockProcessor.<WriteLock, java.util.concurrent.locks.Lock>build(writeLockAnnotation, holder)
                        .lockNameIs(WriteLock::name)
                        .lockIs(name -> lockFactory.getReadWriteLock(name).writeLock());
                LockProcessor<WriteLock, java.util.concurrent.locks.Lock> lockInfo = initLockInfo(writeLockAnnotation.timeout(), writeLockAnnotation.timeUnit(), processor);

                lockProcessorList.add(lockInfo);
            }

            boolean lockFailed = false;
            try {
                for (LockProcessor processor : lockProcessorList) {
                    Throwable ex = processor.doLock();
                    if (ex != null) {
                        lockFailed = true;
                        throw ex;
                    }
                }

                return invocation.proceed();
            }finally {
                for (LockProcessor processor : lockProcessorList){
                    try {
                        processor.doUnlock();
                    }catch (Exception  ex){
                        if (!lockFailed){
                            log.error("unlock {} error!", invocation.getMethod(), ex);
                        }
                    }
                }
            }
        });


    }


    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        Lock lock = AopUtil.findMethodAnnotation(targetClass, method, Lock.class);
        if (null != lock) {
            return true;
        }
        ReadLock readLock = AopUtil.findMethodAnnotation(targetClass, method, ReadLock.class);
        if (null != readLock) {
            return true;
        }
        WriteLock writeLock = AopUtil.findMethodAnnotation(targetClass, method, WriteLock.class);
        if (null != writeLock) {
            return true;
        }
        return false;
    }

    @Override
    public int getOrder(){
        return Integer.MIN_VALUE;
    }

    protected <A extends Annotation> LockProcessor<A, java.util.concurrent.locks.Lock> initLockInfo(long timeout, TimeUnit timeUnit, LockProcessor<A, java.util.concurrent.locks.Lock> lockProcessor) {
        return lockProcessor
                .lock(lock -> lock.tryLock(timeout, timeUnit))
                .unlock(lock -> {
                    lock.unlock();
                    return true;
                }).init();
    }
}
