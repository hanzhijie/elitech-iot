package com.elitech.iot.lock.redis;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.stereotype.Component;

/**
 * redis 配置信息.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/12
 */
@Getter
@Component
public class RedisLockProperties {
    @Autowired
    private RedisProperties redisProperties;

    /**
     * 获取redis连接模式.
     *
     * @return
     */
    public RedisMode getRedisMode() {
        if (redisProperties.getCluster() != null) {
            // 集群模式
            return RedisMode.CLUSTER;
        } else if (redisProperties.getSentinel() != null) {
            // 哨兵模式
            return RedisMode.SENTINEL;
        } else if (redisProperties.getJedis() != null) {
            // Jedis 连接池模式
            return RedisMode.JEDIS_POOL;
        } else if (redisProperties.getLettuce() != null) {
            // Lettuce 连接池模式
            return RedisMode.LETTUCE_POOL;
        } else {
            // 单机模式
            return RedisMode.SINGLE;
        }
    }
}
