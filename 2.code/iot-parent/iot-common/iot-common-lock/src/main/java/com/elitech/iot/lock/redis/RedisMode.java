package com.elitech.iot.lock.redis;

/**
 * redis 集群模式.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/12
 */
public enum RedisMode {
    /**
     * 单机模式.
     */
    SINGLE,

    /**
     * Jedis连接池模式.
     */
    JEDIS_POOL,

    /**
     * Lettuce 连接池模式.
     */
    LETTUCE_POOL,

    /**
     * 集群模式.
     */
    CLUSTER,

    /**
     * 哨兵模式.
     */
    SENTINEL;
}
