package com.elitech.iot.lock.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 写锁的注解.
 * <p>
 * 用于方法上，对方法加锁。
 * </p>
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/11
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface WriteLock {
    /**
     * 锁名.
     * <p>支持表达式，表达式使用 ${} 进行标识；如果此值为空,则使用方法名称作为锁的名称。</p>
     * <pre>
     *     &#064;WriteLock("elitech_lock_${#id}")
     *     public void doSomeThing(String id){
     *
     *     }
     *
     *     &#064;WriteLock(value="elitech_lock_${#id}",condition="#id!=null")
     *     public void doSomeThing(String id){
     *
     *     }
     * </pre>
     *
     * @return 锁的名称
     */
    String[] name() default {};

    /**
     * 锁的条件表达式,当满足条件的时候才执行锁
     * e.g.
     * <pre>
     *     &#064;WriteLock(value="elitech_lock_${#id}",condition="#id!=null")
     *     public void foo(String id){
     *
     *     }
     * </pre>
     *
     * @return 条件表达式
     */
    String condition() default "";

    /**
     * 超时时间.
     * <p>超过此时间不能获取锁则抛出异常{@link InterruptedException}；如果设置为-1，则认为不设置超时时间。</p>
     *
     * @return 超时时间, 默认10秒
     */
    long timeout() default 10;

    /**
     * 超时时间单位.
     * <p>
     * 默认秒。
     * </p>
     *
     * @return 超时时间单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;
}
