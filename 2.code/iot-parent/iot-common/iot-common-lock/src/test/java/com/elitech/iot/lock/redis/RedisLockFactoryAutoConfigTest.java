package com.elitech.iot.lock.redis;

import com.elitech.iot.IotLockApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/13
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IotLockApplication.class})
public class RedisLockFactoryAutoConfigTest {
    @Autowired
    private RedisLockFactory redisLockFactory;

    @Test
    public void autoConfigTest() {
        assertNotNull(redisLockFactory);
    }

}
