package com.elitech.iot.lock.redis;

import com.elitech.iot.IotLockApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.locks.Lock;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IotLockApplication.class})
public class RedisLockFactoryTest {
    @Autowired
    private RedissonClient redissonClient;

    RedisLockFactory lockFactory;

    private int value = 0;
    private int threadCount = 100;
    private int count = 100;
    String lockName = "com.elitech.iot.device.create";

    @Before
    public void setUp() {
        lockFactory = new RedisLockFactory(redissonClient);

    }

    @Test
    public void lockTest() throws InterruptedException {


        Thread[] threads = new Thread[count];

        for (int i = 0; i < threadCount; i++) {
            Thread th = new Thread(new IncrementRunnable());
            threads[i] = th;
            th.start();
        }

        for (int i = 0; i < threadCount; i++) {
            threads[i].join();
        }


        int expected = threadCount * count;
        assertEquals(expected, value);
        System.out.println(value);
    }


    public class IncrementRunnable implements Runnable {
        Lock lock = lockFactory.createLock(lockName);

        @Override
        public void run() {
            for (int i = 0; i < count; i++) {
                lock.lock();
                value++;
                lock.unlock();
            }
        }
    }


}
