package com.elitech.iot.lock.zookeeper;

import com.elitech.iot.IotLockApplication;
import com.elitech.iot.lock.redis.RedisLockFactoryTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/3/13
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IotLockApplication.class})
public class ZookeeperLockFactoryTest {
    @Autowired
    private ZookeeperLockFactory lockFactory;

    private int value = 0;
    private int threadCount = 1000;
    private int count = 1000;
    String lockName = "/com/elitech/iot/lock/device/create";

    @Test
    public void initTest() {
        assertNotNull(lockFactory);
    }

    @Test
    public void lockTest() throws InterruptedException {
        Lock lock = lockFactory.createLock(lockName);
        lock.lock();
        value++;
        lock.unlock();
    }

    @Test
    public void concurrentTest() throws InterruptedException {
        Thread[] threads = new Thread[count];

        LocalDateTime start = LocalDateTime.now();
        for (int i = 0; i < threadCount; i++) {
            Thread th = new Thread(new ZookeeperLockFactoryTest.IncrementRunnable());
            threads[i] = th;
            th.start();
        }

        for (int i = 0; i < threadCount; i++) {
            threads[i].join();
        }

        LocalDateTime end = LocalDateTime.now();
        Duration duration = Duration.between(start, end);


        System.out.println("耗费时间(Sec)：" + duration.getSeconds());

        int expected = threadCount * count;
        assertEquals(expected, value);
        System.out.println(value);
    }


    public class IncrementRunnable implements Runnable {
        Lock lock = lockFactory.createLock(lockName);

        @Override
        public void run() {
            lock.lock();
            for (int i = 0; i < count; i++) {

                value++;

            }
            lock.unlock();
        }
    }
}
