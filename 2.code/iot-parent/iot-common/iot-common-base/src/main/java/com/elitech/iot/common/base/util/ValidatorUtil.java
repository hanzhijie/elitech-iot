package com.elitech.iot.common.base.util;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/21
 */
public class ValidatorUtil {
    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * 数据有效性验证.
     *
     * @param domain 要进行校验的实体对象.
     * @return 如果校验失败则返回校验失败错误消息的Map集合
     */
    public static <T> Map<String, StringBuffer> validate(T domain) {
        Map<String, StringBuffer> errorMap = null;
        Set<ConstraintViolation<T>> set = validator.validate(domain, Default.class);

        if (set != null && set.size() > 0) {
            errorMap = new HashMap<>(10);
            String property = null;

            for (ConstraintViolation<T> cv : set) {
                property = cv.getPropertyPath().toString();
                if (errorMap.get(property) != null) {
                    errorMap.get(property).append("," + cv.getMessage());
                } else {
                    StringBuffer sb = new StringBuffer();
                    sb.append(cv.getMessage());
                    errorMap.put(property, sb);
                }
            }
        }

        return errorMap;
    }
}
