package com.elitech.iot.common.base.api;

/**
 * 公共API请求响应码.
 * 错误码
 * 0 成功 非0失败
 *
 * 1-1000 我们不用，因为有HTTP的一些错误码
 * 1001-1999 定义我们的一些公共的错误码
 *
 * 2xxx base模块
 * 2000-2099 用户账号相关
 * 2100-2199 组织
 * 2200-2299 项目
 * 2300-2399 订单支付
 * 3xxx 设备模块
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019年12月13日
 */
public enum ResultCode {
    /**
     * 系统忙.
     */
    SYSTEM_BUSY(-1, "api.response.code.systemBusy"),

    /**
     * 成功.
     */
    SUCCESS(0, "api.response.code.success"),

    /**
     * 服务器内部错误.
     */
    SERVER_INTERNAL_ERROR(500, "api.response.code.ServerInternalError"),

    /**
     * 参数错误.
     */
    PARAM_ERROR(1001, "api.response.code.paramError"),

    /**
     * 未支持的语言
     */
    LANGUAGE_UNSUPPORT(1002, "api.response.code.languageUnSupport"),
    /**
     * 字典已存在
     */
    DICT_EXIST(1101,"api.response.code.dict.dictExist"),
    /**
     * 字典已存在
     */
    DICT_NOT_EXIST(1102,"api.response.code.dict.dictNotExist"),

    /**------------------------------------------------------用户账号相关--------------------------------------- */

    /**
     * 帐户不存在.
     */
    ACCOUNT_NOT_EXIST(2001, "api.response.code.user.accountNotExist"),

    /**
     * 帐户已经存在.
     */
    ACCOUNT_EXIST(2002, "api.response.code.user.accountExist"),

    /**
     * 密码不正确.
     */
    PASSWORD_ERROR(2003, "api.response.code.user.passwordError"),

    /**
     * 短信验证码不正确
     */
    CHECKCODE_ERROR(2004,"api.response.code.user.checkcodeError"),
/**
     * 图形验证码不正确
     */
    VERIFY_ERROR(2009,"api.response.code.user.checkcodeError"),

    /**
     * 验证码请求过于频繁
     */
    CHECKCODE_FREQUENTLY(2004,"api.response.code.user.checkcodeError"),

    /**
     * 密码长度不够.
     */
    PASSWORD_LENGTH_INSUFFICIENT(2005, "api.response.code.user.insufficientPasswordLength"),

    /**
     * 密码复杂度不够.
     */
    PASSWORD_COMPLEXITY_INSUFFICIENT(2006, "api.response.code.user.insufficientPasswordComplexity"),

    /**
     * 生成Leaf Segment id失败.
     */
    GENERATE_SEGMENT_ID_ERROR(2007, "api.response.code.uid.generateSegmentException"),

    /**
     * 生成Leaf Snowflake id失败.
     */
    GENERATE_SNOWFLAKE_ID_ERROR(2007, "api.response.code.uid.generateSnowflakeException"),


    /**------------------------------------------------------项目相关--------------------------------------- */
    /**
     * 项目已存在
     */
    PROJECT_EXIST(2201, "api.response.code.project.projectExist"),

    /**------------------------------------------------------组织机构相关--------------------------------------- */
    /**
     * 组织机构已存在
     */
    ORGANIZATION_EXIST(2101, "api.response.code.organization.organizationExist"),

    /**-------------------------------------------------------设备相关------------------------------------------*/
    /**
     * 设备类型已存在
     */
    DEVICE_TYPE_EXIST(2301, "api.response.code.deviceType.deviceTypeExist"),
    /**
     * 设备类型不存在
     */
    DEVICE_TYPE_NOT_EXIST(2302, "api.response.code.deviceType.deviceTypeNotExist"),
    /**
     * 参数组名称已存在
     */
    PARAM_GROUP_EXIST(2321, "api.response.code.paramGroup.paramGroupExist"),
    /**
     * 参数组名称不存在
     */
    PARAM_GROUP_NOT_EXIST(2322, "api.response.code.paramGroup.paramGroupNotExist"),

    /**
     * 参数不已存在
     */
    TYPE_PARAM_NOT_EXIST(2340, "api.response.code.typeParam.typeParamNotExist"),
    /**
     * 参数key已存在
     */
    PARAM_KEY_EXIST(2341, "api.response.code.paramKey.paramKeyExist"),
    /**
     * 参数编码已存在
     */
    PARAM_CODE_EXIST(2342, "api.response.code.paramCode.paramCodeExist"),
    /**
     * 参数名称已存在
     */
    PARAM_NAME_EXIST(2343, "api.response.code.paramName.paramNameExist"),
    /**
     * 参数数据类型编码已存在
     */
    PARAM_DATA_TYPE_CODE_EXIST(2360, "api.response.code.paramDataTypeCode.paramDataTypeCodeExist"),
    /**
     * 参数数据类型编码不存在
     */
    PARAM_DATA_TYPE_CODE_NOT_EXIST(2361, "api.response.code.paramDataTypeCode.paramDataTypeCodeNotExist"),
    /**-------------------------------------------------------订单支付相关------------------------------------------*/
    /**
     * 支付结果失败
     */
    PAY_RESULT_ERROR(2301,"api.response.code.payment.error"),
    /**
     * 退款失败
     */
    PAY_REFUND_ERROR(2302,"api.response.code.payment.refund.error"),
    /**
     *  退款申请已存在，无需重复申请
     */
    PAY_REFUND_APPLY_EXIST(2303,"api.response.code.payment.apply.refund.exist"),
    /**
     *  退款申请不存在
     */
    PAY_REFUND_APPLY_NOT_EXIST(2304,"api.response.code.payment.apply.refund.notExist"),
    /**
     *  订单不存在
     */
    PAY_ORDER_NOT_EXIST(2305,"api.response.code.payment.order.notExist"),
    /**
     * 未知错误.
     */
    UNKNOWN_ERROR(-1000, "api.response.code.unknownError");

    /**
     * 响应码.
     */
    private int code;

    /**
     * 消息消息key(多语言key).
     */
    private String messageKey;

    /**
     * 创建一个ResponseCode的实例.
     *
     * @param code       响应码
     * @param messageKey
     */
    ResultCode(int code, String messageKey) {
        this.code = code;
        this.messageKey = messageKey;
    }

    public int getCode() {
        return code;
    }

    public String getMessageKey() {
        return messageKey;
    }


}
