package com.elitech.iot.common.base.util;

import java.util.Random;

/**
 * @ClassName CheckCodeUtil
 * @Description: 验证码工具类
 * @Author dongqg
 * @Date 2020/1/8
 * @Version V1.0
 **/
public class CheckCodeUtil {
    /**
     * @MethodName: getCheckCodeNum
     * @Description: 获取指定位数的纯数字验证码
     * @Param: [Num] 位数
     * @Return: java.lang.String
     * @Author: dongqg
     * @Date: 2020/1/8
    **/
    public static String getCheckCodeNum(int num){
        StringBuilder checkCode = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < num; i++) {
            checkCode.append(random.nextInt(10));
        }
        return checkCode.toString();
    }
}
