package com.elitech.iot.common.base.util;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.UUID;

/**
 * 文件辅助类.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
@Slf4j
public class FileUtil {
    private static final String PATH_SEPARATOR = "/";
    private static final String DRIVE_PATTERN = "^[a-zA-Z]:/.*";

    /**
     * 获取文件名后缀。
     *
     * @param fileName 文件名
     * @return 返回文件后缀名。
     */
    public static String getFileSuffixName(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
    }

    /**
     * 创建文件。
     *
     * @param fullFilePath 文件的全路径，使用POSIX风格。
     * @return 返回创建的文件，若路径为null，返回null。
     * @throws IOException 创建文件异常。
     */
    public static File create(String fullFilePath) throws IOException {
        if (fullFilePath == null) {
            return null;
        }
        return create(newFile(fullFilePath));
    }

    /**
     * 创建文件。
     *
     * @param file 要创建的文件对象
     * @return 返回创建的文件，若路径为null，返回null。
     * @throws IOException 创建文件异常。
     */
    public static File create(File file) throws IOException {
        if (null == file) {
            return null;
        }

        if (!file.exists()) {
            System.setProperty("file.encoding", "UTF-8");
            mkParentDirs(file);
            file.createNewFile();
        }
        return file;
    }

    /**
     * 创建给定文件或目录的父目录。
     *
     * @param file 文件或目录
     * @return 返回父目录。
     */
    public static File mkParentDirs(File file) {
        final File parentFile = file.getParentFile();
        if (null != parentFile) {
            parentFile.mkdirs();
        }
        return parentFile;
    }

    /**
     * 创建File对象。<br>
     * 自动识别相对或绝对路径，相对路径将自动从ClassPath下查找。
     *
     * @param path 文件路径
     * @return 返回新创建的File对象。
     * @throws UnsupportedEncodingException 路径包含中文时解码可能发生异常。
     */
    public static File newFile(String path) throws UnsupportedEncodingException {
        if (StringUtil.isEmpty(path)) {
            throw new NullPointerException("File path is empty!");
        }
        return new File(getAbsolutePath(path));
    }

    /**
     * 获取绝对路径，相对于classes的根目录。<br>
     * 如果给定就是绝对路径，则返回原路径，并把所有\替换为/。
     *
     * @param path 相对路径
     * @return 返回绝对路径
     * @throws UnsupportedEncodingException 编码异常（不支持UTF-8编码）， 路径包含中文时解码可能发生异常。
     */
    private static String getAbsolutePath(String path) throws UnsupportedEncodingException {


        if (path == null) {
            path = StringUtil.EMPTY;
        } else {
            path = path.replaceAll("[/\\\\]+", "/");

            if (path.startsWith(PATH_SEPARATOR) || path.matches(DRIVE_PATTERN)) {
                // 给定的路径已经是绝对路径了
                return path;
            }
        }

        // 相对路径
        ClassLoader classLoader = ClassUtil.getClassLoader();
        URL url = classLoader.getResource(path);

        String resultPath = url != null ? url.getPath() : classLoader.getResource(StringUtil.EMPTY).getPath() + path;
        resultPath = URLDecoder.decode(resultPath, "UTF-8");

        return resultPath;
    }

    /**
     * 创建File对象。
     *
     * @param parent 父目录
     * @param path   文件路径
     * @return 返回新创建的File对象。
     */
    public static File newFile(File parent, String path) {
        if (StringUtil.isEmpty(path)) {
            throw new NullPointerException("File path is empty!");
        }
        return new File(parent, path);
    }

    /**
     * 创建File对象。
     *
     * @param parent 父目录
     * @param path   文件路径
     * @return 回新创建的File对象。
     */
    public static File newFile(String parent, String path) {
        if (StringUtil.isEmpty(path) || StringUtil.isEmpty(parent)) {
            throw new NullPointerException("parent or path is empty!");
        }
        return new File(parent, path);
    }

    /**
     * 读取文件到输出流。
     *
     * @param out      用于读取文件的输出Stream
     * @param filename 文件名称（文件的绝对路径名称）
     * @throws IOException 读取文件发生异常
     */
    public static void readFile(OutputStream out, String filename) throws IOException {
        if (StringUtil.isEmpty(filename)) {
            return;
        }

        File file = new File(filename);
        readFile(out, file);
    }

    /**
     * 读取文件到输出流。
     *
     * @param out  用于读取文件的输出Stream
     * @param file 要读取的文件
     * @throws IOException 读取文件异常
     */
    public static void readFile(OutputStream out, File file) throws IOException {
        if (file.exists()) {
            FileInputStream fileInput = null;
            BufferedInputStream bufferedInput = null;
            try {
                fileInput = new FileInputStream(file);
                bufferedInput = new BufferedInputStream(fileInput);
                // 输出的缓存
                byte[] bufferedByte = new byte[1024];
                int i;
                // 读取文件的输入流
                while ((i = bufferedInput.available()) > 0) {
                    if (i < 1024) {
                        bufferedByte = new byte[i];
                    }
                    fileInput.read(bufferedByte);
                    out.write(bufferedByte);
                }
            } finally {
                try {
                    if (bufferedInput != null) {
                        bufferedInput.close();
                    }
                    if (fileInput != null) {
                        fileInput.close();
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
        }
    }

    /**
     * 将数据写入到文件。<br>
     * 注意：如果文件本来就有内容将会被覆盖。
     *
     * @param filename 文件名(绝对路径)
     * @param data     要写入到文件的数据
     * @throws IOException 文件写入异常
     */
    public static void writeFile(String filename, byte[] data) throws IOException {
        if (StringUtil.isEmpty(filename)) {
            throw new IOException("filename不能为empty.");
        }

        File file = new File(filename);
        writeFile(file, data);
    }

    /**
     * 将数据写入到文件。<br>
     * 注意：如果文件本来就有内容将会被覆盖。
     *
     * @param file 要写入数据的文件
     * @param data 要写入的数据
     * @throws IOException 文件写入异常
     */
    public static void writeFile(File file, byte[] data) throws IOException {
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        if (!file.exists()) {
            file.createNewFile();
        }

        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(data);
            fos.flush();
        }
    }

    /**
     * 根据路径删除指定的目录或文件，无论存在与否
     *
     * @param folderPath 要删除的目录或文件
     * @return 删除成功返回 true，否则返回 false。
     */
    public static boolean deletePath(String folderPath) {
        boolean flag = false;
        try {
            File file = new File(folderPath);
            // 判断目录或文件是否存在
            if (file.exists()) {
                // 判断是否为文件
                if (file.isFile()) {
                    // 为文件时调用删除文件方法
                    flag = deleteFile(folderPath);
                } else {
                    // 为目录时调用删除目录方法
                    flag = deleteDirectory(folderPath);
                }
            }
        } finally {

        }
        return flag;
    }

    /**
     * 删除单个文件
     *
     * @param filePath 被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String filePath) {
        boolean flag = false;

        File file = new File(filePath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            flag = file.delete();
        }

        return flag;
    }

    /**
     * 删除目录（文件夹）以及目录下的文件
     *
     * @param folderPath 被删除目录的文件路径
     * @return 目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String folderPath) {
        boolean flag;

        // 如果sPath不以文件分隔符结尾，自动添加文件分隔符
        if (!folderPath.endsWith(File.separator)) {
            folderPath = folderPath + File.separator;
        }
        File dirFile = new File(folderPath);
        // 如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        // 删除文件夹下的所有文件(包括子目录)
        File[] files = dirFile.listFiles();
        if (files != null) {
            for (File file : files) {
                // 删除子文件
                if (file.isFile()) {
                    flag = deleteFile(file.getAbsolutePath());
                    if (!flag) {
                        break;
                    }
                } // 删除子目录
                else {
                    flag = deleteDirectory(file.getAbsolutePath());
                    if (flag) {
                        continue;
                    }
                    break;
                }
            }
        }

        // 删除当前目录
        if (dirFile.delete()) {
            flag = true;
        } else {
            flag = false;
        }

        return flag;
    }

    /**
     * 获得随机UUID文件名
     *
     * @param fileName 文件名
     * @return uuid文件名
     */
    public static String generateRandomFileName(String fileName) {
        //首相获得扩展名，然后生成一个UUID码作为名称，然后加上扩展名
        String ext = fileName.substring(fileName.lastIndexOf("."));
        return UUID.randomUUID().toString() + ext;
    }
}
