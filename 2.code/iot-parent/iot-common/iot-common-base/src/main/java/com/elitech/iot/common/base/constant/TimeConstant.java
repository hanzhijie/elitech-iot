package com.elitech.iot.common.base.constant;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
public class TimeConstant {
    public static final String DEFAULT_TIME_ZONE_NAME = "UTC";

}
