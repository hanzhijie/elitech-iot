package com.elitech.iot.common.base.util;

import java.text.NumberFormat;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @Date 2019/12/15
 */
public class StringUtil {
    /**
     * 空字符串.
     */
    public static final String EMPTY = "";
    private static final String SLASH = "/";

    /**
     * 首字母大写.
     *
     * @param str 要进行转换的字符串
     * @return
     */
    public static String upperCaseFirst(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * 首字母小写.
     *
     * @param str 要进行转换的字符串
     * @return
     */
    public static String lowerCaseFirst(String str) {
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    /**
     * 下划线，转换为驼峰式
     *
     * @param underscore 下划线分隔字符串
     * @return 驼峰式字符串
     */
    public static String underlineToCamelCase(String underscore) {
        StringBuilder result = new StringBuilder();
        if (underscore != null && underscore.trim().length() > 0) {
            boolean flag = false;
            for (int i = 0; i < underscore.length(); i++) {
                char ch = underscore.charAt(i);
                if ("_".charAt(0) == ch) {
                    flag = true;
                } else {
                    if (flag) {
                        result.append(Character.toUpperCase(ch));
                        flag = false;
                    } else {
                        result.append(ch);
                    }
                }
            }
        }
        return result.toString();
    }

    /**
     * 判断字符串是否为null或空.
     *
     * @param str 要进行判断的字符串
     * @return 如果字符串为null或""则返回true，否则返回false。
     */
    public static boolean isEmpty(String str) {
        if (str == null || "".equals(str)) {
            return true;
        }

        return false;
    }

    /**
     * 字符串左补0.
     *
     * @param value 要进行左补0的字符串
     * @param len   字符串总长度
     * @return 返回左补0后的字符串
     */
    public static String leftPaddingZero(int value, int len) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        nf.setMaximumIntegerDigits(len);
        nf.setMinimumIntegerDigits(len);

        return nf.format(value);
    }

    /**
     * 判断末尾是否/，如果没有追加/
     *
     * @param str
     * @return
     */
    public static String endWithSlash(String str) {
        if (!isEmpty(str)) {
            if (!str.endsWith(SLASH)) {
                return str + SLASH;
            }
            return str;
        }
        return EMPTY;
    }
}
