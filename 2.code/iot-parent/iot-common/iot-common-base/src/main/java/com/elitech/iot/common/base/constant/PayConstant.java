package com.elitech.iot.common.base.constant;

/**
 * 支付常量类
 * @author st
 * @date 2020-03-16
 */
public class PayConstant {
    /**
     * 支付宝支付，网关返回码，10000为网关调用成功
     */
    public static final String ALI_COMMON_CODE_SUCCESS = "10000";
    /**
     *支付宝支付结果状态，交易完成
     */
    public static final String TRADE_STATUS_FINISHED ="TRADE_FINISHED";
    /**
     *支付宝支付结果状态，交易成功
     */
    public static final String TRADE_STATUS_SUCCESS ="TRADE_SUCCESS";
    /**
     *微信支付结果状态，交易成功
     */
    public static final String WECHAT_STATUS_SUCCESS ="SUCCESS";
    /**
     * 微信支付
     */
    public static final Byte PAY_TYPE_WECHAT=1;
    /**
     * 支付宝
     */
    public static final Byte PAY_TYPE_ALI=2;
    /**
     * 订单状态:未支付
     */
    public static final Byte PAY_STATUS_UNPAD=1;
    /**
     * 订单状态：已支付
     */
    public static final Byte PAY_STATUS_PAID=2;
    /**
     * 订单状态：已退款
     */
    public static final Byte PAY_STATUS_REFUND=3;
    /**
     * 退款状态：未退款
     */
    public static final Byte REFUND_STATUS_NO=0;
    /**
     * 退款状态：已退款
     */
    public static final Byte REFUND_STATUS_YES=1;
}
