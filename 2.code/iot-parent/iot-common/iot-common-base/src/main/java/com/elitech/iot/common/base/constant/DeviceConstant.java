package com.elitech.iot.common.base.constant;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
public class DeviceConstant {
    /**
     * 是否自动发送短信：1是
     */
    public static final Byte SMS_AUTO_YES = 1;
    /**
     * 是否自动发送短信：0否
     */
    public static final Byte SMS_AUTO_NO = 0;
    /**
     * 是否wifi设备:1是
     */
    public static final Byte IS_WIFI_YES = 1;
    /**
     * 是否wifi设备:0否
     */
    public static final Byte IS_WIFI_NO = 0;

    /**
     * 连接类型:  0长连接
     */
    public static final Byte CONNECT_TYPE_LONG = 0;
    /**
     * 连接类型:  1短连接
     */
    public static final Byte CONNECT_TYPE_SHORT = 1;
    /**
     * sim卡默认流量：30M
     */
    public static final int DEFAULT_SIM_TRAFFIC_30 = 30;
    /**
     * sim卡默认流量：100M
     */
    public static final int DEFAULT_SIM_TRAFFIC_100 = 100;
    /**
     * sim卡默认流量：300M
     */
    public static final int DEFAULT_SIM_TRAFFIC_300 = 300;
    /**
     * 数据地图模式：有传感器数据与位置数据
     */
    public static final Byte MAP_MODE_DATA_MAP = 0;
    /**
     * 数据地图模式：有传感器数据与位置数据
     */
    public static final Byte MAP_MODE_DATA_NOMAP = 1;
    /**
     * 数据地图模式：有传感器数据与位置数据
     */
    public static final Byte MAP_MODE_NODATA_NOMAP = 2;
    /**
     *  温度单位: 1摄氏度；
     */
    public static final Byte TEMPERATURE_UNIT_C = 1;
    /**
     * 温度单位: 2华氏度；
     */
    public static final Byte TEMPERATURE_UNIT_F = 2;
    /**
     * 默认分组名称
     */
    public  static final String NORMAL_GROUP_NAME="默认分组";

}
