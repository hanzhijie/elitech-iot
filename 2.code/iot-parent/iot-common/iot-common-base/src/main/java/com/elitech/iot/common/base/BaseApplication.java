package com.elitech.iot.common.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
@SpringBootApplication
@Configuration
public class BaseApplication {
    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(BaseApplication.class, args);
    }

    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
