package com.elitech.iot.common.base.constant;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
public class SymbolConstant {
    /**
     * 逗号
     */
    public static final String COMMA_NAME = ",";

}
