package com.elitech.iot.common.base.constant;

/**
 * @ClassName CalibrationConstant
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/4/1
 * @Version V1.0
 **/
public class CalibrationConstant {
    /**
     * 运费
     */
    public static final Integer CALIBRATION_FREIGHT_FEE = 15;

    /**
     * 校准点类型：温度
     */
    public static final String CALIBRATION_POINT_TEMPERATURE = "1";

    /**
     * 校准点类型：湿度
     */
    public static final String CALIBRATION_POINT_HUMIDITY = "2";

    /**
     * 服务类型,不加急
     */
    public static final Integer APPLICATION_SERVICE_TYPE_NORMAL = 0;

    /**
     * 加急
     */
    public static final Integer APPLICATION_SERVICE_TYPE_URGENT = 1;

    /**
     * 是否开票
     */
    public static final Byte APPLICATION_INVOICE_FLAG = 0;


    /**
     * 发票类型 个人
     */
    public static final Byte APPLICATION_INVOICE_TYPE_PERSONAL = 1;

    /**
     * 发票类型 企业
     */
    public static final Byte APPLICATION_INVOICE_TYPE_COMPAMY = 2;
}
