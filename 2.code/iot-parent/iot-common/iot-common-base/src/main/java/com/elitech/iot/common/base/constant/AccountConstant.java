package com.elitech.iot.common.base.constant;

/**
 * @ClassName AccountConstant
 * @Description: 用户模块常量类
 * @Author dongqg
 * @Date 2020/2/12
 * @Version V1.0
 **/
public class AccountConstant {
    /**
     * 注册验证码存储前缀
     */
    public static final String REGISTER_CHECKCODE = "register_checkcode";
    /**
     * 手机登录验证码存储前缀
     */
    public static final String LOGIN_CHECKCODE = "login_checkcode";

    /**
     * 邮箱注册标题
     */
    public static final String EMAIL_REGISTER_SUBJECT = "（精创冷云）邮箱注册激活码";

    /**
     * 邮箱注册验证码内容前缀
     */
    public static final String EMAIL_REGISTER_CONTENT_PREFIX = "您此次注册精创冷云的验证码为：";
    /**
     * 邮箱注册验证码内容后缀
     */
    public static final String EMAIL_REGISTER_CONTENT_SUFFIX = "（请在十分钟内激活，过期无效）";

    /**
     * 图片验证码存储前缀
     */
    public static final String VERIFY_CODE_IP = "verify_code_ip";
    /**
     * 忘记密码验证码
     */
    public static final String FORGOT_CHECKCODE = "forgot_checkcode";
    /**
     * 邮箱重置密码标题
     */
    public static final String EMAIL_FORGOT_SUBJECT = "（精创冷云）重置密码";
    /**
     * 邮箱重置密码内容前缀
     */
    public static final String EMAIL_FORGOT_CONTENT_PREFIX = "您此次重置密码的验证码为：";
    /**
     * 邮箱重置密码内容后缀
     */
    public static final String EMAIL_FORGOT_CONTENT_SUFFIX = "（请在十分钟内激活，过期无效）";


    /**
     * 密码长度限制最多32位（加密后）
     */
    public static final int PASSWORD_LENGH = 32;
    /**
     * 注册类型-手机
     */
    public static final String REGISTER_TYPE_PHONE = "1";
    /**
     * 注册类型-邮件
     */
    public static final String REGISTER_TYPE_EMAIL = "2";
    /**
     * 重置密码-通过手机重置
     */
    public static final String FORGOT_TYPE_PHONE = "1";
    /**
     * 重置密码-通过邮件重置
     */
    public static final String FORGOT_TYPE_EMAIL = "2";
    /**
     * 手机登录验证码
     */
    public static final String PHONE_CHECKCODE_LOGIN = "1";
    /**
     * 手机注册验证码
     */
    public static final String PHONE_CHECKCODE_REGISTER = "2";
    /**
     * 手机重置密码验证码
     */
    public static final String PHONE_CHECKCODE_FORGOT = "3";
    /**
     * 邮箱注册验证码
     */
    public static final String EMAIL_CHECKCODE_REGISTER = "1";
    /**
     * 邮箱重置密码验证码
     */
    public static final String EMAIL_CHECKCODE_FORGOT = "2";

}
