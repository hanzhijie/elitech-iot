package com.elitech.iot.common.base.api;


import com.elitech.iot.common.base.constant.TimeConstant;
import com.elitech.iot.common.base.i18n.I18nUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * 统一响应信息.
 *
 * @param <T> 响应信息中的数据的类型
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/15
 */
@Data
@Getter
@ApiModel(value = "result", description = "REST API响应信息(所有rest API都返回该类的对象)")
public class ResultMessage<T> {
    /**
     * 响应码.
     */
    @ApiModelProperty(value = "响应码", required = true)
    private int code;

    /**
     * 响应提示消息.
     */
    @ApiModelProperty(value = "响应消息", required = true)
    private String message;

    /**
     * 错误消息.
     */
    @ApiModelProperty(value = "错误消息")
    private String error;

    /**
     * 响应时间.
     */
    @ApiModelProperty(value = "响应时间", required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    private LocalDateTime time;

    /**
     * 响应数据
     */
    @ApiModelProperty(value = "响应数据")
    private T data;

    /**
     * 创建一个ResultMessage对象.
     */
    public ResultMessage() {
        time = LocalDateTime.now(ZoneId.of(TimeConstant.DEFAULT_TIME_ZONE_NAME));
    }

    /**
     * 创建一个ResultMessage对象.
     *
     * @param code    响应码
     * @param message 响应消息
     * @param data    响应数据
     */
    public ResultMessage(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
        time = LocalDateTime.now(ZoneId.of(TimeConstant.DEFAULT_TIME_ZONE_NAME));
    }

    /**
     * 成功响应信息.
     *
     * @param data 响应数据
     * @param <T>  响应数据类型
     * @return 执行成功返回ResultMessage对象
     */
    public static <T> ResultMessage success(T data) {
        String message = I18nUtil.getMessage(ResultCode.SUCCESS.getMessageKey());
        return new ResultMessage().code(0).message(message).data(data);
    }

    /**
     * 失败响应信息.
     *
     * @param code    响应码
     * @param message 消息消息
     * @return
     */
    public static ResultMessage fail(int code, String message) {
        return new ResultMessage().code(code).message(message);
    }

    /**
     * 失败响应信息.
     *
     * @param resultCode 统一响应码
     * @return 失败响应消息
     */
    public static ResultMessage fail(ResultCode resultCode) {
        String message = I18nUtil.getMessage(resultCode.getMessageKey());
        return new ResultMessage().code(resultCode.getCode()).message(message);
    }

    /**
     * 设置ResultMessage对象的响应码.
     *
     * @param resultCode 响应码
     * @return ResultMessage对象
     */
    public ResultMessage code(int resultCode) {
        this.code = resultCode;
        return this;
    }

    /**
     * 设置ResultMessage对象的响应消息.
     *
     * @param resultMessage 响应消息
     * @return ResultMessage对象
     */
    public ResultMessage message(String resultMessage) {
        this.message = resultMessage;
        return this;
    }

    /**
     * 错误详细信息.
     *
     * @param detailMessage 错误信息国际化key
     * @return ResultMessage对象
     */
    public ResultMessage detailMessage(String detailMessage) {
        this.error = detailMessage;
        return this;
    }

    /**
     * 设置ResultMessage对象的响应数据.
     *
     * @param data 响应数据
     * @return ResultMessage对象
     */
    public ResultMessage data(T data) {
        this.data = data;
        return this;
    }

    /**
     * 是否成功.
     *
     * @return 成功返回true，失败返回false
     */
    @JsonIgnore
    public boolean isSuccess() {
        return code == 0;
    }

    /**
     * 是否失败.
     *
     * @return 失败返回true，失败返回false
     */
    @JsonIgnore
    public boolean isFail() {
        return code != 0;
    }
}
