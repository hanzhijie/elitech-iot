package com.elitech.iot.common.base.util;

import lombok.ToString;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
@ToString
public class StringUtilTest {
    @Test
    public void isEmpty() {
        boolean expected = true;
        boolean actual = StringUtil.isEmpty("");
        assertEquals(expected, actual);

        expected = false;
        actual = StringUtil.isEmpty(" ");
        assertEquals(expected, actual);
    }

    @Test
    public void underlineToCamelCase() {
        String expected = "accountId";
        String columnName = "account_id";
        String actual = StringUtil.underlineToCamelCase(columnName);
        assertEquals(expected, actual);
    }

    @Test
    public void upperCaseFirst() {
        String expected = "AccountId";
        String columnName = "accountId";
        String actual = StringUtil.upperCaseFirst(columnName);
        assertEquals(expected, actual);
    }

    @Test
    public void endWithSlash() {
        String expected = "D;/git/";
        String dir = "D;/git";
        String actual = StringUtil.endWithSlash(dir);
        assertEquals(expected, actual);
    }

    @Test
    public void leftPaddingZero() {
        String expected = "00000003";
        String actual = StringUtil.leftPaddingZero(3, 8);
        assertEquals(expected, actual);
    }

    @Test
    public void lowerCaseFirst() {
        String expected = "accountId";
        String columnName = "AccountId";
        String actual = StringUtil.lowerCaseFirst(columnName);
        assertEquals(expected, actual);
    }

    @Test
    public void splitTest() {
        String s = "base-service,auth-service";
        String[] split = s.split(",");
        HashSet<String> set = new HashSet<>();
        Collections.addAll(set, split);
        System.out.println(set);

    }
}
