package com.elitech.iot.common.base.util;

import org.junit.Test;

import java.io.IOException;
import java.lang.annotation.*;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/17
 */
public class ClassUtilTest {
    @Test
    public void getClasses() throws IOException, ClassNotFoundException {
        List<Class<?>> list = ClassUtil.getClasses("com.elitech.iot");
        assertTrue(list.size() > 0);
    }

    @Test
    public void getClassLoader() {
        ClassLoader expected = Thread.currentThread().getContextClassLoader();
        ClassLoader actual = ClassUtil.getClassLoader();
        assertEquals(expected, actual);

        actual = ClassUtil.getContextClassLoader();
        assertEquals(expected, actual);
    }

    @Test
    public void getAllClassByInterface() throws IOException, ClassNotFoundException {
        List<Class> classList = ClassUtil.getAllClassByInterface("com.elitech.iot.common.base.util", IInterface.class);
        assertTrue(classList.size() > 0);
    }

    @Test
    public void getAllClassBySuperClass() throws IOException, ClassNotFoundException {
        List<Class> classList = ClassUtil.getAllClassBySuperClass("com.elitech.iot.common.base.util", A.class);
        assertTrue(classList.size() == 2);
    }

    @Test
    public void getAllClassByAnnotation() throws IOException, ClassNotFoundException {

        List<Class> classList = ClassUtil.getAllClassByAnnotation("com.elitech.iot.common.base.util", B.class);
        assertTrue(classList != null);

        assertTrue(classList.size() == 1);
    }


    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface B {        //这是一个自定义注解
        String value();
    }

    @B("/hello")
    public class A implements IInterface {

    }

    public interface IInterface {

    }

    public class C extends A {
    }

    public class D extends C {

    }
}
