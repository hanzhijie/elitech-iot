package com.elitech.iot.common.base.util;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public class FileUtilTest {
    @Test
    public void generateRandomFileName() {
        String expected = "logo.png";
        String actual = FileUtil.generateRandomFileName(expected);
        assertNotEquals(expected, actual);

        assertEquals("png", FileUtil.getFileSuffixName(actual));
    }

    @Test
    public void createByFileName() throws IOException {
        String expected = "D:/temp/logo.png";
        File file = new File(expected);
        assertTrue(!file.exists());

        File actual = FileUtil.create(expected);
        assertTrue(actual.exists());

        FileUtil.deleteFile(file.getAbsolutePath());

        assertTrue(!file.exists());
    }

    @Test
    public void createByFile() throws IOException {
        String expected = "D:/temp/logo.png";
        File file = new File(expected);
        assertTrue(!file.exists());

        File actual = FileUtil.create(file);
        assertTrue(actual.exists());

        FileUtil.deleteFile(file.getAbsolutePath());

        assertTrue(!file.exists());
    }

    @Test
    public void mkParentDirs() throws IOException {
        String expected = "D:/temp/1";
        File file = new File(expected);
        assertTrue(!file.exists());

        File actual = FileUtil.mkParentDirs(file);
        assertTrue(actual.exists());

        FileUtil.deleteDirectory(file.getAbsolutePath());

        assertTrue(!file.exists());
    }

    @Test
    public void readWrite() throws IOException {
        String expected = "HelloWorld.";
        String fileName = "D:/temp/test.log";
        File file = new File(fileName);
        assertTrue(!file.exists());

        FileUtil.writeFile(fileName, expected.getBytes());
        assert (file.exists());

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        FileUtil.readFile(out, fileName);
        String actual = new String(out.toByteArray());
        assertEquals(expected, actual);

        FileUtil.deletePath(file.getAbsolutePath());
        assertTrue(!file.exists());
    }

    @Test
    public void newFile() throws UnsupportedEncodingException {
        String expected = "D:\\temp\\test.log";
        File file = FileUtil.newFile(expected);

        assertEquals(expected, file.getAbsolutePath());
    }

    @Test
    public void newFileParent() {
        String expected = "D:\\temp\\test.log";
        String parent = "D:\\temp";
        String fileName = "test.log";
        File file = FileUtil.newFile(parent, fileName);

        assertEquals(expected, file.getAbsolutePath());

        file = FileUtil.newFile(new File(parent), fileName);
        assertEquals(expected, file.getAbsolutePath());
    }
}
