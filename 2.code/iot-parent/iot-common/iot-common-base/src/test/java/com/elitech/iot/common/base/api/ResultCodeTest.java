package com.elitech.iot.common.base.api;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public class ResultCodeTest {
    @Test
    public void systemBusy() {
        String expected = "api.response.code.systemBusy";
        assertEquals(expected, ResultCode.SYSTEM_BUSY.getMessageKey());
    }
}
