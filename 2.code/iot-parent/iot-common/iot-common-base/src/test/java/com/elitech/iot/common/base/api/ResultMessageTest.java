package com.elitech.iot.common.base.api;

import com.elitech.iot.common.base.BaseApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BaseApplication.class)
public class ResultMessageTest {
    @Test
    public void test() {
        String data = "DATA";
        String message = "成功";
        ResultMessage<String> resultMessage = ResultMessage.success(data);
        assertEquals(data, resultMessage.getData());
        assertEquals(message, resultMessage.getMessage());
        assertEquals(ResultCode.SUCCESS.getCode(), resultMessage.getCode());
    }
}
