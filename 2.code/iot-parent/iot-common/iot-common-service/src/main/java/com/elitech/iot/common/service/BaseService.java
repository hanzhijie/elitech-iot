package com.elitech.iot.common.service;

import com.elitech.iot.common.dao.IBaseDao;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * 统一服务基类.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-19
 * @since V1.0.0
 */
public abstract class BaseService<Domain, PrimaryKey> implements IBaseService<Domain, PrimaryKey> {
    /**
     * 获取dao实现.
     *
     * @return
     */
    protected abstract IBaseDao<Domain, PrimaryKey> getDao();

    @Override
    public int insert(Domain domain) {
        return getDao().insert(domain);
    }

    @Override
    public int insertList(List<Domain> list) {
        return getDao().insertList(list);
    }

    @Override
    public int updateByPk(Domain domain) {
        return getDao().updateByPk(domain);
    }

    @Override
    public int deleteByPk(PrimaryKey pk) {
        return getDao().deleteByPk(pk);
    }

    @Override
    public int deleteByPkList(List<PrimaryKey> pkList) {
        return getDao().deleteByPkList(pkList);
    }

    @Override
    public Domain selectByPk(PrimaryKey pk) {
        return getDao().selectByPk(pk);
    }

    @Override
    public List<Domain> selectByPkList(List<PrimaryKey> pkList) {
        return getDao().selectByPkList(pkList);
    }

    @Override
    public List<Domain> selectAll() {
        return getDao().selectAll();
    }

    @Override
    public PageInfo<Domain> pageListByWhere(int start, int pageSize, Map<String, Object> whereMap) {
        return getDao().pageListByWhere(start, pageSize, whereMap);
    }

    @Override
    public List<Domain> selectListByDomain(Domain domain) {
        return getDao().selectListByDomain(domain);
    }
}
