package com.elitech.iot.common.dao;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

/**
 * @param <Domain>     领域模型类型
 * @param <PrimaryKey> 主键类型
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-19
 * @since V1.0.0
 */
public abstract class BaseDao<Domain, PrimaryKey> implements IBaseDao<Domain, PrimaryKey> {
    /**
     * 获取Dao类的真实数据类型
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    private Class getEntityClass() {
        Class clazz = (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return clazz;
    }

    /**
     * 获取数据库操作的完全限定的名称.<br>
     * 如：com.winner.wechat.number.mapper.WxNumberMapper.queryById
     *
     * @param method Mapper.xml配置文件中方法名称
     * @return 返回完全限定方法名称
     */
    protected String getQualifiedMethod(String method) {
        String entityClassName = getEntityClass().getName();
        String methodName = entityClassName.replace(".domain.", ".dao.impl.") + "DaoImpl." + method;
        methodName = methodName.replace(".entity.", ".dao.impl.");

        return methodName;
    }

    protected SqlSessionTemplate sqlSessionTemplate;

    /**
     * 设置SqlSessionTemplate. <br>
     * 子类可以覆写。
     *
     * @param sqlSessionTemplate SqlSessionTemplate模拟
     */
    @Autowired
    protected void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
        this.sqlSessionTemplate = sqlSessionTemplate;
    }

    /**
     * 新增实体对象.
     *
     * @param domain 要新增的实体对象
     * @return 返回插入行数
     */
    @Override
    public int insert(Domain domain) {
        return sqlSessionTemplate.insert(getQualifiedMethod("insert"), domain);
    }

    /**
     * 批量新增数据.
     *
     * @param list 数据列表
     * @return 返回插入行数
     */
    @Override
    public int insertList(List<Domain> list) {
        return sqlSessionTemplate.insert(getQualifiedMethod("insertList"), list);
    }

    /**
     * 根据id更新实体对象.
     *
     * @param domain 要更新的实体对象
     * @return 返回更新的行数。
     */
    @Override
    public int updateByPk(Domain domain) {
        return sqlSessionTemplate.update(getQualifiedMethod("updateByPk"), domain);
    }

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键id
     * @return 返回删除的行数
     */
    @Override
    public int deleteByPk(PrimaryKey pk) {
        return sqlSessionTemplate.delete(getQualifiedMethod("deleteByPk"), pk);
    }

    /**
     * 根据id批量删除.
     *
     * @param idList 要删除实体对象的主键列表
     * @return 返回删除的行数
     */
    @Override
    public int deleteByPkList(List<PrimaryKey> idList) {
        return sqlSessionTemplate.delete(getQualifiedMethod("deleteByPkList"), idList);
    }


    /**
     * 根据id查询.
     *
     * @param pk 主键id
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    @Override
    public Domain selectByPk(PrimaryKey pk) {
        return sqlSessionTemplate.selectOne(getQualifiedMethod("selectByPk"), pk);
    }

    /**
     * 根据多个id查询.
     *
     * @param pkList id列表
     * @return 返回指定id的实体对象列表。.
     */
    @Override
    public List<Domain> selectByPkList(List<PrimaryKey> pkList) {
        return sqlSessionTemplate.selectList(getQualifiedMethod("selectByPkList"), pkList);
    }

    /**
     * 查询数据库表中所有记录.
     *
     * @return 返回表中所有记录。
     */
    @Override
    public List<Domain> selectAll() {
        return sqlSessionTemplate.selectList(getQualifiedMethod("selectAll"));
    }

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    @Override
    public PageInfo<Domain> pageListByWhere(int start, int pageSize, Map<String, Object> whereMap) {
        PageInfo<Domain> page = PageHelper.startPage(start, pageSize)
                .doSelectPageInfo(() -> sqlSessionTemplate.selectList(getQualifiedMethod("pageListByWhere"), whereMap));

        return page;
    }


    /**
     * 根据实体类查询
     *
     * @param domain 实体类
     * @return List<T> 查询结果
     */
    @Override
    public List<Domain> selectListByDomain(Domain domain) {
        return sqlSessionTemplate.selectList(getQualifiedMethod("selectListByDomain"), domain);
    }
}
