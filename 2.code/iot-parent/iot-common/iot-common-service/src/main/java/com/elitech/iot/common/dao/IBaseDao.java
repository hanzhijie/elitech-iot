package com.elitech.iot.common.dao;

import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 数据访问接口.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019-12-19
 * @since V1.0.0
 */
public interface IBaseDao<Domain, PrimaryKey> {

    /**
     * 新增数据.
     *
     * @param domain 要新增的实体对象
     * @return 返回插入行数
     */
    int insert(Domain domain);

    /**
     * 批量新增数据.
     *
     * @param list 数据列表
     * @return 返回插入行数
     */
    int insertList(List<Domain> list);

    /**
     * 根据id更新实体对象.
     *
     * @param domain 要更新的实体对象
     * @return 返回更新的行数。
     */
    int updateByPk(Domain domain);

    /**
     * 根据主键删除实体对象.
     *
     * @param pk 主键
     * @return 返回删除的行数
     */
    int deleteByPk(PrimaryKey pk);

    /**
     * 根据主键批量删除.
     *
     * @param pkList 要删除实体对象的主键
     * @return 返回删除的行数
     */
    int deleteByPkList(List<PrimaryKey> pkList);

    /**
     * 根据id查询.
     *
     * @param pk 主键
     * @return 返回指定id的实体对象，如果不存在则返回null。
     */
    Domain selectByPk(PrimaryKey pk);

    /**
     * 根据多个id查询.
     *
     * @param pkList 主键列表
     * @return 返回指定主键的实体对象列表。
     */
    List<Domain> selectByPkList(List<PrimaryKey> pkList);

    /**
     * 查询表中所有记录.
     *
     * @return 返回表中所有记录。
     */
    List<Domain> selectAll();

    /**
     * 条件分页查询.
     *
     * @param start    数据库查询记录偏移值
     * @param pageSize 每页数据条数
     * @param whereMap 查询条件。
     * @return 返回满足条件的分页数据 ,及数据条数。
     */
    PageInfo<Domain> pageListByWhere(int start, int pageSize, Map<String, Object> whereMap);

    /**
     * 根据实体类查询
     *
     * @param domain 实体类
     * @return List<Domain> 查询结果
     */
    List<Domain> selectListByDomain(Domain domain);
}
