package com.elitech.iot.code.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/15
 */
public class CodeUtilTest {
    private String tableName = "user_account";
    private String columnName = "user_id";
    private String tableNamePrefix = "user_";
    private String columnNamePrefix = "user_";

    @Test
    public void columnToField() {
        String expected = "userId";
        assertEquals(expected, CodeUtil.columnToField(columnName, columnNamePrefix));
    }

    @Test
    public void tableToClass() {
        String expected = "UserAccount";
        assertEquals(expected, CodeUtil.tableToClassName(tableName, tableNamePrefix));
    }

    @Test
    public void tableToInstancePrefix() {
        String expected = "userAccount";
        assertEquals(expected, CodeUtil.tableToInstanceName(tableName, tableNamePrefix));
    }
}
