package com.elitech.iot.code.generator.backend.service.dao;

import com.elitech.iot.code.CodeGeneratorApplication;
import com.elitech.iot.code.generator.backend.service.dao.DaoImplCodeGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CodeGeneratorApplication.class})
public class DaoImplCodeGeneratorTest {
    String dbName = "elitech_iot";
    String tableName = "demo";

    @Autowired
    private DaoImplCodeGenerator generator;

    @Test
    public void genCode() throws IOException {
        generator.genCode(dbName, tableName);
    }
}
