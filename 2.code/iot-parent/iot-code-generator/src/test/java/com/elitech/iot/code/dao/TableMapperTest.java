package com.elitech.iot.code.dao;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.elitech.iot.code.CodeGeneratorApplication;
import com.elitech.iot.code.domain.Column;
import com.elitech.iot.code.domain.Table;

/**
 * 数据库元表、字段元数据查询测试.
 * 
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019年12月13日
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CodeGeneratorApplication.class })
public class TableMapperTest {
	@Autowired
	private TableMapper mapper;
	
	String dbName = "elitech_iot";
	String tableName = "demo";

	@Test
	public void findAllTables() {
		List<String> list = mapper.findAllTables();
		assertNotNull(list);
		
		System.out.println("ALL DataBase all tables: ");
		list.forEach(t -> System.out.println(t));
	}

	@Test
	public void findTableByDb() {
		List<Table> list = mapper.findTableByDb(dbName);
		assertNotNull(list);
		
		System.out.println(dbName + " DataBase all tables: ");
		list.forEach(t -> System.out.println(t));
	}
	
	@Test
	public void findTableByDbAndTable() {
		Table table = mapper.findTableByDbAndTable(dbName, tableName);
		assertNotNull(table);
		
		System.out.println(dbName + " DataBase " + tableName + "info: ");
		System.out.println(table);
	}

	@Test
	public void findColumnByTable() {
		List<Column> list = mapper.findColumnByDbAndTable(dbName, tableName);
		assertNotNull(list);
		
		System.out.println(dbName + " DataBase " + tableName + " cloumns: ");
		list.forEach(c -> System.out.println(c));
	}

}
