package com.elitech.iot.code.generator.backend.facade;

import com.elitech.iot.code.CodeGeneratorApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CodeGeneratorApplication.class})
public class FeignCodeGeneratorTest {
    String dbName = "elitech_iot";
    String tableName = "demo";

    @Autowired
    private FeignCodeGenerator generator;

    @Test
    public void genCode() throws IOException {
        generator.genCode(dbName, tableName);
    }
}
