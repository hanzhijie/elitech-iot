package com.elitech.iot.code.config;

import com.elitech.iot.code.CodeGeneratorApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * @author winner
 * @date 2019/12/15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CodeGeneratorApplication.class })
public class CodeGeneratorPropertiesTest {
    @Autowired
    private CodeGeneratorProperties properties;

    @Test
    public void packageName(){
        String expected = "com.elitech.iot";
        assertEquals(expected, properties.getRootPackageName());
    }
}
