package com.elitech.iot.code.util;

import com.elitech.iot.code.config.CodeGeneratorProperties;
import com.elitech.iot.code.domain.Column;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public class ColumnUtilTest {
    private static Column column;
    private static CodeGeneratorProperties config;

    @BeforeClass
    public static void init() {
        column = new Column();
        column.setDataType("varchar");
        column.setCharacterMaximumLength(50);
        column.setCharacterOctetLength(150);
        column.setCloumnType("varchar(50)");
        column.setColumnComment("账号");
        column.setColumnDefault("");
        column.setExtra("");
        column.setColumnName("account_name");
        column.setNullAble(false);
        column.setNumericPrecision(0);
        column.setOrdinalPosition(1);
        column.setNumericScale(0);
        column.setTableName("user_account");
        column.setTableSchema("elitech_iot");
        column.setColumnKey("UNI");

        config = new CodeGeneratorProperties();
        config.setAuthor("wangjiangmin");
        config.setAutoGeneratorKey(false);
        config.setColumnNamePrefix("user_");
        config.setModule("user");
        config.setModuleDir("iot-user");
        config.setRootDir("D:\\");
        config.setRootPackageName("com.elitech.iot");
        config.setServiceProjectDir("user-service");
        config.setFacadeProjectDir("user-facade");
        config.setTableNamePrefix("user_");
    }

    @Test
    public void getDataType() {
        String expected = "String";
        assertEquals(expected, ColumnUtil.getPrimitiveDataType(column));
    }

    @Test
    public void getFormLabelLocaleKey() {
        String expected = "com.elitech.iot.user.accountName";
        assertEquals(expected, ColumnUtil.getFormLabelLocaleKey(config, column));
    }

    @Test
    public void getFiledValidatorAnnotationList() {
        int expected = 2;
        List<String> list = ColumnUtil.getFiledValidatorAnnotationList(config, column);
        assertNotNull(list);
        assertEquals(2, list.size());
        list.forEach(v -> {
            System.out.println(v);
        });
    }
}
