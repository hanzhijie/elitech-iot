package com.elitech.iot.code;

import org.apache.velocity.app.VelocityEngine;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public class VelocityEngineFactoryTest {
    @Test
    public void test() {
        VelocityEngine engine = VelocityEngineFactory.create();
        assertNotNull(engine);

        String expected = "[class]";
        String actual = engine.getProperty("resource.loader").toString();
        assertEquals(expected, actual);
    }
}
