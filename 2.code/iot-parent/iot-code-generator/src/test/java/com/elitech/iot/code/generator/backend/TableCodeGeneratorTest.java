package com.elitech.iot.code.generator.backend;

import com.elitech.iot.code.CodeGeneratorApplication;
import com.elitech.iot.code.generator.TableCodeGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/24
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CodeGeneratorApplication.class})
public class TableCodeGeneratorTest {
    String dbName = "elitech_iot";
    String tableName = "demo";
    //String[] tables = new String[]{"device", "device_guid", "device_guid_export_log", "device_param", "device_param_data_type", "device_param_group", "device_project_device_ref", "device_real_time_data", "device_type", "device_type_param"};
    //String[] tables = new String[]{"base_account", "base_account_role_rel", "base_client_details", "base_organization", "base_organization_detail", "base_permission", "base_project", "base_project_account", "base_role", "base_role_permission_rel", "base_user_profile"};
    String[] tables = new String[]{"calibration_price_conf"};

    //String[] tables = new String[]{"demo"};

    @Autowired
    private TableCodeGenerator tableCodeGenerator;

    @Test
    public void genCode() throws IOException {
        for (String tableName : tables) {
            tableCodeGenerator.generate(dbName, tableName);
        }
    }
}
