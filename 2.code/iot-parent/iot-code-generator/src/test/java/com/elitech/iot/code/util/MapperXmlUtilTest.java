package com.elitech.iot.code.util;

import com.elitech.iot.code.config.CodeGeneratorProperties;
import com.elitech.iot.code.domain.Column;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public class MapperXmlUtilTest {
    private static Column columnAccount;
    private static Column columnId;
    private static CodeGeneratorProperties config;
    private static List<Column> columnList = new ArrayList<>(4);

    @BeforeClass
    public static void init() {
        columnAccount = new Column();
        columnAccount.setDataType("varchar");
        columnAccount.setCharacterMaximumLength(50);
        columnAccount.setCharacterOctetLength(150);
        columnAccount.setCloumnType("varchar(50)");
        columnAccount.setColumnComment("账号");
        columnAccount.setColumnDefault("");
        columnAccount.setExtra("");
        columnAccount.setColumnName("account_name");
        columnAccount.setNullAble(false);
        columnAccount.setNumericPrecision(0);
        columnAccount.setOrdinalPosition(1);
        columnAccount.setNumericScale(0);
        columnAccount.setTableName("user_account");
        columnAccount.setTableSchema("elitech_iot");
        columnAccount.setColumnKey("UNI");

        columnId = new Column();
        columnId.setDataType("varchar");
        columnId.setCharacterMaximumLength(50);
        columnId.setCharacterOctetLength(150);
        columnId.setCloumnType("varchar(50)");
        columnId.setColumnComment("ID");
        columnId.setColumnDefault("");
        columnId.setExtra("auto_increment");
        columnId.setColumnName("account_id");
        columnId.setNullAble(false);
        columnId.setNumericPrecision(0);
        columnId.setOrdinalPosition(1);
        columnId.setNumericScale(0);
        columnId.setTableName("user_account");
        columnId.setTableSchema("elitech_iot");
        columnId.setColumnKey("PRI");

        columnList.add(columnAccount);
        columnList.add(columnId);

        config = new CodeGeneratorProperties();
        config.setAuthor("wangjiangmin");
        config.setAutoGeneratorKey(false);
        config.setColumnNamePrefix("user_");
        config.setModule("user");
        config.setModuleDir("iot-user");
        config.setRootDir("D:\\");
        config.setRootPackageName("com.elitech.iot");
        config.setServiceProjectDir("user-service");
        config.setFacadeProjectDir("user-facade");
        config.setTableNamePrefix("user_");
    }

    @Test
    public void getAutoIncrementAttribute() {
        String expected = "useGeneratedKeys=\"true\" keyProperty=\"accountId\"";
        String actual = MapperXmlUtil.getAutoIncrementAttribute(columnList, config.getColumnNamePrefix());
        assertEquals(expected, actual);
    }

    @Test
    public void getBatchInsertValueString() {
        String expected = "#{entity.accountName}\r\n";
        String actual = MapperXmlUtil.getInsertListValueString(columnList, config.getColumnNamePrefix());
        assertEquals(expected, actual);
    }

    @Test
    public void getInsertColumnString() {
        String expected = "`account_name`\r\n";
        String actual = MapperXmlUtil.getInsertColumnString(columnList);
        assertEquals(expected, actual);
    }

    @Test
    public void getUpdateSetString() {
        String expected = "`account_name` = #{accountName}\r\n";
        String actual = MapperXmlUtil.getUpdateSetString(columnList, config.getColumnNamePrefix());
        assertEquals(expected, actual);
    }
}
