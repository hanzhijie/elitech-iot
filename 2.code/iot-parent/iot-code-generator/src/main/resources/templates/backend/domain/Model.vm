/**
*  Copyright © 2020-2120 $!{CODE_CONFIG.company}. All Rights Reserved.
*  CreateTime：  $!{CODE_TIME}
*/

#if ($STRING_UTIL.isEmpty($CODE_CONFIG.childModule))
package $!{CODE_CONFIG.rootPackageName}.$!{CODE_CONFIG.module}.model;
#else
package $!{CODE_CONFIG.rootPackageName}.$!{CODE_CONFIG.module}.$!{CODE_CONFIG.childModule}.model;
#end

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
#if ($!STRING_UTIL.isEmpty($TABLE_COMMENT))
* $!CODE_UTIL.tableToClassName($TABLE_NAME, $CODE_CONFIG.tableNamePrefix)实体类.
#else
* $!{TABLE_COMMENT}.
#end
* @author $!{CODE_CONFIG.author}
* @since $!{CODE_CONFIG.since}
*
*/
@Getter
@Setter
@ApiModel(description = "$!{TABLE_COMMENT}")
public class $!CODE_UTIL.tableToClassName($!TABLE_NAME, $!CODE_CONFIG.tableNamePrefix)Model implements Serializable {
private static final long serialVersionUID = 1L;

#foreach ($col in $CODE_COLUMN_LIST)
##生成参数名
	#set( $fieldName = $!CODE_UTIL.columnToField($col.columnName) )
	#set( $primitiveType=$!COLUMN_UTIL.getPrimitiveDataType($col))
	#set( $apiModelProperty = "@ApiModelProperty(value = """ + $!{col.columnComment} + """)")

/**
* $!col.columnComment.
*/
	#if ($col.dataType == "time")
	@DateTimeFormat(pattern = "HH:mm:ss")
	@JsonFormat(pattern = "HH:mm:ss", timezone = "UTC")
	#elseif ($col.dataType == "date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	#elseif ($col.dataType == "datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
	#elseif ($col.dataType == "timestamp")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
	#end
	#foreach ($validator in $!COLUMN_UTIL.getFiledValidatorAnnotationList($CODE_CONFIG, $col))
	$!validator
	#end
$!{apiModelProperty}
    private $!{primitiveType} $!CODE_UTIL.columnToField($col.columnName, $!CODE_CONFIG.columnNamePrefix);
#end
}