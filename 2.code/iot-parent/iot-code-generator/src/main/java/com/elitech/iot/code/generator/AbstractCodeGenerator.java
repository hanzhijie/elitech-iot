package com.elitech.iot.code.generator;

import com.elitech.iot.code.VelocityEngineFactory;
import com.elitech.iot.code.config.CodeGeneratorProperties;
import com.elitech.iot.code.dao.TableMapper;
import com.elitech.iot.code.domain.Column;
import com.elitech.iot.code.domain.Table;
import com.elitech.iot.code.util.CodeUtil;
import com.elitech.iot.code.util.ColumnUtil;
import com.elitech.iot.code.util.MapperXmlUtil;
import com.elitech.iot.common.base.util.FileUtil;
import com.elitech.iot.common.base.util.StringUtil;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * 代码生成的父类抽象类，读取配置文件，生成代码，各层代码生成类需要继承该父类
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public abstract class AbstractCodeGenerator implements CodeGenerator {
    @Autowired
    private CodeGeneratorProperties config;

    @Autowired
    private TableMapper mapper;

    /**
     * 是否资源文件.
     *
     * @return 如果是java代码返回false 、 如果是资源文件返回true
     */
    protected boolean isResourceFile() {
        return false;
    }

    /**
     * 获取资源文件的存放目录.
     *
     * @return 如果是java代码可以不用重写该方法，资源类代码生成器需要重写该方法.
     */
    protected String getResourceDir() {
        return null;
    }

    /**
     * 生成代码.
     *
     * @param dbName    数据库名称
     * @param tableName 表名称
     * @return 根据指定模板和表名生成相应的代码字符串
     */
    private String genCodeString(String dbName, String tableName) {
        VelocityContext context = new VelocityContext();
        context.put("CODE_CONFIG", config);
        context.put("CODE_COMPANY", config.getCompany());
        context.put("CODE_AUTHOR", config.getAuthor());
        context.put("CODE_ROOT_PACKAGE", config.getRootPackageName());
        context.put("CODE_MODULE", config.getModule());

        context.put("CODE_TIME", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        context.put("CODE_DOMAIN", CodeUtil.tableToClassName(tableName, config.getTableNamePrefix()));

        List<Column> columnList = mapper.findColumnByDbAndTable(dbName, tableName);
        context.put("CODE_COLUMN_LIST", columnList);
        context.put("CODE_UTIL", CodeUtil.class);
        context.put("COLUMN_UTIL", ColumnUtil.class);
        context.put("STRING_UTIL", StringUtil.class);
        context.put("MAPPER_XML_UTIL", MapperXmlUtil.class);

        Table table = mapper.findTableByDbAndTable(dbName, tableName);
        context.put("TABLE_NAME", tableName);
        context.put("TABLE_COMMENT", table.getTableComment());

        List<Column> pkColumnList = ColumnUtil.getPrimaryKey(columnList);
        String primaryType = "";
        Column pkColumn = null;
        if (pkColumnList != null || pkColumnList.size() > 0) {
            pkColumn = pkColumnList.get(0);
            String primitiveType = ColumnUtil.getPrimitiveDataType(pkColumn);
            primaryType = ColumnUtil.getObjectDataType(primitiveType);
        }
        context.put("PRIMARY_KEY_OBJECT_TYPE", primaryType);
        context.put("PRIMARY_KEY_COLUMN", pkColumn);

        Template template = getTemplate(getTemplateFile());
        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        return writer.toString();
    }

    public void genCode(String dbName, String tableName) throws IOException {
        String code = genCodeString(dbName, tableName);
        File codeFile = getCodeFile(tableName);
        System.out.println("********* gen ode file: " + codeFile.getPath());

        FileUtil.deleteFile(codeFile.getPath());
        FileUtil.writeFile(codeFile, code.getBytes("UTF-8"));
    }

    /**
     * 生成代码文件.
     *
     * @param tableName 表名
     * @return 根据表名生成相应的代码文件
     */
    private File getCodeFile(String tableName) {
        String rootDir = config.getRootDir();
        String rootPackage = config.getRootPackageName();
        String facadeProjectDir = config.getFacadeProjectDir();
        String serviceProjectDir = config.getServiceProjectDir();
        String apiProjectDir = config.getApiProjectDir();
        String vueViewsRootDir = config.getVueViewsRootDir();

        String layerProjectDir;
        CodeLayerType codeLayerType = getCodeLayerType();
        switch (codeLayerType) {
            case API:
                layerProjectDir = apiProjectDir;
                break;
            case FACADE:
                layerProjectDir = facadeProjectDir;
                break;
            case SERVICE:
                layerProjectDir = serviceProjectDir;
                break;
            case VUE:
            default:
                layerProjectDir = vueViewsRootDir;
        }

        String resourceRootDir = isResourceFile() ? "resources" : "java";

        String module = config.getModule();
        String moduleDir = config.getModuleDir();
        String childModule = config.getChildModule();
        String className = CodeUtil.tableToClassName(tableName, config.getTableNamePrefix());
        String layer = getLayer();
        layer = layer.replace(".", File.separator);

        String classSuffix;
        String fileSuffix;
        String fileName;

        Path path;
        // vue 前端代码
        if (this.getCodeLayerType().equals(CodeLayerType.VUE)) {
            classSuffix = getClassSuffix();
            fileSuffix = getFileSuffix();
            fileName = classSuffix + fileSuffix;

            if (StringUtil.isEmpty(childModule)) {
                path = Paths.get(vueViewsRootDir, module, className, fileName);
            } else {
                path = Paths.get(vueViewsRootDir, module, childModule, className, fileName);
            }
        }
        // java 后端代码
        else {
            classSuffix = getClassSuffix();
            fileSuffix = getFileSuffix();
            fileName = className + classSuffix + fileSuffix;

            if (isResourceFile()) {
                if (StringUtil.isEmpty(childModule)) {
                    path = Paths.get(rootDir, moduleDir, layerProjectDir, "src", "main", resourceRootDir, getResourceDir(), module, fileName);
                } else {
                    path = Paths.get(rootDir, moduleDir, layerProjectDir, "src", "main", resourceRootDir, getResourceDir(), module, childModule, fileName);
                }
            } else {
                if (StringUtil.isEmpty(childModule)) {
                    path = Paths.get(rootDir, moduleDir, layerProjectDir, "src", "main", resourceRootDir, rootPackage.replace(".", File.separator), module, layer, fileName);
                } else {
                    path = Paths.get(rootDir, moduleDir, layerProjectDir, "src", "main", resourceRootDir, rootPackage.replace(".", File.separator), module, childModule, layer, fileName);
                }
            }
        }

        return path.toFile();
    }

    /**
     * 获取Velocity模板.
     *
     * @param templateFile 模板文件
     * @return
     */
    private Template getTemplate(String templateFile) {
        VelocityEngine engine = VelocityEngineFactory.create();
        Template template = engine.getTemplate(templateFile, "UTF-8");

        return template;
    }
}
