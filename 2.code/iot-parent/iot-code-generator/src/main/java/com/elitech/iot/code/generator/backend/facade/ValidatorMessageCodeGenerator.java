package com.elitech.iot.code.generator.backend.facade;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;

/**
 * 验证信息代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/19
 */
public class ValidatorMessageCodeGenerator extends AbstractCodeGenerator {
    /**
     * 模板文件.
     */
    public static final String TEMPLATE_FILE = "/templates/backend/domain/Domain.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.FACADE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "domain";
    }

    @Override
    public String getClassSuffix() {
        return "";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
