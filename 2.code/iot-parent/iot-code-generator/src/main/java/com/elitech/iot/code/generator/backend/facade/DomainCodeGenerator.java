package com.elitech.iot.code.generator.backend.facade;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * Domain代码生成器
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/18
 */
@Component
public class DomainCodeGenerator extends AbstractCodeGenerator {
    /**
     * 模板文件.
     */
    public static final String TEMPLATE_FILE = "/templates/backend/domain/Domain.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.FACADE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "domain";
    }

    @Override
    public String getClassSuffix() {
        return "";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
