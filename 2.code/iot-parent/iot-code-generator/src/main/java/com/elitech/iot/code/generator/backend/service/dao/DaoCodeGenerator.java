package com.elitech.iot.code.generator.backend.service.dao;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * Dao代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/18
 */
@Component
public class DaoCodeGenerator extends AbstractCodeGenerator {
    public static final String TEMPLATE_FILE = "/templates/backend/dao/Dao.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.SERVICE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "dao";
    }

    @Override
    public String getClassSuffix() {
        return "Dao";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
