package com.elitech.iot.code.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 读取配置文件内容，获取配置信息。
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/15
 */
@Component
@PropertySource(factory = CompositePropertySourceFactory.class, value = "classpath:code.yml")
@ConfigurationProperties(prefix = "code")
@Setter
@Getter
public class CodeGeneratorProperties {
    /**
     * 根包名.（不包含模块）
     */
    private String rootPackageName;

    /**
     * 项目文件根目录.
     */
    private String rootDir;

    /**
     * 业务模块名称.
     */
    private String module;

    /**
     * 模块所在文件目录.
     */
    private String moduleDir;

    /**
     * 二级模块名称.（多个模块代码在一个项目中时使用）
     */
    private String childModule;

    /**
     * facade项目文件目录.
     */
    private String facadeProjectDir;

    /**
     * 服务项目文件目录.
     */
    private String serviceProjectDir;

    /**
     * API项目所在目录.
     */
    private String apiProjectDir;

    /**
     * 是否自动生成主键.
     */
    private boolean autoGeneratorKey;

    /**
     * 数据库表名前缀.
     */
    private String tableNamePrefix;

    /**
     * 数据库表列名前缀.
     */
    private String columnNamePrefix;

    /**
     * 公司信息.
     */
    private String company;

    /**
     * 作者.
     */
    private String author;

    /**
     * javadoc since.
     */
    private String since;

    /**
     * vue 项目views页面根目录.
     */
    private String vueViewsRootDir;
}
