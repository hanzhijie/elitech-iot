package com.elitech.iot.code.util;

import com.elitech.iot.common.base.util.StringUtil;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/15
 */
public class CodeUtil {
    /**
     * 数据库列名转换为类字段名称.
     *
     * @param columnName 列名
     * @param prefix     列名前缀
     * @return 下划线转换为驼峰是命名的字符串
     */
    public static String columnToField(String columnName, String prefix) {
        if (StringUtil.isEmpty(prefix)) {
            columnName = columnName.substring(columnName.indexOf(prefix));
        }

        return StringUtil.underlineToCamelCase(columnName);
    }

    /**
     * 表名转换为类名.
     *
     * @param tableName 数据库表名(下划线分隔)
     * @param prefix    表名前缀
     * @return 单词首字母全部大写的字符串
     */
    public static String tableToClassName(String tableName, String prefix) {
        if (!StringUtil.isEmpty(prefix)) {
            tableName = tableName.replace(prefix, "");
        }
        String camelCase = StringUtil.underlineToCamelCase(tableName);
        return StringUtil.upperCaseFirst(camelCase);
    }

    /**
     * 表名转换为实例名前缀(如xxxService， xxxDao).
     *
     * @param tableName 数据库表名(下划线分隔)
     * @param prefix    表名前缀
     * @return 单词首字母全部大写的字符串
     */
    public static String tableToInstanceName(String tableName, String prefix) {
        if (StringUtil.isEmpty(prefix)) {
            tableName = tableName.substring(tableName.indexOf(prefix));
        }
        String camelCase = StringUtil.underlineToCamelCase(tableName);
        return camelCase;
    }
}
