package com.elitech.iot.code.generator.backend.service.api;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * 控制层代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/18
 */
@Component
public class RestControllerCodeGenerator extends AbstractCodeGenerator {
    public static final String TEMPLATE_FILE = "/templates/backend/api/RestController.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.API;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "api";
    }

    @Override
    public String getClassSuffix() {
        return "RestController";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
