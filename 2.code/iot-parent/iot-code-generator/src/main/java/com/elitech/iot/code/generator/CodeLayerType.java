package com.elitech.iot.code.generator;

/**
 * 代码分层枚举
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/18
 */
public enum CodeLayerType {
    /**
     * 代码所在分层Facade.
     */
    FACADE,

    /**
     * 代码所在分层Api.
     */
    SERVICE,

    /**
     * 代码所在分层Api.
     */
    API,

    /**
     * vue前端页面.
     */
    VUE
}
