package com.elitech.iot.code.generator.backend.service.dao;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * mapper映射文件代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/18
 */
@Component
public class MapperXmlCodeGenerator extends AbstractCodeGenerator {
    public static final String TEMPLATE_FILE = "/templates/backend/dao/MapperXml.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.SERVICE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "";
    }

    @Override
    public String getClassSuffix() {
        return "Mapper";
    }

    @Override
    public String getFileSuffix() {
        return ".xml";
    }

    @Override
    public boolean isResourceFile() {
        return true;
    }

    @Override
    public String getResourceDir() {
        return "mybatis";
    }
}
