package com.elitech.iot.code.generator;

import com.elitech.iot.code.generator.backend.facade.DomainCodeGenerator;
import com.elitech.iot.code.generator.backend.facade.FeignCodeGenerator;
import com.elitech.iot.code.generator.backend.facade.ModelCodeGenerator;
import com.elitech.iot.code.generator.backend.service.api.RestControllerCodeGenerator;
import com.elitech.iot.code.generator.backend.service.dao.DaoCodeGenerator;
import com.elitech.iot.code.generator.backend.service.dao.DaoImplCodeGenerator;
import com.elitech.iot.code.generator.backend.service.dao.MapperXmlCodeGenerator;
import com.elitech.iot.code.generator.backend.service.service.ServiceCodeGenerator;
import com.elitech.iot.code.generator.backend.service.service.ServiceImplCodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/24
 */
@Component
public class TableCodeGenerator {
    @Autowired
    private DomainCodeGenerator domainCodeGenerator;

    @Autowired
    ModelCodeGenerator modelCodeGenerator;

    @Autowired
    DaoCodeGenerator daoCodeGenerator;

    @Autowired
    MapperXmlCodeGenerator mapperXmlCodeGenerator;

    @Autowired
    DaoImplCodeGenerator daoImplCodeGenerator;

    @Autowired
    ServiceCodeGenerator serviceCodeGenerator;

    @Autowired
    ServiceImplCodeGenerator serviceImplCodeGenerator;

    @Autowired
    RestControllerCodeGenerator restControllerCodeGenerator;

    @Autowired
    FeignCodeGenerator feignCodeGenerator;

    /**
     * 生成一个表对应的各层的所有代码.
     *
     * @param dbName    数据库名称
     * @param tableName 表名称
     * @throws IOException
     */
    public void generate(String dbName, String tableName) throws IOException {
        // 领域模型
        domainCodeGenerator.genCode(dbName, tableName);
        modelCodeGenerator.genCode(dbName, tableName);

        // 数据访问
        daoCodeGenerator.genCode(dbName, tableName);
        mapperXmlCodeGenerator.genCode(dbName, tableName);
        daoImplCodeGenerator.genCode(dbName, tableName);

        // 服务
        serviceCodeGenerator.genCode(dbName, tableName);
        serviceImplCodeGenerator.genCode(dbName, tableName);

        // api
        restControllerCodeGenerator.genCode(dbName, tableName);

        // feign client
        feignCodeGenerator.genCode(dbName, tableName);
    }
}
