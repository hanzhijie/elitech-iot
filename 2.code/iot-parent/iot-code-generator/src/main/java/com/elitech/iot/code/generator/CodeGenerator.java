package com.elitech.iot.code.generator;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/15
 */
public interface CodeGenerator {
    /**
     * 代码分层类型.
     * <p>用于确定生成代码所在路径</p>
     *
     * @return 代码分层类型
     */
    CodeLayerType getCodeLayerType();

    /**
     * 模板文件.
     *
     * @return 代码模板文件相对路径
     */
    String getTemplateFile();

    /**
     * 代码分层（用于生成包名）.
     *
     * @return 代码分成名称(dao 、 dao.impl 、 service 、 service.impl等)
     */
    String getLayer();

    /**
     * 获取类名后缀.
     *
     * @return 分层代码后缀(Dao 、 DaoImpl 、 Service 、 ServiceImpl等)
     */
    String getClassSuffix();

    /**
     * 文件后缀.
     *
     * @return 生成代码文件的后缀（.java）.
     */
    String getFileSuffix();
}
