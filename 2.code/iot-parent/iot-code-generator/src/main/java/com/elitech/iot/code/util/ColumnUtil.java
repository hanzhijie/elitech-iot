package com.elitech.iot.code.util;

import com.elitech.iot.code.config.CodeGeneratorProperties;
import com.elitech.iot.code.domain.Column;
import com.elitech.iot.common.base.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 数据库表列根据类.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/15
 */
public class ColumnUtil {
    private static final String JAVA_DATA_TYPE_STRING = "String";
    private static final String JAVA_DATA_TYPE_BYTE = "byte";
    private static final String JAVA_DATA_TYPE_BYTES = "byte[]";
    private static final String JAVA_DATA_TYPE_SHORT = "short";
    private static final String JAVA_DATA_TYPE_INT = "int";
    private static final String JAVA_DATA_TYPE_LONG = "long";
    private static final String JAVA_DATA_TYPE_BOOLEAN = "boolean";
    private static final String JAVA_DATA_TYPE_BIG_INTEGER = "java.math.BigInteger";
    private static final String JAVA_DATA_TYPE_FLOAT = "float";
    private static final String JAVA_DATA_TYPE_DOUBLE = "double";
    private static final String JAVA_DATA_TYPE_DECIMAL = "java.math.BigDecimal";
    private static final String JAVA_DATA_TYPE_DATE = "java.time.LocalDate";
    private static final String JAVA_DATA_TYPE_TIME = "java.time.LocalTime";
    private static final String JAVA_DATA_TYPE_DATETIME = "java.time.LocalDateTime";
    private static final String JAVA_DATA_TYPE_TIMESTAMP = "java.time.LocalDateTime";


    private static final String MYSQL_DATA_TYPE_VARCHAR = "varchar";
    private static final String MYSQL_DATA_TYPE_BLOB = "blob";
    private static final String MYSQL_DATA_TYPE_TINY_BLOB = "tinyblob";
    private static final String MYSQL_DATA_TYPE_MEDIUM_BLOB = "mediumblob";
    private static final String MYSQL_DATA_TYPE_LONG_BLOB = "longblob";
    private static final String MYSQL_DATA_TYPE_CHAR = "char";
    private static final String MYSQL_DATA_TYPE_TINY_TEXT = "tinytext";
    private static final String MYSQL_DATA_TYPE_MEDIUM_TEXT = "mediumtext";
    private static final String MYSQL_DATA_TYPE_TEXT = "text";
    private static final String MYSQL_DATA_TYPE_INTEGER = "int";
    private static final String MYSQL_DATA_TYPE_TINY_INT = "tinyint";
    private static final String MYSQL_DATA_TYPE_SMALL_INT = "smallint";
    private static final String MYSQL_DATA_TYPE_MEDIUM_INT = "mediumint";
    private static final String MYSQL_DATA_TYPE_BIT = "bit";
    private static final String MYSQL_DATA_TYPE_BIG_INT = "bigint";
    private static final String MYSQL_DATA_TYPE_FLOAT = "float";
    private static final String MYSQL_DATA_TYPE_DOUBLE = "double";
    private static final String MYSQL_DATA_TYPE_DECIMAL = "decimal";
    private static final String MYSQL_DATA_TYPE_DATE = "date";
    private static final String MYSQL_DATA_TYPE_TIME = "time";
    private static final String MYSQL_DATA_TYPE_DATETIME = "datetime";
    private static final String MYSQL_DATA_TYPE_TIMESTAMP = "timestamp";
    private static final String MYSQL_DATA_TYPE_YEAR = "year";
    private static final String MYSQL_DATA_TYPE_BINARY = "binary";
    private static final String MYSQL_DATA_TYPE_VARBINARY = "varbinary";
    private static final String MYSQL_DATA_TYPE_ENUM = "enum";
    private static final String MYSQL_DATA_TYPE_SET = "set";
    private static final String MYSQL_DATA_TYPE_JSON = "json";
    private static final String MYSQL_DATA_TYPE_LONG_TEXT = "longtext";
    private static final String UNSIGNED = "UNSIGNED";

    /**
     * short字节数（2字节）.
     */
    private static final int SHORT_BYTE_COUNT = 2;

    /**
     * int字节数（4字节）.
     */
    private static final int INT_BYTE_COUNT = 4;

    /**
     * long字节数（8字节）.
     */
    private static final int LONG_BYTE_COUNT = 8;


    /**
     * 获取数据列对应的java Primitive数据类型.
     *
     * @param column 数据列
     * @return 返回数据列的数据类型tableSchema = "elitech_iot"
     */
    public static String getPrimitiveDataType(Column column) {
        switch (column.getDataType()) {
            case MYSQL_DATA_TYPE_BIG_INT:
                return JAVA_DATA_TYPE_BIG_INTEGER;
            case MYSQL_DATA_TYPE_BINARY:
            case MYSQL_DATA_TYPE_VARBINARY:
            case MYSQL_DATA_TYPE_BLOB:
            case MYSQL_DATA_TYPE_TINY_BLOB:
            case MYSQL_DATA_TYPE_MEDIUM_BLOB:
            case MYSQL_DATA_TYPE_LONG_BLOB:
                return JAVA_DATA_TYPE_BYTES;
            case MYSQL_DATA_TYPE_BIT:
                return JAVA_DATA_TYPE_BOOLEAN;
            case MYSQL_DATA_TYPE_DATE:
            case MYSQL_DATA_TYPE_YEAR:
                return JAVA_DATA_TYPE_DATE;
            case MYSQL_DATA_TYPE_DATETIME:
                return JAVA_DATA_TYPE_DATETIME;
            case MYSQL_DATA_TYPE_DECIMAL:
                return JAVA_DATA_TYPE_DECIMAL;
            case MYSQL_DATA_TYPE_DOUBLE:
                return JAVA_DATA_TYPE_DOUBLE;
            case MYSQL_DATA_TYPE_FLOAT:
                return JAVA_DATA_TYPE_FLOAT;
            case MYSQL_DATA_TYPE_TINY_INT:
            case MYSQL_DATA_TYPE_SMALL_INT:
            case MYSQL_DATA_TYPE_MEDIUM_INT:
            case MYSQL_DATA_TYPE_INTEGER:
                String boolType = "tinyint(1)";
                if (column.getCloumnType().contains(boolType)) {
                    return JAVA_DATA_TYPE_BYTE;
                }

                // 无符整数
                if (column.getCloumnType().contains(UNSIGNED)) {
                    if (column.getNumericPrecision() <= SHORT_BYTE_COUNT) {
                        return JAVA_DATA_TYPE_SHORT;
                    } else if (column.getNumericPrecision() < INT_BYTE_COUNT) {
                        return JAVA_DATA_TYPE_INT;
                    } else if (column.getNumericPrecision() < LONG_BYTE_COUNT) {
                        return JAVA_DATA_TYPE_LONG;
                    } else {
                        return JAVA_DATA_TYPE_BIG_INTEGER;
                    }
                } else { // 有符整数
                    if (column.getNumericPrecision() < SHORT_BYTE_COUNT) {
                        return JAVA_DATA_TYPE_BYTE;
                    } else if (column.getNumericPrecision() <= INT_BYTE_COUNT) {
                        return JAVA_DATA_TYPE_INT;
                    } else if (column.getNumericPrecision() <= LONG_BYTE_COUNT) {
                        return JAVA_DATA_TYPE_LONG;
                    } else {
                        return JAVA_DATA_TYPE_BIG_INTEGER;
                    }
                }
            case MYSQL_DATA_TYPE_TIME:
                return JAVA_DATA_TYPE_TIME;
            case MYSQL_DATA_TYPE_TIMESTAMP:
                return JAVA_DATA_TYPE_TIMESTAMP;

            case MYSQL_DATA_TYPE_CHAR:
            case MYSQL_DATA_TYPE_VARCHAR:
            case MYSQL_DATA_TYPE_TINY_TEXT:
            case MYSQL_DATA_TYPE_MEDIUM_TEXT:
            case MYSQL_DATA_TYPE_TEXT:
            case MYSQL_DATA_TYPE_LONG_TEXT:
            case MYSQL_DATA_TYPE_ENUM:
            case MYSQL_DATA_TYPE_SET:
            case MYSQL_DATA_TYPE_JSON:
            default:
                return JAVA_DATA_TYPE_STRING;
        }
    }

    /**
     * 获取引用类型.
     *
     * @param javaPrimitiveType java  Primitive类型
     * @return primitive类型对应的引用类型.
     */
    public static String getObjectDataType(String javaPrimitiveType) {
        switch (javaPrimitiveType) {
            case JAVA_DATA_TYPE_BOOLEAN:
            case JAVA_DATA_TYPE_BYTE:
            case JAVA_DATA_TYPE_SHORT:
            case JAVA_DATA_TYPE_INT:
            case JAVA_DATA_TYPE_LONG:
            case JAVA_DATA_TYPE_FLOAT:
            case JAVA_DATA_TYPE_DOUBLE:
                return StringUtil.upperCaseFirst(javaPrimitiveType);
            default:
                return javaPrimitiveType;
        }
    }

    /**
     * 获取列中的主键列
     *
     * @param columnList
     * @return
     */
    public static List<Column> getPrimaryKey(List<Column> columnList) {
        return columnList.stream().filter(c -> "PRI".equalsIgnoreCase(c.getColumnKey())).collect(Collectors.toList());
    }

    /**
     * 获取表单Label的多语言key.
     *
     * @param config 代码生成配置文件
     * @param column 数据库表列信息
     * @return 列对应的表单Label信息的多语言key
     */
    public static String getFormLabelLocaleKey(CodeGeneratorProperties config, Column column) {
        String fieldName = CodeUtil.columnToField(column.getColumnName(), config.getColumnNamePrefix());
        if (config.getChildModule() == null) {
            return String.format("%s.%s.%s", config.getRootPackageName(), config.getModule(), fieldName);
        }
        return String.format("%s.%s.%s.%s", config.getRootPackageName(), config.getModule(), config.getChildModule(), fieldName);
    }

    /**
     * 获取字段的验证注解信息.
     *
     * @param config 代码生成配置文件
     * @param column 数据库表列信息
     * @return 字段的验证注解
     */
    public static List<String> getFiledValidatorAnnotationList(CodeGeneratorProperties config, Column column) {
        String domain = "model";
        List<String> validatorList = new ArrayList<>();
        String fieldDataType = getPrimitiveDataType(column);
        String fieldName = CodeUtil.columnToField(column.getColumnName(), config.getColumnNamePrefix());
        String className = CodeUtil.tableToClassName(column.getTableName(), config.getTableNamePrefix());

        // @NotNull 验证
        if (!column.isNullAble()) {
            String notNullValidator;
            if (StringUtil.isEmpty(config.getChildModule())) {
                notNullValidator = String.format("@NotNull(message = \"{%s.%s.%s.%s.%s.NotNUll}\")", config.getRootPackageName(), config.getModule(), domain, className, fieldName);
            } else {
                notNullValidator = String.format("@NotNull(message = \"{%s.%s.%s.%s.%s.%s.NotNUll}\")", config.getRootPackageName(), config.getModule(), config.getChildModule(), domain, className, fieldName);
            }
            validatorList.add(notNullValidator);
        }

        long length = column.getCharacterMaximumLength();
        // String Length validator
        if (JAVA_DATA_TYPE_STRING.equalsIgnoreCase(fieldDataType) && length > 0) {
            String lengthValidator;
            if (length < Integer.MAX_VALUE) {
                if (StringUtil.isEmpty(config.getChildModule())) {
                    lengthValidator = String.format("@Length(max = %d, message = \"{%s.%s.%s.%s.%s.Length}\")", length, config.getRootPackageName(), config.getModule(), domain, className, fieldName);
                } else {
                    lengthValidator = String.format("@Length(max = %d, message = \"{%s.%s.%s.%s.%s.%s.Length}\")", length, config.getRootPackageName(), config.getModule(), config.getChildModule(), domain, className, fieldName);
                }
            } else {
                if (StringUtil.isEmpty(config.getChildModule())) {
                    lengthValidator = String.format("@Length(max = Integer.MAX_VALUE, message = \"{%s.%s.%s.%s.%s.Length}\")", config.getRootPackageName(), config.getModule(), domain, className, fieldName);
                } else {
                    lengthValidator = String.format("@Length(max = Integer.MAX_VALUE, message = \"{%s.%s.%s.%s.%s.%s.Length}\")", config.getRootPackageName(), config.getModule(), config.getChildModule(), domain, className, fieldName);
                }
            }

            validatorList.add(lengthValidator);
        }

        return validatorList;
    }
}
