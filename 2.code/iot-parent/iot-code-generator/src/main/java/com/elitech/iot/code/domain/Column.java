package com.elitech.iot.code.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 数据库表的列信息.
 * 
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019年12月12日
 *
 */
@Getter
@Setter
public class Column implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2947271747046511430L;

	/**
	 * 数据库名称.
	 */
	private String tableSchema;

	
	/**
	 * 表名.
	 */
	private String tableName;
	
	/**
	 * 列名.
	 */
	private String columnName;
	
	/**
	 * 列顺序号.
	 */
	private int ordinalPosition;
	
	/**
	 * 列的默认值.
	 */
	private String columnDefault;
	
	/**
	 * 是否可以为null：可以为true、不可以为false.
	 */
	private boolean nullAble;
	
	/**
	 * 字段的数据类型.
	 */
	private String dataType;
	
	/**
	 * 最大字符长度(多字节字符算一个字符).
	 */
	private long characterMaximumLength;
	
	/**
	 * 最大字节长度(多字节字符算多个， characterMaximumLength多字节算一个字符).
	 */
	private long characterOctetLength;
	
	/**
	 * 数值类型数据的长度(bit、tinyint、smallint、mediumint、decimal、int、float、bigint、double有该值，).
	 */
	private int numericPrecision;
	
	/**
	 * 数值类型的小数位数(非数值类型为0).
	 */
	private int numericScale;
	
	/**
	 * 列类型(与dataType的区别在于cloumnType包含长度，符号位unsigned，enum\set的取值信息).
	 */
	private String cloumnType;

	/**
	 * 列的索引信息(主键索引PRI、唯一索引UNI、非唯一索引MUL)：没有索引则为空字符串.
	 */
	private String columnKey;
	
	/**
	 * 列的附加信息(如auto_increment).
	 */
	private String extra;
	
	/**
	 * 列的说明.
	 */
	private String columnComment;

	@Override
	public String toString() {
		return "Column [tableSchema=" + tableSchema + ", tableName=" + tableName + ", columnName=" + columnName
				+ ", ordinalPosition=" + ordinalPosition + ", columnDefault=" + columnDefault + ", nullAble=" + nullAble
				+ ", dataType=" + dataType + ", characterMaximumLength=" + characterMaximumLength
				+ ", characterOctetLength=" + characterOctetLength + ", numericPrecision=" + numericPrecision
				+ ", numericScale=" + numericScale + ", cloumnType=" + cloumnType + ", columnKey=" + columnKey
				+ ", extra=" + extra + ", columnComment=" + columnComment + "]";
	}
	
	
}
