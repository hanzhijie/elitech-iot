package com.elitech.iot.code.generator.backend.facade;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * Model代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/20
 */
@Component
public class ModelCodeGenerator extends AbstractCodeGenerator {
    /**
     * 模板文件.
     */
    public static final String TEMPLATE_FILE = "/templates/backend/domain/Model.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.FACADE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "model";
    }

    @Override
    public String getClassSuffix() {
        return "Model";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
