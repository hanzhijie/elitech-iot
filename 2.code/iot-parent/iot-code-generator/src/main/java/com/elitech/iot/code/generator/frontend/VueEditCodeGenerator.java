package com.elitech.iot.code.generator.frontend;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/1/10
 */
@Component
public class VueEditCodeGenerator extends AbstractCodeGenerator {
    /**
     * 模板路径.
     */
    public static final String TEMPLATE_FILE = "/templates/frontend/Edit.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.VUE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "vue";
    }

    @Override
    public String getClassSuffix() {
        return "Update";
    }

    @Override
    public String getFileSuffix() {
        return ".vue";
    }
}
