package com.elitech.iot.code;

import org.apache.velocity.app.VelocityEngine;

import java.util.Properties;

/**
 * Velocity 模板引擎工厂.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public class VelocityEngineFactory {
    private static VelocityEngine engine;

    /**
     * 创建模板引擎.
     *
     * @return 返回一个VelocityEngine对象。<br>
     * 注意：file.resource.loader.path属性是否正确（模板文件的路径）。
     */
    public static VelocityEngine create() {
        if (engine == null) {
            Properties properties = new Properties();

            properties.setProperty("class.resource.loader.description", "Velocity ClassPath Resource Loader");

            //从classpath加载模板文件
            properties.setProperty("resource.loader", "class");
            properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            properties.setProperty("class.resource.loader.cache", "true");
            properties.setProperty("class.resource.loader.modificationCheckInterval", "30");
            properties.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.Log4JLogChute");
            properties.setProperty("runtime.log.logsystem.log4j.logger", "org.apache.velocity");
            properties.setProperty("directive.set.null.allowed", "true");

            engine = new VelocityEngine();
            engine.init(properties);
        }

        return engine;
    }
}
