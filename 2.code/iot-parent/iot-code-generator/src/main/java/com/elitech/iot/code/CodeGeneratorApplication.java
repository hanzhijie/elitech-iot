package com.elitech.iot.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 代码生成工具应用程序.
 * 
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019年12月13日
 *
 */
@SpringBootApplication
public class CodeGeneratorApplication {
	public static void main(String[] args) {
		SpringApplication.run(CodeGeneratorApplication.class, args);
	}
}
