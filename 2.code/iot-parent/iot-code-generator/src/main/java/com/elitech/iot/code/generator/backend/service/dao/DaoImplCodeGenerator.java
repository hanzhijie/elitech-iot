package com.elitech.iot.code.generator.backend.service.dao;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * Dao实现类代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/19
 */
@Component
public class DaoImplCodeGenerator extends AbstractCodeGenerator {
    public static final String TEMPLATE_FILE = "/templates/backend/dao/DaoImpl.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.SERVICE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "dao.impl";
    }

    @Override
    public String getClassSuffix() {
        return "DaoImpl";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
