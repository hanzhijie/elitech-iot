package com.elitech.iot.code.util;

import com.elitech.iot.code.domain.Column;
import com.elitech.iot.common.base.util.StringUtil;

import java.util.List;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/16
 */
public class MapperXmlUtil {
    private static int delLen = 3;

    /**
     * 获取insert sql语句参数列表字符串.
     *
     * @param list 表的列集合
     * @return 返回insert sql语句参数列表字符串。
     */
    public static String getInsertColumnString(List<Column> list) {
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        for (Column col : list) {
            // 不要自增长列
            if (col.getExtra() != null && !col.getExtra().contains("auto_increment")) {
                // 加"`"是为了避免有些表名是数据库的关键字
                sb.append("`");
                sb.append(col.getColumnName());
                sb.append("`");
                sb.append(",");
                sb.append("\r\n");
            }
        }

        // 删除最后一个逗号
        sb.deleteCharAt(sb.length() - delLen);
        return sb.toString();
    }

    /**
     * 获取表的insert sql语句值的列表字符串.
     *
     * @param list 表的值集合
     * @return 返回insert sql语句值的列表字符串。
     */
    public static String getInsertValueString(List<Column> list, String columnPrefix) {
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        for (Column col : list) {
            // 不要自增长列
            if (col.getExtra() != null && !col.getExtra().contains("auto_increment")) {
                String fieldName = CodeUtil.columnToField(col.getColumnName(), columnPrefix);
                String paramName = String.format("#{%s}", fieldName);
                sb.append(paramName).append(",");
                sb.append("\r\n");
            }
        }

        // 删除最后一个逗号
        sb.deleteCharAt(sb.length() - delLen);
        return sb.toString();
    }

    /**
     * 获取批量插入时insert sql语句值的列表字符串.
     *
     * @param list 表的值集合
     * @return 返回批量插入时insert sql语句值的列表字符串。
     */
    public static String getInsertListValueString(List<Column> list, String columnPrefix) {
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        for (Column col : list) {
            // 不要自增长列
            if (col.getExtra() != null && !col.getExtra().contains("auto_increment")) {
                String fieldName = CodeUtil.columnToField(col.getColumnName(), columnPrefix);
                String paramName = String.format("#{domain.%s}", fieldName);
                sb.append(paramName).append(",");
                sb.append("\r\n");
            }
        }

        // 删除最后一个逗号
        sb.deleteCharAt(sb.length() - delLen);
        return sb.toString();
    }

    /**
     * 获取表的update语句值列表字符串.
     *
     * @param list 表的值集合
     * @return 返回update参数列表字符串。
     */
    public static String getUpdateSetString(List<Column> list, String columnPrefix) {
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        for (Column col : list) {
            // 不要自增长列
            if (col.getExtra() != null && !col.getExtra().contains("auto_increment")) {
                String fieldName = CodeUtil.columnToField(col.getColumnName(), columnPrefix);

                // 加"`"是为了避免有些列名是数据库的关键字
                sb.append("`").append(col.getColumnName()).append("`");
                sb.append(" = ");
                sb.append("#{").append(fieldName).append("}");
                sb.append(",");
                sb.append("\r\n");
            }
        }

        // 删除最后一个逗号
        sb.deleteCharAt(sb.length() - delLen);
        return sb.toString();
    }

    /**
     * 生成自增长配置字符串. <br>
     * 如：useGeneratedKeys="true" keyProperty="id".
     *
     * @param list 数据表的所有列
     * @return 如果有自增长主键则返回相应的自增长字符串，否则返回空字符串。
     */
    public static String getAutoIncrementAttribute(List<Column> list, String columnPrefix) {
        for (Column col : list) {
            if (col.getExtra() != null && col.getExtra().contains("auto_increment")) {
                String property = CodeUtil.columnToField(col.getColumnName(), columnPrefix);
                String str = String.format("useGeneratedKeys=\"true\" keyProperty=\"%s\"", property);
                return str;
            }
        }
        return "";
    }
}
