package com.elitech.iot.code.generator.backend.service.service;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * 服务实现层代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/19
 */
@Component
public class ServiceImplCodeGenerator extends AbstractCodeGenerator {
    public static final String TEMPLATE_FILE = "/templates/backend/service/ServiceImpl.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.SERVICE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "service.impl";
    }

    @Override
    public String getClassSuffix() {
        return "ServiceImpl";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
