package com.elitech.iot.code.generator.backend.service.service;

import com.elitech.iot.code.generator.AbstractCodeGenerator;
import com.elitech.iot.code.generator.CodeLayerType;
import org.springframework.stereotype.Component;

/**
 * service层接口代码生成
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019/12/19
 */
@Component
public class ServiceCodeGenerator extends AbstractCodeGenerator {
    public static final String TEMPLATE_FILE = "/templates/backend/service/Service.vm";

    @Override
    public CodeLayerType getCodeLayerType() {
        return CodeLayerType.SERVICE;
    }

    @Override
    public String getTemplateFile() {
        return TEMPLATE_FILE;
    }

    @Override
    public String getLayer() {
        return "service";
    }

    @Override
    public String getClassSuffix() {
        return "Service";
    }

    @Override
    public String getFileSuffix() {
        return ".java";
    }
}
