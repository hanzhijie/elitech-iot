package com.elitech.iot.code.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 数据库表信息.
 * 
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019年12月13日
 *
 */
@Getter
@Setter
public class Table implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6051884841818906160L;
	
	/**
	 * 数据库名称.
	 */
	private String tableSchema;

	
	/**
	 * 表名.
	 */
	private String tableName;

	

	/**
	 * 是否有自增长字段.
	 */
	private boolean autoIncrement;
	
	/**
	 * 表说明.
	 */
	private String tableComment;

	/**
	 * 创建时间.
	 */
	private Date createTime;

	@Override
	public String toString() {
		return "Table [tableSchema=" + tableSchema + ", tableName=" + tableName + ", autoIncrement=" + autoIncrement
				+ ", tableComment=" + tableComment + ", createTime=" + createTime + "]";
	}
	
	
}
