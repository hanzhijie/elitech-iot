package com.elitech.iot.code.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.elitech.iot.code.domain.Column;
import com.elitech.iot.code.domain.Table;
import org.springframework.stereotype.Component;

/**
 * 数据库表、字段元数据访问.
 * 
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2019年12月13日
 *
 */
@Mapper
@Component
public interface TableMapper {
	/**
	 * 获取数据库中所有的表名.
	 * 
	 * @return 返回数据库中所有的表名。
	 */
	@Select("show tables")
	public List<String> findAllTables();

	/**
	 * 查询数据库中的所有表.
	 * 
	 * @param dbName 数据库名
	 * @return 返回指定数据库中所有的表。
	 */
	@Select("SELECT  t.TABLE_SCHEMA AS tableSchema,  t.TABLE_NAME tableName,  IF(ISNULL(t.AUTO_INCREMENT), FALSE, TRUE) AS autoIncrement,  t.TABLE_COMMENT AS tableComment,  t.CREATE_TIME AS createTime FROM TABLES t WHERE t.TABLE_SCHEMA = #{dbName}")
	public List<Table> findTableByDb(@Param("dbName") String dbName);

	/**
	 * 查询表的详细信息.
	 * 
	 * @param dbName    数据库名.
	 * @param tableName 表名.
	 * @return 返回表的详细信息。
	 */
	@Select("SELECT  t.TABLE_SCHEMA AS tableSchema,  t.TABLE_NAME tableName,  IF(ISNULL(t.AUTO_INCREMENT), FALSE, TRUE) AS autoIncrement,  t.TABLE_COMMENT AS tableComment,  t.CREATE_TIME AS createTime FROM TABLES t WHERE t.TABLE_SCHEMA = #{dbName} AND t.TABLE_NAME = #{tableName}")
	public Table findTableByDbAndTable(@Param("dbName") String dbName, @Param("tableName") String tableName);

	/**
	 * 查询数据表的所有列信息.
	 * 
	 * @param dbName    数据库名<br>
	 *                  指定数据库名是为了避免不同的数据库有相同的表名。
	 * @param tableName 表名
	 * @return 返回表的所有列。
	 */
	@Select("SELECT c.TABLE_SCHEMA AS tableSchema,  c.TABLE_NAME AS tableName,  c.COLUMN_NAME columnName,  c.ORDINAL_POSITION ordinalPosition,  c.COLUMN_DEFAULT columnDefault,  IF(c.IS_NULLABLE = 'NO', FALSE, TRUE) AS nullAble,  c.DATA_TYPE AS dataType,  c.CHARACTER_MAXIMUM_LENGTH AS characterMaximumLength,  c.CHARACTER_OCTET_LENGTH AS characterOctetLength,  IFNULL(c.NUMERIC_PRECISION, 0) AS numericPrecision,  IFNULL(c.NUMERIC_SCALE, 0) AS numericScale,  c.COLUMN_TYPE AS cloumnType,  c.COLUMN_KEY AS columnKey,  c.EXTRA AS extra,  c.COLUMN_COMMENT AS columnComment FROM COLUMNS c WHERE c.TABLE_SCHEMA = #{dbName} AND c.TABLE_NAME = #{tableName}")
	public List<Column> findColumnByDbAndTable(@Param("dbName") String dbName, @Param("tableName") String tableName);
}
