package com.elitech.iot.auth.config.auth.security;

import com.elitech.iot.auth.domain.BaseUserDetails;
import com.elitech.iot.auth.service.MyUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * @ClassName MyAuthenticationProvider
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/6
 * @Version V1.0
 **/
@Configuration
public class MyAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private MyUserDetailsServiceImpl userDetailsService;
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();


        BaseUserDetails userInfo = userDetailsService.loadUserByUsername(userName);
        String passwordU = userInfo.getAccountPassword();

        if (!userInfo.getPassword().equals(passwordU)) {
            throw new BadCredentialsException("用户名密码不正确，请重新登陆！");
        }

        return new UsernamePasswordAuthenticationToken(userInfo, password, userInfo.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
