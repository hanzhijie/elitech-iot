package com.elitech.iot.auth.service;

import com.elitech.iot.auth.dao.DaoClientDetails;
import com.elitech.iot.auth.domain.BaseClientDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

/**
 * @ClassName MyClientDetailsService
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/9
 * @Version V1.0
 **/
@Service
public class MyClientDetailsServiceImpl implements ClientDetailsService {
    @Autowired
    private DaoClientDetails daoClientDetails;
    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        BaseClientDetails baseClientDetails = daoClientDetails.queryClientDetailsByClientId(clientId);
        return baseClientDetails;
    }
}
