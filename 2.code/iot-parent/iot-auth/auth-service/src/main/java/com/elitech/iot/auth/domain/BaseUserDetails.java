package com.elitech.iot.auth.domain;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @ClassName BaseUserDetails
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/6
 * @Version V1.0
 **/
@Data
public class BaseUserDetails implements UserDetails {
    /**
     * 账号id.
     */
    private java.math.BigInteger id;
    /**
     * 手机号国家码.
     */
    private String phoneCountyCode;
    /**
     * 手机号.
     */
    private String phone;
    /**
     * 邮箱.
     */
    private String email;
    /**
     * 密码：32位MD5大写字符串.
     */
    private String accountPassword;
    /**
     * 用户名可以用来登录：手机号、邮箱注册时可以填写用户名，登录可以使用用户名登录.
     */
    private String userName;
    /**
     * 账号头像，相对url地址.
     */
    private String accountPhoto;

    /**
     * 微信openid.
     */
    private String wechat;
    /**
     * qq.
     */
    private String qq;
    /**
     * facebook.
     */
    private String facebook;
    /**
     * twitter.
     */
    private String twitter;
    /**
     * linkedin.
     */
    private String linkedin;
    /**
     * 组织机构id.
     */
    private java.math.BigInteger organizationId;
    /**
     * 组织路径.
     */
    private String organizationPath;
    /**
     * 创建时间（格林尼治时间）.
     */
    private java.time.LocalDateTime gmtCreate;
    /**
     * 最后更新时（格林尼治时间）.
     */
    private java.time.LocalDateTime gmtModified;
    /**
     * 最后登录时间（格林尼治时间）.
     */
    private java.time.LocalDateTime lastLoginTime;
    /**
     * 字符串转换成为long类型.
     */
    private java.math.BigInteger lastLoginIp;
    /**
     * 最后登录方式：1web、2IOS、3Android、4微信小程序、5微信公众号.
     */
    private Integer lastLoginType;

    private Collection<? extends GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.accountPassword;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
