package com.elitech.iot.auth.config.auth.token;

import com.elitech.iot.auth.domain.BaseUserDetails;
import com.elitech.iot.auth.service.MyUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @MethodName:
 * @Description: TODO
 * @Param:
 * @Return:
 * @Author: dongqg
 * @Date: 2020/2/18
 **/
@Component
public class CustomUserAuthenticationConverter extends DefaultUserAuthenticationConverter {

    @Autowired
    private MyUserDetailsServiceImpl userDetailsService;

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        LinkedHashMap<String, Object> response = new LinkedHashMap();
        String name = authentication.getName();
        response.put("username", name);

        Object principal = authentication.getPrincipal();
        BaseUserDetails baseUserDetails = null;
        if (principal instanceof BaseUserDetails) {
            baseUserDetails = (BaseUserDetails) principal;
        } else {
            //refresh_token默认不去调用userdetailService获取用户信息，这里我们手动去调用，得到 UserJwt
            UserDetails userDetails = userDetailsService.loadUserByUsername(name);
            if (!Objects.isNull(userDetails)) {
                baseUserDetails = (BaseUserDetails) userDetails;
            }
        }
        response.put("userName", baseUserDetails.getUsername());
        response.put("id", baseUserDetails.getId());
        response.put("photo", baseUserDetails.getAccountPhoto());
        response.put("lastTime", baseUserDetails.getLastLoginTime());
        response.put("organizationId",baseUserDetails.getOrganizationId());
        if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
            response.put("authorities", AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
        }
        return response;
    }

}
