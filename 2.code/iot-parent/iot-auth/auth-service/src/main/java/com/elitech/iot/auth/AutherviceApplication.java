package com.elitech.iot.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 设备服务器的程序.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020年01月02日
 */
@SpringBootApplication
@EnableDiscoveryClient
@Configuration
@MapperScan(basePackages = "com.elitech.iot.auth.dao")
public class AutherviceApplication {

    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(AutherviceApplication.class, args);
    }

    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Bean(name = "restTemplate")
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
