package com.elitech.iot.auth.service;

import com.elitech.iot.auth.domain.AuthToken;

/**
 * @ClassName AuthService
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/11
 * @Version V1.0
 **/
public interface AuthService {
    AuthToken getToken(String userName, String password, String clientId, String clientSecret);
}
