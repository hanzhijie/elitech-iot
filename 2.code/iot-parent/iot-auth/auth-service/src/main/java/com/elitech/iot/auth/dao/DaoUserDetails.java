package com.elitech.iot.auth.dao;

import com.elitech.iot.auth.domain.BaseUserDetails;

import java.math.BigInteger;
import java.util.List;

/**
 * @MethodName:
 * @Description: TODO
 * @Param:
 * @Return:
 * @Author: dongqg
 * @Date: 2020/2/18
**/
public interface DaoUserDetails {
    /**
     * @MethodName:
     * @Description: 根据用户名查询用户信息
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/18
    **/
    BaseUserDetails queryUserDetails(String userName);


    /**
     * @MethodName:
     * @Description: 根据用户id查询权限列表
     * @Param:
     * @Return:
     * @Author: dongqg
     * @Date: 2020/2/18
    **/
    List<String> queryAuthority(BigInteger id);
}
