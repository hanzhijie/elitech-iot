package com.elitech.iot.auth.service;

import com.elitech.iot.auth.dao.DaoUserDetails;
import com.elitech.iot.auth.domain.BaseUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

/**
 * @ClassName MyUserDetailService
 * @Description: 自定义UserDetailsService，从数据库查询用户信息。
 * @Author dongqg
 * @Date 2020/2/6
 * @Version V1.0
 **/
@Service
public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private DaoUserDetails daoUserDetails;
    /**
     * 根据用户名查询用户信息
     * @param s
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public BaseUserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        //根据用户名查询用户信息，得到自定义的BaseUserDetials
        BaseUserDetails baseUserDetails = daoUserDetails.queryUserDetails(s);
        if (baseUserDetails == null){
            throw new BadCredentialsException("用户名不存在！");
        }
        //根据用户id查询用户权限
        List<String> permissions = daoUserDetails.queryAuthority(baseUserDetails.getId());
        HashSet<SimpleGrantedAuthority> permissionsSet = new HashSet<>();
        for (String permission : permissions) {
            permissionsSet.add(new SimpleGrantedAuthority(permission));
        }
        baseUserDetails.setAuthorities(permissionsSet);
        //封装用户名密码和权限信息并返回
        return  baseUserDetails;
    }
}
