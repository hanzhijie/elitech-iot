package com.elitech.iot.auth.dao;


import com.elitech.iot.auth.domain.BaseClientDetails;

/**
 * @MethodName:
 * @Description: TODO
 * @Param:
 * @Return:
 * @Author: dongqg
 * @Date: 2020/2/18
**/
public interface DaoClientDetails {
    /**
     * @MethodName: queryClientDetailsByClientId
     * @Description: 根据客户端id获取客户端详情
     * @Param: clientId
     * @Return: BaseClientDetails
     * @Author: dongqg
     * @Date: 2020/2/18
    **/
    BaseClientDetails queryClientDetailsByClientId(String clientId);
}
