package com.elitech.iot.auth.controller;

import com.elitech.iot.auth.domain.AuthToken;
import com.elitech.iot.auth.service.AuthService;
import com.elitech.iot.common.base.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName AuthController
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/11
 * @Version V1.0
 **/
@RestController
public class AuthController {
    @Autowired
    private AuthService authService;
    /**
     * 客户端id
     */
    @Value("${auth.clientId}")
    private String clientId;
    /**
     * 客户端秘钥
     */
    @Value("${auth.clientSecret}")
    private String clientSecret;

    @RequestMapping("/get/token")
    public AuthToken getToken(@RequestParam("userName") String userName ,
                              @RequestParam("password") String password){
        if(StringUtil.isEmpty(userName)){
            throw new RuntimeException("用户名不允许为空");
        }
        if(StringUtil.isEmpty(password)){
            throw new RuntimeException("密码不允许为空");
        }
        //申请令牌
        AuthToken authToken = authService.getToken(userName,password,clientId,clientSecret);
        return authToken;
    }
}
