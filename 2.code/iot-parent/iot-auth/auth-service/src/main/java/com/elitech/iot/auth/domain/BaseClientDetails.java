package com.elitech.iot.auth.domain;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.*;

/**
 * @ClassName BaseClientDetails
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/6
 * @Version V1.0
 **/
@Data
public class BaseClientDetails implements ClientDetails {
    private static final long serialVersionUID = 1L;

    /**
     * 客户端信息主键 id.
     */
    private java.math.BigInteger id;
    /**
     * 客户端id.
     */
    private String clientId;
    /**
     * 客户密钥.
     */
    private String clientSecret;
    /**
     * 客户端类型：1web云平台、2IOS、3Android、4微信小程序、5微信公众号、100第三方、1000设备.
     */
    private int clientType;
    /**
     * 资源id.
     */
    private String resourceIds;
    /**
     * 作用范围.
     */
    private String scope;
    /**
     * 授权类型.
     */
    private String authorizedGrantTypes;
    /**
     * 重定向地址.
     */
    private String webServerRedirectUri;
    /**
     * 角色.
     */
    private String authorities;
    /**
     * access_token有效时间.
     */
    private long accessTokenValidity;
    /**
     * refresh_token有效时间.
     */
    private long refreshTokenValidity;
    /**
     * 自动授权.
     */
    private int autoApprove;
    /**
     * 附加信息.
     */
    private String additionalInformation;
    /**
     * 创建时间.
     */
    private java.time.LocalDateTime gmtCreate;
    /**
     * 最后修改时间.
     */
    private java.time.LocalDateTime gmtModified;
    @Override
    public String getClientId() {
        return this.clientId;
    }

    @Override
    public Set<String> getResourceIds() {

        String[] split = this.resourceIds.split(",");
        HashSet<String> set = new HashSet<>();
        Collections.addAll(set, split);
        return set;
    }

    @Override
    public boolean isSecretRequired() {
        return this.clientSecret != null;
    }

    @Override
    public String getClientSecret() {
        return this.clientSecret;
    }

    @Override
    public boolean isScoped() {
        return this.scope != null;
    }

    @Override
    public Set<String> getScope() {
        String[] split = this.scope.split(",");
        HashSet<String> set = new HashSet<>();
        Collections.addAll(set, split);
        return set;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        String[] split = this.authorizedGrantTypes.split(",");
        HashSet<String> set = new HashSet<>();
        set.addAll(Arrays.asList(split));
        return set;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        String[] split = this.webServerRedirectUri.split(",");
        HashSet<String> set = new HashSet<>();
        set.addAll(Arrays.asList(split));
        return set;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        String[] split = this.authorities.split(",");
        HashSet<GrantedAuthority> set = new HashSet<>();
        for (String s : split) {
            set.add(new SimpleGrantedAuthority(s));
        }
        return set;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return Math.toIntExact(this.accessTokenValidity);
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return Math.toIntExact(this.refreshTokenValidity);
    }

    @Override
    public boolean isAutoApprove(String s) {
        return autoApprove == 1;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        HashMap<String,Object> map = new HashMap<>();
        map.put("additionalInformation",this.additionalInformation);
        return map;
    }
}
