package com.elitech.iot.auth.config.auth.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.bootstrap.encrypt.KeyProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import javax.annotation.Resource;
import java.security.KeyPair;

/**
 * @ClassName TokenConfig
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/9
 * @Version V1.0
 **/
@Configuration
public class TokenConfig {
    /**
     * 读取密钥的配置
     * @return
     */
    @Bean("keyProp")
    public KeyProperties keyProperties() {
        return new KeyProperties();
    }

    @Resource(name = "keyProp")
    private KeyProperties keyProperties;
    @Autowired
    private CustomUserAuthenticationConverter customUserAuthenticationConverter;

    @Bean
    public TokenStore tokenStore() {
        //内存存储令牌（jwt令牌）
        return new JwtTokenStore(accessTokenConverter(customUserAuthenticationConverter));
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter(CustomUserAuthenticationConverter customUserAuthenticationConverter) {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyPair keyPair = new KeyStoreKeyFactory(
                //证书路径 elitech.jks
                keyProperties.getKeyStore().getLocation(),
                //证书秘钥 elitech
                keyProperties.getKeyStore().getSecret().toCharArray())
                .getKeyPair(
                        //证书别名 elitech
                        keyProperties.getKeyStore().getAlias(),
                        //证书密码 jcdq123
                        keyProperties.getKeyStore().getPassword().toCharArray());
        converter.setKeyPair(keyPair);
        //配置自定义的CustomUserAuthenticationConverter
        DefaultAccessTokenConverter accessTokenConverter = (DefaultAccessTokenConverter) converter.getAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(customUserAuthenticationConverter);
        return converter;
    }

}
