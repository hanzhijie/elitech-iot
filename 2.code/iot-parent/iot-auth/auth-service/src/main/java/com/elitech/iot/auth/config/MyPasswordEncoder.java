package com.elitech.iot.auth.config;

import com.elitech.iot.common.base.util.Md5Util;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @ClassName MyPasswordEncoder
 * @Description: 自定义密码编码
 * @Author dongqg
 * @Date 2020/2/9
 * @Version V1.0
 **/
@Configuration
public class MyPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        String var = "";
        try {
            var = Md5Util.encodeByMd5(charSequence.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return var;
    }
    @Override
    public boolean matches(CharSequence charSequence, String s) {
        if (s != null && s.length() != 0) {
            return charSequence.toString().equals(s);
        } else {

            return false;
        }

    }
}
