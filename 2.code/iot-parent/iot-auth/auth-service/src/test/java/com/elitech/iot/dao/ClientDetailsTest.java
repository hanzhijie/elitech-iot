package com.elitech.iot.dao;

import com.elitech.iot.auth.AutherviceApplication;
import com.elitech.iot.auth.dao.DaoClientDetails;
import com.elitech.iot.auth.domain.BaseClientDetails;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName ClientDetailsTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/9
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AutherviceApplication.class})
public class ClientDetailsTest {
    @Autowired
    private DaoClientDetails daoClientDetails;

    @Test
    public void queryUserDetials(){
        BaseClientDetails clientDetails = daoClientDetails.queryClientDetailsByClientId("client");
        System.out.println(clientDetails);
        System.out.println(clientDetails.getAdditionalInformation());
        System.out.println(clientDetails.getAuthorities());
        System.out.println(clientDetails.getAuthorizedGrantTypes());
        System.out.println(clientDetails.getRegisteredRedirectUri());
        System.out.println(clientDetails.getResourceIds());
        System.out.println(clientDetails.getScope());

    }
}
