package com.elitech.iot;

import com.elitech.iot.auth.AutherviceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName ParseJwtTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/18
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AutherviceApplication.class})
public class ParseJwtTest {
    /***
     * 校验令牌
     */
    @Test
    public void testParseToken(){
        //令牌
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJsYXN0VGltZSI6bnVsbCwiYXVkIjpbImJhc2Utc2VydmljZSJdLCJzY29wZSI6WyJhbGwiXSwicGhvdG8iOm51bGwsImlkIjoxMzE0LCJ1c2VyTmFtZSI6ImJ1eXVzYW4xMjMiLCJleHAiOjE1ODIxMjI0MzYsImF1dGhvcml0aWVzIjpbIlAxIiwiUDIiLCJQMyJdLCJqdGkiOiIzYzU2MTBhOC04Mjg2LTQxNGItODNkZC0wYjg1ZjFlNTdhNjUiLCJjbGllbnRfaWQiOiJjbGllbnQiLCJ1c2VybmFtZSI6ImJ1eXVzYW4xMjMifQ.RrV6bB4oHpwD-JZbUhL7nW-mDwie2NZkAaQtncIql74nxp3F438ypEhd1O2vKae2_UHmpbpUwhEGlZI3T8fsbul0TbEHsxudMTcvQRtZSe71LKDh6iYrPMsBilXcPKxh7QPBlvG7Z8W9hQl6c25ref_ictOQQMvJ5G3RtNbPO5sDtqka4dRjyasJPqgYr_9RR_-mhwMWC30UZf6YRRoZzS6_pFWKEEuYxWJ39pAwrBwak7ob29TMRt-tKKhDB5yEsfrAwuDRkUc89KTg9cfTiN8yqLol4Od4g53R4j2tJgj4QrTj7HcgLAnRwZuEgeS7JVlxT1fOjA_PJ_skgr9CRA";

        //公钥
        String publickey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhT6838FwTCm+sC3Smu1aFYbx0QCsKJWIB9BqJZBzF6ePFfcH2K7WMFQXZQZ455Hdr6UsuYAlksFJzmTAdfcvQ/RprbJTvroxjyKCZdCU1q/4huOjNvdRzPEMMjLQtcxDblD3One+hjpgBoAZDMjH3Dc2AnS2QKhf4hpDGtS4uLrUHVuRzspIF81z4nCiKcjYYZIRkcMGFcYPOOFhGtSVVixe4Xf0s9yuMS/fXlkpSBUfcMh7rOfb1NOxfZK0HhxfcHSxpw1czUBPsy7LUfcZQ1qOynu5RFQpEKg9v08GS3z3XzsAYrFP+yn8NGX8Sub5PwExl+ttH2cCM7eubAxz8wIDAQAB-----END PUBLIC KEY-----";

        //校验Jwt
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publickey));

        //获取Jwt原始内容
        String claims = jwt.getClaims();
        System.out.println(claims);
        //jwt令牌
        String encoded = jwt.getEncoded();
        System.out.println(encoded);
    }
}
