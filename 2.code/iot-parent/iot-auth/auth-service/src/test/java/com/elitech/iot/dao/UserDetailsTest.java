package com.elitech.iot.dao;

import com.elitech.iot.auth.AutherviceApplication;
import com.elitech.iot.auth.dao.DaoUserDetails;
import com.elitech.iot.auth.domain.BaseUserDetails;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @ClassName UserDetailsTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/7
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AutherviceApplication.class})
public class UserDetailsTest {
    @Autowired
    private DaoUserDetails daoUserDetails;

    @Test
    public void queryUserDetials(){
        BaseUserDetails zhangsan = daoUserDetails.queryUserDetails("budsfefe");
        System.out.println(zhangsan);
        List<String> list = daoUserDetails.queryAuthority(zhangsan.getId());
        System.out.println(list);
    }
}
