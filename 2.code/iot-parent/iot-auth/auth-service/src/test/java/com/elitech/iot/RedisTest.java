package com.elitech.iot;

import com.elitech.iot.auth.AutherviceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName RedisTest
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/10
 * @Version V1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AutherviceApplication.class})
@EnableAutoConfiguration
public class RedisTest {
   /* @Autowired
    private RedisUtil redisUtil;*/
    @Autowired
    private RedisTemplate redisTemplate;
   /* @Test
    public void redisTest(){
        redisUtil.set("yangming","skeigh");
    }*/
    @Test
    public void redisTest1(){
        redisTemplate.opsForValue().set("seggsesef", "d153", 600, TimeUnit.SECONDS);
    }

}
