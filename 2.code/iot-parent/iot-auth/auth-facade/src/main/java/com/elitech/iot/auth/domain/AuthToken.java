package com.elitech.iot.auth.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName AuthToken
 * @Description: TODO
 * @Author dongqg
 * @Date 2020/2/11
 * @Version V1.0
 **/
@Data
public class AuthToken implements Serializable {
    /**
     * 令牌信息
     */
    private String accessToken;
    /**
     * 刷新token(refresh_token)
     */
    private String refreshToken;
    /**
     * jwt短令牌
     */
    private String jti;

}
