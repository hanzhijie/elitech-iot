package com.elitech.iot.auth.client;

import com.elitech.iot.auth.domain.AuthToken;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @MethodName: 
 * @Description: TODO
 * @Param: 
 * @Return: 
 * @Author: dongqg
 * @Date: 2020/2/18
**/
@Component
@FeignClient(name = "auth-service", path="/auth")
public interface AuthClient {

    /**
     * @MethodName: getToken
     * @Description: 获取token的feign调用
     * @Param: [userName, password]
     * @Return: com.elitech.iot.base.domain.AuthToken
     * @Author: dongqg
     * @Date: 2020/2/18
    **/
    @RequestMapping("/get/token")
    AuthToken getToken(@RequestParam("userName") String userName,
                       @RequestParam("password") String password);

}
