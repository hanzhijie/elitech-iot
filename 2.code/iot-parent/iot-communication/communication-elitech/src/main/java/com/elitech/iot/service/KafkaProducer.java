package com.elitech.iot.service;

import com.elitech.iot.protocol.glog.ElitechGlogMessage;
import com.elitech.iot.protocol.iot.ElitechIotMessage;
import com.elitech.iot.constants.KafkaTopicConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/10
 */
@Component
public class KafkaProducer {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private KafkaTemplate<String, ElitechIotMessage> iotMessageKafkaTemplate;

    @Autowired
    private KafkaTemplate<String, ElitechGlogMessage> glogMessageKafkaTemplate;

    public void sendMessage(String message) {
        //String message = "Hello Kafka!";

        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(KafkaTopicConstants.DEVICE_DATA_KAFKA_TOPIC, message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println(throwable);
            }

            @Override
            public void onSuccess(SendResult<String, String> message) {
                System.out.println(message);
            }
        });
    }

    /**
     * 向Kafka消息中间件发送设备消息（数据帧未解析）.
     *
     * @param message 精创物联网通讯协议数据包
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ListenableFuture<SendResult<String, ElitechIotMessage>> sendElitechIotMessage(ElitechIotMessage message) {
        ListenableFuture<SendResult<String, ElitechIotMessage>> future = iotMessageKafkaTemplate.send(KafkaTopicConstants.ELITECH_IOT_PROTOCOL_KAFKA_TOPIC, message);
        future.addCallback(new ListenableFutureCallback<SendResult<String, ElitechIotMessage>>() {
            @Override
            public void onFailure(Throwable ex) {

            }

            @Override
            public void onSuccess(SendResult<String, ElitechIotMessage> result) {

            }
        });
        return future;
    }

    @Transactional(rollbackFor = Exception.class)
    public ListenableFuture<SendResult<String, ElitechGlogMessage>> sendElitechGlogMessage(ElitechGlogMessage message) {
        ListenableFuture<SendResult<String, ElitechGlogMessage>> future = glogMessageKafkaTemplate.send(KafkaTopicConstants.ELITECH_IOT_PROTOCOL_KAFKA_TOPIC, message);
        future.addCallback(new ListenableFutureCallback<SendResult<String, ElitechGlogMessage>>() {
            @Override
            public void onFailure(Throwable ex) {

            }

            @Override
            public void onSuccess(SendResult<String, ElitechGlogMessage> result) {

            }
        });
        return future;
    }
}
