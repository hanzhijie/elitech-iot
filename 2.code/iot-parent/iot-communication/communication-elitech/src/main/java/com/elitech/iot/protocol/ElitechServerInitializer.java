package com.elitech.iot.protocol;

import com.elitech.iot.protocol.glog.ElitechLogTransportHandler;
import com.elitech.iot.protocol.glog.GlogMessageDecoder;
import com.elitech.iot.protocol.iot.EliecthIotMessageDecoder;
import com.elitech.iot.protocol.iot.ElitechIotTransportHandler;
import com.elitech.iot.protocol.iot.ElitechIotTransportContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */
public class ElitechServerInitializer extends ChannelInitializer<SocketChannel> {
    private static final int READER_IDLE_TIME_SECONDS = 30;
    private static final int WRITER_IDLE_TIME_SECONDS = 60;
    private static final int ALL_IDLE_TIME_SECONDS = 60;

    private final ElitechIotTransportContext context;

    public ElitechServerInitializer(ElitechIotTransportContext context) {
        this.context = context;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        ch.id().asLongText();
        pipeline.addLast(new IdleStateHandler(READER_IDLE_TIME_SECONDS, WRITER_IDLE_TIME_SECONDS, ALL_IDLE_TIME_SECONDS, TimeUnit.SECONDS));

        // 注册精创物联网通讯协议解码器
        pipeline.addLast(new EliecthIotMessageDecoder());
        // 注册Elitch GLog通讯协议解码器
        pipeline.addLast(new GlogMessageDecoder());

        // 注册精创物联网通讯协议处理器
        pipeline.addLast(new ElitechIotTransportHandler(context));
        // 注册Elitech GLog通讯协议处理器
        pipeline.addLast(new ElitechLogTransportHandler(context));
    }
}
