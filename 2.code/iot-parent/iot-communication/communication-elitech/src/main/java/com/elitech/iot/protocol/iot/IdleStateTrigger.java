package com.elitech.iot.protocol.iot;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.nio.channels.SocketChannel;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/19
 */
@Slf4j
public class IdleStateTrigger extends ChannelInboundHandlerAdapter {
    @Override
    public void channelInactive(ChannelHandlerContext channelHandlerContext) throws Exception {
        SocketChannel socketChannel = (SocketChannel) channelHandlerContext.channel();
        String deviceId = NettyConnectionManager.getDeviceId(socketChannel);

        if (!StringUtils.isEmpty(deviceId)) {
            log.info("客户端设备连接断开：{}", deviceId);
            NettyConnectionManager.unRegisterConnection(deviceId);
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object event) throws Exception {
        SocketChannel socketChannel = (SocketChannel) ctx.channel();
        String deviceId = NettyConnectionManager.getDeviceId(socketChannel);
        if (StringUtils.isEmpty(deviceId)) {
            return;
        }

        if (event instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) event;
            IdleState state = idleStateEvent.state();

            if (state == IdleState.READER_IDLE) {
                log.info("设备离线{}", deviceId);
            }
        } else {
            super.userEventTriggered(ctx, event);
        }
    }
}
