package com.elitech.iot.protocol.iot;

import com.elitech.iot.SessionMsgListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */
@Slf4j
public class ElitechIotTransportHandler extends ChannelInboundHandlerAdapter implements GenericFutureListener<Future<? super Void>>, SessionMsgListener {
    private ElitechIotTransportContext context;
    private String sessionId;

    private volatile InetSocketAddress address;


    public ElitechIotTransportHandler(ElitechIotTransportContext context) {
        this.context = context;
        this.sessionId = UUID.randomUUID().toString();
    }

    @Override
    public void operationComplete(Future<? super Void> future) throws Exception {

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        if (msg instanceof ElitechIotMessage) {
            ElitechIotMessage message = (ElitechIotMessage) msg;
            processElitechV12Message(ctx, message);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    private void processElitechV12Message(ChannelHandlerContext ctx, ElitechIotMessage msg) {
        address = (InetSocketAddress) ctx.channel().remoteAddress();

        ListenableFuture<SendResult<String, ElitechIotMessage>> future = this.context.getKafkaProducer().sendElitechIotMessage(msg);
        future.addCallback(new ListenableFutureCallback<SendResult<String, ElitechIotMessage>>() {
            @Override
            public void onFailure(Throwable ex) {

            }

            @Override
            public void onSuccess(SendResult<String, ElitechIotMessage> result) {

            }
        });
    }

    private void processDeviceResponse() {

    }
}
