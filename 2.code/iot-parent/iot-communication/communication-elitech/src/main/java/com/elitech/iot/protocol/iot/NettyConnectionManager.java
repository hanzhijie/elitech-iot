package com.elitech.iot.protocol.iot;

import java.nio.channels.SocketChannel;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 连接管理去i
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/19
 */
public class NettyConnectionManager {
    private static final ConcurrentHashMap<String, SocketChannel> connectionMap = new ConcurrentHashMap();

    /**
     * 注册连接通道.
     *
     * @param deviceId
     * @param socketChannel
     */
    public static void rgisterConnection(String deviceId, SocketChannel socketChannel) {
        connectionMap.putIfAbsent(deviceId, socketChannel);
    }

    /**
     * 删除连接通道.
     *
     * @param deviceId 设备id
     */
    public static void unRegisterConnection(String deviceId) {
        connectionMap.remove(deviceId);
    }

    /**
     * 刪除连接通道.
     *
     * @param socketChannel
     */
    public static void unRegister(SocketChannel socketChannel) {
        for (String deviceId : connectionMap.keySet()) {
            SocketChannel sc = connectionMap.get(deviceId);
            if (sc.equals(socketChannel)) {
                connectionMap.remove(deviceId);
                return;
            }
        }
    }

    /**
     * 获取设备id.
     * @param socketChannel 连接通道
     * @return
     */
    public static String getDeviceId(SocketChannel socketChannel){
        for (String deviceId : connectionMap.keySet()) {
            SocketChannel sc = connectionMap.get(deviceId);
            if (sc.equals(socketChannel)) {
                return deviceId;
            }
        }

        return null;
    }

    /**
     * 向设备发送数据.
     *
     * @param deviceId 设备id
     * @param message 数据包
     */
    public static void sendMessage(String deviceId, ElitechIotMessage message) {
        SocketChannel socketChannel = connectionMap.get(deviceId);
        // socketChannel.write();
    }
}
