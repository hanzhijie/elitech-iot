package com.elitech.iot.protocol.iot.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */
@Slf4j
public class ElitechIotV12Decoder extends ReplayingDecoder<Void> {
    /**
     * 以及协议空包长度为16（HEAD 1 + GUID 10 + HEAD END 1 + ProtocolLength 2 + Flags Crc 1+ ProtocolEnd 1 = 16）.
     */
    public final int PROTOCOL_BASE_LENGTH = 16;

    /**
     * 数据包最大长度(防止socket字节流攻击).
     */
    public final int PROTOCOL_MAX_LENGTH = 2048;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in.readableBytes() >= PROTOCOL_BASE_LENGTH) {
            if(in.readableBytes() > PROTOCOL_MAX_LENGTH){
                log.error("DataLength too long, length=" + in.readableBytes());
                in.skipBytes(in.readableBytes());
            }

            int readIndex = 0;
            while(true){
                // 获取包头开始位置
                readIndex = in.readerIndex();

                // 标记读取位置
                in.markReaderIndex();


            }
        }
    }
}
