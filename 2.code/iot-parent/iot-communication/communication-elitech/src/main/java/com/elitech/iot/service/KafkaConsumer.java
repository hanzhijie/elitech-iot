package com.elitech.iot.service;

import com.elitech.iot.constants.KafkaTopicConstants;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/10
 */
@Component
public class KafkaConsumer {
    /**
     * 精创一级协议消息监听（数据帧未解析）.
     *
     * @param consumerRecord
     * @param ack
     */
    @KafkaListener(topics = KafkaTopicConstants.ELITECH_IOT_PROTOCOL_KAFKA_TOPIC, groupId = KafkaTopicConstants.DEVICE_DATA_DEFAULT_GROUP_ID)
    private void kafkaIotConsumer(ConsumerRecord<String, String> consumerRecord, Acknowledgment ack) {
        System.out.println("**********************************");
        System.out.println("**********************************Kafka IOT Message Consumer: " + consumerRecord.topic() + ":  " + consumerRecord.value());

        // 消费完成之后手动确认
        ack.acknowledge();
    }

    /**
     * Glog协议消息监听（数据未解析）.
     *
     * @param consumerRecord
     * @param ack
     */
    @KafkaListener(topics = KafkaTopicConstants.ELITECH_GLOG_PROTOCOL_KAFKA_TOPIC, groupId = KafkaTopicConstants.DEVICE_DATA_DEFAULT_GROUP_ID)
    private void kafkaGlogConsumer(ConsumerRecord<String, String> consumerRecord, Acknowledgment ack) {
        System.out.println("**********************************");
        System.out.println("**********************************Kafka Glog Consumer: " + consumerRecord.topic() + ":  " + consumerRecord.value());

        // 消费完成之后手动确认
        ack.acknowledge();
    }
}
