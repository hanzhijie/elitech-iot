package com.elitech.iot.protocol.iot;

/**
 * 设备类型.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/18
 */
public enum DeviceType {


    /**
     * RCW-400.
     */
    RCW_400(0x0000, "RCW-400"),

    /**
     * RCW-600.
     */
    RCW_600(0x0001, "RCW-600"),

    /**
     * RCW-800.
     */
    RCW_800(0x0002, "RCW-800"),

    /**
     * RCW-400A.
     */
    RCW_400A(0x0003, "RCW-400A"),

    /**
     * RCW-800A.
     */
    RCW_800A(0x0004, "RCW-800A"),

    /**
     * RCW-10B.
     */
    RCW_10B(0x0005, "RCW-10B"),

    /**
     * RCW-600A.
     */
    RCW_600A(0x0006, "RCW-600A"),

    /**
     * RCW_600WiFi.
     */
    RCW_600WIFI(0x0007, "RCW_600WiFi"),

    /**
     * RCW_800WiFi.
     */
    RCW_800WIFI(0x0008, "RCW_800WiFi"),

    /**
     * RCW-200
     */
    RCW_200(0x0009, "RCW-200"),

    /**
     * RCW-1000.
     */
    RCW_300(0x000A, "RCW-1000"),

    /**
     * RCW-1100.
     */
    RCW_1100(0x000B, "RCW-1100"),

    /**
     * RMS-200.
     */
    RMS_200(0x000C, "RMS-200"),

    /**
     * RMS-300.
     */
    RMS_300(0x000D, "RMS-300"),

    /**
     * EK-3030E.
     */
    EK_3030E(0x000E, "EK-3030E"),

    /**
     * ECM-400.
     */
    ECM_400(0x000F, "ECM-400"),

    /**
     * LTC-500.
     */
    LTC_500(0x0010, "LTC-500"),

    /**
     * RCW-400A1.1.
     */
    RCW_400A11(0x0013, "RCW-400A1.1"),

    /**
     * RCW-800.JD.
     */
    RCW_800JD(0x0014, "RCW-800.JD"),

    /**
     * LWSSH200.
     */
    LWSSH200(0x0017, "LWSSH200"),

    /**
     * TemLohW1H.
     */
    TEM_LOG_W1H(0x001D, "TemLogW1H"),

    /**
     * TemLogW1.
     */
    TEM_LOG_W1(0x001E, "TemLogW1");

    private short typeId;
    private String typeName;
    private DeviceType(int typeId, String typeName){
        this.typeId = (short) typeId;
        this.typeName = typeName;
    }

    /**
     * get device type id.
     * @return
     */
    public  short getTypeId(){
        return this.typeId;
    }

    /**
     * get device type name.
     * @return
     */
    public String getTypeName(){
        return this.getTypeName();
    }
}
