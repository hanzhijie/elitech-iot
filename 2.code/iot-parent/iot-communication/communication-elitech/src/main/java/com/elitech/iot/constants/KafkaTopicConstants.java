package com.elitech.iot.constants;

import org.apache.kafka.common.protocol.types.Field;

/**
 * Kafka常量.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/1
 */
public class KafkaTopicConstants {
    /**
     * 设备监测数据topic.
     */
    public static final String DEVICE_DATA_KAFKA_TOPIC = "elitech.iot.device.message";

    /**
     * 精创物联网通讯协议数据包 Topic.
     */
    public static final String ELITECH_IOT_PROTOCOL_KAFKA_TOPIC = "elitech.iot.protocol";

    /**
     * 精创物联网通讯协议数据包 Topic.
     */
    public static final String ELITECH_GLOG_PROTOCOL_KAFKA_TOPIC = "elitech.iot.protocol";


    /**
     * 设备事件topic.
     */
    public static final String DEVICE_EVENT_KAFKA_TOPIC = "elitech.iot.device.event";

    public static final String DEVICE_DATA_DEFAULT_GROUP_ID = "elitech_iot_default_group";
}
