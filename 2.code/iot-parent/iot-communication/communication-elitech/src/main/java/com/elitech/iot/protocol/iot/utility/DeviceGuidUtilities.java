package com.elitech.iot.protocol.iot.utility;

import io.netty.buffer.ByteBuf;

import java.util.Objects;

/**
 * 设备GUID辅助类.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/18
 */
public class DeviceGuidUtilities {
    /**
     * 设备guid的长度.
     */
    public static final int GUID_LENGTH = 20;

    /**
     * 设备guid16进制字符串长度.
     */
    public static final int HEX_GUID_LENGTH = 10;

    /**
     * 将20为设备guid字符串转换成10位16进制数字，并写入缓冲区.
     *
     * @param guid 设备guid字符串，长度为20位数字.
     * @param buf  数据缓冲区
     */
    public static void writeGuid(String guid, ByteBuf buf) {
        Objects.requireNonNull(guid);

        if (guid.length() != GUID_LENGTH) {
            throw new IllegalArgumentException("device guid length is not equal 20");
        }

        String oneCharterOfGuid;
        byte oneCharGuidValue = 0;

        for (int i = 0; i < HEX_GUID_LENGTH; i++) {
            oneCharterOfGuid = guid.substring(i * 2, i * 2 + 2);
            oneCharGuidValue = Byte.parseByte(oneCharterOfGuid, 10);

            buf.writeByte(oneCharGuidValue);
        }
    }

    /**
     * 读取guid.
     *
     * @param buf 数据缓冲区
     * @return 设备guid（20个字符）
     */
    public static String readGuid(ByteBuf buf) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < HEX_GUID_LENGTH; i++) {
            sb.append(String.format("%02d", buf.readByte()));
        }

        return sb.toString();
    }
}
