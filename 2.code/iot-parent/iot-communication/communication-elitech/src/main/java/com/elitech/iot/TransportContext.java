package com.elitech.iot;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */
@Data
@Slf4j
public class TransportContext {

}
