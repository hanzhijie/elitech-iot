package com.elitech.iot.protocol.iot;

import lombok.Data;
import org.apache.kafka.common.protocol.types.Field;

/**
 * 精创物联网通讯协议.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */
@Data
public class ElitechIotMessage {
    /**
     * 一级协议头.
     */
    public final static byte HEAD = 0x55;

    /**
     * 一级协议尾.
     */
    public final static byte PROTOCOL_END = 0x45;

    /**
     * 一级协议头长度.
     */
    public final static short LEVEL1_BASE_PACKAGE_LENGTH = 16;

    /**
     * 一级协议结尾长度2（crc + protocolEnd）
     */
    public final static short LEVEL1_BASE_END_LENGTH = 2;

    /**
     * 数据帧偏移位置.
     */
    public final static short DATA_FRAME_OFFSET = 14;

    /**
     * 设备guid.
     * <p>
     * 20 位 GUID 平台入网号：也称为平台入网号，简写为 GUID，实际为 20 位数字，占长度为 10 位，
     * 负责入网设备与平台之间通信的唯一标识码，所有的入网设备都必须拥有一个 GUID（出厂时写到设备中），按照 16 进制编码，每两个数字换算成 16 进制。
     * </p>
     */
    private String deviceId;

    /**
     * 二级协议数据.
     */
    private byte[] dataFrame;

    /**
     * 协议包长度(长度为 2 位，16 进制格式。).
     */
    private int protocolLength;

    /**
     * 采用 CRC 校验，前面字节加起来的和然后取低字节，作为校和。长度为 1 位，最大 255
     */
    private byte crc;

    /**
     * 创建一个ElitechIotV12Message（精创物联网通讯数据包）实例.
     *
     * @param deviceId       设备id
     * @param protocolLength 协议长度
     * @param dataFrame
     */
    public ElitechIotMessage(String deviceId, int protocolLength, byte[] dataFrame, byte crc) {
        this.deviceId = deviceId;
        this.protocolLength = protocolLength;
        this.dataFrame = dataFrame;
        this.crc = crc;
    }
}
