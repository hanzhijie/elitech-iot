package com.elitech.iot.protocol.iot;

import com.elitech.iot.TransportContext;
import com.elitech.iot.service.KafkaProducer;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */
@Component
public class ElitechIotTransportContext extends TransportContext {
    @Autowired
    @Getter
    private KafkaProducer kafkaProducer;


}
