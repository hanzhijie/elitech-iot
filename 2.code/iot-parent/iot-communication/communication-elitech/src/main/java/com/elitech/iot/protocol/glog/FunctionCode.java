package com.elitech.iot.protocol.glog;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/21
 */
public enum FunctionCode {

    /**
     * 获取IP和Port.
     */
    GET_IP_PORT(0x0001),

    /**
     * 接入校验（请求接入获取的ip和port）.
     */
    GET_JOIN_IP_PORT(0x0002),

    /**
     * 上传实时数据.
     */
    UPLOAD_REALTIME_DATA(0x0003),

    /**
     * 上传离线数据.
     */
    UPLOAD_OFFLINE_DATA(0x0004),

    /**
     * 上传参数.
     */
    UPLOAD_PARAM(0x0005),

    /**
     * 上传定位信息.
     */
    UPLOAD_POSITION(0x0006),

    /**
     * 获取参数.
     */
    GET_PARAM(0x0007),

    /**
     * 获取程序版本.
     */
    GET_SOFTWARE_VERSION(0x000E),

    /**
     * 获取程序包.
     */
    GET_SOFTWARE(0x000F),

    /**
     * 平台设置设备参数.
     */
    SET_PARAM(0x0011);

    private int functionCode;

    private FunctionCode(int code) {
        this.functionCode = code;
    }


    public int getFunctionCode() {
        return this.functionCode;
    }
}
