package com.elitech.iot.protocol.iot;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/18
 */
public enum CmdType {
    /**
     * 设置参数.
     */
    SET_PARAM(0x0081),

    /**
     * 平台向设备返回错误码.
     */
    RETURN_ERROR_CODE(0x001e),

    /**
     * 设备升级(设备向平台请求软件包).
     */
    DEVICE_UOPGRADE(0x0082),

    /**
     * 平台向设备响应软件版本号.
     */
    SOFTWARE_VERSION(0x0025),

    /**
     * 设备上报参数.
     */
    REPORT_PARAM(0x8081),

    /**
     * 设备上报子机GUID.
     */
    REPORT_CHILD_GUID(0x8082),

    /**
     * 设备上报hard id
     */
    REPORT_HARD_ID(0x8083),

    /**
     * 上报离线参数.
     */
    REPORT_OFFLINE_PARAM(0x8084);


    private int cmdType;
    private CmdType(int cmdType){
        this.cmdType = cmdType;
    }

    public int getCmdType(){
        return this.cmdType;
    }
}
