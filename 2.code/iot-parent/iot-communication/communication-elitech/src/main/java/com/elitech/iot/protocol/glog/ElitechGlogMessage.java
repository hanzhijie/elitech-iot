package com.elitech.iot.protocol.glog;

import lombok.Data;
import org.apache.kafka.common.protocol.types.Field;

import java.util.Date;

/**
 * GLOG通讯协议.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/20
 */
@Data
public class ElitechGlogMessage {
    /**
     * 协议头
     */
    public static final byte HEAD = (byte) 0xaa;

    public static final byte PACKAGE_LENGTH = 22;

    public static final byte PACKAGE_END_LENGTH = 3;

    /**
     * 设备id(SN).
     */
    private String deviceId;

    /**
     * 时间（自1970年开始的秒数）.
     */
    private Date time;

    /**
     * 设备类型.
     */
    private short deviceType;

    /**
     * 功能.
     */
    private short function;

    /**
     * 数据长度.
     */
    private short length;

    /**
     * 数据域.
     */
    private byte[] data;

    /**
     * 校验码.
     */
    private int crc;

    /**
     * 协议尾.
     */
    private static final byte END = (byte) 0xAB;


    /**
     *
     * @param deviceId
     * @param time
     * @param deviceType
     * @param funcCode
     * @param length
     * @param data
     * @param crc
     */
    public ElitechGlogMessage(String deviceId, Date time, short deviceType, short funcCode, short length, byte[] data, int crc) {
        this.deviceId = deviceId;
        this.time = time;
        this.deviceType = deviceType;
        this.function = funcCode;
        this.length = length;
        this.data = data;
        this.crc = crc;
    }
}
