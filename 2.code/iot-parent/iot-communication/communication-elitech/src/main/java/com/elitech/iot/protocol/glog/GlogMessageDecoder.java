package com.elitech.iot.protocol.glog;

import com.elitech.iot.protocol.iot.utility.CrcUtility;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * ELitech GLog消息解码.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/24
 */
@Slf4j
public class GlogMessageDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        byte head = in.readByte();
        in.resetReaderIndex();

        if (head == ElitechGlogMessage.HEAD) {
            ElitechGlogMessage message = readElitechLogMessage(ctx, in);

            // 如果读取不到一个完整的消息直接返回，等待接收到更多的数据之间在进行解析.
            if (message == null) {
                return;
            }

            out.add(message);

        } else {

            ctx.fireChannelRead(in);
        }
    }

    /**
     * 读取Elitech GLog 协议数据包.
     *
     * @param ctx
     * @param in
     * @return
     */
    private ElitechGlogMessage readElitechLogMessage(ChannelHandlerContext ctx, ByteBuf in) {
        if (in.readableBytes() < ElitechGlogMessage.PACKAGE_LENGTH) {
            return null;
        }

        // HEAD（1字节）
        in.readByte();

        // SN（8字节）
        byte[] snBytes = new byte[8];
        in.readBytes(snBytes);
        String deviceId = new String(snBytes);

        // 时间（4字节）
        long time = in.readInt();
        Date dt = new Date(time * 1000);

        // 设备类型（2字节）
        short deviceType = in.readShort();

        // 功能（2字节）
        short funcCode = in.readShort();

        // 长度（长度）
        short length = in.readShort();

        // 数据长度不够，返回等待
        if (in.readableBytes() < length + ElitechGlogMessage.PACKAGE_END_LENGTH) {
            in.resetReaderIndex();
            return null;
        }

        // data
        byte[] data = new byte[length];
        in.readBytes(data);

        // 校验
        int check = in.readUnsignedShort();

        // 验证校验码
        in.resetReaderIndex();
        int calcCheck = CrcUtility.calcCrc16(in, ElitechGlogMessage.PACKAGE_LENGTH + length - ElitechGlogMessage.PACKAGE_END_LENGTH);
        if (calcCheck != check) {
            byte[] errorData = new byte[ElitechGlogMessage.PACKAGE_LENGTH + length];
            in.resetReaderIndex();

            in.readBytes(errorData);
            String errorDataString = Arrays.toString(errorData);
            log.error("GLog设备发送数据包的校验码不正确：" + errorDataString);

            return null;
        }

        // 跳过校验码
        in.skipBytes(2);

        // END 0xAB
        byte end = in.readByte();

        ElitechGlogMessage message = new ElitechGlogMessage(deviceId, dt, deviceType, funcCode, length, data, check);
        return message;
    }
}
