package com.elitech.iot.protocol.glog;

import com.elitech.iot.SessionMsgListener;
import com.elitech.iot.protocol.iot.ElitechIotMessage;
import com.elitech.iot.protocol.iot.ElitechIotTransportContext;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/24
 */
public class ElitechLogTransportHandler extends ChannelInboundHandlerAdapter implements GenericFutureListener<Future<? super Void>>, SessionMsgListener {
    private ElitechIotTransportContext context;
    private String sessionId;

    private volatile InetSocketAddress address;


    public ElitechLogTransportHandler(ElitechIotTransportContext context) {
        this.context = context;
        this.sessionId = UUID.randomUUID().toString();
    }

    @Override
    public void operationComplete(Future<? super Void> future) throws Exception {

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        if (msg instanceof ElitechGlogMessage) {
            ElitechGlogMessage message = (ElitechGlogMessage) msg;
            processElitechLogMessage(ctx, message);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    private void processElitechLogMessage(ChannelHandlerContext ctx, ElitechGlogMessage msg) {
        address = (InetSocketAddress) ctx.channel().remoteAddress();

        ListenableFuture<SendResult<String, ElitechGlogMessage>> future = this.context.getKafkaProducer().sendElitechGlogMessage(msg);
        future.addCallback(new ListenableFutureCallback<SendResult<String, ElitechGlogMessage>>() {
            @Override
            public void onFailure(Throwable ex) {

            }

            @Override
            public void onSuccess(SendResult<String, ElitechGlogMessage> result) {

            }
        });
    }

    private void processDeviceResponse() {

    }
}
