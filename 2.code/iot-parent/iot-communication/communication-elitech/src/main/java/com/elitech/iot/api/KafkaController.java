package com.elitech.iot.api;

import com.elitech.iot.constants.KafkaTopicConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/10
 */
@RestController
public class KafkaController {
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @GetMapping("/message/send")
    @Transactional(transactionManager = "kafkaTransactionManager",rollbackFor = Exception.class)
    public boolean send(@RequestParam String message) {
        ListenableFuture<SendResult<String, Object>> feature = kafkaTemplate.send(KafkaTopicConstants.DEVICE_DATA_KAFKA_TOPIC, message);
        feature.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {

            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {

            }
        });
        return true;
    }
}
