package com.elitech.iot.protocol.iot;

/**
 * 参数的数据类型.
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/18
 */
public enum  DataType {
    /**
     * Null 型（类型： 0，0字节 ）.
     */
    NULL(0, 0),

    /**
     * Char 型(类型： 1，1字节 ).
     */
    CHAR(1, 1),

    /**
     * Boolean 型(类型： 2，1字节 ).
     */
    BOOLEAN(2,1),

    /**
     * Int型(类型： 3，2字节 ).
     */
    INT(3,2),

    /**
     * Long 型(类型： 4，4字节 ).
     */
    LONG(4, 4),

    /**
     * Date型(类型： 7，3字节 ).
     */
    DATE(7, 3),

    /**
     * TimeT 型(类型： 8，3字节 ).
     */
    TIME(8, 3),

    /**
     * DateTime型(类型： 类型： 9，6字节 ).
     */
    DATETIME(9, 6),

    /**
     * StringSUnicode) 型(类型： 10 ，变长字节 ，变长字节 ).
     */
    STRING_UNICODE(10, -1),

    /**
     * String(AscII) 型(类型： 类型： 11 ，变长字节 ，变长字节 ).
     */
    STRING_ASCII(11, -1),

    /**
     * Bit型(类型： 类型： 12 ，变长字节 ，变长字节 ，变长字节 ).
     */
    BIT(12, -1),

    /**
     * Enum 型(类型： 13，1字节 ).
     */
    ENUM(13, 1),

    /**
     * Password(AscII)型(类型： 14，变长字节 )
     */
    PASSWORD_ASCII(14, -1);


    private int id;
    private byte length;
    private DataType(int id, int length){
        this.id = id;
        this.length = (byte) length;
    }

    public int getId(){
        return this.id;
    }

    public int getLength(){
        return this.length;
    }

    public static DataType getDatType(int typeId){
        for(DataType type : DataType.values()){
            if(type.getId() == typeId){
                return type;
            }
        }

        throw new IllegalArgumentException("type id not exists");
    }
}
