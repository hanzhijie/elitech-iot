package com.elitech.iot.protocol.iot;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * 解析数据分包.(根据数据帧的长度字段分包)
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/18
 */
public class IotProtocolLengthFieldBasedFrameDecoder extends LengthFieldBasedFrameDecoder {
    /**
     * 数据帧的最大长度.
     */
    public static final int MAX_FRAME_LENGTH = 65535;

    /**
     * 长度字段的偏移地址.
     */
    private static final int LENGTH_FIELD_OFFSET = 12;

    /**
     * 长度字段的字节数
     */
    private static final int LENGTH_FIELD_LENGTH = 2;

    /**
     * create an instance of IotProtocolLengthFieldBasedFrameDecoder.
     */
    public IotProtocolLengthFieldBasedFrameDecoder() {
        super(MAX_FRAME_LENGTH, LENGTH_FIELD_OFFSET, LENGTH_FIELD_LENGTH, 2, 0);
    }
}

