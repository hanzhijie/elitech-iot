package com.elitech.iot.protocol.iot;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */
public class ElitechIotFrame {
    /**
     * 帧起始符.
     */
    public static final int FRAME_START_FLAG = 0x68;

    public static final int FRAME_END_FLAG = 0x16;

    /**
     * 帧起始符.
     */
    private byte frameStartFlag;

    /**
     * 设备类型.
     */
    private short type;

    /**
     * 标记起始地址.
     */
    private short mid;

    /**
     * 命令序号.
     */
    private short seq;

    /**
     * 数据帧起始符.
     */
    private byte dataFrameStartFlag;

    /**
     * 控制码.
     */
    private short controlCode;

    /**
     * 数据长度.
     */
    private short dataLength;

    private byte[] data;

    /**
     * 校验码.
     */
    private byte cs;

    /**
     * 结束码.
     */
    private byte endCode;
}
