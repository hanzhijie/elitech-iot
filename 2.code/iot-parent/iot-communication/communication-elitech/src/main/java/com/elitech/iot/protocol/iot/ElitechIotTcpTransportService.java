package com.elitech.iot.protocol.iot;

import com.elitech.iot.protocol.ElitechServerInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.ResourceLeakDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 精创冷链设备网络通讯服务器.
 * <p></p>
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/14
 */

@Service("ElitechIotTcpTransportService")
//@ConditionalOnExpression("'${transport.elitech.v12.enabled}'=='true'")
public class ElitechIotTcpTransportService {
    Logger log = LoggerFactory.getLogger(ElitechIotTcpTransportService.class);

    @Value("${transport.elitech.v12.host}")
    private String host;

    @Value("${transport.elitech.v12.port}")
    private Integer port;

    @Value("${transport.elitech.v12.leakDetectorLevel}")
    private String leakDetectorLevel;

    @Value("${transport.elitech.v12.bossGroupThreadCount}")
    private Integer bossGroupThreadCount;

    @Value("${transport.elitech.v12.workerGroupThreadCount}")
    private Integer workerGroupThreadCount;

    @Value("${transport.elitech.v12.keepAlive}")
    private Boolean keepAlive;

    @Autowired
    private ElitechIotTransportContext context;

    private Channel serverChannel;

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;

    @PostConstruct
    public void init() throws Exception {
        log.info("Setting resource leak detector level {}.", leakDetectorLevel);
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.valueOf(leakDetectorLevel.toUpperCase()));

        log.info("Starting Elitech IOT transport(V1、V2)...");
        bossGroup = new NioEventLoopGroup(bossGroupThreadCount);
        workerGroup = new NioEventLoopGroup(workerGroupThreadCount);
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ElitechServerInitializer(context))
                .childOption(ChannelOption.SO_KEEPALIVE, keepAlive);

        serverChannel = bootstrap.bind(host, port).sync().channel();
        log.info(" Elitech IOT transport(V1、V2) Started!");
    }

    @PreDestroy
    public void shutdown() throws InterruptedException {
        log.info("Stopping Elitech IOT transport(V1、V2)...");
        try {
            serverChannel.close().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();

        }

        log.info("Elitech IOT transport(V1、V2) stopped!");
    }
}
