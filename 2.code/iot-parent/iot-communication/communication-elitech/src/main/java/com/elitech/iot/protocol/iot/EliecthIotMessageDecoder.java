package com.elitech.iot.protocol.iot;

import com.elitech.iot.protocol.glog.ElitechGlogMessage;
import com.elitech.iot.protocol.iot.utility.CrcUtility;
import com.elitech.iot.protocol.iot.utility.DeviceGuidUtilities;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

/**
 * 精创物联网通讯协议解码器.
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/21
 */
@Slf4j
public class EliecthIotMessageDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        byte head = in.readByte();
        in.resetReaderIndex();

        // 只处理精创物联网通讯协议，不处理Elitech Log 通讯协议
        if (head == ElitechIotMessage.HEAD) {
            ElitechIotMessage message = readElitechIotMessage(ctx, in);
            // 如果读取不到一个完整的消息直接返回，等待接收到更多的数据之间在进行解析.
            if (message == null) {
                return;
            }

            out.add(message);
        } else {
            ctx.fireChannelRead(in);
        }
    }

    private ElitechIotMessage readElitechIotMessage(ChannelHandlerContext ctx, ByteBuf in) {
        // HEAD
        in.readByte();

        // GUID
        String deviceId = DeviceGuidUtilities.readGuid(in);

        // HEAD END
        in.readByte();

        // Protocol Length
        Short length = in.readShort();

        // 可读数据长度小于一个完成数据包的长度，直接返回等待继续读取更多数据
        if (in.readableBytes() < length + ElitechIotMessage.LEVEL1_BASE_END_LENGTH) {
            in.resetReaderIndex();
            return null;
        }

        in.skipBytes(length);

        // 读取验证码
        byte crc = in.readByte();

        byte end = in.readByte();

        // 标记数据帧的起始位置，如果校验码正确则继续从此处开始读取数据帧的内容
        in.resetReaderIndex();
        byte checkCrc = CrcUtility.calcCrc(in, ElitechIotMessage.LEVEL1_BASE_PACKAGE_LENGTH + length - 2);

        // 如果校验失败则读取整个数据包，并记录错误的数据包
        if (crc != checkCrc) {
            byte[] errorData = new byte[ElitechIotMessage.LEVEL1_BASE_PACKAGE_LENGTH + length];
            in.resetReaderIndex();

            in.readBytes(errorData);
            String errorDataString = Arrays.toString(errorData);
            log.error("设备发送的数据包校验码不正确：" + errorDataString);

            return null;
        }

        // 跳转到起始位置
        in.resetReaderIndex();

        in.skipBytes(ElitechIotMessage.DATA_FRAME_OFFSET);

        // Protocol Data
        byte[] data = new byte[length];
        in.readBytes(data);

        // 计算数据帧校验码
        int sum = 0;
        for (int i = 0; i < data.length - 2; i++) {
            sum  += data[i];
        }
        byte frameCheck = (byte) (sum % 256);

        byte l2Crc = data[data.length - 2];
        if (l2Crc != frameCheck){
            byte[] errorData = new byte[ElitechIotMessage.LEVEL1_BASE_PACKAGE_LENGTH + length];
            in.resetReaderIndex();

            in.readBytes(errorData);
            String errorDataString = Arrays.toString(errorData);
            log.error("设备发生数据包的数据帧校验码不正确：" + errorDataString);

            return null;
        }

        // Flags CRC
        in.skipBytes(1);

        // Protocol End 0x45
        byte protocolEnd = in.readByte();


        return new ElitechIotMessage(deviceId, length, data, crc);
    }
}
