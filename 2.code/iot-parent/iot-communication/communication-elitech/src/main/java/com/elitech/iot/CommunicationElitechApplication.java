package com.elitech.iot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 精创设备通讯服务器.
 * <p>兼容2020前的v1、v2协议.</p>
 *
 * @author wangjiangmin <wjm@e-elitech.com>
 * @since v1.0.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@Configuration
public class CommunicationElitechApplication {


    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(CommunicationElitechApplication.class, args);
    }

    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
