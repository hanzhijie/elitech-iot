package com.elitech.iot;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/24
 */
@RequiredArgsConstructor
@Slf4j
public abstract class AbstractClient implements EndPoint {
    @NonNull
    private String hostName;

    @NonNull
    private int port;

    @NonNull
    @Getter(value = AccessLevel.PROTECTED)
    private int connectionTimeout;

    protected final CountDownLatch countDownLatch = new CountDownLatch(1);
    protected String respMsg;

    @SneakyThrows
    public void send(Object message) {
        doOpen();
        doConnect();
        write(message);
    }

    @SneakyThrows
    public String receive() {
        boolean b = countDownLatch.await(getConnectionTimeout(), TimeUnit.MILLISECONDS);
        if (!b) {
            log.error("Timeout(" + getConnectionTimeout() + "ms) when receiving response message");
        }
        return respMsg;
    }

    private void write(Object message) {
        Channel channel = getChannel();
        if (null != channel) {
            ChannelFuture f = channel.writeAndFlush(byteBufferFrom(message)).syncUninterruptibly();
            if (!f.isSuccess()) {
                log.error("Failed to send message to " + getRemoteAddress() + f.cause().getMessage());
            }
        }
    }

    private ByteBuf byteBufferFrom(Object message) {
        return message instanceof String ? Unpooled.copiedBuffer((String) message, StandardCharsets.UTF_8) : Unpooled.copiedBuffer((byte[]) message);
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return new InetSocketAddress(hostName, port);
    }

    @Override
    public InetSocketAddress getLocalAddress() {
        throw new RuntimeException("This method is not need to be implemented");
    }

    /**
     * Open client.
     *
     * @throws Throwable
     */
    protected abstract void doOpen() throws Throwable;

    /**
     * Connect to server.
     *
     * @throws Throwable
     */
    protected abstract void doConnect() throws Throwable;

    /**
     * Get the connected channel.
     *
     * @return channel
     */
    protected abstract Channel getChannel();
}
