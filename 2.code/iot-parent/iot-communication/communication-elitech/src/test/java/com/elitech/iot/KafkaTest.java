package com.elitech.iot;

import com.elitech.iot.service.KafkaProducer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class KafkaTest {
    @Autowired
    private KafkaProducer kafkaProducer;

    @Test
    public void send(){
        kafkaProducer.sendMessage("Hello");
    }

}
