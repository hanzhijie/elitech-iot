package com.elitech.iot;

import java.net.InetSocketAddress;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/24
 */
public interface EndPoint {
    InetSocketAddress getLocalAddress();

    InetSocketAddress getRemoteAddress();
}
