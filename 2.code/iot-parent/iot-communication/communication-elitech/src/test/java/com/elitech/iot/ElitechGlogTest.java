package com.elitech.iot;

import com.elitech.iot.protocol.iot.utility.CrcUtility;
import org.junit.Test;

import java.io.Console;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * @author wangjiangmin <wjm@e-elitech.com>
 * @date 2020/2/24
 */
public class ElitechGlogTest {
    @Test
    public void sendData() throws IOException {
        byte[] data = new byte[]{(byte) 0xAA,
                // sn
                0x68, 0x69, 0x30, 0x30, 0x30, 0x30, 0x30, 0x31,
                // time
                0x5c, 0x24, 0x66, 0x01,
                // 设备类型
                0x00, 0x12,
                // 功能
                0x00, 0x01,
                // 数据长度
                0x00, 0x06,
                // 数据
                0x0c, (byte) 0xA8, 0x01, 0x01, 0x15, (byte) 0xCC,
                // crc16
                (byte) 0x99, 0x0F,
                // end
                (byte) 0xAB};
        int checkCode = CrcUtility.calcCrc16(data, 0, data.length - 3);
        assertEquals(39183, checkCode);

        NettyClient client = new NettyClient("127.0.0.1", 5580, 45000);
        client.send(data);

        System.in.read();
    }
}
