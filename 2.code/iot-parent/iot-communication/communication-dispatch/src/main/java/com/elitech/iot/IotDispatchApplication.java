package com.elitech.iot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 分发服务器.
 *
 * @author wangjiangmin
 * @date 2019-12-24
 */
@SpringBootApplication
@EnableDiscoveryClient
@Configuration
public class IotDispatchApplication {
    /**
     * 应用程序入口.
     *
     * @param args 启动时传入命令行参数.
     */
    public static void main(String[] args) {
        SpringApplication.run(IotDispatchApplication.class, args);
    }

    /**
     * 统一服务端时区为UTC 时间.
     */
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}